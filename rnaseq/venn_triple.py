import argparse
import csv
import os

parser = argparse.ArgumentParser("set operations")
parser.add_argument('-a','--fileA',required=True)
parser.add_argument('-b','--fileB',required=True)
parser.add_argument('-c','--fileC',required=True)
parser.add_argument('-f','--logCutOff',required=True)
args=vars(parser.parse_args())

fileA=args['fileA']
fileB=args['fileB']
fileC=args['fileC']
logCutOff=args['logCutOff']


def cutoff_filter(filelines,logCutOff):
    print("enter: cutoff_filter():")
    filterSet = set()
    filteroutput=""
    for line in filelines:
        if line.startswith('\t'):
            print("continue")
            continue
        else:
            splitline=line.split('\t')
            #pos values
            if (logCutOff > 0):
                if (float(splitline[1]) >= logCutOff):
                    filterSet.add(splitline[0])
                    filteroutput += line
            elif (logCutOff < 0):
                if (float(splitline[1]) <= logCutOff):
                    filterSet.add(splitline[0])
                    filteroutput += line
            elif (logCutOff == 0):
                filterSet.add(splitline[0])
                filteroutput += line
            else:
                print("FTW")
                print("line:",line)
                sys.exit(0)
    return filterSet

    
def get_outdir(fileA,fileB,fileC,logCutOff):
    origAbase,origAext = os.path.splitext(fileA)
    origBbase,origBext = os.path.splitext(fileB)
    origCbase,origCext = os.path.splitext(fileC)
    outdir=origAbase+'_'+origBbase+'_'+origCbase+'_'+logCutOff
    return outdir


def writevenn(filterASet,filterBSet,filterCSet,ABintersect,ACintersect,BCintersect,ABCintersect,ABunique,ACunique,BCunique,Aunique,Bunique,Cunique,outdir):
    if (not os.path.isdir(outdir)):
        os.mkdir(outdir)
    os.chdir(outdir)
    
    with open("filterASet",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in filterASet:
            output.write(ensgene + '\n')

    with open("filterBSet",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in filterBSet:
            output.write(ensgene + '\n')

    with open("filterCSet",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in filterCSet:
            output.write(ensgene + '\n')

    with open("ABintersect",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in ABintersect:
            output.write(ensgene + '\n')

    with open("ACintersect",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in ACintersect:
            output.write(ensgene + '\n')

    with open("BCintersect",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in BCintersect:
            output.write(ensgene + '\n')

    with open("ABCintersect",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in ABCintersect:
            output.write(ensgene + '\n')

    with open("ABunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in ABunique:
            output.write(ensgene + '\n')

    with open("ACunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in ACunique:
            output.write(ensgene + '\n')

    with open("BCunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in BCunique:
            output.write(ensgene + '\n')

    with open("Aunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in Aunique:
            output.write(ensgene + '\n')

    with open("Bunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in Bunique:
            output.write(ensgene + '\n')

    with open("Cunique",'w') as output:
        output.write("ENSG_id"+'\n')
        for ensgene in Cunique:
            output.write(ensgene + '\n')




def venn(fileA,fileB,fileC,logCutOff,outdir):

    with open(fileA,'r') as inputA:
        fileAlines = inputA.readlines()

    with open(fileB,'r') as inputB:
        fileBlines = inputB.readlines()

    with open(fileC,'r') as inputC:
        fileClines = inputC.readlines()

    print(fileA+" filter")
    filterASet=cutoff_filter(fileAlines,logCutOff)
    print(fileB+" filter")
    filterBSet=cutoff_filter(fileBlines,logCutOff)
    print(fileC+" filter")
    filterCSet=cutoff_filter(fileClines,logCutOff)

    ABintersect = filterASet & filterBSet
    ACintersect = filterASet & filterCSet
    BCintersect = filterBSet & filterCSet
    ABCintersect = filterASet & filterBSet & filterCSet
    ABunique = ABintersect - filterCSet
    ACunique = ACintersect - filterBSet
    BCunique = BCintersect - filterASet
    Aunique = filterASet - filterBSet - filterCSet
    Bunique = filterBSet - filterASet - filterCSet
    Cunique = filterCSet - filterASet - filterBSet
    print("len(fileA)",len(fileA))
    print("len(fileB)",len(fileB))
    print("len(fileC)",len(fileC))
    print("len(filterASet)",len(filterASet))
    print("len(filterBSet)",len(filterBSet))
    print("len(filterCSet)",len(filterCSet))
    print("len(ABintersect)",len(ABintersect))
    print("len(ACintersect)",len(ACintersect))
    print("len(BCintersect)",len(BCintersect))
    print("len(ABCintersect)",len(ABCintersect))
    print("len(ABunique)",len(ABunique))
    print("len(ACunique)",len(ACunique))
    print("len(BCunique)",len(BCunique))
    print("len(Aunique)",len(Aunique))
    print("len(Bunique)",len(Bunique))
    print("len(Cunique)",len(Cunique))
    writevenn(filterASet,filterBSet,filterCSet,ABintersect,ACintersect,BCintersect,ABCintersect,ABunique,ACunique,BCunique,Aunique,Bunique,Cunique,outdir)


outdir=get_outdir(fileA,fileB,fileC,logCutOff)
print ("outdir=",outdir)
venn(fileA,fileB,fileC,float(logCutOff),outdir)
