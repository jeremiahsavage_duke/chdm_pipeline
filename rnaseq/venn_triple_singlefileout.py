import argparse
import csv
import os

parser = argparse.ArgumentParser("set operations")
parser.add_argument('-a','--fileA',required=True)
parser.add_argument('-b','--fileB',required=True)
parser.add_argument('-c','--fileC',required=True)
parser.add_argument('-f','--logCutOff',required=True)
args=vars(parser.parse_args())

fileA=args['fileA']
fileB=args['fileB']
fileC=args['fileC']
logCutOff=args['logCutOff']


def cutoff_filter(filelines,logCutOff):
    print("enter: cutoff_filter():")
    filterSet = set()
    filteroutput=""
    for line in filelines:
        if line.startswith('\t'):
            print("continue")
            continue
        else:
            splitline=line.split('\t')
            #pos values
            if (logCutOff > 0):
                if (float(splitline[1]) >= logCutOff):
                    filterSet.add(splitline[0])
                    filteroutput += line
            elif (logCutOff < 0):
                if (float(splitline[1]) <= logCutOff):
                    filterSet.add(splitline[0])
                    filteroutput += line
            elif (logCutOff == 0):
                filterSet.add(splitline[0])
                filteroutput += line
            else:
                print("FTW")
                print("line:",line)
                sys.exit(0)
    return filterSet

    


def writevenn(ABCintersect,ABunique,ACunique,BCunique,Aunique,Bunique,Cunique,fileA,fileB,fileC,logCutOff):
    origAbase,origAext = os.path.splitext(fileA)
    origBbase,origBext = os.path.splitext(fileB)
    origCbase,origCext = os.path.splitext(fileC)

    outfile = origAbase + '_' + origBbase + '_' + origCbase + '_' + str(logCutOff) + origAext
    
    with open(outfile,'w') as output:
        output.write("ENSG_id"+'\t'+origAbase+'\t'+origBbase+'\t'+origCbase+'\n')
        for ensgene in ABCintersect:
            output.write(ensgene + '\t' + "Y" + '\t' + "Y" + '\t' + "Y" + '\n')
        for ensgene in ABunique:
            output.write(ensgene + '\t' + "Y" + '\t' + "Y" + '\t' + "N" + '\n')
        for ensgene in ACunique:
            output.write(ensgene + '\t' + "Y" + '\t' + "N" + '\t' + "Y" + '\n')
        for ensgene in BCunique:
            output.write(ensgene + '\t' + "N" + '\t' + "Y" + '\t' + "Y" + '\n')
        for ensgene in Aunique:
            output.write(ensgene + '\t' + "Y" + '\t' + "N" + '\t' + "N" + '\n')
        for ensgene in Bunique:
            output.write(ensgene + '\t' + "N" + '\t' + "Y" + '\t' + "N" + '\n')
        for ensgene in Cunique:
            output.write(ensgene + '\t' + "N" + '\t' + "N" + '\t' + "Y" + '\n')


def venn(fileA,fileB,fileC,logCutOff):

    with open(fileA,'r') as inputA:
        fileAlines = inputA.readlines()

    with open(fileB,'r') as inputB:
        fileBlines = inputB.readlines()

    with open(fileC,'r') as inputC:
        fileClines = inputC.readlines()

    print(fileA+" filter")
    filterASet=cutoff_filter(fileAlines,logCutOff)
    print(fileB+" filter")
    filterBSet=cutoff_filter(fileBlines,logCutOff)
    print(fileC+" filter")
    filterCSet=cutoff_filter(fileClines,logCutOff)

    ABintersect = filterASet & filterBSet
    ACintersect = filterASet & filterCSet
    BCintersect = filterBSet & filterCSet
    ABCintersect = filterASet & filterBSet & filterCSet
    ABunique = ABintersect - filterCSet
    ACunique = ACintersect - filterBSet
    BCunique = BCintersect - filterASet
    Aunique = filterASet - filterBSet - filterCSet
    Bunique = filterBSet - filterASet - filterCSet
    Cunique = filterCSet - filterASet - filterBSet
    print("len(fileA)",len(fileA))
    print("len(fileB)",len(fileB))
    print("len(fileC)",len(fileC))
    print("len(filterASet)",len(filterASet))
    print("len(filterBSet)",len(filterBSet))
    print("len(filterCSet)",len(filterCSet))
    print("len(ABintersect)",len(ABintersect))
    print("len(ACintersect)",len(ACintersect))
    print("len(BCintersect)",len(BCintersect))
    print("len(ABCintersect)",len(ABCintersect))
    print("len(ABunique)",len(ABunique))
    print("len(ACunique)",len(ACunique))
    print("len(BCunique)",len(BCunique))
    print("len(Aunique)",len(Aunique))
    print("len(Bunique)",len(Bunique))
    print("len(Cunique)",len(Cunique))
    writevenn(ABCintersect,ABunique,ACunique,BCunique,Aunique,Bunique,Cunique,fileA,fileB,fileC,logCutOff)


venn(fileA,fileB,fileC,float(logCutOff))
