import argparse
import os
import urllib2

parser = argparse.ArgumentParser("get uniprot GO data")
parser.add_argument('-i','--infile',required=True)
args=vars(parser.parse_args())

infile=args['infile']


def get_genesymbol(html,key):
    for line in html:
        if line.startswith(key+' '):
            aid = line.split(';')[0]
            aid = aid.split('=')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_genename(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[0]
            aid = aid.split('=')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_refseqnp_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_refseqnm_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[2]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_ensdart_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_ensdarp_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[2]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_uniprot_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[0]
            aid = line.split('   ')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_ncbigene_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[0]
            aid = line.split(' ')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_ncbigene_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_kegg_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_keggortho_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_orthodb_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_genetree_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_eggnog_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_hogenom_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_hovergen_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_inparanoid_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_oma_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_unigene_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_ctd_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"

def get_zfin_id(html,key):
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            return aid
    return "N/A"


def get_a_ids(html,key):
    aid_terms=""
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[1]
            aid = aid.lstrip().rstrip()
            aid_terms += ';' + aid
    aid_terms=aid_terms.lstrip(';')
    if (aid_terms == ""):
        return "N/A"
    else:
        return aid_terms


def get_a_terms(html,key):
    aid_terms=""
    for line in html:
        if line.startswith(key):
            aid = line.split(';')[2]
            aid = aid.lstrip().rstrip()
            aid_terms += ';' + aid
    aid_terms=aid_terms.lstrip(';')
    if (aid_terms == ""):
        return "N/A"
    else:
        return aid_terms








def getuniprot(infile):

    with open(infile,'r') as input:
        inlines = input.readlines()

    ensdarglist = list()
    for line in inlines[1:]:
        ensdarg = line.split('\t')[0]
        ensdarglist.append(ensdarg)

    origbase,origext = os.path.splitext(infile)

    outfile = origbase+"_uniprot"+origext
    output=open(outfile,'w')


    output.write("ensdarg_id	genesymbol	genename	refseqnp_id	refseqnm_id	ensdart_id	ensdarp_id	uniprot_id	ncbigene_id	kegg_id	keggortho_id	orthodb_id	genetree_id	eggnog_id	hogenom_id	hovergen_id	inparanoid_id	oma_id	unigene_id	ctd_id	zfin_id	gene3d_ids	gene3d_terms	GO_ids	GO_terms	InterPro_ids	InterPro_terms	pirsf_ids	pirsf_terms	pfam_ids	pfam_terms	smart_ids	smart_terms	supfam_ids	supfam_terms	prosite_ids	prosite_terms" + '\n')
    for ensdarg in ensdarglist:
        print('http://www.uniprot.org/uniprot/?query='+ensdarg+'&format=txt')
        response=urllib2.urlopen('http://www.uniprot.org/uniprot/?query='+ensdarg+'&format=txt')
        html=response.read()
        html = html.split('\n')
        ##singles##
        genesymbol = get_genesymbol(html,"GN")
        genename = get_genename(html,"DE   SubName:")
        refseqnp_id = get_refseqnp_id(html,"DR   RefSeq")
        refseqnm_id = get_refseqnm_id(html,"DR   RefSeq")
        ensdart_id = get_ensdart_id(html,"DR   Ensembl")
        ensdarp_id = get_ensdarp_id(html,"DR   Ensembl")
        uniprot_id = get_uniprot_id(html, "AC")
        ncbigene_id = get_ncbigene_id(html,"DR   GeneID")
        kegg_id = get_kegg_id(html,"DR   KEGG")
        keggortho_id = get_keggortho_id(html,"DR   KO")
        orthodb_id = get_orthodb_id(html,"DR   OrthoDB")
        genetree_id = get_genetree_id(html,"DR   GeneTree")
        eggnog_id = get_eggnog_id(html, "DR   eggNOG")
        hogenom_id = get_hogenom_id(html,"DR   HOGENOM")
        hovergen_id = get_hovergen_id(html,"DR   HOVERGEN")
        inparanoid_id = get_inparanoid_id(html,"DR   InParanoid")
        oma_id = get_oma_id(html,"DR   OMA")
        unigene_id = get_unigene_id(html,"DR   UniGene")
        ctd_id = get_ctd_id(html,"DR   CTD;")
        zfin_id = get_zfin_id(html,"DR   ZFIN")
        ##multiples##
        cath_ids=get_a_ids(html,"DR   Gene3D")
        cath_terms = get_a_terms(html,"DR   Gene3D")
        go_ids=get_a_ids(html,"DR   GO")
        go_terms = get_a_terms(html,"DR   GO")
        interpro_ids = get_a_ids(html,"DR   InterPro")
        interpro_terms = get_a_terms(html,"DR   InterPro")
        pirsf_ids = get_a_ids(html,"DR   PIRSF")
        pirsf_terms = get_a_terms(html,"DR   PIRSF")
        pfam_ids = get_a_ids(html,"DR   Pfam")
        pfam_terms = get_a_terms(html,"DR   Pfam")
        smart_ids = get_a_ids(html,"DR   SMART")
        smart_terms = get_a_terms(html,"DR   SMART")
        supfam_ids = get_a_ids(html,"DR   SUPFAM")
        supfam_terms = get_a_terms(html,"DR   SUPFAM")
        prosite_ids = get_a_ids(html,"DR   PROSITE")
        prosite_terms = get_a_terms(html,"DR   PROSITE")

        TAB = '\t'
        output.write(ensdarg + TAB +genesymbol + TAB + genename + TAB + refseqnp_id + TAB + refseqnm_id + TAB + ensdart_id + TAB + ensdarp_id + TAB + uniprot_id + TAB + ncbigene_id + TAB + kegg_id + TAB + keggortho_id + TAB + orthodb_id + TAB + genetree_id + TAB + eggnog_id + TAB + hogenom_id + TAB + hovergen_id + TAB + inparanoid_id + TAB + oma_id + TAB + unigene_id + TAB + ctd_id + TAB + zfin_id + TAB + cath_ids + TAB + cath_terms + TAB + go_ids + TAB + go_terms + TAB + interpro_ids + TAB + interpro_terms + TAB + pirsf_ids + TAB + pirsf_terms + TAB + pfam_ids + TAB + pfam_terms + TAB + smart_ids + TAB + smart_terms + TAB + supfam_ids + TAB + supfam_terms + TAB + prosite_ids + TAB + prosite_terms + '\n')
        
    output.close()



getuniprot(infile)
