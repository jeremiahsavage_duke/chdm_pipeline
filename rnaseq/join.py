import argparse
import csv
import os

parser = argparse.ArgumentParser("join two files")
parser.add_argument('-a','--fileA',required=True)
parser.add_argument('-b','--fileB',required=True)
parser.add_argument('-f','--firstcolumn',required=True)
parser.add_argument('-s','--secondcolumn',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

fileA=args['fileA']
fileB=args['fileB']
firstcolumn=int(args['firstcolumn'])
secondcolumn=int(args['secondcolumn'])
outfile=args['outfile']


def get_newBline(bline,secondcolumn):
    barray = bline.split('\t')
    barray.pop(secondcolumn)
    newBline = '\t'.join(barray).rstrip('\n')
    return newBline


def joinfiles(fileA,fileB,fileC,logCutOff,outdir):
    with open(fileA,'r') as inputA:
        fileAlines = inputA.readlines()

    with open(fileB,'r') as inputB:
        fileBlines = inputB.readlines()

    joinfile=open(outfile,'w')

    for aline in fileAlines:
        for bline in fileBlines:
            fileAvalue = aline.split('\t')[firstcolumn].rstrip('\n')
            fileBvalue = bline.split('\t')[secondcolumn].rstrip('\n')
            if (fileAvalue == fileBvalue):
                newBline=get_newBline(bline,secondcolumn)
                outline = aline.rstrip('\n') + '\t' + newBline + '\n'
                joinfile.write(outline)
                break
        outline = aline
        joinfile.write(outline)

    joinfile.close()

joinfiles(fileA,fileB,firstcolumn,secondcolumn,outfile)
