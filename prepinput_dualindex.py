import os
import shutil
import subprocess
import pickle
import re
from Bio.Seq import Seq
#from Bio.Alphabet import IUPAC
from multiprocessing import Pool


def isfastqgz(s):
    return s.endswith(".fastq.gz")

def isfastq(s):
    return s.endswith(".fastq")

def whichtag(fqfile,tagdict):
    for t in tagdict:
        if t in fqfile:
            return t
    return None

def existsdictfile(f):
    return os.path.exists(f)

def buildfastqgzlist(currdir):
    filelist=os.listdir(currdir)
    return sorted(list(filter(isfastqgz,filelist)))

def buildfastqlist(currdir):
    filelist=os.listdir(currdir)
    return sorted(list(filter(isfastq,filelist)))

def parsubproc(subcall):
    print('parallel')
    print('subcall=',subcall)
    subprocess.call([subcall],shell=True)

def uncompressfastqgzfiles(fastqgzlist):
    subproclist=list()
    for f in fastqgzlist:
        fbase,fext = os.path.splitext(f)
        if not os.path.exists(fbase):
            subcall='gunzip -c ' + f + ' > ' + fbase #should parse gz
            print('gunzipcall: ', subcall)
            subproclist.append(subcall)
            p=Pool(len(subproclist))
            p.map(parsubproc,subproclist)
            subprocess.call([subcall],shell=True)

def buildpresenttagslist(fastqlist,tagindex5list,tagindex7list):
    tagdict=dict()
    dualtags = fastqlist[0].split('_')[1]
    tag1 = dualtags.split('-')[0]
    tag2 = dualtags.split('-')[1]
    tagdict["i5"] = tag1
    tagdict["i7"] = tag2
    return tagdict

def writeR1adapter3p(nextera_dict,tagdict):
    D500 = nextera_dict["D500"].seq.reverse_complement().complement()
    print("D500=",D500)
    tagi5 = Seq(tagdict["i5"]).reverse_complement()
    print("tagi5=",tagi5)
    D500spec = str(D500).replace("]NNNNNNNN[",str(tagi5))
    r1adapterfile3p = "R1adapter3p.fasta"
    outfile = open(r1adapterfile3p,"w")
    outfile.write(">D500"+'\n')
    outfile.write(D500spec+'\n')
    outfile.close()
    return r1adapterfile3p

def writeR2adapter3p(nextera_dict,tagdict):
    D500 = nextera_dict["D500"].seq.complement()
    tagi5 = Seq(tagdict["i5"])
    D500spec = str(D500).replace("[NNNNNNNN]",str(tagi5))
    D700 = nextera_dict["D700"].seq.complement()
    tagi7 = Seq(tagdict["i7"]).reverse_complement()
    D700spec = str(D700).replace("[NNNNNNNN]",str(tagi7))
    r2adapterfile3p = "R2adapter3p.fasta"
    outfile = open(r2adapterfile3p,"w")
    outfile.write(">D500"+'\n')
    outfile.write(D500spec+'\n')
    outfile.write(">D700"+'\n')
    outfile.write(D700spec+'\n')
    outfile.close()
    return r2adapterfile3p

def writeR1adapter5p():
    return r1adapterfile5p

def writeR2adapter5p():
    return r2adapterfile5p

def createadapterremovalfiles(truseqtemplatefile,tagdict,replacestring):
    from Bio import SeqIO
    nextera_dict = SeqIO.to_dict(SeqIO.parse(truseqtemplatefile,"fasta"))
    r1adapterfile3p=writeR1adapter3p(nextera_dict,tagdict)
    r2adapterfile3p=writeR2adapter3p(nextera_dict,tagdict)
    #r1adapterfile5p=writeR1adapter5p(nextera_dict,tagdict)
    #r2adapterfile5p=writeR2adapter5p(nextera_dict,tagdict)
    nextera_dict["D500"] = str(nextera_dict["D500"].seq).replace(replacestring,tagdict["i5"])
    nextera_dict["D700"] = str(nextera_dict["D700"].seq).replace(replacestring,tagdict["i7"])
    print(nextera_dict["D500"])
    print(nextera_dict["D700"])
    #make files
    origname,origextension=os.path.splitext(truseqtemplatefile)
    origname=os.path.basename(origname)
    tagfile=origname+"_"+tagdict["i5"]+"_"+tagdict["i7"]+origextension
    print("create tagfile:",tagfile)
    shutil.copyfile(truseqtemplatefile,tagfile)
    #edit files
    outfile = open(tagfile,"w")
    outfile.write(">D500"+'\n')
    outfile.write(nextera_dict["D500"]+'\n')
    outfile.write(">D700"+'\n')
    outfile.write(nextera_dict["D700"]+'\n')
    outfile.close()
    return tagfile

def buildfastqtagdict(fastqlist,tagfile):
    fastqtagdict=dict()
    for fqfile in fastqlist:
        #tagstring=whichtag(fqfile,tagdict)
        fastqtagdict[fqfile]=tagfile
    return fastqtagdict

def writefastqtagdict(fastqtagdict,dictfile):
    output=open(dictfile,'wb')
    pickle.dump(fastqtagdict,output)
    output.close()

def readfastqtagdict(dictfile):
    input=open(dictfile,'rb')
    fastqtagdict=pickle.load(input)
    input.close()
    return fastqtagdict

def concatreadn(fastqlist):
    r1list=[]
    r2list=[]
    for fqfile in fastqlist:
        if "_R1_" in fqfile:
            r1list.append(fqfile)
        if "_R2_" in fqfile:
            r2list.append(fqfile)
    outname1,origextension=os.path.splitext(r1list[0])
    outname2,origextension=os.path.splitext(r2list[0])
    shortname1=re.sub("_L.*_R1_...","",outname1)+"_R1"+origextension
    shortname2=re.sub("_L.*_R2_...","",outname2)+"_R2"+origextension
    if os.path.exists(shortname1):
        print("fastq files already concat:",shortname1)
    else:
        cat1string=" ".join(r1list)
        cat1call="time cat " + cat1string + " > " + shortname1
        print(cat1call)
        subprocess.call([cat1call],shell=True)
    if os.path.exists(shortname2):
        print("fastq files already concat:",shortname2)
    else:
        cat2string=" ".join(r2list)
        cat2call="time cat " + cat2string + " > " + shortname2
        print(cat2call)
        subprocess.call([cat2call],shell=True)
    fqlist=[shortname1,shortname2]
    return fqlist

def prepinput(dictfile,currdir,tagindex5list,tagindex7list,truseqtemplatefile,replacestring):
    if not existsdictfile(dictfile):
        fastqgzlist=buildfastqgzlist(currdir)
        uncompressfastqgzfiles(fastqgzlist)
        fastqlist=buildfastqlist(currdir)
        fastqlist=concatreadn(fastqlist)
        tagdict=buildpresenttagslist(fastqlist,tagindex5list,tagindex7list)
        print("tagdict=",tagdict)
        #need an if not exist adapterfiles
        tagfile = createadapterremovalfiles(truseqtemplatefile,tagdict,replacestring)
        fastqtagdict=buildfastqtagdict(fastqlist,tagfile)
        print("fastqtagdict=",fastqtagdict)
        writefastqtagdict(fastqtagdict,dictfile)
        createadapterremovalfiles(truseqtemplatefile,tagdict,replacestring)
    else:
        fastqtagdict=readfastqtagdict(dictfile)
    return fastqtagdict
