import os
import subprocess
#project imports
import pipeutil

def runscythe(fastqtagdict,templatefile):
    scythepreppedlist=[]
    thiscall="_scythe"
    for f in fastqtagdict:
        adapterfile=fastqtagdict[f]
        print("adapterfile=",adapterfile)
    #adapterfile=pipeutil.tag2adapterfile(tag,templatefile)
        origname,origextension=os.path.splitext(f)
        scytheoutput=origname+thiscall+origextension
        scythepreppedlist.append(scytheoutput)
        scythediscarded=origname+"_scythediscarded"+origextension
        if os.path.exists(scytheoutput):#should count num lines in out+match
        #or create successful completion out file
            print("scythe processing already completed:",scytheoutput)
        else:
            scythecall="time scythe -a " + adapterfile + " -o " + \
                scytheoutput + " -m " + scythediscarded + " " + \
                f + " -qsanger"
            print(scythecall)
            subprocess.call([scythecall],shell=True)
    return scythepreppedlist,thiscall

def runtrimmomatic(fastqtagdict, templatefile, lastcall):
    trimmomaticpreppeddict={}
    pairedenddict=pipeutil.buildpairedenddict(fastqtagdict)
    for p in pairedenddict:
        tag=fastqtagdict[p]
        adapterfile=pipeutil.tag2adapterfile(tag,templatefile)
        origname1,origextension=os.path.splitext(p)
        input1=origname1+lastcall+origextension
        origname2,origextension=os.path.splitext(pairedenddict[p])
        input2=origname2+lastcall+origextension
        output1paired=origname1+"_paired"+origextension
        output1unpaired=origname1+"_unpaired"+origextension
        output2paired=origname2+"_paired"+origextension
        output2unpaired=origname2+"_unpaired"+origextension
        outputlog=origname1+".stats"
        trimmomaticpreppeddict[output1paired]=output2paired
        if os.path.exists(output1paired):
            print("trimmomatic processing already completed:",output1paired)
        else:
            trimmomaticcall="time java -classpath ~/bin/trimmomatic-0.22.jar org.usadellab.trimmomatic.TrimmomaticPE -phred33 -trimlog " + outputlog + " " + input1 + " " + input2 + " " + output1paired + " " + output1unpaired + " " + output2paired + " " + output2unpaired + " ILLUMINACLIP:"+adapterfile+":2:40:15 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15"
            subprocess.call([trimmomaticcall],shell=True)
    return trimmomaticpreppeddict,"_trimmomatic"

def runfastqqualitytrimmer(prevpreppedlist,lastcall):
    outputlist=list()
    thiscall="_fastqqt"
    for f in prevpreppedlist:
        print("to quality trim:",f)
        origname,origextension=os.path.splitext(f)
        newname=origname.replace(lastcall,thiscall)
        output=newname+origextension
        outputlist.append(output)
        if os.path.exists(output):#should count num lines in out+match
        #or create successful completion out file
            print("fastq quality -t 15 processing already completed:",output)
        else:
            fastqcall="time fastq_quality_trimmer -t 15 -i " + f + " -o " + output + " -Q33"
            print(fastqcall)
            subprocess.call([fastqcall],shell=True)

    return outputlist,thiscall

def runfast5ptrimmer(prevpreppedlist,lastcall):
    print("Enter 5p trimmer")
    thiscall="_fastx5p"
    outputlist=list()
    for f in prevpreppedlist:
        print(thiscall + " " + f)
        origname,origextension=os.path.splitext(f)
        newname=origname.replace(lastcall,thiscall)
        output=newname+origextension
        outputlist.append(output)
        if os.path.exists(output):#should count num lines in out+match
        #or create successful completion out file
            print("fastx trim to 18 processing already completed:",output)
        else:
            acall="time fastx_trimmer -f 18 -i " + f + " -o " + output + " -Q33"
            print(acall)
            subprocess.call([acall],shell=True)
    return outputlist,thiscall


def trimfilter(fastqtagdict,templatefile,dotrim):
    lastcall=str()
    paireddict=dict()
    if dotrim:
        preppedlist,lastcall=runscythe(fastqtagdict,templatefile)
        paireddict=pipeutil.buildpairedenddict(preppedlist)
    else:
        nonpreppedlist,lastcall=pipeutil.tagdict2fqlist(fastqtagdict)
        print('trimfilter_dualindex::nonpreppedlist=',nonpreppedlist)
        paireddict=pipeutil.buildpairedenddict(nonpreppedlist)
    return paireddict,lastcall
