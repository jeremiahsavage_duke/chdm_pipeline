import inspect
import os
import re
import subprocess

def sam2bam(samlist):
    fname=inspect.stack()[0][3]
    bamlist=[]
    for s in samlist:
        origname,origextension=os.path.splitext(s)
        outfile=origname+".bam"
        bamlist.append(outfile)
        if os.path.exists(outfile):
            print("sam2bam already completed:",outfile)
        else:
            samtoolscall="time samtools view -bS -o " + outfile + " " + s
            subprocess.call([samtoolscall],shell=True)
    return bamlist,fname

def picardsam2bam(samlist):
    fname=inspect.stack()[0][3]
    bamlist=[]
    for samfile in samlist:
        origname,origextension=os.path.splitext(samfile)
        outfile=origname+".bam"
        bamlist.append(outfile)
        if os.path.exists(outfile):
            print("picardsam2bam already completed:",outfile)
        else:
            homedir=os.path.expanduser('~/')
            picardsam2bamcall="time java -d64 -Xmx4G -Djava.io.tmpdir="+homedir+"/.local/tmp/ -jar ~/.local/bin/picard/SortSam.jar INPUT="+samfile+ " OUTPUT="+outfile+" SORT_ORDER=coordinate CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT TMP_DIR="+homedir+"/.local/tmp/"
            print("picardsam2bamcall:",picardsam2bamcall)
            subprocess.call([picardsam2bamcall],shell=True)
    return bamlist,fname


def sortbams(bamlist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    for infile in bamlist:
        origname,origextension=os.path.splitext(infile)
        if(lastcall=="sam2bam"):
            outfile=origname+"_"+fname # 'samtools sort' automagically adds '.bam'
            outfilename=outfile+origextension
        else:
            outfile=origname.replace(lastcall,fname)
            outfilename=outfile+origextension
        outlist.append(outfilename)
        if os.path.exists(outfilename):
            print("bamfile already sorted:",outfilename)
        else:
            sortbamcall="time samtools sort " + infile + " " + outfile
            subprocess.call([sortbamcall],shell=True)
    return outlist,fname

def mergebams(bamlist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfiles=" ".join(bamlist)
    outname,origextension=os.path.splitext(bamlist[0])
    shortname=re.sub("_L.*_R1_...","",outname)
    newname=shortname.replace(lastcall,fname)
    outfile=newname+origextension
    outlist.append(outfile)
    if os.path.exists(outfile):
        print("bamfiles already merged:",outfile)
    else:
        mergebamscall="time samtools merge -nr " + outfile + " " + bamfiles
        subprocess.call([mergebamscall],shell=True)
    return outlist,fname

def fillmdbam(bamlist,referencegenomefasta,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    origname,origextension=os.path.splitext(bamlist[0])
    outname=origname.replace(lastcall,fname)
    outfile=outname+".sam"
    outlist.append(outfile)
    if os.path.exists(outfile):
        print("bamfile already fillmd'd:",outfile)
    else:
        fillmdcall="time samtools fillmd -Ar " + bamlist[0] + " " + referencegenomefasta + " > " + outfile
        subprocess.call([fillmdcall],shell=True)
    return outlist,fname

def rmdupbam(bamlist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    origname,origextension=os.path.splitext(bamlist[0])
    outname=origname.replace(lastcall,fname)
    outfile=outname+origextension
    outlist.append(outfile)
    if os.path.exists(outfile):
        print("rmdup bam file already generated:",outfile)
    else:
        rmdupcall="time samtools rmdup " + bamlist[0] + " " + outfile
        subprocess.call([rmdupcall],shell=True)
    return outlist,fname

def picardmarkduplicates(bamlist,lastcall,isrna):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname+"_"+fname
    if isrna:
        outdir,outname=os.path.split(outname)
    print('sam2bam.py::picardmarkduplicates()::outname=',outname)
    outfile=outname+origextension
    print('sam2bam.py::picardmarkduplicates()::outfile=',outfile)
    outlist.append(outfile)
    if os.path.exists(outfile):
        print("markduplicates already generated:",outfile)
    else:
        subcall="time java -d64 -Xmx4G -jar ~/.local/bin/picard/MarkDuplicates.jar INPUT=" + bamfile + " OUTPUT=" + outfile + " METRICS_FILE="+origname+"_"+fname+".metrics" + " CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT MAX_RECORDS_IN_RAM=2147483647"
        print(fname+" subcall:",subcall)
        subprocess.call([subcall],shell=True)
    return outlist,fname


def sam2bamrmdup(samlist,referencegenomefasta,isrna):
#    bamlist,lastcall=sam2bam(samlist) #[file]_[info].bam
#    sortedbamlist,lastcall=sortbams(bamlist,lastcall) #[file]_[info]_sorted.bam
#    mergedbamfile,lastcall=mergebams(sortedbamlist,lastcall) #[file].bam
#    sortedmergedbamlist,lastcall=sortbams(mergedbamfile,lastcall) #[file]_sorted.bam ?needed?
#    fillmdsamlist,lastcall=fillmdbam(sortedbamlist,referencegenomefasta,lastcall) #[file]_baq.bam
#    bamlist,samcall=sam2bam(fillmdsamlist)
#    rmdupbamlist,lastcall=rmdupbam(bamlist,lastcall)
    print("call picardsam2bam():",samlist)
    bamlist,lastcall=picardsam2bam(samlist)
    print ("call picardmarkduplicates()",bamlist)
    bamlist,lastcall=picardmarkduplicates(bamlist,lastcall,isrna)
    return bamlist,lastcall
