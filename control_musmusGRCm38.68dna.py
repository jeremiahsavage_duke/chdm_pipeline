import os
import sys
import subprocess
import inspect
# local
import pipeutil

homedir=os.path.expanduser("~/")
pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)
#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"truseq_adapters.fasta"))
TAGINDEXLIST=pipeutil.addquote("ATCACG, CGATGT, TTAGGC, TGACCA, ACAGTG, GCCAAT, CAGATC, ACTTGA, GATCAG, TAGCTT, GGCTAC, CTTGTA, AGTCAA, AGTTCC, ATGTCA, CCGTCC, GTCCGC, GTGAAA, GTGGCC, GTTTCG, CGTACG, GAGTGG, ACTGAT, ATTCCT")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNN]")

#variable
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/home2/Mus_musculus.GRCm38.68.dna.toplevel/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/ftp.ensembl.org/pub/release-68/fasta/mus_musculus/dna/Mus_musculus.GRCm38.68.dna.toplevel.fa.gz"))


subprocesscall="python "+pipelinedir+"pipeline.py " +" -t "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -s " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR+ " -c " + CHROMOSOMEFASTALIST + " -g " + GENOMESOURCE

subprocess.call([subprocesscall],shell=True)

### cleanup
#rm *.sam *.bam *scythe* *paired* *.stats *.dict *.out nohup* *.fasta
