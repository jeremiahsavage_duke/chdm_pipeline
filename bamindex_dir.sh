for f in *.bam ; do echo java -d64 -Xmx4G -jar ~/local/bin/picard/SortSam INPUT="$f" OUTPUT=sorted_"$f" SORT_ORDER=coordinate ; done | parallel -j+0

for f in *.bam ; do echo java -d64 -Xmx4G -jar ~/local/bin/picard/BuildBamIndex.jar INPUT="$f" ; done | parallel -j+0

for f in *.bam ; do echo java -d64 -Xmx4G -jar ~/local/bin/picard/BuildBamIndex.jar INPUT="$f" ; done | nohup parallel -j+0 &


