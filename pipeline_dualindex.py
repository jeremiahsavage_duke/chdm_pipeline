import os
import re
import argparse
#project imports
import prepinput_dualindex
import trimfilter_dualindex
import alignreads
import sam2bam
import prepreferencegenome
import rungatk
DOTRIM=False

parser=argparse.ArgumentParser("run the pipe")
parser.add_argument('-s','--truseqtemplate', required=True)
parser.add_argument('-5','--i5adapter', required=True)
parser.add_argument('-7','--i7adapter', required=True)
parser.add_argument('-d','--dictfile', required=True)
parser.add_argument('-n','--stringreplaced', required=True)
parser.add_argument('-r','--referencedir', required=True)
parser.add_argument('-c','--chromolist', required=True)
parser.add_argument('-g','--genomesource', required=True)
parser.add_argument('-m','--genomename',required=True)
parser.add_argument('-b','--baitfile', required=False)
parser.add_argument('-t','--targetfile', required=False)
parser.add_argument('-v','--vcffile', required=False)
parser.add_argument('--gtffile',required=True)
args=vars(parser.parse_args())
mutagenized=False
sampleploidy=2
isrna=False

truseqtemplate=args['truseqtemplate']
tagindex5list=''.join(args['i5adapter'].split()).split(',')
tagindex7list=''.join(args['i7adapter'].split()).split(',')
fastqtagdictfile=args['dictfile']
replacestring=args['stringreplaced']
referencedir=args['referencedir']
chromosomefastalist=''.join(args['chromolist'].split()).split(',')
genomesource=args['genomesource']
baitfile=args['baitfile']
targetfile=args['targetfile']
vcffile=args['vcffile']
genomename=args['genomename']
gtffile=args['gtffile']
### need a variable for fastq format (sanger, illumina, phred, solexa)

referencedir += os.sep

currdir=os.getcwd()

referencegenomefasta=prepreferencegenome.prepreferencegenome(referencedir,genomesource,genomename)
fastqtagdict=prepinput_dualindex.prepinput(fastqtagdictfile,currdir,tagindex5list,tagindex7list,truseqtemplate,replacestring)
preppedfastqdict,lastcall=trimfilter_dualindex.trimfilter(fastqtagdict,truseqtemplate,DOTRIM)
print("preppedfastqdict=",preppedfastqdict)
samlist=alignreads.alignreads(preppedfastqdict,referencedir,referencegenomefasta,lastcall,isrna,gtffile)
bamlist,lastcall=sam2bam.sam2bamrmdup(samlist,referencegenomefasta,isrna)
rungatk.rungatk(bamlist,referencegenomefasta,baitfile,targetfile,vcffile,lastcall,sampleploidy,mutagenized)
