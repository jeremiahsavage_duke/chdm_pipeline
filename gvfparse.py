import os
import argparse

parser=argparse.ArgumentParser("gvf input file reader")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def GVFparse(infile):
    with open(infile,"r") as gvfinfile:
        print("hi")
        lines = gvfinfile.readlines()


    gvfoutfile = open(outfile,'w')
    gvfoutfile.write("##fileformat=VCFv4.0\n")
    gvfoutfile.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n")

    
    for line in lines:
        if line.startswith("##"):
            print("comment:",line)
        else:
            data = line.split()
            contig=data[0]
            source=data[1]
            varianttype=data[2]
            start=data[3]
            originalstart=start
            end=data[4]
            score=data[5]
            strand=data[6]
            phase=data[7]
            attributes=data[8].split(';')
            attributesdict=dict()
            for attribute in attributes:
                key=attribute.split('=')[0]
                #print("key=",key)
                value=attribute.split('=')[1]
                #print("value=",value)
                attributesdict[key]=value
                #gvfoutfile.write(key+'\t'+value+'\n')

            #if (varianttype="insertion"):
            #    print()
            #elif (varianttype="deletion"):
            #    print()
            #elif (varianttype="substitution"):
            #    print()
            if ( "SNV" in varianttype ):
                dbName=attributesdict['Dbxref'].split(':')[0]
                dbID=attributesdict['Dbxref'].split(':')[1]                        
                gvfoutfile.write(contig+'\t'+start+'\t'+dbID+'\t'+attributesdict['Reference_seq']+'\t'+attributesdict['Variant_seq']+'\t'+score+'\t'+"."+'\t'+"Database="+dbName+";dbID="+dbID+'\n')

    gvfoutfile.close()
        

    
GVFparse(infile)
