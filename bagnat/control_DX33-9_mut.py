from control_base import *

POOLSIZE=pipeutil.addquote("40")
ISRNA=str(False)
MUTAGENIZED=str(True)
SNPTRACK_CHROMOSOME=2
SNPTRACK_STARTPEAK=32686272
SNPTRACK_ENDPEAK=48653704

subprocesscall="python3 "+ pipelinedir + "../pipeline.py" + " -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + " -p " + POOLSIZE + " -z " + str(MUTAGENIZED) + " --snptrack_chromosome " + str(SNPTRACK_CHROMOSOME) + " --snptrack_startpeak " + str(SNPTRACK_STARTPEAK) + " --snptrack_endpeak " + str(SNPTRACK_ENDPEAK) + ' --isrna ' + ISRNA + ' --gtffile ' + GTFFILE

subprocess.call([subprocesscall],shell=True)
