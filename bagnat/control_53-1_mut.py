from control_base import *

POOLSIZE=pipeutil.addquote("44")
ISRNA=str(False)
MUTAGENIZED=str(True)
SNPTRACK_CHROMOSOME=22
SNPTRACK_STARTPEAK=3258443
SNPTRACK_ENDPEAK=20702624

subprocesscall="python3 "+ pipelinedir + "../pipeline.py" + " -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + " -p " + POOLSIZE + " -z " + str(MUTAGENIZED) + " --snptrack_chromosome " + str(SNPTRACK_CHROMOSOME) + " --snptrack_startpeak " + str(SNPTRACK_STARTPEAK) + " --snptrack_endpeak " + str(SNPTRACK_ENDPEAK) + ' --isrna ' + ISRNA + ' --gtffile ' + GTFFILE

subprocess.call([subprocesscall],shell=True)
