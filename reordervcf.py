import os
import argparse

parser=argparse.ArgumentParser("gvf input file reader")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

order=["chr1", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15", "chr16", "chr17", "chr18", "chr19", "chr2", "chr20", "chr21", "chr22", "chr23", "chr24", "chr25", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", "chr9"]


with open(infile,"r") as vcfinput:
    lines=vcfinput.readlines()
vcfoutfile=open(outfile,'w')

vcfoutfile.write("##fileformat=VCFv4.0\n")
vcfoutfile.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n")

for chromo in order:
    print("chromo=",chromo)
    for line in lines:
        if line.startswith(chromo+'\t'):
            vcfoutfile.write(line)

vcfoutfile.close()
