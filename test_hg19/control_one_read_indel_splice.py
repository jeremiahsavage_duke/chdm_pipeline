from control_base import *

POOLSIZE=pipeutil.addquote("1")
ISRNA=str(False)
MUTAGENIZED=str(False)
SNPTRACK_CHROMOSOME=0
SNPTRACK_STARTPEAK=0
SNPTRACK_ENDPEAK=0

subprocesscall="python3 "+ pipelinedir + "../pipeline.py" + " -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + " -p " + POOLSIZE + " -z " + str(MUTAGENIZED) + " --snptrack_chromosome " + str(SNPTRACK_CHROMOSOME) + " --snptrack_startpeak " + str(SNPTRACK_STARTPEAK) + " --snptrack_endpeak " + str(SNPTRACK_ENDPEAK) + ' --isrna ' + ISRNA + ' --gtffile ' + GTFFILE + ' --filterdir ' + str(FILTERDIR)

subprocess.call([subprocesscall],shell=True)
