import os
import sys
import subprocess
import inspect
# local
homedir=os.path.expanduser("~/")
print(os.path.normpath(homedir+'chdm_pipeline'))
sys.path.append(os.path.normpath(homedir+'chdm_pipeline'))
#print('sys.path=',sys.path)
import pipeutil
print [key for key in locals().keys()
       if isinstance(locals()[key], type(sys)) and not key.startswith('__')]

pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)

#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"/chdm_pipeline/truseq_adapters.fasta"))
TAGINDEXLIST=pipeutil.addquote("ATCACG, CGATGT, TTAGGC, TGACCA, ACAGTG, GCCAAT, CAGATC, ACTTGA, GATCAG, TAGCTT, GGCTAC, CTTGTA, AGTCAA, AGTTCC, ATGTCA, CCGTCC, GTCCGC, GTGAAA, GTGGCC, GTTTCG, CGTACG, GAGTGG, ACTGAT, ATTCCT, TGGTCA, GCCAAT, CTTGTA, TGACCA")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNN]")

#variable
FILTERDIR="None"
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/hg19broad/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/hg19broad/ucsc.hg19.fasta.gz"))
GENOMENAME=pipeutil.addquote("ucsc.hg19.fasta")
VCFFILE=pipeutil.addquote(os.path.normpath(homedir+"dbsnp_138.hg19.vcf"))
GTFFILE=pipeutil.addquote("Homo_sapiens.GRCh37.74.gtf")
BAITFILE="None"
TARGETFILE="None"
