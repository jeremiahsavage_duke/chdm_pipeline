import os
import argparse

parser=argparse.ArgumentParser("refseq reduced file parser")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def gbff_parse(infile,outfile):
    with open(infile,'r') as gbfffile:
        gbfflines = gbfffile.readlines()

    gbffoutfile=open(outfile,'w')

    nextline=""
    nextchromosome=False
    for gbffline in gbfflines:
        if gbffline.startswith("LOCUS"):
            gbffoutfile.write(nextline)
            nextline='\n'+gbffline.split()[1]
            nextchromosome=True
        if gbffline.startswith("                     /chromosome="):
            nextline+='\t'+gbffline.split('"')[1]
            nextchromosome=False
        if gbffline.startswith("                     /gene="):
            if (nextchromosome==True):
                nextline+='\t'+"?"
            nextline+='\t'+gbffline.split('"')[1]
    gbffoutfile.close()

gbff_parse(infile,outfile)
