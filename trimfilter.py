import os
import inspect
import subprocess
from multiprocessing import Pool
#project imports
import pipeutil
'''
    for k, v in fastqtagdict.items():
        print('----------')
        print('k=',k)
        print('v=',v)
        if v is None:
            print('NOOOONE!!')
            fastqlist.append(k)
        else:
'''

def parsubproc(subcall):
    print('parallel')
    print('\tsubcall=',subcall)
    subprocess.call([subcall],shell=True)

def runscythe(fastqtagdict,templatefile):
    print()
    print()
    print()
    print("trimfilter.py::runscythe()::fastqtagdict=",fastqtagdict)
    fastqlist=list()
    subproclist=list()
    returnstr=str()
    for f in fastqtagdict:
        print('>>>>>>>>>')
        print('f=',f)
        tag=fastqtagdict[f]
        if tag is None:
            fastqlist.append(f)
            continue
        print('tag=',tag)
        returnstr='_scythe'
        adapterfile=pipeutil.tag2adapterfile(tag,templatefile)
        origname,origextension=os.path.splitext(f)
        scytheoutput=origname+"_scythe"+origextension
        print('scytheoutput=',scytheoutput)
        fastqlist.append(scytheoutput)
        scythediscarded=origname+"_scythediscarded"+origextension
        if os.path.exists(scytheoutput):#should count num lines in out+match
        #or create successful completion out file
            print("scythe processing already completed:",scytheoutput)
        else:
            subcall="time scythe -a " + adapterfile + " -o " + \
                scytheoutput + " -m " + scythediscarded + " " + \
                f + " -qsanger"
            subproclist.append(subcall)
            print('--------------------------')
            print("trimfilter.py::runscythe()::fastqlist=",fastqlist)
            print('subproclist=',subproclist)
            print('len(subproclist)=',len(subproclist))
            p=Pool(len(subproclist))
            p.map(parsubproc,subproclist)
    return fastqlist,returnstr

def runtrimmomatic(fastqtagdict, templatefile, lastcall):
    trimmomaticpreppeddict={}
    pairedenddict=pipeutil.buildpairedenddict(fastqtagdict)
    for p in pairedenddict:
        tag=fastqtagdict[p]
        adapterfile=pipeutil.tag2adapterfile(tag,templatefile)
        origname1,origextension=os.path.splitext(p)
        input1=origname1+lastcall+origextension
        origname2,origextension=os.path.splitext(pairedenddict[p])
        input2=origname2+lastcall+origextension
        output1paired=origname1+"_paired"+origextension
        output1unpaired=origname1+"_unpaired"+origextension
        output2paired=origname2+"_paired"+origextension
        output2unpaired=origname2+"_unpaired"+origextension
        outputlog=origname1+".stats"
        trimmomaticpreppeddict[output1paired]=output2paired
        if os.path.exists(output1paired):
            print("trimmomatic processing already completed:",output1paired)
        else:
            trimmomaticcall="time java -classpath ~/bin/trimmomatic-0.22.jar org.usadellab.trimmomatic.TrimmomaticPE -phred33 -trimlog " + outputlog + " " + input1 + " " + input2 + " " + output1paired + " " + output1unpaired + " " + output2paired + " " + output2unpaired + " ILLUMINACLIP:"+adapterfile+":2:40:15 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15"
            subprocess.call([trimmomaticcall],shell=True)
    return trimmomaticpreppeddict,"_trimmomatic"

def trimfilter(fastqtagdict,templatefile):
    callerframerecord = inspect.stack()[1]
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    fastqlist,lastcall=runscythe(fastqtagdict,templatefile)
    print('trimfilter.py::trimfilter::'+str(info.lineno)+'::fastqlist=',fastqlist)
    preppeddict=pipeutil.buildpairedenddict(fastqlist)
    print('trimfilter.py::trimfilter::'+str(info.lineno)+'::preppeddict=',preppeddict)
    #trimmomaticpreppeddict=runtrimmomatic(fastqtagdict,templatefile,"_scythe")
    return preppeddict,lastcall
