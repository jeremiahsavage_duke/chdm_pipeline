import os
import sys
import subprocess
import inspect
# local
homedir=os.path.expanduser("~/")
print(os.path.normpath(homedir+'chdm_pipeline'))
sys.path.append(os.path.normpath(homedir+'chdm_pipeline'))
#print('sys.path=',sys.path)
import pipeutil
print [key for key in locals().keys()
       if isinstance(locals()[key], type(sys)) and not key.startswith('__')]

pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)

#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"/chdm_pipeline/truseq_adapters.fasta"))
TAGINDEXLIST=pipeutil.addquote("ATCACG, CGATGT, TTAGGC, TGACCA, ACAGTG, GCCAAT, CAGATC, ACTTGA, GATCAG, TAGCTT, GGCTAC, CTTGTA, AGTCAA, AGTTCC, ATGTCA, CCGTCC, GTCCGC, GTGAAA, GTGGCC, GTTTCG, CGTACG, GAGTGG, ACTGAT, ATTCCT, TGGTCA")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNN]")

#variable
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/zv9/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/ftp.ensembl.org/pub/release-71/fasta/danio_rerio/dna/Danio_rerio.Zv9.71.dna.toplevel.fa.gz"))
GENOMENAME=pipeutil.addquote("Danio_rerio.Zv9.71.dna.toplevel.fa")
VCFFILE=pipeutil.addquote(os.path.normpath(homedir+"Danio_rerio.Zv9.71.vcf"))
GTFFILE=pipeutil.addquote("cuffcmp.combined.gtf")
BAITFILE="None"
TARGETFILE="None"
