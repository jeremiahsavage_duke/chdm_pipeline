import os
import sys
import subprocess
import inspect
# local
import pipeutil

homedir=os.path.expanduser("~/")
pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)
#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"/chdm_pipeline/nextera_adapters.fasta"))
TAGi5LIST=pipeutil.addquote("TAGATCGC, CTCTCTAT, TATCCTCT, AGAGTAGA, GTAAGGAG, ACTGCATA, AAGGAGTA, CTAAGCCT")
TAGi7LIST=pipeutil.addquote("TAAGGCGA, CGTACTAG, AGGCAGAA, TCCTGAGC, GGACTCCT, TAGGCATG, CTCTCTAC, CAGAGAGG, GCTACGCT, CGAGGCTG, AAGAGGCA, GTAGAGGA")
#QTAGINDEXLIST=pipeutil.addquote("TAGATCGC, CTCTCTAT, TATCCTCT, AGAGTAGA, GTAAGGAG, ACTGCATA, AAGGAGTA, CTAAGCCT, TAAGGCGA, CGTACTAG, AGGCAGAA, TCCTGAGC, GGACTCCT, TAGGCATG, CTCTCTAC, CAGAGAGG, GCTACGCT, CGAGGCTG, AAGAGGCA, GTAGAGGA")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNNNN]")

#variable
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/hg19broad/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/ftp.broadinstitute.org/bundle/2.5/hg19/ucsc.hg19.fasta.gz"))
GENOMENAME=pipeutil.addquote("ucsc.hg19.fasta")
GTFFILE=pipeutil.addquote("Homo_sapiens.GRCh37.71.gtf")
VCFFILE=pipeutil.addquote(os.path.normpath(homedir+"/hg19broad/dbsnp_137.hg19.vcf"))
BAITFILE="None"
TARGETFILE="None"

subprocesscall="python "+pipelinedir+"pipeline_dualindex.py " +" -s "+ TRUSEQTEMPLATE + " -5 " + TAGi5LIST + " -7 " + TAGi7LIST +" -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR+ " -c " + CHROMOSOMEFASTALIST + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + ' --gtffile ' + GTFFILE

subprocess.call([subprocesscall],shell=True)

### cleanup
#rm *.sam *.bam *scythe* *paired* *.stats *.dict *.out nohup* *.fasta
