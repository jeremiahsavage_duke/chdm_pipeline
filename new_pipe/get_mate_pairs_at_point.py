#!/usr/bin/env python3
import argparse
import glob
import os
import sys
import pysam

parser=argparse.ArgumentParser('get bam file mate-pair reads at defined interval')
parser.add_argument('-d','--directory',required=True)
parser.add_argument('-s','--startposition',required=True)
parser.add_argument('-e','--endposition',required=True)
parser.add_argument('-c','--chromosome',required=True)
args=vars(parser.parse_args())

chromosome=args['chromosome']
directory=args['directory']
start_position=int(args['startposition'])
end_position=int(args['endposition'])


def get_unmapped_mate(fastqreads,aread):
    for fastqread in fastqreads:
        if fastqread.qname==aread.qname:
            if fastqread.is_read1 and aread.is_read1:
                continue
            elif fastqread.is_read2 and aread.is_read2:
                continue
            else:
                return fastqread

def get_outfile_name(infile,start_position,end_position):
    basename,extension=os.path.splitext(infile)
    newfile=basename+'_'+str(start_position)+'_'+str(end_position)+extension
    return newfile


def main(chromosome,directory,start_position,end_position):
    globpattern=os.path.join(directory,'*.bam')
    bamfiles=glob.glob(globpattern)

    pysamdict=dict()
    for bamfile in bamfiles:
        pysamdict[os.path.basename(bamfile)]=pysam.Samfile(bamfile,'rb')

    fastqdict=dict()
    for pysamfile in pysamdict:
        fastqreads=pysamdict[pysamfile].fetch(reference=chromosome,start=start_position-1,end=end_position,until_eof=True)
        fastqdict[pysamfile]=fastqreads

    rowalldict=dict()
    for pysamfile in pysamdict:
        fastqreads=pysamdict[pysamfile].fetch(until_eof=True)
        rowalldict[pysamfile]=fastqreads


    for pysamfile in fastqdict:
        outfile=get_outfile_name(pysamfile,start_position,end_position)
        outfile_open=pysam.Samfile(outfile,'wb',template=pysamdict[pysamfile])
        qnameset=set()
        for alignedread in fastqdict[pysamfile]:
            if alignedread.qname in qnameset:
                continue
            qnameset.add(alignedread.qname)
            
            if alignedread.is_paired:
                outfile_open.write(alignedread)
                if alignedread.mate_is_unmapped:
                    unmapped_mate=get_unmapped_mate(rowalldict[pysamfile],alignedread)
                    outfile_open.write(unmapped_mate)
                else:
                    mate_pair=pysamdict[pysamfile].mate(alignedread)
                    outfile_open.write(mate_pair)
            else:
                sys.exit('is_not_paired')
    

if __name__=='__main__':
    main(chromosome,directory,start_position,end_position)
