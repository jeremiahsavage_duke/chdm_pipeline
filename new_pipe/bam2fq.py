#!/usr/bin/env python3
import glob
import os
import multiprocessing
import subprocess

def parsubproc(subcall):
    print('parallel')
    print('\tsubcall=%s' % subcall)
    subprocess.call([subcall],shell=True)

def main():
    numcpus=str(multiprocessing.cpu_count())

    bamfilelist=glob.glob('*.bam')
    subproclist=list()
    for bamfile in bamfilelist:
        if bamfile.startswith('DM'):
            newbam=bamfile.split('_')[0]+'_shuf.bam'
            tempbam=bamfile.split('_')[0]+'_temp.bam'
            newfastq=newbam.split('.')[0]+'.fastq'
            if not os.path.isfile(newbam):
                command1='samtools bamshuf -O -u '+bamfile+' '+tempbam+' > '+newbam
                command2='samtools bam2fq '+newbam+' > '+newfastq
                subcall=command1+' && '+command2
                #subcall='touch '+newfastq
                print(command1)
                print(command2)
                print(subcall)
                subproclist.append(subcall)
    p=multiprocessing.Pool(len(subproclist))
    p.map(parsubproc,subproclist)



if __name__=='__main__':
   main()
