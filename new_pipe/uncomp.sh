#!/bin/bash

rm uncomfastq.txt
for f in *.gz
do
    outfile="${f%.*}"
    echo $outfile
    if [ ! -f $outfile ]
    then
	#echo $outfile !!
	echo "zcat ${f} > ${outfile}" >> uncomfastq.txt
    fi
done

parallel < uncomfastq.txt
cat uncomfastq.txt
rm uncomfastq.txt
