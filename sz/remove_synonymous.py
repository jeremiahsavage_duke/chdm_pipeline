#!/usr/bin/env python3
import argparse
import os

EXONICFUNC_COL=2

parser=argparse.ArgumentParser('remove synonymous')
parser.add_argument('-t','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']

def main(tsvfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvfile))
    outfile='nonsyn'+origext
    outfile_open=open(outfile,'w')
  
    for tsvline in tsvlines:
        if tsvline.startswith('Func'):
            outfile_open.write(tsvline)
        else:
            tsvsplit=tsvline.split('\t')
            tsvfunc=tsvsplit[EXONICFUNC_COL]
            if tsvfunc.startswith('synonymous'):
                pass
            else:
                outfile_open.write(tsvline)

    outfile_open.close()


if __name__=='__main__':
    main(tsvfile)
