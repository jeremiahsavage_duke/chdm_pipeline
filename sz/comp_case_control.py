#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('compare case and control')
parser.add_argument('-c','--casetable',required=True)
parser.add_argument('-n','--controltable',required=True)
args=vars(parser.parse_args())

casetablefile=args['casetable']
controltablefile=args['controltable']


def get_versus_line(case_chr,case_bp,case_freq,controllines):
    for controlline in controllines:
        controlline=controlline.split('\n')[0]
        controlsplit=controlline.split('\t')
        control_chr=controlsplit[0]
        control_bp=controlsplit[1]
        if control_chr==case_chr and control_bp==case_bp:
            control_count=controlsplit[3]
            control_freq=float(controlsplit[4])
            if case_freq < control_freq:
                pct_of = -100 * (control_freq/case_freq)
            elif case_freq > control_freq:
                pct_of= 100 * (case_freq/control_freq)
            elif case_freq == control_freq:
                pct_of= 100
            else:
                pct_of='huh\n'
            newline=control_count+'\t'+str(control_freq)+'\t'+str(pct_of)+'\n'
            return newline
    newline='NA\tNA\tNA\n'
    return newline
        


def main(casetablefile,controltablefile):
    with open(casetablefile,'r') as casefile:
        caseinlines=casefile.readlines()

    with open(controltablefile,'r') as controlfile:
        controlinlines=controlfile.readlines()

    caseorigfile,origext=os.path.splitext(os.path.basename(casetablefile))
    caseoutput=caseorigfile+'_v_control'+origext    
    caseout=open(caseoutput,'w')
    #controlorigfile,origext=os.path.splitext(os.path.basename(controltablefile))
    #controloutput=controlorigfile+'_v_case'+origext    
    #controlout=open(controloutput,'w')

    print('\n\nENTERING CASE TESTS\n\n')
    for caseinline in caseinlines:
        caseinline=caseinline.split('\n')[0]
        casesplit=caseinline.split('\t')
        case_chr=casesplit[0]
        case_bp=casesplit[1]
        case_ofcount=casesplit[2]
        case_count=casesplit[3]
        case_freq=float(casesplit[4])
        controlline=get_versus_line(case_chr,case_bp,case_freq,controlinlines)
        outline=caseinline.strip('\n')+'\t'+controlline
        caseout.write(outline)

    #print('\n\nENTERING CONTROL TESTS\n\n')
    #for controlinline in controlinlines:
        #controlinline=controlinline.split('\n')[0]
        #controlsplit=controlinline.split('\t')
        #control_chr=controlsplit[0]
        #control_bp=controlsplit[1]
        #control_ofcount=controlsplit[2]
        #control_count=controlsplit[3]
        #control_freq=float(controlsplit[4])
        #caseline=get_versus_line(control_chr,control_bp,control_freq,caseinlines)
        #outline=controlinline.strip('\n')+'\t'+controlline
        #controlout.write(outline)


    caseout.close()
    #controlout.close()
        


if __name__=='__main__':
    main(casetablefile,controltablefile)
