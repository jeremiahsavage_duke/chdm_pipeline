#!/usr/bin/env python3
import argparse
import os

RFG=15

parser=argparse.ArgumentParser('keep altered proteins')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_prot'+origext    
    tsvoutfile_open=open(tsvoutfile,'w')

    newtsvlines=''

    for tsvinline in tsvinlines:
        tsvsplit=tsvinline.split('\t')
        if tsvsplit[0]=='chr':
            newtsvlines+=tsvinline
        else:
            rfgvalue=tsvsplit[RFG]
            print('rfgvalue=%s' % (rfgvalue))
            if rfgvalue.startswith('nonsynonymous') or rfgvalue.startswith('splicing') or rfgvalue.startswith('stopgain'):
                newtsvlines+=tsvinline


    for newtsvline in newtsvlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
