#!/usr/bin/env python3
import argparse
import itertools
import os

HEADER='Func	Gene	ExonicFunc	AAChange	Conserved	SegDup	ESP6500si_EA	1000g2012apr_EUR	dbSNP138	AVSIFT	LJB_PhyloP	LJB_PhyloP_Pred	LJB_SIFT	LJB_SIFT_Pred	LJB_PolyPhen2	LJB_PolyPhen2_Pred	LJB_LRT	LJB_LRT_Pred	LJB_MutationTaster	LJB_MutationTaster_Pred	LJB_GERP++	Chr	Start	End	Ref	Obs	chr' + \
'\t'+'pos'+'\t'+'id'+'\t'+'ref'+'\t'+'alt'+'\t'+'qual'+'\t'+'filter'+'\t'+'format'+'\t'+'case_genotype'+'\t'+'case_patient'+'\t'+\
'case_chr'+'\t'+'case_pos'+'\t'+'case_count_of'+'\t'+'case_count'+'\t'+'case_frequency'+'\t'+\
'case_patient'+'\t'+'case_genotype'+'\t'+'case_het_count'+'\t'+'case_hom_count'+'\t'+'control_count'+'\t'+'control_frequency'+'\t'+'percent_of'+'\t'+'control_patient(s)'+'\t'+'control_genotype(s)'+'\t'+'control_het_count'+'\t'+'control_hom_count'+'\n'


parser=argparse.ArgumentParser('merge sorted and count freq files')
parser.add_argument('-s','--sortedtsv',required=True)
parser.add_argument('-c','--countfreqtsv',required=True)
args=vars(parser.parse_args())

sortedtsv=args['sortedtsv']
countfreqtsv=args['countfreqtsv']

def writeheader(outfile_open):
    outfile_open.write(HEADER)
    #outfile_open.write('chr\tpos\tid\tref\talt\tqaul\tfilter\tinfo\tformat\tgenotype\tpatient_sample\tpatient_chr\tpatient_pos\tpatient_count_of\tcase_allele_count\tcase_allele_frequency\tcontrol_allele_count\tcontrol_allele_frequency\tpercent_of\n')



def main(sortedtsv,countfreqtsv):
    with open(sortedtsv) as sortedtsv_open:
        sortedtsvlines=sortedtsv_open.readlines()

    with open(countfreqtsv,'r') as countfreqtsv_open:
        countfreqtsvlines=countfreqtsv_open.readlines()

    for n,item in enumerate(sortedtsvlines):
        if '\n' in item:
            newitem=item.replace('\n','\t')
            sortedtsvlines[n]=newitem

    for n,item in enumerate(countfreqtsvlines):
        if '\n' in item:
            newitem=item.replace('\n','')
            countfreqtsvlines[n]=newitem

            
    newlines=itertools.zip_longest(sortedtsvlines,countfreqtsvlines)
    newlist=list(newlines)
    for n,item in enumerate(newlist):
        replacelist=list(item)
        if replacelist[1] is None:
            replacelist[1]=''
            newlist[n]=replacelist

    origfile,origext=os.path.splitext(sortedtsv)
    outfile=origfile+'_v_control'+origext
    outfile_open=open(outfile,'w')
    
    writeheader(outfile_open)
    for newline in newlist:
        outfile_open.write(''.join(newline)+'\n')

    outfile_open.close()

if __name__=='__main__':
    main(sortedtsv,countfreqtsv)
