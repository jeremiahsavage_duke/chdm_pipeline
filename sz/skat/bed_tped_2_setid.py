#!/usr/bin/env python3
import argparse
import os
import sys
##bed##
#chr start stop name
#
#+
#
##tped##
#chr var_chrX_NNN
#
#=
#
##setid##
#name var_chrX_NNN

parser=argparse.ArgumentParser('generate setid')
parser.add_argument('-t','--tpedfile',required=True)
parser.add_argument('-b','--bedfile',required=True)
args=vars(parser.parse_args())

tpedfile=args['tpedfile']
bedfile=args['bedfile']



def main(tpedfile,bedfile):
    with open(tpedfile,'r') as tpedfile_open:
        tpedlines=tpedfile_open.readlines()

    with open(bedfile,'r') as bedfile_open:
        bedlines=bedfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tpedfile))

    setidfile=origfile+'.setid'
    setidfile_open=open(setidfile,'w')

    bedlist=list()
    for bedline in bedlines:
        bedsplit=bedline.strip().split(' ')
        print('bedsplit=%s' % bedsplit)
        bedlist.append(bedsplit)

    for tpedline in tpedlines:
        variant=tpedline.split('\t')[1]
        variant_chr=variant.split('_')[1].strip('chr')
        variant_pos=int(variant.split('_')[2])
        for bedrecord in bedlist:
            bed_chr=bedrecord[0]
            bed_start=int(bedrecord[1])
            bed_end=int(bedrecord[2])
            bed_name=bedrecord[3]
            if (variant_chr==bed_chr) and (variant_pos>=bed_start) and (variant_pos<=bed_end):
                setid_name=bed_name
                setid_pos=variant
                setid_str=setid_name+'\t'+setid_pos+'\n'
                setidfile_open.write(setid_str)
                break
    setidfile_open.close()


if __name__=='__main__':
    main(tpedfile,bedfile)
