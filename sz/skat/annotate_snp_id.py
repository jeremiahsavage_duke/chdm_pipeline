#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys


parser=argparse.ArgumentParser('annotate gene_diff with gene name')
parser.add_argument('-b','--bedfile',required=True)
parser.add_argument('-s','--skatdata',required=True)
args=vars(parser.parse_args())

bedfile=args['bedfile']
skatdata=args['skatdata']


def get_genename(this_chr,this_pos,bed_list):
    #print('bed_list[0]=%s' % bed_list[0])
    for beditem in bed_list:
        #print('beditem=%s' % beditem)
        bed_chr=beditem[0]
        bed_start=beditem[1]
        bed_end=beditem[2]
        bed_gene=beditem[3]
        if this_chr==bed_chr:
            if int(this_pos) >= int(bed_start) and int(this_pos) <= int(bed_end):
                return bed_gene
        return 'None'


def main(bedfile,skatdata):
    with open(bedfile,'r') as bedfile_open:
        bedlines=bedfile_open.readlines()

    with open(skatdata,'r') as skatdata_open:
        skatlines=skatdata_open.readlines()

    origfile,origext=os.path.splitext(skatdata)
    outfile=origfile+'_annot.tsv'
    outfile_open=open(outfile,'w')

    bed_list=list()
    for bedline in bedlines:
        bedsplit=bedline.strip('\n').split(' ')
        #print('bedsplit=%s' % bedsplit)
        #print('bedsplit=%s' % bedsplit)
        chromosome=bedsplit[0]
        startpos=bedsplit[1]
        endpos=bedsplit[2]
        genename=bedsplit[3]
        bed_list.append(bedsplit)
    #print('bed_list=%s' % bed_list)

    for skatline in skatlines:
        skatsplit=skatline.split(',')
        #print('skatsplit=%s' % skatsplit)
        if skatsplit[1].strip('"')=='SetID':
            skatsplit.insert(1,'gene')
            newline='\t'.join(skatsplit)
            outfile_open.write(newline)
        else:
            colnum=skatsplit[0].strip('"')
            setid=skatsplit[1].strip('"')
            pvalue=skatsplit[2]
            nmarkerall=skatsplit[3]
            nmarkertest=skatsplit[4]
            this_chr=setid.split('_')[1].strip('chr')
            this_pos=setid.split('_')[2]
            if this_chr=='MT':
                continue

            for beditem in bed_list:
                #print('beditem=%s' % beditem)
                bed_chr=beditem[0]
                bed_start=beditem[1]
                bed_end=beditem[2]
                bed_gene=beditem[3]
                if this_chr==bed_chr:
                    if int(this_pos) >= int(bed_start) and int(this_pos) <= int(bed_end):
                        genename=bed_gene
                        break

            #genename=get_genename(this_chr,this_pos,bed_list)
            skatsplit.insert(1,genename)
            newline='\t'.join(skatsplit)
            outfile_open.write(newline)
            
    outfile_open.close()


if __name__=='__main__':
    main(bedfile,skatdata)
