#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('sort a file')
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']


def main(vcffile):
    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    mapfile=origfile+'.map'
    
    mapfile_open=open(mapfile,'w')

    vcfcomment=0
    for vcfnum,vcfline in enumerate(vcflines):
        if vcfline.startswith('#'):
            vcfcomment+=1
            continue
        else:
            vcfsplit=vcfline.split('\t')
            chromosome=vcfsplit[0]
            position=vcfsplit[1]
            paternalID="0"
            maternalID="0"
            sex="0"
            mapline=chromosome+' '+'SNP'+str(vcfnum-vcfcomment+1)+' '+'0'+' '+position+'\n'
            mapfile_open.write(mapline)

    mapfile_open.close()


if __name__=='__main__':
    main(vcffile)
