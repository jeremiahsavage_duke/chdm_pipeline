#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('read the code below')
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

def is_monogenic(genotype_list):
    hom_ref_count=0
    het_count=0
    hom_alt_count=0
    for genotype in genotype_list:
        GT=genotype.split(':')[0]
        if GT=='0/0':
            hom_ref_count+=1
        elif GT=='0/1' or GT=='1/0':
            het_count+=1
        elif GT=='1/1':
            hom_alt_count+=1
    if hom_alt_count==len(genotype_list):
        return True
    elif hom_ref_count==len(genotype_list):
        return True
    elif het_count==len(genotype_list):
        return True
    else:
        return False


def main(vcffile):
    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()


    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    filteredfile=origfile+'_filtered'+origext
    removedfile=origfile+'_removed'+origext
    filteredfile_open=open(filteredfile,'w')
    removedfile_open=open(removedfile,'w')

    for i,vcfline in enumerate(vcflines):
        if (i==0) or (i==1):
            filteredfile_open.write(vcfline)
            removedfile_open.write(vcfline)
        else:
            vcfsplit=vcfline.strip().split('\t')
            genotype_list=vcfsplit[9:]
            position_monogenic=is_monogenic(genotype_list)
            if position_monogenic:
                removedfile_open.write(vcfline)
            else:
                filteredfile_open.write(vcfline)

    filteredfile_open.close()
    removedfile_open.close()


if __name__=='__main__':
    main(vcffile)
