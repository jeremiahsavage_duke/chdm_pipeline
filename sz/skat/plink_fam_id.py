#!/usr/bin/env python3
import argparse
import os


parser=argparse.ArgumentParser('copy id to fam')
parser.add_argument('-f','--famfile',required=True)
args=vars(parser.parse_args())

famfile=args['famfile']

ID_COL=0
FAM_COL=1


def main(famfile):

    with open(famfile,'r') as famfile_open:
        famlines=famfile_open.readlines()


    newfam=list()
    for famline in famlines:
        famsplit=famline.split('\t')
        famid=famsplit[ID_COL]
        famsplit[FAM_COL]=famid
        newfam.append(famsplit)

        
    origfile,origext=os.path.splitext(os.path.basename(famfile))
    newfile=origfile+'_new'+origext
    newfile_open=open(newfile,'w')

    for fam in newfam:
        newline='\t'.join(fam)
        newfile_open.write(newline)
    
    newfile_open.close()


if __name__=='__main__':
    main(famfile)
