#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('sort a file')
parser.add_argument('-f','--phenofile',required=True)
args=vars(parser.parse_args())

phenofile=args['phenofile']


def main(phenofile):
    with open(phenofile,'r') as phenofile_open:
        phenolines=phenofile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(phenofile))
    samplefile=origfile+'.sample'
    
    samplefile_open=open(samplefile,'w')

    for phenoline in phenolines:
        phenosplit=phenoline.split('\t')
        sample=phenosplit[0]
        #phenotype=phenosplit[1]
        sampleline=sample+'\t'+'NED'+'\t'+'EUR'+'\t'+'ILLUMINA'+'\n'
        samplefile_open.write(sampleline)

    samplefile_open.close()


if __name__=='__main__':
    main(phenofile)
