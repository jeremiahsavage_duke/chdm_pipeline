#!/usr/bin/env python3
import argparse
import natsort
import os
import glob
import subprocess
import sys


parser=argparse.ArgumentParser('for SKAT a pre-SetID file')
parser.add_argument('-o','--dir1',required=True)
parser.add_argument('-t','--dir2',required=True)
args=vars(parser.parse_args())

dir1=args['dir1']
dir2=args['dir2']


def get_vcf2(list2,vcf1):
    print('vcf1=%s' % vcf1)
    for vcf2 in list2:
        if os.path.basename(vcf1)==os.path.basename(vcf2):
            return vcf2
    sys.exit('No pair to vcf1: %s' % vcf1)

def main(dir1,dir2):
    list1=glob.glob(os.path.join(dir1,'*.vcf'))
    list2=glob.glob(os.path.join(dir2,'*.vcf'))
    list1=natsort.natsorted(list1)
    list2=natsort.natsorted(list2)
    outfile_open=open('cat_gatk.sh','w')
    for vcf1 in list1:
        vcf2=get_vcf2(list2,vcf1)
        outfile=os.path.basename(vcf1)
        print('outfile=%s' % outfile)
        
        gatk_command='echo \"java -cp /home/jeremiah/tools/GenomeAnalysisTK.jar org.broadinstitute.sting.tools.CatVariants -R ~/grc37/human_g1k_v37.fasta -V '+vcf1+' -V '+vcf2+' -out '+outfile+'\"\n'
        outfile_open.write(gatk_command)
        #subprocess.call(gatk_command,shell=True)

    outfile_open.close()

if __name__=='__main__':
    main(dir1, dir2)
