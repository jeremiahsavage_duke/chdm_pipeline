#!/usr/bin/env python3
import argparse
import os
import xlrd

parser=argparse.ArgumentParser('convert xls to tsv')
parser.add_argument('-x','--xlsfile',required=True)
parser.add_argument('-n','--sheetnum',required=True)
args=vars(parser.parse_args())

xlsfile=args['xlsfile']
sheetnum=args['sheetnum']

def main(xlsfile,sheetnum):

    origfile,origext=os.path.splitext(os.path.basename(xlsfile))
    newfile=origfile+'.tsv'
    outfile_open=open(newfile,'w')

    workbook=xlrd.open_workbook(xlsfile)
    worksheet=workbook.sheet_by_name(workbook.sheet_names()[int(sheetnum)])

    num_rows=worksheet.nrows-1
    num_cells=worksheet.ncols-1
    curr_row=-1

    while curr_row < num_rows:
        curr_row+=1
        row=worksheet.row(curr_row)
        
        curr_cell=-1
        row_list=list()
        while curr_cell<num_cells:
            curr_cell+=1
            row_list.append(worksheet.cell_value(curr_row,curr_cell))
                            
        newline='\t'.join(row_list)
        outfile_open.write(newline+'\n')

    outfile_open.close()
    
if __name__=='__main__':
    main(xlsfile,sheetnum)
