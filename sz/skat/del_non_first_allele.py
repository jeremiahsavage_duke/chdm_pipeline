#!/usr/bin/env python3
import argparse
import os


parser=argparse.ArgumentParser('plink seq is not multi-allelic')
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

VCF_CHR_COL=0
VCF_POS_COL=1

def main(vcffile):

    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()


    newvcflines=str()
    prev_vcf_chr='0'
    prev_vcf_pos='0'
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            newvcflines+=vcfline
        else:
            vcfsplit=vcfline.strip('\n').split('\t')
            vcf_chr=vcfsplit[VCF_CHR_COL]
            vcf_pos=vcfsplit[VCF_POS_COL]
            if (vcf_chr==prev_vcf_chr) and (vcf_pos==prev_vcf_pos):
                continue
            else:
                prev_vcf_chr=vcf_chr
                prev_vcf_pos=vcf_pos
                newvcflines+=vcfline

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    newfile=origfile+'_DP'+origext
    newfile_open=open(newfile,'w')

    for newline in newvcflines:
        newfile_open.write(newline)

    newfile_open.close()


if __name__=='__main__':
    main(vcffile)
