#!/usr/bin/env python3
import argparse
import os
import re


parser=argparse.ArgumentParser('plink needs DP>10. So, artificial increase of some')
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

VCFREFCOL=3

def main(vcffile):

    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()


    genotype_re=re.compile('(\d+\/\d+):(\.):(\d+):(\d+):(\d+)')
    newvcflines=str()
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            newvcflines+=vcfline
        else:
            vcfsplit=vcfline.strip('\n').split('\t')
            for i,vcfcol in enumerate(vcfsplit):
                #print('i=%s', (i))
                #print('vcfcol=%s' % (vcfcol))
                if (i > 9):
                    genotype_match=genotype_re.match(vcfcol)
                    #print('genotype_match=%s' % genotype_match)
                    DP=int(genotype_match.groups()[2])
                    RR=int(genotype_match.groups()[3])
                    VR=int(genotype_match.groups()[4])
                    if DP<=50:
                        newDP=DP*10
                        newRR=RR*10
                        newVR=VR*10
                        GT=genotype_match.groups()[0]
                        GQ=genotype_match.groups()[1]
                        new_genotype=GT+':'+GQ+':'+str(newDP)+':'+str(newRR)+':'+str(newVR)
                        vcfsplit[i]=new_genotype
            newline='\t'.join(vcfsplit)
            newvcflines+=newline+'\n'

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    newfile=origfile+'_DP'+origext
    newfile_open=open(newfile,'w')

    for newline in newvcflines:
        newfile_open.write(newline)

    newfile_open.close()


if __name__=='__main__':
    main(vcffile)
