#sort
./sort_ped.py -f case.pheno
./sort_ped.py -f control.pheno
#pheno2ped
./pheno_2_ped.py -f case_sorted.pheno
./pheno_2_ped.py -f control_sorted.pheno
#join
cat case.ped control.ped > sz.ped
#map file
./vcf_2_map.py -v ~/sz.vcf
#plink
plink --ped sz.ped --map sz.map --no-fid --no-parents --no-sex --allow-extra-chr
#####restart: vcf_to_ped_convert.pl
cat case_sorted.pheno control_sorted.pheno > sz_sorted.pheno

'''
perl vcf_to_ped_converter.pl -vcf ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20110521/ALL.chr13.phase1_integrated_calls.20101123.snps_indels_svs.genotypes.vcf.gz \
-sample_panel_file ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20110521/phase1_integrated_calls.20101123.ALL.sample_panel \
 -region 13:32889611-32973805 -population GBR -population FIN
'''

./vcf_to_ped_convert.pl -vcf ~/sz_noh.vcf -sample_panel_file sz_sorted.sample -population NED -region 1


###3rd approach
pseq sz new-project --resources hg19_plink/

pseq sz load-vcf --vcf sz.vcf

cat phe.header case_sorted.pheno control_sorted.pheno > sz.phe

pseq sz load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe

pseq sz write-ped --name test
plink -tfile test
=======
pseq sz load-vcf --vcf ~/tools/sz.vcf

cat phe.header case_sorted.pheno control_sorted.pheno > sz.phe


pseq sz new-project --resources hg19_plink/
pseq sz load-vcf --vcf sz.vcf

pseq sz load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq sz write-ped --name scz_pseq
#manually add "2" and "1"
plink --tped scz_pseq.tped --tfam scz_pseq.tfam
###R
library(SKAT)
File.Bed <- "plink.bed"
File.Bim <- "plink.bim"
File.Fam <- "plink.fam"
File.SetID <- "plink.setid"
File.SSD <- "plink.ssd"
File.Info <- "plink.ssd.info"
Generate_SSD_SetID(File.Bed,File.Bim,File.Fam,File.SetID,File.SSD,File.Info)

FAM <- Read_Plink_FAM(File.Fam,Is.binary = TRUE)
y <- FAM$Phenotype
SSD.INFO <- Open_SSD(File.SSD,File.Info)
obj <- SKAT_Null_Model(y ~ 1,out_type = "D")
out <- SKAT.SSD.All(SSD.INFO,obj)
write.csv(out$results,file="skat_pval.csv")





###2014-06-23
for f in *.tsv ; do echo /home/jeremiah/chdm_pipeline/sz/skat/ann2vcf.py -f ${f} ; done | parallel -j+0
mv *.vcf test/
cd test/
sh ./do.sh

~/chdm_pipeline/sz/skat/build_vcf_list_gatk.py  -f ./

cat front.gatk vcf_sorted.gatk > do_gatk.sh
#for f in ../*.vcf ; do echo "/home/jeremiah/bin/vcftools/vcf-sort ${f} > $(basename ${f})" ; done | parallel
sh do_gatk.sh



###2014-06-24
pseq scz_un new-project --resources hg19_plink/
pseq scz_un load-vcf --vcf ultranull.vcf
pseq scz_un load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq scz_un write-ped --name scz_ultranull
plink --tped scz_ultranull.tped --tfam scz_ultranull.tfam


write.csv(out$results,file="skat_un_pval.csv")



##2014-06-26 retrospective


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK.jar -T CombineVariants -R ~/grc37/human_g1k_v37.fasta --variant null.vcf --variant ultrarare.vcf --out ultranull.vcf



##2014-06-27
pseq rpg new-project --resources ../hg19_plink/
pseq rpg load-vcf --vcf ultranull_RPGRIP1L_exonic.vcf
pseq rpg load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq rpg write-ped --name ultranull_rpgrip1l

~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull_rpgrip1l.tped
mv ultranull_rpgrip1l_new.tped ultranull_rpgrip1l.tped
~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f ultranull_rpgrip1l.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv ultranull_rpgrip1l_new.tfam ultranull_rpgrip1l.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f ultranull_rpgrip1l.tfam
mv ultranull_rpgrip1l_new.tfam ultranull_rpgrip1l.tfam
plink --tped ultranull_rpgrip1l.tped --tfam ultranull_rpgrip1l.tfam

############
##2014-06-30
############
~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v ultranull.vcf
mkdir fake_dp
cp ultranull_DP.vcf fake_dp/
cd fake_dp


pseq dp new-project --resources ../hg19_plink/
pseq dp load-vcf --vcf ultranull_DP.vcf
pseq dp load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq dp write-ped --name ultranull_dp

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f ultranull_dp.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv ultranull_dp_new.tfam ultranull_dp.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f ultranull_dp.tfam
mv ultranull_dp_new.tfam ultranull_dp.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull_dp.tped
mv ultranull_dp_new.tped ultranull_dp.tped

plink --tped ultranull_dp.tped --tfam ultranull_dp.tfam

cp ../plink.setid ./

#R#
library(SKAT)
File.Bed <- "plink.bed"
File.Bim <- "plink.bim"
File.Fam <- "plink.fam"
File.SetID <- "plink.setid"
File.SSD <- "plink.ssd"
File.Info <- "plink.ssd.info"
Generate_SSD_SetID(File.Bed,File.Bim,File.Fam,File.SetID,File.SSD,File.Info)

FAM <- Read_Plink_FAM(File.Fam,Is.binary = TRUE)
y <- FAM$Phenotype
SSD.INFO <- Open_SSD(File.SSD,File.Info)
obj <- SKAT_Null_Model(y ~ 1,out_type = "D")
out <- SKAT.SSD.All(SSD.INFO,obj)
write.csv(out$results,file="skat_pval.csv")


###2014-07-07
###make bed file
tail -n +2 CiliaryResequencingGeneList-SZ_April\ 2013-Final.tsv | awk '{ print ($3,$5,$6,$1); }'  > CiliaryResequencingGeneList-SZ_April_2013-Final.bed
sed 's/^chr//g' CiliaryResequencingGeneList-SZ_April_2013-Final.bed | sort -n > ciliary.bed
{ tail -n +24 ciliary.bed &&head -n 23 ciliary.bed; } > cil.bed


~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull_dp.tped
mv ultranull_dp_new.tped ultranull_dp.tped

~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t ultranull_dp.tped -b ../cil.bed

ls -lh scz_ultranull.setid
mv ultranull_dp.setid plink.setid

plink --tped ultranull_dp.tped --tfam ultranull_dp.tfam


####all
mkdir all
cp -a sz.vcf all/
cd all/
~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v sz.vcf

pseq all new-project --resources ../hg19_plink/
pseq all load-vcf --vcf sz_DP.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name all

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f all.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f all.tfam
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t all.tped
mv all_new.tped all.tped

~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t all.tped -b ../cil.bed
mv all.setid plink.setid
plink --tped all.tped --tfam all.tfam



###2014-07-08
mkdir ind_var
cp sz.vcf -a ind_var/
cd ind_var/
~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v sz.vcf
pseq all new-project --resources ../hg19_plink/
pseq all load-vcf --vcf sz_DP.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name all

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f all.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f all.tfam
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t all.tped
mv all_new.tped all.tped

~/chdm_pipeline/sz/skat/tped_2_setid.py -t all.tped
plink --tped all.tped --tfam all.tfam



####
pseq all v-assoc --phenotype scz  > vassoc.txt
(head -n 1 vassoc.txt && tail -n +2 vassoc.txt | sort -s -n -k 20,20 -t$'\t' ) > vassoc_sort.txt

pseq all assoc --phenotype scz --tests BURDEN  > assoc_burden.txt


####2014-07-08
####ALL###
sed '/^Y/d' sz_DP.vcf > no_Y.vcf
ruby ~/tools/atlas2/atlas2-code/utils/split_multiallelic_rows.rb sz_DP.vcf > test.vcf
sed '/^Y/d' test.vcf > no_Y.vcf

mkdir t5
cp -a no_Y.vcf t5/
cd t5/
sed 's/0\/.:/0\/0:/g' no_Y.vcf > no_Y_no_dot.vcf

pseq all new-project --resources ../../hg19_plink/
pseq all load-vcf --vcf no_Y_no_dot.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name ultranull



###ULTRANULL###
ruby ~/tools/atlas2/atlas2-code/utils/split_multiallelic_rows.rb ultranull_DP.vcf > ultranull_allele_split.vcf
mkdir 20140711_ultranull
mv ultranull_allele_split.vcf 20140711_ultranull/
cd 20140711_ultranull/
sed 's/0\/.:/0\/0:/g' ultranull_allele_split.vcf > test.vcf

pseq un new-project --resources ../hg19_plink/
pseq un load-vcf --vcf test_B.vcf
pseq un load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq un write-ped --name ultranull

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f ultranull.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f ultranull.tfam
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull.tped
mv ultranull_new.tped ultranull.tped


###
#2014-07-14
###

cd /home/jeremiah/tools/ind_var
~/chdm_pipeline/sz/skat/vcf_zero_dot_fake_DP.py -v no_Y.vcf
mkdir t6
cp -a no_Y_zdDP.vcf t6/
cd t6/

pseq all new-project --resources ../../hg19_plink/
pseq all load-vcf --vcf no_Y_zdDP.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name ultranull
#### need to re-run ~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py as ID col overwrite
cd /home/jeremiah/tools/ind_var
~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v sz.vcf
sed '/^Y/d' sz_DP.vcf > no_Y.vcf
ruby ~/tools/atlas2/atlas2-code/utils/split_multiallelic_rows.rb  > multi_allele.vcf
~/chdm_pipeline/sz/skat/vcf_zero_dot_fake_DP.py -v multi_allele.vcf
~/chdm_pipeline/sz/skat/fake_fix_low_DP.py -v multi_allele_zdDP.vcf
~/chdm_pipeline/sz/skat/del_non_first_allele.py -v multi_allele_zdDP_DP.vcf
sed '/^Y/d' multi_allele_zdDP_DP_DP.vcf > no_Y.vcf


mkdir t7
cp -a multi_allele_zdDP_DP.vcf t7/
cd t7

pseq all new-project --resources ../../hg19_plink/
pseq all load-vcf --vcf no_Y.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name ultranull

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f ultranull.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f ultranull.tfam
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull.tped
mv ultranull_new.tped ultranull.tped

~/chdm_pipeline/sz/skat/tped_2_setid.py -t ultranull.tped
plink --tped ultranull.tped --tfam ultranull.tfam
cp -a ultranull.setid plink.setid
plink -bfile plink --freq
#>skat it up

###gene based
mkdir t8
cp -a ultranull.t* t8/
cd t8/
~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t ultranull.tped -b ~/tools/cil.bed
cp -a ultranull.setid plink.setid
plink --tped ultranull.tped --tfam ultranull.tfam



######2014-07-15
~/chdm_pipeline/sz/skat/filter_tped_w_list.py -t ultranull.tped -l ultranull_missing_setid.list
mkdir t9
cp -a ultranull_nomissing.tped t9/
cp -a ultranull.tfam plink.setid t9/
cd t9/
plink --tped ultranull_nomissing.tped --tfam ultranull.tfam


####
#2014-07-16
####

head -n 7783 ultranull_nomissing.tped > chr1.tped
mkdir chr
cp -a chr1.tped chr/
cp -a ultranull.tfam chr/
cd chr/
plink --tped chr1.tped --tfam ultranull.tfam
cp -a ../*.setid ./

mkdir bis1
head -n 3367 chr1.tped > chr1_3367.tped
mv chr1_3367.tped bis1/
cp -a *.tfam *.setid bis1/
plink --tped chr1_3367.tped --tfam ultranull.tfam
#one warning

mkdir bik2
head -n 1605 chr1_3367.tped > chr1_1605.tped
mv chr1_1605.tped bik2/
cp -a *.tfam *.setid bik2/
cd bik2/
plink --tped chr1_1605.tped --tfam ultranull.tfam
#no warning

cd ../
mkdir bik3
tail -n +1606 chr1_3367.tped > chr1_l1606.tped
mv chr1_l1606.tped bik3/
cp -a *.tfam *.setid bik3/
cd bik3/
plink --tped chr1_l1606.tped --tfam ultranull.tfam
#one warning


mkdir bik4
head -n 812 chr1_l1606.tped > chr1_812.tped
mv chr1_812.tped bik4/
cp -a *.tfam *.setid bik4/
cd bik4/
plink --tped chr1_812.tped --tfam ultranull.tfam
#one warning

mkdir bik5
head -n 663 chr1_812.tped > chr1_663.tped
mv chr1_663.tped bik5/
cp -a *.tfam *.setid bik5/
cd bik5/
plink --tped chr1_663.tped --tfam ultranull.tfam
#one warning

mkdir bik6
head -n 361 chr1_663.tped > chr1_361.tped
mv chr1_361.tped bik6/
cp -a *.tfam *.setid bik6/
cd bik6/
plink --tped chr1_361.tped --tfam ultranull.tfam
#one warning

mkdir bik7
head -n 204 chr1_361.tped > chr1_204.tped
mv chr1_204.tped bik7/
cp -a *.tfam *.setid bik7/
cd bik7/
plink --tped chr1_204.tped --tfam ultranull.tfam
#one warning

mkdir bik8/
head -n 103 chr1_204.tped > chr1_103.tped
mv chr1_103.tped bik8/
cp -a *.tfam *.setid bik8/
cd bik8/
plink --tped chr1_103.tped --tfam ultranull.tfam
#one warning

mkdir bik9/
head -n 40 chr1_103.tped > chr1_40.tped
mv chr1_40.tped bik9/
cp -a *.tfam *.setid bik9/
cd bik9/
plink --tped chr1_40.tped --tfam ultranull.tfam
#one warning



##2014-07-17
cd /home/jeremiah/tools/ind_var/t7/
mkdir t9/
~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t ultranull.tped -b ~/tools/cil_sort.bed
cp -a ultranull.setid ultranull.t* t9/
cd t9/
mv ultranull.setid plink.setid
plink --tped ultranull.tped --tfam ultranull.tfam

cd /home/jeremiah/tools/ind_var/t7/t8/t9/chr/bis1/bik3/bik4/
head -n 360 chr1_812.tped > chr1_360.tped
mkdir bik5/
mv chr1_360.tped bik5/
cp -a plink.setid *.tfam bik5/
cd bik5/
plink --tped chr1_360.tped --tfam ultranull.tfam
#one warning

head -n 203 chr1_360.tped > chr1_203.tped
mkdir bik6/
mv chr1_203.tped bik6/
cp -a plink.setid *.tfam bik6/
cd bik6/
plink --tped chr1_203.tped --tfam ultranull.tfam
#one warning

head -n 39 chr1_203.tped > chr1_39.tped
mkdir bik7/
mv chr1_39.tped bik7/
cp -a plink.setid *.tfam bik7/
cd bik7/
plink --tped chr1_39.tped --tfam ultranull.tfam
#no warning

cd ../
tail -n +40 chr1_203.tped > chr1_l40.tped
mkdir bik8/
mv chr1_l40.tped bik8/
cp -a plink.setid *.tfam bik8/
cd bik8/
plink --tped chr1_l40.tped --tfam ultranull.tfam
#one warning - one gene

head -n 82 chr1_l40.tped > chr1_82.tped
mkdir bik9/
mv chr1_82.tped bik9/
cp -a plink.setid *.tfam bik9/
cd bik9/
plink --tped chr1_82.tped --tfam ultranull.tfam
#one warning

head -n 41 chr1_82.tped > chr1_41.tped
mkdir bik10/
mv chr1_41.tped bik10/
cp -a plink.setid *.tfam bik10/
cd bik10/
plink --tped chr1_41.tped --tfam ultranull.tfam
#no warning

cd ../
tail -n +42 chr1_82.tped > chr1_l42.tped
mkdir bik11/
mv chr1_l42.tped bik11/
cp -a plink.setid *.tfam bik11/
cd bik11/
plink --tped chr1_l42.tped --tfam ultranull.tfam
#one missing

head -n 20 chr1_l42.tped > chr1_20.tped
mkdir bik12/
mv chr1_20.tped bik12/
cp -a plink.setid *.tfam bik12/
cd bik12/
plink --tped chr1_20.tped --tfam ultranull.tfam
#one missing

head -n 10 chr1_20.tped > chr1_10.tped
mkdir bik13/
mv chr1_10.tped bik13/
cp -a plink.setid *.tfam bik13/
cd bik13/
plink --tped chr1_10.tped --tfam ultranull.tfam
#one missing

head -n 5 chr1_10.tped > chr1_5.tped
mkdir bik14/
mv chr1_5.tped bik14/
cp -a plink.setid *.tfam bik14/
cd bik14/
plink --tped chr1_5.tped --tfam ultranull.tfam
#no warning

cd ../
tail -n 5 chr1_10.tped > chr1_l5.tped
mkdir bik15/
mv chr1_l5.tped bik15/
cp -a plink.setid *.tfam bik15/
cd bik15/
plink --tped chr1_l5.tped --tfam ultranull.tfam
#one warning

head -n 2 chr1_l5.tped > chr1_2.tped
mkdir bik16/
mv chr1_2.tped bik16/
cp -a plink.setid *.tfam bik16/
cd bik16/
plink --tped chr1_2.tped --tfam ultranull.tfam


tail -n 1 chr1_2.tped > chr1_l1.tped
mkdir bik17/
mv chr1_l1.tped bik17/
cp -a plink.setid *.tfam bik17/
cd bik17/
plink --tped chr1_l1.tped --tfam ultranull.tfam



###
#2014-07-18
###
#monogenic removal
cd t7/
mkdir h1/
cp -a multi_allele_zdDP_DP_DP.vcf h1/
cd h1/
sed '/^Y/d' multi_allele_zdDP_DP_DP.vcf > no_Y.vcf
~/chdm_pipeline/sz/skat/seg_monogenic_vcf.py -v no_Y.vcf

pseq all new-project --resources ../../hg19_plink/
pseq all load-vcf --vcf no_Y_filtered.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name all_het


~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f all_het.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv all_het_new.tfam all_het.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f all_het.tfam
mv all_het_new.tfam all_het.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t all_het.tped
mv all_het_new.tped all_het.tped


~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t all_het.tped -b /home/jeremiah/tools/cil_sort.bed
mv all_het.setid plink.setid
plink --tped all_het.tped --tfam all_het.tfam



##futher bisection for 3 warnings
#1: 6 SNPs with either high missing rates or no-variation are excluded! 
#2: 14 SNPs with either high missing rates or no-variation are excluded! 
#3: 2 SNPs with either high missing rates or no-variation are excluded! 

$ wc -l *.tped
97315 all_het.tped

head -n 45000 all_het.tped > all_het_45k.tped
mkdir bis1/
mv all_het_45k.tped bis1/
cp -a plink.setid *.tfam bis1/
cd bis1/
plink --tped all_het_45k.tped --tfam all_het.tfam
#two warnings

mkdir bis2/
head -n 22000 all_het_45k.tped > all_het_22k.tped
mv all_het_22k.tped bis2/
cp -a plink.setid *.tfam bis2/
cd bis2/
plink --tped all_het_22k.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis3/
head -n 11000 all_het_22k.tped > all_het_11k.tped
mv all_het_11k.tped bis3/
cp -a plink.setid *.tfam bis3/
cd bis3/
plink --tped all_het_11k.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis4/
head -n 5500 all_het_11k.tped > all_het_5k.tped
mv all_het_5k.tped bis4/
cp -a plink.setid *.tfam bis4/
cd bis4/
plink --tped all_het_5k.tped --tfam all_het.tfam
#no warnings

cd ../
mkdir bis5/
tail -n +5501 all_het_11k.tped > all_het_l5k.tped
mv all_het_l5k.tped bis5/
cp -a plink.setid *.tfam bis5/
cd bis5/
plink --tped all_het_l5k.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis6/
head -n 2000 all_het_l5k.tped > all_het_2k.tped
mv all_het_2k.tped bis6/
cp -a plink.setid *.tfam bis6/
cd bis6/
plink --tped all_het_2k.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis7/
head -n 1000 all_het_2k.tped > all_het_1k.tped
mv all_het_1k.tped bis7/
cp -a plink.setid *.tfam bis7/
cd bis7/
plink --tped all_het_1k.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis8/
head -n 500 all_het_1k.tped > all_het_500.tped
mv all_het_500.tped bis8/
cp -a plink.setid *.tfam bis8/
cd bis8/
plink --tped all_het_500.tped --tfam all_het.tfam
#no warning

cd ../
mkdir bis9/
tail -n +501 all_het_1k.tped > all_het_l500.tped
mv all_het_l500.tped bis9/
cp -a plink.setid *.tfam bis9/
cd bis9/
plink --tped all_het_l500.tped --tfam all_het.tfam
#1 warning; 6 snps

mkdir bis10/
head -n 250 all_het_l500.tped > all_het_250.tped
mv all_het_250.tped bis10/
cp -a plink.setid *.tfam bis10/
cd bis10/
plink --tped all_het_250.tped --tfam all_het.tfam
#1 warning; 5 snps

cd ../
mkdir bis11/
head -n 251 all_het_l500.tped > all_het_251.tped
mv all_het_251.tped bis11/
cp -a plink.setid *.tfam bis11/
cd bis11/
plink --tped all_het_251.tped --tfam all_het.tfam


cd ../
mkdir bis12/
head -n 350 all_het_l500.tped > all_het_350.tped
mv all_het_350.tped bis12/
cp -a plink.setid *.tfam bis12/
cd bis12/
plink --tped all_het_350.tped --tfam all_het.tfam
#1 warning; 6 snps


cd ../
mkdir bis13/
head -n 300 all_het_l500.tped > all_het_300.tped
mv all_het_300.tped bis13/
cp -a plink.setid *.tfam bis13/
cd bis13/
plink --tped all_het_300.tped --tfam all_het.tfam
#1 warning; 6 snps


cd ../
mkdir bis14/
head -n 275 all_het_l500.tped > all_het_275.tped
mv all_het_275.tped bis14/
cp -a plink.setid *.tfam bis14/
cd bis14/
plink --tped all_het_275.tped --tfam all_het.tfam
#1 warning; 5 snps


cd ../
mkdir bis15/
head -n 290 all_het_l500.tped > all_het_290.tped
mv all_het_290.tped bis15/
cp -a plink.setid *.tfam bis15/
cd bis15/
plink --tped all_het_290.tped --tfam all_het.tfam
#1 warning; 6 snps

cd ../
mkdir bis16/
head -n 282 all_het_l500.tped > all_het_282.tped
mv all_het_282.tped bis16/
cp -a plink.setid *.tfam bis16/
cd bis16/
plink --tped all_het_282.tped --tfam all_het.tfam
#1 warning; 5 snps


cd ../
mkdir bis17/
head -n 286 all_het_l500.tped > all_het_286.tped
mv all_het_286.tped bis17/
cp -a plink.setid *.tfam bis17/
cd bis17/
plink --tped all_het_286.tped --tfam all_het.tfam
#1 warning; 5 snps

cd ../
mkdir bis18/
head -n 288 all_het_l500.tped > all_het_288.tped
mv all_het_288.tped bis18/
cp -a plink.setid *.tfam bis18/
cd bis18/
plink --tped all_het_288.tped --tfam all_het.tfam
#1 warning; 6 snps

cd ../
mkdir bis19/
head -n 287 all_het_l500.tped > all_het_287.tped
mv all_het_287.tped bis19/
cp -a plink.setid *.tfam bis19/
cd bis19/
plink --tped all_het_287.tped --tfam all_het.tfam
#1 warning; 6 snps
