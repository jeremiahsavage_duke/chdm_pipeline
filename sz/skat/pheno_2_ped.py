#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('sort a file')
parser.add_argument('-f','--phenofile',required=True)
args=vars(parser.parse_args())

phenofile=args['phenofile']


def main(phenofile):
    with open(phenofile,'r') as phenofile_open:
        phenolines=phenofile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(phenofile))
    basefile=origfile.split('_')[0]
    pedfile=basefile+'.ped'
    
    pedfile_open=open(pedfile,'w')

    for samplecount,phenoline in enumerate(phenolines):
        phenosplit=phenoline.split('\t')
        sample=phenosplit[0]
        pheno=phenosplit[1]
        family=sample
        paternalID="0"
        maternalID="0"
        sex="0"
        #pedline='0'+' '+sample+' '+paternalID+' '+maternalID+' '+sex+'  '+pheno
        pedline=sample+' '+'0'+' '+paternalID+' '+maternalID+' '+sex+'  '+pheno
        #pedline=str(samplecount+1)+' '+'0'+' '+paternalID+' '+maternalID+' '+sex+'  '+pheno
        pedfile_open.write(pedline)

    pedfile_open.close()


if __name__=='__main__':
    main(phenofile)
