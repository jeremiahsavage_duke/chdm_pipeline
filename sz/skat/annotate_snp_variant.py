#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys


parser=argparse.ArgumentParser('annotate gene_diff with gene name')
parser.add_argument('-v','--vcffile',required=True)
parser.add_argument('-s','--skatdata',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']
skatdata=args['skatdata']


def get_genename(this_chr,this_pos,vcf_list):
    #print('vcf_list[0]=%s' % vcf_list[0])
    for vcfitem in vcf_list:
        #print('vcfitem=%s' % vcfitem)
        vcf_chr=vcfitem[0]
        vcf_start=vcfitem[1]
        vcf_end=vcfitem[2]
        vcf_gene=vcfitem[3]
        if this_chr==vcf_chr:
            if int(this_pos) >= int(vcf_start) and int(this_pos) <= int(vcf_end):
                return vcf_gene
        return 'None'


def main(vcffile,skatdata):
    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()

    with open(skatdata,'r') as skatdata_open:
        skatlines=skatdata_open.readlines()

    origfile,origext=os.path.splitext(skatdata)
    outfile=origfile+'_annot.tsv'
    outfile_open=open(outfile,'w')

    vcf_list=list()
    for vcfline in vcflines:
        vcfsplit=vcfline.strip('\n').split('\t')
        #print('vcfsplit=%s' % vcfsplit)
        #print('vcfsplit=%s' % vcfsplit)
        chromosome=vcfsplit[0]
        startpos=vcfsplit[1]
        refallele=vcfsplit[3]
        altallele=vcfsplit[4]
        vcf_list.append(vcfsplit)
    #print('vcf_list=%s' % vcf_list)

    for skatline in skatlines:
        skatsplit=skatline.split('\t')
        #print('skatsplit=%s' % skatsplit)
        if skatsplit[2].strip('"')=='SetID':
            skatsplit.insert(2,'ref_allele')
            skatsplit.insert(3,'alt_allele')
            newline='\t'.join(skatsplit)
            outfile_open.write(newline)
        else:
            colnum=skatsplit[0].strip('"')
            genename=skatsplit[1]
            setid=skatsplit[2].strip('"')
            pvalue=skatsplit[2]
            nmarkerall=skatsplit[3]
            nmarkertest=skatsplit[4]
            #print('setid=%s' % setid)
            this_chr=setid.split('_')[1].strip('chr')
            this_pos=setid.split('_')[2]

            for vcfitem in vcf_list:
                #print('vcfitem=%s' % vcfitem)
                vcf_chr=vcfitem[0]
                vcf_pos=vcfitem[1]
                vcf_ref=vcfitem[3]
                vcf_alt=vcfitem[4]
                if this_chr==vcf_chr:
                    if this_pos==vcf_pos:
                        this_ref=vcf_ref
                        this_alt=vcf_alt
                        break

            #genename=get_genename(this_chr,this_pos,vcf_list)
            skatsplit.insert(2,this_ref)
            skatsplit.insert(3,this_alt)
            newline='\t'.join(skatsplit)
            outfile_open.write(newline)
            
    outfile_open.close()


if __name__=='__main__':
    main(vcffile,skatdata)
