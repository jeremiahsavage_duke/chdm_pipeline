#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('correct fam file with phenotype')
parser.add_argument('-f','--famfile',required=True)
parser.add_argument('-p','--phefile',required=True)
args=vars(parser.parse_args())

famfile=args['famfile']
phefile=args['phefile']

def main(famfile,phefile):

    with open(phefile,'r') as phefile_open:
        phelines=phefile_open.readlines()

    with open(famfile,'r') as famfile_open:
        famlines=famfile_open.readlines()
        
    phedict=dict()

    for pheline in phelines:
        if pheline.startswith('#'):
            continue
        else:
            phesplit=pheline.strip('\n').split('\t')
            sample_id=phesplit[0]
            sample_pheno=phesplit[1]
            phedict[sample_id]=sample_pheno

    newfam=list()
    for famline in famlines:
        famsplit=famline.strip('\n').split('\t')
        sample_id=famsplit[0]
        sample_pheno=phedict[sample_id]
        famsplit[5]=sample_pheno
        newfam.append(famsplit)

    origfile,origext=os.path.splitext(os.path.basename(famfile))
    newfile=origfile+'_new'+origext
    newfile_open=open(newfile,'w')

    for fam in newfam:
        newline='\t'.join(fam)+'\n'
        newfile_open.write(newline)

    newfile_open.close()


if __name__=='__main__':
    main(famfile,phefile)
