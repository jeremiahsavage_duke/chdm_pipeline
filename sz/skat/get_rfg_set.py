#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('just finding the RFG set')
parser.add_argument('-f','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

def get_datavalue(vcfinfosplit,akey):
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        datakey=datasplit[0]
        if (datakey==akey):
            datavalue=datasplit[1].split('(')[0]
            return datavalue
    

def get_gene_value(vcfinfo):
    rfgkey='RFG'
    
    vcfinfosplit=vcfinfo.split(';')

    rfgvalue=get_datavalue(vcfinfosplit,rfgkey)
    return rfgvalue


def main(vcffile):
    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    rfgfile=origfile+'_rfgset.tsv'
    rfgfile_open=open(rfgfile,'w')

    rfgset=set()
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            continue
        else:
            vcfsplit=vcfline.split('\t')
            vcfinfo=vcfsplit[7].strip()
            rfgvalue=get_gene_value(vcfinfo)
            rfgset.add(rfgvalue)

    for rfg in rfgset:
        rfgfile_open.write(rfg+'\n')
    rfgfile_open.close()


if __name__=='__main__':
    main(vcffile)
