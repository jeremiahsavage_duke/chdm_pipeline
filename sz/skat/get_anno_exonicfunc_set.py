#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('just finding the ExonicFunc set')
parser.add_argument('-a','--annofile',required=True)
args=vars(parser.parse_args())

annofile=args['annofile']
FUNC_COL=0
EXONICFUNC_COL=2
    


def main(annofile):
    with open(annofile,'r') as annofile_open:
        annolines=annofile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(annofile))

    effile=origfile+'_exonicfunc.tsv'
    effile_open=open(effile,'w')

    exonicfunc_set=set()
    for annoline in annolines:
        if annoline.startswith('Func'):
            continue
        else:
            annosplit=annoline.split(',')
            if annosplit[FUNC_COL].strip('"') == 'exonic':
                this_exonicfunc=annosplit[EXONICFUNC_COL].strip('"')
                exonicfunc_set.add(this_exonicfunc)

    for exonicfunc in exonicfunc_set:
        effile_open.write(exonicfunc+'\n')
    effile_open.close()


if __name__=='__main__':
    main(annofile)
