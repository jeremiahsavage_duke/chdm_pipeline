#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('read the code below')
parser.add_argument('-a','--annovarexomesummary',required=True)
args=vars(parser.parse_args())

annovarfile=args['annovarexomesummary']
EXONICFUNC_NONSYN_SET=set(['nonsynonymous SNV', 'unknown', 'stoploss SNV', 'stopgain SNV'])


def main(annovarfile):
    with open(annovarfile,'r') as annovarfile_open:
        annolines=annovarfile_open.readlines()


    origfile,origext=os.path.splitext(os.path.basename(annovarfile))
    retainfile=origfile+'_retain'+origext
    retainfile_open=open(retainfile,'w')

    for i,annoline in enumerate(annolines):
        if (i==0):
            retainfile_open.write(annoline)
        else:
            annosplit=annoline.strip().split(',')
            func=annosplit[0].strip('"')
            exonicfunc=annosplit[2].strip('"')
            if (func=='splicing') or (exonicfunc in EXONICFUNC_NONSYN_SET):
                retainfile_open.write(annoline)
            
    retainfile_open.close()


if __name__=='__main__':
    main(annovarfile)
