#!/usr/bin/env python3
import argparse
import csv
import os
import sys
import re


parser=argparse.ArgumentParser('for SKAT a pre-SetID file')
parser.add_argument('-f','--annfile',required=True)
args=vars(parser.parse_args())

annfile=args['annfile']

HEADER='#CHROM'+'\t'+'POS'+'\t'+'ID'+'\t'+'REF'+'\t'+'ALT'+'\t'+'QUAL'+'\t'+'FILTER'+'\t'+'INFO'+'\t'+'FORMAT'
CHROM_COL=21
POS_COL=22
ID_COL=8
REF_COL=24
ALT_COL=25
QUAL_COL=31
FILTER_COL=32
INFO_COL=33
FORMAT_COL=34
SAMPLE_COL=35

def get_samplename(annfile):
    samplename=annfile.split('_')[0]
    return samplename


def main(annfile):
    annlist=list()
    with open(annfile,'r') as annfile_open:
        annlines=annfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(annfile))

    samplename=get_samplename(annfile)
    
    vcffile=samplename+'.vcf'
    vcffile_open=open(vcffile,'w')

    header=HEADER+'\t'+samplename+'\n'
    vcffile_open.write(header)

    chrre=re.compile('.*,\w+,\d+,\d+,[ATCG],[ATCG],(\w+),\d+,.*')
    posre=re.compile('.*,\w+,\d+,\d+,[ATCG],[ATCG],\w+,(\d+),.*')
    refre=re.compile('.*,\w+,\d+,\d+,([ATCG]),[ATCG],\w+,\d+,.*')
    altre=re.compile('.*,\w+,\d+,\d+,[ATCG],([ATCG]),\w+,\d+,.*')
    idre=re.compile('.*,(rs\d+),.*')
    qualre=re.compile('.*,[ATCG],[ATCG],(\d+),PASS,ReqIncl.*')
    filterre=re.compile('.*,[ATCG],[ATCG],\d+,(PASS),ReqIncl.*')
    infore=re.compile('.*(ReqIncl=.*)GT:VR:')
    formatre=re.compile('.*(GT:.*:GQ).*')
    samplere=re.compile('.*([01]/[01]:\d+:\d+:\d+:.*)')
    
    for annline in annlines:
        #print('annline=%s\n' % annline)
        if annline.startswith('Func'):
            continue
        else:
            #vcfchr=annline.split(',')[CHROM_COL]
            #vcfpos=annline.split(',')[POS_COL]
            #vcfid=annline.split(',')[ID_COL]
            #vcfref=annline.split(',')[REF_COL]
            #vcfalt=annline.split(',')[ALT_COL]
            #vcfqual=annline.split(',')[QUAL_COL]
            #vcffilter=annline.split(',')[FILTER_COL]
            chrmatch=chrre.match(annline)
            vcfchr=chrmatch.groups()[0]
            #print('vcfchr=%s' % vcfchr)
            posmatch=posre.match(annline)
            vcfpos=posmatch.groups()[0]
            #print('vcfpos=%s' % vcfpos)
            refmatch=refre.match(annline)
            vcfref=refmatch.groups()[0]
            #print('vcfref=%s' % vcfref)
            altmatch=altre.match(annline)
            vcfalt=altmatch.groups()[0]
            #print('vcfalt=%s' % vcfalt)
            idmatch=idre.match(annline)
            if str(idmatch)=='None':
                vcfid='.'
            else:
                vcfid=idmatch.groups()[0]
            #print('vcfid=%s' % vcfid)
            qualmatch=qualre.match(annline)
            vcfqual=qualmatch.groups()[0]
            #print('vcfqual=%s' % vcfqual)
            filtermatch=filterre.match(annline)
            vcffilter=filtermatch.groups()[0]
            #print('vcffilter=%s' % vcffilter)


            infomatch=infore.match(annline)
            vcfinfo=infomatch.groups()[0].strip(',')
            vcfinfo='.'
            #print('vcfinfo=%s' % vcfinfo)
            formatmatch=formatre.match(annline)
            vcfformat=formatmatch.groups()[0]
            #print('vcfformat=%s' % vcfformat)
            samplematch=samplere.match(annline)
            vcfsample=samplematch.groups()[0]
            #print('vcfsample=%s' % vcfsample)
            vcffile_open.write(vcfchr +'\t'+ vcfpos +'\t'+ vcfid+'\t'+vcfref+'\t'+vcfalt+'\t'+vcfqual+'\t'+vcffilter+'\t'+vcfinfo+\
                               '\t'+vcfformat+'\t'+vcfsample+'\n')
            
    vcffile_open.close()


if __name__=='__main__':
    main(annfile)
