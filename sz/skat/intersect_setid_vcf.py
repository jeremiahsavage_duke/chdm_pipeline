#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('intersect setid vcf file')
parser.add_argument('-s','--setidfile',required=True)
parser.add_argument('-i','--setidstring',required=True)
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

setidfile=args['setidfile']
setidstring=args['setidstring']
vcffile=args['vcffile']

def main(setidfile,vcffile,setidstring):

    with open(setidfile,'r') as setidfile_open:
        setidlines=setidfile_open.readlines()

    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()
        
    setid_variant_dict=dict()

    for setidline in setidlines:
        setidsplit=setidline.strip('\n').split('\t')
        setid=setidsplit[0]
        variant=setidsplit[1]
        setid_variant_dict[variant]=setid

    newvcflines=str()
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            newvcflines+=vcfline
        else:
            vcfsplit=vcfline.strip('\n').split('\t')
            vcf_chrom=vcfsplit[0]
            vcf_pos=vcfsplit[1]
            variant='var_chr'+vcf_chrom+'_'+vcf_pos
            setidvalue=setid_variant_dict[variant]
            if setidvalue==setidstring:
                newvcflines+=vcfline

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    newfile=origfile+'_'+setidstring+origext
    newfile_open=open(newfile,'w')

    for newvcfline in newvcflines:
        newfile_open.write(newvcfline)

    newfile_open.close()


if __name__=='__main__':
    main(setidfile,vcffile,setidstring)
