#!/usr/bin/env python3
import argparse
import glob
import os
import natsort

parser=argparse.ArgumentParser('for GATK CombineVariants')
parser.add_argument('-f','--vcfdir',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdir']


def main(vcfdir):
    vcflist=glob.glob(os.path.join(vcfdir,'*.vcf'))
    vcflist=natsort.natsorted(vcflist)

    output='vcf_sorted.gatk'
    
    outstring=str()

    for vcf in vcflist:
        outstring+=' --variant '+vcf

    outfile_open=open(output,'w')

    outfile_open.write(outstring)

    outfile_open.close()


if __name__=='__main__':
    main(vcfdir)
