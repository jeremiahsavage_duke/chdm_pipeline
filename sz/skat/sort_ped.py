#!/usr/bin/env python3
import argparse
import csv
import operator
import os
import natsort

parser=argparse.ArgumentParser('sort a file')
parser.add_argument('-f','--tsvinput',required=True)
args=vars(parser.parse_args())

tsvinput=args['tsvinput']


def main(tsvinput):
    with open(tsvinput,'r') as tsvfile:
        tsvreader=csv.reader(tsvfile,delimiter=' ',quotechar='"')
        list1=natsort.natsorted(tsvreader,key=operator.itemgetter(0))

    origfile,origext=os.path.splitext(os.path.basename(tsvinput))
    tsvoutput=origfile+'_sorted'+origext
    
    with open(tsvoutput,'w') as tsvout:
        tsvwriter=csv.writer(tsvout,delimiter='\t',quotechar='"')
        tsvwriter.writerows(list1)


if __name__=='__main__':
    main(tsvinput)
