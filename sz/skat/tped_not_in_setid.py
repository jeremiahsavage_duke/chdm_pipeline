#!/usr/bin/env python3
import argparse
import os


parser=argparse.ArgumentParser('find tped entries missing a setid')
parser.add_argument('-t','--tpedfile',required=True)
parser.add_argument('-s','--setidfile',required=True)
args=vars(parser.parse_args())

tpedfile=args['tpedfile']
setidfile=args['setidfile']

VCF_CHR_COL=0
VCF_POS_COL=1

def main(tpedfile,setidfile):
    with open(setidfile,'r') as setidfile_open:
        setidlines=setidfile_open.readlines()

    with open(tpedfile,'r') as tpedfile_open:
        tpedlines=tpedfile_open.readlines()

    setid_dict=dict()
    for setidline in setidlines:
        setidsplit=setidline.strip().split('\t')
        setid_dict[setidsplit[1]]=setidsplit[0]

    tpedID_list=list()
    for tpedline in tpedlines:
        tpedid=tpedline.strip().split('\t')[1]
        tpedID_list.append(tpedid)

        
    tpedmissing_list=list()
    isin=0
    isout=0
    for tpedid in tpedID_list:
        if tpedid in setid_dict.keys():
            isin+=1
        else:
            isout+=1
            tpedmissing_list.append(tpedid)

        
    
    origfile,origext=os.path.splitext(os.path.basename(tpedfile))
    newfile=origfile+'_missing_setid.list'
    newfile_open=open(newfile,'w')

    for tpedmissingid in tpedmissing_list:
        newfile_open.write(tpedmissingid+'\n')

    newfile_open.close()


if __name__=='__main__':
    main(tpedfile,setidfile)
