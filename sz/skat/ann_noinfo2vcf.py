#!/usr/bin/env python3
import argparse
import datetime
import os
import sys
import re


parser=argparse.ArgumentParser('for SKAT a pre-SetID file')
parser.add_argument('-f','--annfile',required=True)
args=vars(parser.parse_args())

annfile=args['annfile']

FUNC_COL=0
GENE_COL=1
EXONICFUNC_COL=2
AACHANGE_COL=3
CONSERVED_COL=4
SEGDUP_COL=5
ESP6500SIEA_COL=6
ONEKG2012APR_EUR_COL=7
DBSNP138_COL=8
AVISIFT_COL=9
LJB_PHYLOP_COL=10
LJB_PHYLOP_PRED_COL=11
LJB_SIFT_COL=12
LJB_SIFT_PRED_COL=13
LJB_POLYPHEN2_COL=14
LJB_POLYPHEN2_PRED_COL=15
LJB_LRT_COL=16
LJB_LRT_PRED_COL=17
LJB_MUTATIONTASTER_COL=18
LJB_MUTATIONTASTER_PRED_COL=19
LJB_GERP_COL=20
CHR_COL=21
START_COL=22
END_COL=23
REF_COL=24
OBS_COL=25
OTHER_CHR_COL=26
OTHER_START_COL=27
OTHER_ID_COL=28
OTHER_REF_COL=29
OTHER_OBS_COL=30
OTHER_SCORE_COL=31
OTHER_FILTER_COL=32
OTHER_INFO_COL=33
OTHER_FORMAT_COL=34
OTHER_GENOTYPE_COL=35

TOPHEADER='''##fileformat=VCFv4.2 
##fileDate='''+str(datetime.datetime.now().year)+str(datetime.datetime.now().month).zfill(2)+str(datetime.datetime.now().day).zfill(2)+'\n'

TOPHEADER+='''##source=ann_noinfo2vcf.py
##INFO=<ID=FC,Number=1,Type=String,Description="Exonic or Splicing">
##INFO=<ID=GN,Number=1,Type=String,Description="Gene Name">
##INFO=<ID=EF,Number=1,Type=String,Description="Type of nonsynonymous variant">
##INFO=<ID=AA,Number=1,Type=String,Description="The AA Change">
##INFO=<ID=EA,Number=1,Type=Float,Description="ESP6500si MAF">
##INFO=<ID=EU,Number=1,Type=Float,Description="1000 Genomes MAF">
##FORMAT=<ID=GT,Number=1,Type=Integer,Description="Genotype">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">
##FORMAT=<ID=RR,Number=1,Type=Integer,Description="Reference Read Depth">
##FORMAT=<ID=VR,Number=1,Type=Integer,Description="Major Variant Read Depth">
'''

HEADER=TOPHEADER+'#CHROM'+'\t'+'POS'+'\t'+'ID'+'\t'+'REF'+'\t'+'ALT'+'\t'+'QUAL'+'\t'+'FILTER'+'\t'+'INFO'+'\t'+'FORMAT'

def get_anndict(annline):
    ann_dict=dict()
    annsplit=annline.strip().split('\t')
    ann_dict['func']=annsplit[FUNC_COL]
    ann_dict['gene']=annsplit[GENE_COL]
    ann_dict['exonicfunc']=annsplit[EXONICFUNC_COL]
    if ann_dict['exonicfunc']=='':
        ann_dict['exonicfunc']='.'
    ann_dict['aachange']=annsplit[AACHANGE_COL]
    if ann_dict['aachange']=='':
        ann_dict['aachange']='.'
    ann_dict['conserved']=annsplit[CONSERVED_COL]
    ann_dict['segdup']=annsplit[SEGDUP_COL]
    ann_dict['esp6500si']=annsplit[ESP6500SIEA_COL]
    ann_dict['onekg']=annsplit[ONEKG2012APR_EUR_COL]
    if ann_dict['onekg']=='':
        ann_dict['onekg']='.'
    ann_dict['dbsnp']=annsplit[DBSNP138_COL]
    if ann_dict['dbsnp']=='':
        ann_dict['dbsnp']='.'
    ann_dict['avisift']=annsplit[AVISIFT_COL]
    ann_dict['ljbphylop']=annsplit[LJB_PHYLOP_COL]
    ann_dict['ljbphyloppred']=annsplit[LJB_LRT_PRED_COL]
    ann_dict['ljbsift']=annsplit[LJB_SIFT_COL]
    ann_dict['ljbsiftpred']=annsplit[LJB_SIFT_PRED_COL]
    ann_dict['ljbpoly2']=annsplit[LJB_POLYPHEN2_COL]
    ann_dict['ljbpoly2ped']=annsplit[LJB_POLYPHEN2_PRED_COL]
    ann_dict['ljblrt']=annsplit[LJB_LRT_COL]
    ann_dict['ljblrtpred']=annsplit[LJB_LRT_PRED_COL]
    ann_dict['ljbmut']=annsplit[LJB_MUTATIONTASTER_COL]
    ann_dict['ljbmutpred']=annsplit[LJB_MUTATIONTASTER_PRED_COL]
    ann_dict['ljbgerp']=annsplit[LJB_GERP_COL]
    ann_dict['chrom']=annsplit[CHR_COL]
    ann_dict['start']=annsplit[START_COL]
    ann_dict['end']=annsplit[END_COL]
    ann_dict['ref']=annsplit[REF_COL]
    ann_dict['obs']=annsplit[OBS_COL]
    ann_dict['other_chrom']=annsplit[OTHER_CHR_COL]
    ann_dict['other_start']=annsplit[OTHER_START_COL]
    ann_dict['other_id']=annsplit[OTHER_ID_COL]
    ann_dict['other_ref']=annsplit[OTHER_REF_COL]
    ann_dict['other_obs']=annsplit[OTHER_OBS_COL]
    ann_dict['other_score']=annsplit[OTHER_SCORE_COL]
    ann_dict['other_filter']=annsplit[OTHER_FILTER_COL]
    ann_dict['other_info']=annsplit[OTHER_INFO_COL]
    ann_dict['other_format']=annsplit[OTHER_FORMAT_COL]
    ann_dict['other_genotype']=annsplit[OTHER_GENOTYPE_COL]

    for key,value in ann_dict.items():
        ann_dict[key]=value.strip('"')

    return ann_dict

def main(annfile):
    annlist=list()
    with open(annfile,'r') as annfile_open:
        annlines=annfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(annfile))


    sample_set=set()
    for annline in annlines:
        samplere=re.compile('.*[01]/[01]:\d+:\d+:\d+:\.*\"\t(\d+)')
        samplere_match=samplere.match(annline)
        this_sample=samplere_match.groups()[0].strip('"')
        sample_set.add(this_sample)


    vcffile_dict=dict()
    vcffile_open_dict=dict()
    for sample in sample_set:
        filename=origfile+'_'+sample+'.vcf'
        vcffile_dict[sample]=filename
        vcffile_open_dict[sample]=open(filename,'w')
        header=HEADER+'\t'+sample+'\n'

        vcffile_open_dict[sample].write(header)
        
    for annline in annlines:
        ann_dict=get_anndict(annline)

        formatre=re.compile('.*(GT:.*:GQ).*')
        genotypere=re.compile('.*([01]/[01]:\d+:\d+:\d+:\.*)')
        samplere=re.compile('.*[01]/[01]:\d+:\d+:\d+:\.*\"\t(\d+)')

        formatre_match=formatre.match(annline)
        genotypere_match=genotypere.match(annline)
        samplere_match=samplere.match(annline)
        this_format=formatre_match.groups()[0].strip('"')
        this_genotype=genotypere_match.groups()[0].strip('"')
        this_sample=samplere_match.groups()[0].strip('"')

        this_func='FC='+'"'+ann_dict['func']+'"'
        this_exonicfunc='EF='+'"'+ann_dict['exonicfunc']+'"'
        this_gene='GN='+'"'+ann_dict['gene']+'"'
        this_aachange='AA='+'"'+ann_dict['aachange']+'"'
        this_esp6500='EA='+'"'+ann_dict['esp6500si']+'"'
        this_1kG='EU='+'"'+ann_dict['onekg']+'"'

        this_info=this_func+';'+this_exonicfunc+';'+this_gene+';'+this_aachange+';'+this_esp6500+';'+this_1kG

        vcfline=ann_dict['chrom']+'\t'+ann_dict['start']+'\t'+ann_dict['dbsnp']+'\t'+ann_dict['ref']+'\t'+ann_dict['obs']+'\t'+ \
                 ann_dict['other_score']+'\t'+ann_dict['other_filter']+'\t'+this_info+'\t'+this_format+'\t'+this_genotype

        vcffile_open_dict[this_sample].write(vcfline+'\n')

    for sample in sample_set:
        vcffile_open_dict[sample].close()


if __name__=='__main__':
    main(annfile)
