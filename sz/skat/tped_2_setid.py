#!/usr/bin/env python3
import argparse
import os
import sys
##tped##
#chr var_chrX_NNN
#
#=>
#
##setid##
#name var_chrX_NNN

parser=argparse.ArgumentParser('generate setid')
parser.add_argument('-t','--tpedfile',required=True)
args=vars(parser.parse_args())

tpedfile=args['tpedfile']


def main(tpedfile):
    with open(tpedfile,'r') as tpedfile_open:
        tpedlines=tpedfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tpedfile))

    setidfile=origfile+'.setid'
    setidfile_open=open(setidfile,'w')


    for tpedline in tpedlines:
        variant=tpedline.split('\t')[1]
        variant_chr=variant.split('_')[1].strip('chr')
        variant_pos=variant.split('_')[2]
        newstring='var_chr'+variant_chr+'_'+variant_pos
        setid_str=newstring+'\t'+newstring+'\n'
        setidfile_open.write(setid_str)

    setidfile_open.close()


if __name__=='__main__':
    main(tpedfile)
