#!/usr/bin/env python3
import argparse
import os


parser=argparse.ArgumentParser('atlas2 generates problem DP for plink. correct it.')
parser.add_argument('-v','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

VCFREFCOL=3

def main(vcffile):

    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()


    newvcflines=str()
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            newvcflines+=vcfline
        else:
            vcfsplit=vcfline.strip('\n').split('\t')
            vcfref=vcfsplit[VCFREFCOL]
            for i,vcfcol in enumerate(vcfsplit):
                if (vcfcol.startswith('0/.:')) or (vcfcol.startswith('./.:')):
                    vcfsplit[i]='0/0:.:100:100:0'
            newline='\t'.join(vcfsplit)
            newvcflines+=newline+'\n'

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    newfile=origfile+'_zdDP'+origext
    newfile_open=open(newfile,'w')

    for newline in newvcflines:
        newfile_open.write(newline)

    newfile_open.close()


if __name__=='__main__':
    main(vcffile)
