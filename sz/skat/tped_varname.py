#!/usr/bin/env python3
import argparse
import os


parser=argparse.ArgumentParser('custom tped var name')
parser.add_argument('-t','--tpedfile',required=True)
args=vars(parser.parse_args())

tpedfile=args['tpedfile']

CHR_COL=0
VAR_COL=1
POS_COL=3

def main(tpedfile):

    with open(tpedfile,'r') as tpedfile_open:
        tpedlines=tpedfile_open.readlines()


    newtped=list()
    for tpedline in tpedlines:
        tpedsplit=tpedline.split('\t')
        tpedchr=tpedsplit[CHR_COL]
        tpedpos=tpedsplit[POS_COL]
        if tpedchr=='23':
            tpedsplit[VAR_COL]='var_chrX_'+tpedpos
        elif tpedchr=='24':
            tpedsplit[VAR_COL]='var_chrY_'+tpedpos
        elif tpedchr=='26':
            tpedsplit[VAR_COL]='var_chrMT_'+tpedpos
        else:
            tpedsplit[VAR_COL]='var_chr'+tpedchr+'_'+tpedpos
        newtped.append(tpedsplit)


    origfile,origext=os.path.splitext(os.path.basename(tpedfile))
    newfile=origfile+'_new'+origext
    newfile_open=open(newfile,'w')

    for tped in newtped:
        newline='\t'.join(tped)
        newfile_open.write(newline)

    newfile_open.close()


if __name__=='__main__':
    main(tpedfile)
