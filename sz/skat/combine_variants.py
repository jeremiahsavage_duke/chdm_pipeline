#!/usr/bin/env python3
import argparse
import os
import natsort
import glob
import sys


parser=argparse.ArgumentParser('for SKAT a pre-SetID file')
parser.add_argument('-o','--dir1',required=True)
parser.add_argument('-t','--dir2',required=True)
args=vars(parser.parse_args())

dir1=args['dir1']
dir2=args['dir2']


def get_vcf2(list2,vcf1):
    print('vcf1=%s' % vcf1)
    for vcf2 in list2:
        if os.path.basename(vcf1)==os.path.basename(vcf2):
            return vcf2
    sys.exit('No pair to vcf1: %s' % vcf1)

def main(dir1,dir2):
    list1=glob.glob(os.path.join(dir1,'*.vcf'))
    list2=glob.glob(os.path.join(dir2,'*.vcf'))
    list1=natsort.natsorted(list1)
    list2=natsort.natsorted(list2)

    for vcf1 in list1:
        vcf2=get_vcf2(list2,vcf1)
        outfile=os.path.basename(vcf1)
        outfile_open=open(outfile,'w')
        
        with open(vcf1,'r') as vcf1_open:
            vcf1lines=vcf1_open.readlines()

        with open(vcf2,'r') as vcf2_open:
            vcf2lines=vcf2_open.readlines()


        for vcf1line in vcf1lines:
            outfile_open.write(vcf1line)

        for vcf2line in vcf2lines:
            if vcf2line.startswith('#'):
                continue
            else:
                outfile_open.write(vcf2line)
    
        outfile_open.close()

if __name__=='__main__':
    main(dir1, dir2)
