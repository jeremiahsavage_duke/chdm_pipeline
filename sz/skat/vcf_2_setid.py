#!/usr/bin/env python3
import argparse
import os
import sys

parser=argparse.ArgumentParser('for SKAT a pre-SetID file')
parser.add_argument('-f','--vcffile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']

INTRONIC_RFG_SET=set(['UTR3','upstream,downstream','ncRNA_intronic','upstream','downstream','UTR5','ncRNA_UTR3','unknown','ncRNA_UTR5','intergenic','intronic'])
EXONIC_RFG_SET=set(['nonsynonymous_SNV/exonic_splicing','splicing','ncRNA_exonic','ncRNA_splicing','stoploss_SNV','synonymous_SNV/exonic_splicing','synonymous_SNV','stopgain_SNV','nonsynonymous_SNV'])

EXONIC_SYNONYMOUS_SET=set(['ncRNA_exonic','synonymous_SNV','nonsynonymous_SNV'])
EXONIC_NONSYNONYMOUS_SET=set(['nonsynonymous_SNV/exonic_splicing','splicing','ncRNA_splicing','stoploss_SNV','synonymous_SNV/exonic_splicing'])

def get_genevalue(vcfinfosplit,akey,nameset,lastrun,prevvalue):
    if lastrun==True:
        if (prevvalue != '.') and (prevvalue != 'NONE'):
            nameset.remove(prevvalue)
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        datakey=datasplit[0].strip()
        if datakey==akey:
            datavalue=datasplit[1].strip()
            if ',' in datavalue:
                datavalueset=datavalue.split(',')
                thisset=set()
                for avalue in datavalueset:
                    altvalue=avalue.split('(')[0].strip()
                    thisset.add(altvalue)
                for asetitem in thisset:
                    if asetitem in nameset:
                        datavalue=asetitem
                        newvalue=False
                        return datavalue,newvalue
                if (lastrun) and (prevvalue != 'NONE') and (prevvalue != '.'): # lastrun
                    datavalue=prevvalue
                    newvalue=False
                    nameset.add(datavalue)
                    return datavalue,newvalue
                for asetitem in thisset:
                    if (asetitem != 'NONE') and (asetitem != '.') and (not asetitem.startswith('ENSG0')):
                        datavalue=asetitem
                        newvalue=True
                        nameset.add(datavalue)
                        return datavalue,newvalue
                #stuck with 'NONE' or '.'
                #print('akey=%s' % akey)
                #print('vcfinfosplit=%s\n' % vcfinfosplit)
                datavalue=asetitem.pop()
                newvalue=False
                return datavalue,newvalue
            else:
                datavalue=datasplit[1].split('(')[0].strip()
                if datavalue in nameset:
                    newvalue=False
                    return datavalue,newvalue
                elif (datavalue != '.') and (datavalue != 'NONE'):
                    newvalue=True
                    nameset.add(datavalue)
                    return datavalue,newvalue
                else:
                    newvalue=False
                    return datavalue,newvalue


def get_datavalue(vcfinfosplit,akey,nameset):
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        datakey=datasplit[0].strip()
        if datakey==akey:
            datavalue=datasplit[1].split('(')[0].strip()
            return datavalue    
                
                
def get_gene_value(vcfinfo,genenameset,functionnameset):
    gnkey='GN'
    rfgkey='RFG'
    gikey='GI'
    
    vcfinfosplit=vcfinfo.split(';')
    lastrun=False
    prevvalue=''
    gnvalue,newvalue=get_genevalue(vcfinfosplit,gnkey,genenameset,lastrun,prevvalue)
    if (gnvalue=='.') or (newvalue==True):
        lastrun=True
        prevvalue=gnvalue
        givalue,newvalue=get_genevalue(vcfinfosplit,gikey,genenameset,lastrun,prevvalue)
        #print('gnvalue=%s' % gnvalue)
        #print('givalue=%s' % str(givalue))
        gnvalue=givalue

    #print('genenameset=%s' % genenameset)
    #print('gnvalue=%s' % str(gnvalue))
    #print('vcfinfo=%s\n' % vcfinfo)

    rfgvalue=get_datavalue(vcfinfosplit,rfgkey,functionnameset)
    if rfgvalue in EXONIC_RFG_SET:
        snpid=gnvalue+'_exonic'
        return snpid
    elif rfgvalue in INTRONIC_RFG_SET:
        snpid=gnvalue+'_intronic'
        return snpid
    else:
        sys.exit('Not in any set: %s' % (str(rfgvalue)))


def main(vcffile):
    with open(vcffile,'r') as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    presetidfile=origfile+'.setid'
    presetidfile_open=open(presetidfile,'w')

    genenameset=set()
    functionnameset=set()
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            continue
        else:
            vcfsplit=vcfline.split('\t')
            chromosome=vcfsplit[0].strip()
            position=vcfsplit[1].strip()
            vcfinfo=vcfsplit[7].strip()
            genename=get_gene_value(vcfinfo,genenameset,functionnameset)
            presetidline=genename+'\t'+'var_chr'+chromosome+'_'+position+'\n'
            presetidfile_open.write(presetidline)

    presetidfile_open.close()


if __name__=='__main__':
    main(vcffile)
