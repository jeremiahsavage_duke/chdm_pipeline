#!/usr/bin/env python3
import argparse
import os
import glob


MAF_FREQ_01=0.001
MAF_FREQ_02_LOW=0.001
MAF_FREQ_02_HIGH=0.01
MAF_FREQ_03_LOW=0.01
MAF_FREQ_03_HIGH=0.05
THOUSANDMAFKEY='TEUR'
ESPREFCOUNT='ET'
ESPALTCOUNT='EV'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('filter vcfs by MAF')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
        if afile.endswith('.tsv'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def get_info_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if datasplit[0].endswith(akey):
            return datasplit[1]


def get_maf_lt(vcflines,mafkey,maf_cutoff):
    keptvalues=''
    keptnulls=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
            keptnulls+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_value=get_info_value(mafkey,vcfinfo)
                if (maf_value is None) or (maf_value is '.'):
                    keptnulls+=vcfline
                else:
                    maf_freq=float(maf_value)
                    if maf_freq <= maf_cutoff:
                        keptvalues+=vcfline
    return keptvalues,keptnulls


def get_maf_count_lt(vcflines,maf_ref_key,maf_alt_key,maf_cutoff):
    keptvalues=''
    keptnulls=''
    keptrefzero=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
            keptnulls+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_ref_value=get_info_value(maf_ref_key,vcfinfo)
                maf_alt_value=get_info_value(maf_alt_key,vcfinfo)
                if (maf_ref_value is None) or (maf_ref_value is '.'):
                    keptnulls+=vcfline
                else:
                    maf_ref_count=int(maf_ref_value)
                    maf_alt_count=int(maf_alt_value)
                    if maf_ref_count==0: # no divide by zero
                        maf_freq=999999999
                    else:
                        maf_freq=float(maf_alt_count)/float(maf_ref_count)
                    if maf_freq <= maf_cutoff:
                        keptvalues+=vcfline
                    if maf_ref_count==0:
                        keptrefzero+=vcfline
    return keptvalues,keptnulls,keptrefzero


def get_maf_range(vcflines,maf_freq_key,maf_low_cutoff,maf_high_cutoff):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_value=get_info_value(maf_freq_key,vcfinfo)
                if (maf_value is None) or (maf_value is '.'):
                    continue
                else:
                    maf_freq=float(maf_value)
                    if (maf_freq >= maf_low_cutoff) and (maf_freq <= maf_high_cutoff):
                        keptvalues+=vcfline
    return keptvalues


def get_maf_count_range(vcflines,maf_ref_key,maf_alt_key,maf_low_cutoff,maf_high_cutoff):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_ref_value=get_info_value(maf_ref_key,vcfinfo)
                maf_alt_value=get_info_value(maf_alt_key,vcfinfo)
                if (maf_ref_value is None) or (maf_ref_value is '.'):
                    continue
                else:
                    maf_ref_count=int(maf_ref_value)
                    maf_alt_count=int(maf_alt_value)
                    if maf_ref_count==0: # no divide by zero
                        maf_freq=999999999
                    else:
                        maf_freq=float(maf_alt_count)/float(maf_ref_count)
                    if (maf_freq >= maf_low_cutoff) and (maf_freq <= maf_high_cutoff):
                        keptvalues+=vcfline
    return keptvalues


#def get_maf_gt(vcflines,mafkey,maf_cutoff):
#    keptlines=''
#    for vcfline in vcflines:
#        if vcfline.startswith('#'):
#            keptlines+=vcfline
#    return keptlines


def write_maf_to_tsv(vcflines,openfile):
    for vcfline in vcflines:
        openfile.write(vcfline)


def filter_thousand(vcffile):
    mafkey=THOUSANDMAFKEY
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    outfile_lt_01_nulls=open(origfile+'_'+mafkey+'_low_nulls.tsv','w')
    outfile_lt_01_values=open(origfile+'_'+mafkey+'_low_values.tsv','w')
    outfile_range_01_values=open(origfile+'_'+mafkey+'_range_low_values.tsv','w')
    outfile_range_02_values=open(origfile+'_'+mafkey+'_range_high_values.tsv','w')

    maf_value_lt_val01,maf_value_lt_null01=get_maf_lt(vcflines,mafkey,MAF_FREQ_01)
    maf_value_range_val01=get_maf_range(vcflines,mafkey,MAF_FREQ_02_LOW,MAF_FREQ_02_HIGH)
    maf_value_range_val02=get_maf_range(vcflines,mafkey,MAF_FREQ_03_LOW,MAF_FREQ_03_HIGH)

    write_maf_to_tsv(maf_value_lt_val01,outfile_lt_01_values)
    write_maf_to_tsv(maf_value_lt_null01,outfile_lt_01_nulls)
    write_maf_to_tsv(maf_value_range_val01,outfile_range_01_values)
    write_maf_to_tsv(maf_value_range_val02,outfile_range_02_values)

    outfile_lt_01_nulls.close()
    outfile_lt_01_values.close()
    outfile_range_01_values.close()
    outfile_range_02_values.close()





def filter_esp(vcffile):
    mafrefcount=ESPREFCOUNT
    mafaltcount=ESPALTCOUNT
    mafkey=mafaltcount

    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    outfile_lt_01_nulls=open(origfile+'_'+mafkey+'_low_nulls.tsv','w')
    outfile_lt_01_values=open(origfile+'_'+mafkey+'_low_values.tsv','w')
    outfile_range_01_values=open(origfile+'_'+mafkey+'_range_low_values.tsv','w')
    outfile_range_02_values=open(origfile+'_'+mafkey+'_range_high_values.tsv','w')
    outfile_refzero=open(origfile+'_refzero.tsv','w')

    maf_value_lt_val01,maf_value_lt_null01,keptrefzero=get_maf_count_lt(vcflines,mafrefcount,mafaltcount,MAF_FREQ_01)
    maf_value_range_val01=get_maf_count_range(vcflines,mafrefcount,mafaltcount,MAF_FREQ_02_LOW,MAF_FREQ_02_HIGH)
    maf_value_range_val02=get_maf_count_range(vcflines,mafrefcount,mafaltcount,MAF_FREQ_03_LOW,MAF_FREQ_03_HIGH)

    write_maf_to_tsv(maf_value_lt_val01,outfile_lt_01_values)
    write_maf_to_tsv(maf_value_lt_null01,outfile_lt_01_nulls)
    write_maf_to_tsv(maf_value_range_val01,outfile_range_01_values)
    write_maf_to_tsv(maf_value_range_val02,outfile_range_02_values)
    write_maf_to_tsv(keptrefzero,outfile_refzero)

    outfile_lt_01_nulls.close()
    outfile_lt_01_values.close()
    outfile_range_01_values.close()
    outfile_range_02_values.close()
    outfile_refzero.close()

    
def main(vcfdir):
    vcflist=get_vcf_list(vcfdir)
    for vcffile in vcflist:
        filter_thousand(vcffile)
        if 'SNPs' in vcffile:
            filter_esp(vcffile)


if __name__=='__main__':
    main(vcfdir)
