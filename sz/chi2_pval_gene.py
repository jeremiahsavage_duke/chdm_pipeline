#!/usr/bin/env python3
import argparse
import os
import sys
from scipy.stats import chi2_contingency
from numpy import array

CHR_COL=0
CASE_HET_COL=6
CASE_HOM_COL=7
CONTROL_HET_COL=13
CONTROL_HOM_COL=14
GENE_COL=18

CASE_COUNT=480
CONTROL_COUNT=480


parser=argparse.ArgumentParser('consolidate allels to single row')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def do_calc(total,allele_count):
    if is_autosome(total['total_chr_name']):
        case_allele_count=CASE_COUNT * allele_count * 2
        control_allele_count=CONTROL_COUNT * allele_count * 2
    elif is_allosome(total['total_chr_name']):
        case_allele_count=CASE_COUNT * allele_count * 1
        control_allele_count=CONTROL_COUNT * allele_count * 1
    else:
        sys.exit('huh')
    #print('case_allele_count=%s' % (case_allele_count))
    #print('control_allele_count=%s' % (control_allele_count))
    a=total['total_case_count']
    b=case_allele_count-total['total_case_count']
    c=total['total_control_count']
    d=control_allele_count-total['total_control_count']
    #print(str(a))
    #print(str(b))
    #print(str(c))
    #print(str(d))
    cc_array=array([a,b,c,d]).reshape(2,-1)
    #print('cc_array=%s' % (cc_array))
    result=chi2_contingency(cc_array)
    res_chi2=result[0]
    res_pval=result[1]
    return res_chi2, res_pval

def is_autosome(this_chr_name):
    if this_chr_name.isdigit():
        return True
    else:
        return False

def is_allosome(this_chr_name):
    if (this_chr_name=='X') or (this_chr_name=='Y') or (this_chr_name=='M'):
        return True
    else:
        return False

def add_to_count(this,total):
    total['total_chr_name']=this['this_chr_name']
    if is_autosome(this['this_chr_name']):
        #print('is_autosome')
        if this['this_case_het']=='NA':
            #print('case_NA')
            total['total_case_count']+=0 #compiler should comment this out
            total['total_control_count']+=int(this['this_control_het'])+2*int(this['this_control_hom'])
        elif this['this_control_het']=='NA':
            #print('control_NA')
            total['total_case_count']+=int(this['this_case_het'])+2*int(this['this_case_hom'])
            total['total_control_count']+=0 #compiler should comment this out
        else:
            #print('all#')
            total['total_case_count']+=int(this['this_case_het'])+2*int(this['this_case_hom'])
            total['total_control_count']+=int(this['this_control_het'])+2*int(this['this_control_hom'])
    elif is_allosome(this['this_chr_name']):
        #print('is_allosome')
        if this['this_case_het']=='NA':
            total['total_case_count']+=0 #compiler should comment this out
            total['total_control_count']+=int(this['this_control_het'])+1*int(this['this_control_hom'])
        elif this['this_control_het']=='NA':
            total['total_case_count']+=int(this['this_case_het'])+1*int(this['this_case_hom'])
            total['total_control_count']+=0 #compiler should comment this out
        else:
            total['total_case_count']+=int(this['this_case_het'])+1*int(this['this_case_hom'])
            total['total_control_count']+=int(this['this_control_het'])+1*int(this['this_control_hom'])
    return total


def reset_total(this,total):
    if is_autosome(this['this_chr_name']):
        #print('is_autosome_reset')
        if this['this_case_het']=='NA':
            #print('case_NA')
            total['total_case_count']=0 #compiler should comment this out
            total['total_control_count']=int(this['this_control_het'])+2*int(this['this_control_hom'])
        elif this['this_control_het']=='NA':
            #print('control_NA')
            total['total_case_count']=int(this['this_case_het'])+2*int(this['this_case_hom'])
            total['total_control_count']=0 #compiler should comment this out
        else:
            #print('all#')
            total['total_case_count']=int(this['this_case_het'])+2*int(this['this_case_hom'])
            total['total_control_count']=int(this['this_control_het'])+2*int(this['this_control_hom'])
    elif is_allosome(this['this_chr_name']):
        #print('is_allosome_reset')
        if this['this_case_het']=='NA':
            total['total_case_count']=0 #compiler should comment this out
            total['total_control_count']=int(this['this_control_het'])+1*int(this['this_control_hom'])
        elif this['this_control_het']=='NA':
            total['total_case_count']=int(this['this_case_het'])+1*int(this['this_case_hom'])
            total['total_control_count']=0 #compiler should comment this out
        else:
            total['total_case_count']=int(this['this_case_het'])+1*int(this['this_case_hom'])
            total['total_control_count']=int(this['this_control_het'])+1*int(this['this_control_hom'])
    return total
        

def write_prev_count(res_gene_chi2,res_gene_pval,allele_count,prev_lino_no,tsvinlines):
    tsvprevline=tsvinlines[prev_lino_no].strip()
    newprevline=tsvprevline+'\t'+str(res_gene_chi2)+'\t'+str(res_gene_pval)+'\t'+str(allele_count)+'\n'
    tsvinlines[prev_lino_no]=newprevline
    #print(res_gene_chi2)
    #print(res_gene_pval)
    #print(str(prev_lino_no)+'\n'+'\n')


def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_gene_pval'+origext
    tsvoutfile_open=open(tsvoutfile,'w')

    newtsvlines=str()
    prev=dict()
    prev['prev_line_no']=int()
    curr_line_no=0
    autosome_allele_count=0
    allosome_allele_count=0
    total=dict()
    total['total_case_count']=0
    total['total_control_count']=0
    total['total_gene_name']=str()
    total['total_chr_name']=str()

    for lineno,tsvinline in enumerate(tsvinlines):
        tsvsplit=tsvinline.split('\t')
        this=dict()
        this['this_case_het']=tsvsplit[CASE_HET_COL].strip()
        this['this_case_hom']=tsvsplit[CASE_HOM_COL].strip()
        this['this_control_het']=tsvsplit[CONTROL_HET_COL].strip()
        this['this_control_hom']=tsvsplit[CONTROL_HOM_COL].strip()
        this['this_gene_name']=tsvsplit[GENE_COL].strip()
        this['this_chr_name']=tsvsplit[CHR_COL].strip()

        if total['total_gene_name']=='': #first line of file
            #print(this['this_gene_name'])
            #print('first line of file')
            allele_count=1
            total['total_gene_name']=this['this_gene_name']
        elif total['total_gene_name']==this['this_gene_name']: #matching & adding
            #print('matching and adding')
            allele_count+=1
            total=add_to_count(this,total)
        elif total['total_gene_name']!=this['this_gene_name']: #writing
            #print('this_gene_name=%s' % this['this_gene_name'])
            #print('allele_count=%s' % (allele_count))
            #print('writing')
            res_gene_chi2,res_gene_pval=do_calc(total,allele_count)
            write_prev_count(res_gene_chi2,res_gene_pval,allele_count,lineno-1,tsvinlines)
            total['total_chr_name']=this['this_chr_name']
            total['total_gene_name']=this['this_gene_name']
            total=reset_total(this,total)
            allele_count=1

    for newtsvline in tsvinlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
