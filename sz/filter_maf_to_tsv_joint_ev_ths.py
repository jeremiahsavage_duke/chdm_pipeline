#!/usr/bin/env python3
import argparse
import os
import glob


MAF_FREQ_ULTRA=0.001
MAF_FREQ_RARE_LOW=0.001
MAF_FREQ_RARE_HIGH=0.01
MAF_FREQ_COMMON_LOW=0.01
MAF_FREQ_COMMON_HIGH=0.05
THOUSANDMAFKEY='TEUR'
ESPREFCOUNT='ET'
ESPALTCOUNT='EV'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('filter vcfs by MAF')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
        if afile.endswith('.tsv'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def get_info_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if datasplit[0].endswith(akey):
            return datasplit[1]


def write_maf_to_tsv(ultrarare_lines,rare_lines,common_lines,null_lines,polymorphic_lines,vcffile):
    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    outfile_ultrarare=open(origfile+'_COMB_ultrarare.tsv','w')
    outfile_rare=open(origfile+'_COMB_rare.tsv','w')
    outfile_common=open(origfile+'_COMB_common.tsv','w')
    outfile_null=open(origfile+'_COMB_null.tsv','w')
    outfile_polymorphic=open(origfile+'_COMB_polymorphic.tsv','w')

    for ultrarare in ultrarare_lines:
        outfile_ultrarare.write(ultrarare)
    for rare in rare_lines:
        outfile_rare.write(rare)
    for common in common_lines:
        outfile_common.write(common)
    for null in null_lines:
        outfile_null.write(null)
    for polymorphic in polymorphic_lines:
        outfile_polymorphic.write(polymorphic)

    outfile_rare.close()
    outfile_common.close()
    outfile_null.close()
    outfile_polymorphic.close()


def get_esp_maf(vcfinfo):
    esp_ref_key=ESPREFCOUNT
    esp_alt_key=ESPALTCOUNT
    esp_ref_value=get_info_value(esp_ref_key,vcfinfo)
    esp_alt_value=get_info_value(esp_alt_key,vcfinfo)
    if (esp_ref_value is None) or (esp_ref_value is '.'):
        return '.'
    else:
        esp_ref_count=int(esp_ref_value)
        esp_alt_count=int(esp_alt_value)
        if esp_ref_count==0: # no divide by zero
            esp_freq=999999999
        else:
            esp_freq=float(esp_alt_count)/float(esp_ref_count)
    return esp_freq


def get_thou_maf(vcfinfo):
    thousand_mafkey=THOUSANDMAFKEY
    maf_value=get_info_value(thousand_mafkey,vcfinfo)
    if (maf_value is None) or (maf_value is '.'):
        return '.'
    else:
        maf_freq=float(maf_value)
    return maf_freq


def filter_esp_thousand(vcffile):
    ultrarare=''
    rare=''
    common=''
    null=''
    polymorphic=''
    truemaf=None
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()
  
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            continue
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                espmaf=get_esp_maf(vcfinfo)
                thousandmaf=get_thou_maf(vcfinfo)
                if espmaf is '.' and thousandmaf is '.':
                    truemaf='.'
                elif espmaf is '.' and isinstance(thousandmaf,float):
                    truemaf=thousandmaf
                elif isinstance(espmaf,float) and thousandmaf is '.':
                    truemaf=espmaf
                elif isinstance(espmaf,float) and isinstance(thousandmaf,float):
                    if thousandmaf>=espmaf:
                        truemaf=thousandmaf
                    else:
                        truemaf=espmaf
                if truemaf is '.':
                    null+=vcfline
                elif truemaf < MAF_FREQ_ULTRA:
                    ultrarare+=vcfline
                elif truemaf >= MAF_FREQ_RARE_LOW and truemaf <= MAF_FREQ_RARE_HIGH:
                    rare+=vcfline
                elif truemaf >= MAF_FREQ_COMMON_LOW and truemaf <= MAF_FREQ_COMMON_HIGH:
                    common+=vcfline
                else:
                    polymorphic+=vcfline

    write_maf_to_tsv(ultrarare,rare,common,null,polymorphic,vcffile)
    return ultrarare,rare,common,null,polymorphic

def main(vcfdir):
    vcflist=get_vcf_list(vcfdir)
    ultrarare=''
    rare=''
    common=''
    null=''
    polymorphic=''
    for vcffile in vcflist:
        if 'SNPs' in vcffile:
            newultra,newrare,newcommon,newnull,newpolymorphic=filter_esp_thousand(vcffile)
            ultrarare+=newultra
            rare+=newrare
            common+=newcommon
            null+=newnull
            polymorphic+=newpolymorphic
        elif 'INDELs' in vcffile:
            pass
            #filter_esp(vcffile)


if __name__=='__main__':
    main(vcfdir)
