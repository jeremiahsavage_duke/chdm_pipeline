#!/usr/bin/env python3
import argparse
import itertools
import os
import re

INFOCOL=33

parser=argparse.ArgumentParser('remove INFOCOL')
parser.add_argument('-t','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']

def main(tsvfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    origfile,origext=os.path.splitext(tsvfile)
    outfile=origfile+'_noquote'+origext
    outfile_open=open(outfile,'w')

    for tsvline in tsvlines:
        tsvsplit=tsvline.split('\t')
        for i,tsvcol in enumerate(tsvsplit):
            newcol=tsvcol.strip('"')
            tsvsplit[i]=newcol
        newline='\t'.join(tsvsplit)
        outfile_open.write(newline)

    outfile_open.close()



if __name__=='__main__':
    main(tsvfile)
