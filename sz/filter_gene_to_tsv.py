#!/usr/bin/env python3
import argparse
import os
import glob


GENELIST=['DISC1','PCM1','NOS1','HOOK3','RPGRIP1L','NPHP8']
GENEKEY='GN'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('filter vcfs by GENELIST')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist



def get_gene_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if (datasplit[0]=='IGN') or (datasplit[0]=='GN'):
            return datasplit[1]



def get_genes(vcflines,infokey,genelist):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                gene_value=get_gene_value(infokey,vcfinfo)
                if gene_value in genelist:
                    newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                    keptvalues+=newvcfline
    return keptvalues



def write_maf_to_tsv(vcflines,openfile):
    for vcfline in vcflines:
        openfile.write(vcfline)



def filter_genes(vcffile):
    infokey=GENEKEY
    genelist=GENELIST
    
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    outfile_lt_01=open(origfile+'_genes.tsv','w')
    gene_val01=get_genes(vcflines,infokey,genelist)
    write_maf_to_tsv(gene_val01,outfile_lt_01)
    outfile_lt_01.close()


def make_clean():
    xls_list=glob.glob('*.xls')
    for afile in xls_list:
        os.remove(afile)



def main(vcfdir):
    #make_clean()
    vcflist=get_vcf_list(vcfdir)
    for vcffile in vcflist:
        filter_genes(vcffile)



if __name__=='__main__':
    main(vcfdir)
