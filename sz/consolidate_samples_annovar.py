#!/usr/bin/env python3
import argparse
import os
import sys

CASE_GENOTYPE=34
CASE_SAMPLE=35
CASE_COUNT_OF=38
CASE_COUNT=39
CASE_HET=43
CASE_HOM=44

parser=argparse.ArgumentParser('consolidate allels to single row')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_shrink'+origext    
    tsvoutfile_open=open(tsvoutfile,'w')

    newtsvlines=''
    case_genotype_list=list()
    case_sample_list=list()
    case_count_of_list=list()
    case_het_list=list()
    case_hom_list=list()

    lenheader=0
    for tsvinline in tsvinlines:
        tsvsplit=tsvinline.split('\t')

        if tsvsplit[0]=='Func':
            newtsvlines+=tsvinline
            lenheader=len(tsvsplit)
            continue

        if len(tsvsplit)<lenheader: ##KLUDGE
            for newtsvline in newtsvlines:
                tsvoutfile_open.write(newtsvline)
            tsvoutfile_open.close()
            sys.exit('What a kludge')

        this_case_count_of=tsvsplit[CASE_COUNT_OF]
        this_case_count=tsvsplit[CASE_COUNT]
        print('this_case_patient_count_of=%s' % (this_case_count_of))
        print('this_case_allele_count=%s' % (this_case_count))

        if int(this_case_count)==1:
            newtsvline=tsvinline
            newtsvlines+=newtsvline


        elif int(this_case_count_of)==1 and int(this_case_count_of) < int(this_case_count):
            this_case_genotype=tsvsplit[CASE_GENOTYPE]
            this_case_sample=tsvsplit[CASE_SAMPLE]
            this_case_het=tsvsplit[CASE_HET].strip()
            this_case_hom=tsvsplit[CASE_HOM].strip()
            case_genotype_list.append(this_case_genotype)
            case_sample_list.append(this_case_sample)
            case_count_of_list.append(this_case_count_of)
            case_het_list.append(this_case_het)
            case_hom_list.append(this_case_hom)


        elif int(this_case_count_of)>1 and int(this_case_count_of) < int(this_case_count):
            this_case_genotype=tsvsplit[CASE_GENOTYPE]
            this_case_sample=tsvsplit[CASE_SAMPLE]
            this_case_het=tsvsplit[CASE_HET].strip()
            this_case_hom=tsvsplit[CASE_HOM].strip()
            case_genotype_list.append(this_case_genotype)
            case_sample_list.append(this_case_sample)
            case_count_of_list.append(this_case_count_of)
            case_het_list.append(this_case_het)
            case_hom_list.append(this_case_hom)


        elif int(this_case_count_of)>1 and int(this_case_count_of) == int(this_case_count):
            this_case_genotype=tsvsplit[CASE_GENOTYPE]
            this_case_sample=tsvsplit[CASE_SAMPLE]
            this_case_het=tsvsplit[CASE_HET].strip()
            this_case_hom=tsvsplit[CASE_HOM].strip()
            case_genotype_list.append(this_case_genotype)
            case_sample_list.append(this_case_sample)
            case_count_of_list.append(this_case_count_of)
            case_het_list.append(this_case_het)
            case_hom_list.append(this_case_hom)

            tsvsplit[CASE_GENOTYPE]=';'.join(case_genotype_list)
            tsvsplit[CASE_SAMPLE]=';'.join(case_sample_list)
            tsvsplit[CASE_COUNT_OF]=';'.join(case_count_of_list)

            total_het=0
            for het in case_het_list:
                total_het+=int(het)
            total_hom=0
            for hom in case_hom_list:
                total_hom+=int(hom)
            tsvsplit[CASE_HET]=str(total_het)
            tsvsplit[CASE_HOM]=str(total_hom)

            newtsvline='\t'.join(tsvsplit)
            newtsvlines+=newtsvline


            del case_genotype_list[:]
            del case_sample_list[:]
            del case_count_of_list[:]
            del case_het_list[:]
            del case_hom_list[:]

        else:
            sys.exit('Game Over')

    for newtsvline in newtsvlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
