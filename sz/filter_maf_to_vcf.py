#!/usr/bin/env python3
import argparse
import csv
import os
import glob


MAF_FREQ_ULTRA=0.001
MAF_FREQ_RARE_LOW=0.001
MAF_FREQ_RARE_HIGH=0.01
MAF_FREQ_COMMON_LOW=0.01
MAF_FREQ_COMMON_HIGH=0.05
ESPMAFCOL=6
THOUSANDMAFCOL=7
VCFFILTERCOL=32




parser=argparse.ArgumentParser('filter annovar csv by MAF')
parser.add_argument('-d','--csvdirectory',required=True)
args=vars(parser.parse_args())

csvdir=args['csvdirectory']

def get_csv_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('exome_summary.csv'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def write_maf_to_tsv(ultrarare_lines,rare_lines,common_lines,null_lines,polymorphic_lines,vcffile):
    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    outfile_ultrarare=open(origfile+'_ultrarare.tsv','w')
    outfile_rare=open(origfile+'_rare.tsv','w')
    outfile_common=open(origfile+'_common.tsv','w')
    outfile_null=open(origfile+'_null.tsv','w')
    outfile_polymorphic=open(origfile+'_polymorphic.tsv','w')

    for ultrarare in ultrarare_lines:
        outfile_ultrarare.write(ultrarare)
    for rare in rare_lines:
        outfile_rare.write(rare)
    for common in common_lines:
        outfile_common.write(common)
    for null in null_lines:
        outfile_null.write(null)
    for polymorphic in polymorphic_lines:
        outfile_polymorphic.write(polymorphic)

    outfile_rare.close()
    outfile_common.close()
    outfile_null.close()
    outfile_polymorphic.close()



def get_maf(vcfsplit,mafcol):
    print('get_maf()')
    mafvalue=vcfsplit[mafcol].strip()
    print('mafvalue=%s' % mafvalue)
    if (mafvalue is None) or (mafvalue is '.') or (mafvalue is ''):
        print('.')
        return '.'
    else:
        maf_freq=float(mafvalue)
        print(str(maf_freq))
        return maf_freq


def filter_esp_thousand(csvfile):
    print('\nfilter_esp_thousand()')
    print('csvfile=%s' % (csvfile))
    ultrarare=''
    rare=''
    common=''
    null=''
    polymorphic=''
    truemaf=None

    #with open(csvfile,'r') as csvfile_open:
        #csvlines=csvfile_open.readlines()


    csvlist=list()
    with open(csvfile,'r') as csvfile_open:
        csvreader=csv.reader(csvfile_open, delimiter=',',quotechar='"')
        for csvrow in csvreader:
            print('csvrow=%s\n' % (csvrow))
            csvlist.append(csvrow)


    for csvrow in csvlist:
        if csvrow[0].startswith('Func'):
            headersplit=csvrow
            headertsv=','.join(headersplit)+'\n'
            null+=headertsv
            ultrarare+=headertsv
            rare+=headertsv
            common+=headertsv
            polymorphic+=headertsv
        else:
            csvsplit=csvrow
            csvfilter=csvsplit[VCFFILTERCOL]
            if 'PASS' in csvfilter:
                espmaf='a'
                thousandmaf='a'
                truemaf='a'
                espmaf=get_maf(csvsplit,ESPMAFCOL)
                thousandmaf=get_maf(csvsplit,THOUSANDMAFCOL)
                #print('\nespmaf=%s' % (espmaf))
                #print('thousandmaf=%s' % (thousandmaf))
                #GET VALUES
                if espmaf=='.' and thousandmaf=='.':
                    truemaf='dot'
                elif espmaf=='.'  and isinstance(thousandmaf,float):
                    truemaf=thousandmaf
                elif isinstance(espmaf,float) and thousandmaf=='.':
                    truemaf=espmaf
                elif isinstance(espmaf,float) and isinstance(thousandmaf,float):
                    if thousandmaf>=espmaf:
                        truemaf=thousandmaf
                    else:
                        truemaf=espmaf
                #print('truemaf=%s' % (truemaf))
                #print('csvline=%s' % (csvline))
                #GROUP VALUES
                if truemaf is 'dot':
                    null+=','.join(csvsplit)+'\n'
                elif truemaf < MAF_FREQ_ULTRA:
                    ultrarare+=','.join(csvsplit)+'\n'
                elif truemaf >= MAF_FREQ_RARE_LOW and truemaf <= MAF_FREQ_RARE_HIGH:
                    rare+=','.join(csvsplit)+'\n'
                elif truemaf >= MAF_FREQ_COMMON_LOW and truemaf <= MAF_FREQ_COMMON_HIGH:
                    common+=','.join(csvsplit)+'\n'
                else:
                    polymorphic+=','.join(csvsplit)+'\n'
    #WRITE VALUES
    origfile,origext=os.path.splitext(os.path.basename(csvfile))
    combfile=origfile+'_COMB'+origext
    write_maf_to_tsv(ultrarare,rare,common,null,polymorphic,combfile)
    return ultrarare,rare,common,null,polymorphic

            

'''
    for csvline in csvlines:
        if csvline.startswith('Func'):
            headersplit=csvline.split(',')
            headertsv='\t'.join(headersplit)
            null+=headertsv
            ultrarare+=headertsv
            rare+=headertsv
            common+=headertsv
            polymorphic+=headertsv
        else:
            csvsplit=csvline.split(',')
            csvfilter=csvsplit[VCFFILTERCOL]
            if 'PASS' in csvfilter:
                espmaf='a'
                thousandmaf='a'
                truemaf='a'
                espmaf=get_maf(csvsplit,ESPMAFCOL)
                thousandmaf=get_maf(csvsplit,THOUSANDMAFCOL)
                print('\nespmaf=%s' % (espmaf))
                print('thousandmaf=%s' % (thousandmaf))
                #GET VALUES
                if espmaf=='.' and thousandmaf=='.':
                    truemaf='dot'
                elif espmaf=='.'  and isinstance(thousandmaf,float):
                    truemaf=thousandmaf
                elif isinstance(espmaf,float) and thousandmaf=='.':
                    truemaf=espmaf
                elif isinstance(espmaf,float) and isinstance(thousandmaf,float):
                    if thousandmaf>=espmaf:
                        truemaf=thousandmaf
                    else:
                        truemaf=espmaf
                print('truemaf=%s' % (truemaf))
                print('csvline=%s' % (csvline))
                #GROUP VALUES
                if truemaf is 'dot':
                    null+='\t'.join(csvsplit)
                elif truemaf < MAF_FREQ_ULTRA:
                    ultrarare+='\t'.join(csvsplit)
                elif truemaf >= MAF_FREQ_RARE_LOW and truemaf <= MAF_FREQ_RARE_HIGH:
                    rare+='\t'.join(csvsplit)
                elif truemaf >= MAF_FREQ_COMMON_LOW and truemaf <= MAF_FREQ_COMMON_HIGH:
                    common+='\t'.join(csvsplit)
                else:
                    polymorphic+='\t'.join(csvsplit)
    #WRITE VALUES
    origfile,origext=os.path.splitext(os.path.basename(csvfile))
    combfile=origfile+'_COMB'+origext
    write_maf_to_tsv(ultrarare,rare,common,null,polymorphic,combfile)
    return ultrarare,rare,common,null,polymorphic
'''

def filter_esp(csvfile):
    print('\nfilter_esp()')
    print('csvfile=%s' % (csvfile))
    ultrarare=''
    rare=''
    common=''
    null=''
    polymorphic=''
    truemaf=None
    #with open(csvfile) as csvfile_open:
        #csvlines=csvfile_open.readlines()

    csvlist=list()
    with open(csvfile,'r') as csvfile_open:
        csvreader=csv.reader(csvfile_open, delimiter=',',quotechar='"')
        for csvrow in csvreader:
            #print('csvrow=%s\n' % (csvrow))
            csvlist.append(csvrow)

            
        
    for csvrow in csvlist:
        if csvrow[0].startswith('Func'):
            headersplit=csvrow
            headertsv=','.join(headersplit)+'\n'
            null+=headertsv
            ultrarare+=headertsv
            rare+=headertsv
            common+=headertsv
            polymorphic+=headertsv
        else:
            csvsplit=csvrow
            csvfilter=csvsplit[VCFFILTERCOL]
            if 'PASS' in csvfilter:
                espmaf='a'
                truemaf='a'
                espmaf=None
                espmaf=get_maf(csvsplit,ESPMAFCOL)
                #GET VALUES
                #print('espmaf=%s' % (espmaf))
                if espmaf=='.':
                    truemaf='dot'
                elif isinstance(espmaf,float):
                    truemaf=espmaf
                #print('truemaf=%s' % (truemaf))
                #GROUP VALUES
                if truemaf=='dot':
                    null+=','.join(csvsplit)+'\n'
                elif truemaf < MAF_FREQ_ULTRA:
                    ultrarare+=','.join(csvsplit)+'\n'
                elif truemaf >= MAF_FREQ_RARE_LOW and truemaf <= MAF_FREQ_RARE_HIGH:
                    #print('isarare')
                    #print(csvsplit)
                    rare+=','.join(csvsplit)+'\n'
                elif truemaf >= MAF_FREQ_COMMON_LOW and truemaf <= MAF_FREQ_COMMON_HIGH:
                    common+=','.join(csvsplit)+'\n'
                else:
                    polymorphic+=','.join(csvsplit)+'\n'
    #WRITE VALUES
    origfile,origext=os.path.splitext(os.path.basename(csvfile))
    espfile=origfile+'_ESP'+origext
    write_maf_to_tsv(ultrarare,rare,common,null,polymorphic,espfile)
    return ultrarare,rare,common,null,polymorphic


def filter_thousand(csvfile):
    print('\nfilter_thousand()')
    print('csvfile=%s' % (csvfile))
    ultrarare=''
    rare=''
    common=''
    null=''
    polymorphic=''
    truemaf=None
    with open(csvfile) as csvfile_open:
        csvlines=csvfile_open.readlines()
  
    for csvline in csvlines:
        if csvline.startswith('Func'):
            headersplit=csvline.split(',')
            headertsv='\t'.join(headersplit)
            null+=headertsv
            ultrarare+=headertsv
            rare+=headertsv
            common+=headertsv
            polymorphic+=headertsv
        else:
            csvsplit=csvline.split(',')
            csvfilter=csvsplit[VCFFILTERCOL]
            if 'PASS' in csvfilter:
                thousandmaf='a'
                truemaf='a'
                thousandmaf=get_maf(csvsplit,THOUSANDMAFCOL)
                print('thousandmaf=%s' % (thousandmaf))
                #GET VALUES
                if thousandmaf=='.':
                    truemaf='dot'
                elif isinstance(thousandmaf,float):
                    truemaf=thousandmaf
                print('truemaf=%s' % (truemaf))
                #GROUP VALUES
                if truemaf=='dot':
                    null+='\t'.join(csvsplit)
                elif truemaf < MAF_FREQ_ULTRA:
                    ultrarare+='\t'.join(csvsplit)
                elif truemaf >= MAF_FREQ_RARE_LOW and truemaf <= MAF_FREQ_RARE_HIGH:
                    print('isarare')
                    rare+='\t'.join(csvsplit)
                elif truemaf >= MAF_FREQ_COMMON_LOW and truemaf <= MAF_FREQ_COMMON_HIGH:
                    common+='\t'.join(csvsplit)
                else:
                    polymorphic+='\t'.join(csvsplit)
    #WRITE VALUES
    
    tsvnametemplate=csvfile
    origfile,origext=os.path.splitext(os.path.basename(csvfile))
    thousandfile=origfile+'_THOUSAND'+origext
    write_maf_to_tsv(ultrarare,rare,common,null,polymorphic,thousandfile)
    return ultrarare,rare,common,null,polymorphic




def main(csvdir):
    csvlist=get_csv_list(csvdir)
    print('csvlist=%s' % (csvlist))
    print('len(csvlist)=%s' % (len(csvlist)))
    for csvfile in csvlist:
        if 'SNPs' in csvfile:
            #newultra,newrare,newcommon,newnull,newpolymorphic=filter_esp_thousand(csvfile)
            newultra,newrare,newcommon,newnull,newpolymorphic=filter_esp(csvfile)
            #newultra,newrare,newcommon,newnull,newpolymorphic=filter_thousand(csvfile)
        elif 'INDELs' in csvfile:
            pass
            #newultra,newrare,newcommon,newnull,newpolymorphic=filter_esp_thousand(csvfile)
            #newultra,newrare,newcommon,newnull,newpolymorphic=filter_esp(csvfile)
            #newultra,newrare,newcommon,newnull,newpolymorphic=filter_thousand(csvfile)


if __name__=='__main__':
    main(csvdir)
