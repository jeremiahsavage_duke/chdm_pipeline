#!/usr/bin/env python3
import argparse
import os

CHR_COL=0
POS_COL=1
COUNTOF_COL=2
COUNT_COL=3
FREQ_COL=4
SAMPLE_COL=5
GENOTYPE_COL=6
HET_COL=7
HOM_COL=8

parser=argparse.ArgumentParser('compare case and control')
parser.add_argument('-c','--casetable',required=True)
parser.add_argument('-n','--controltable',required=True)
args=vars(parser.parse_args())

casetablefile=args['casetable']
controltablefile=args['controltable']

def get_control_values(case_chr,case_bp,case_freq,controlinlines):
    for controlinline in controlinlines:
        controlinline=controlinline.strip('\n')
        controlsplit=controlinline.split('\t')
        control_chr=controlsplit[CHR_COL]
        control_bp=controlsplit[POS_COL]
        c_sample=controlsplit[SAMPLE_COL]
        c_genotype=controlsplit[GENOTYPE_COL]
        c_het=controlsplit[HET_COL]
        c_hom=controlsplit[HOM_COL]
        if control_chr==case_chr and control_bp==case_bp:
            c_count_of=controlsplit[COUNTOF_COL]
            c_count=controlsplit[COUNT_COL]
            c_freq=float(controlsplit[FREQ_COL])
            if case_freq < c_freq:
                percent_of= -100 * (c_freq/case_freq)
            elif case_freq > c_freq:
                percent_of= 100 * (case_freq/c_freq)
            elif case_freq == c_freq:
                percent_of= 100
            else:
                percent_of='huh\n'
            return percent_of,controlinline
            #return c_count,c_freq,percent_of,c_sample,c_genotype,c_genotype,c_het,c_hom
    sys.exit('Why here?')


def main(casetablefile,controltablefile):
    with open(casetablefile,'r') as casefile:
        caseinlines=casefile.readlines()

    with open(controltablefile,'r') as controlfile:
        controlinlines=controlfile.readlines()

    caseorigfile,origext=os.path.splitext(os.path.basename(casetablefile))
    sampleoutfile=caseorigfile+'_v_control'+origext    
    sampleoutfile_open=open(sampleoutfile,'w')

    control_keys=set()
    for controlinline in controlinlines:
         controlinline=controlinline.strip()
         controlsplit=controlinline.split('\t')
         control_chr=controlsplit[CHR_COL]
         control_bp=controlsplit[POS_COL]
         control_keys.add((control_chr,control_bp))

    case_keys=set()
    for caseinline in caseinlines:
        caseinline=caseinline.strip()
        casesplit=caseinline.split('\t')
        case_chr=casesplit[0]
        case_bp=casesplit[1]
        case_keys.add((case_chr,case_bp))

    for caseinline in caseinlines:
        caseinline=caseinline.strip()
        casesplit=caseinline.split('\t')
        case_chr=casesplit[CHR_COL]
        case_bp=casesplit[POS_COL]
        case_count_of=casesplit[COUNTOF_COL]
        case_count=casesplit[COUNT_COL]
        case_freq=float(casesplit[FREQ_COL])
        case_sample=casesplit[SAMPLE_COL]
        case_genotype=casesplit[GENOTYPE_COL]
        case_het=casesplit[HET_COL]
        case_hom=casesplit[HOM_COL]

        if (case_chr,case_bp) in control_keys:
            percent_of,controlinline=get_control_values(case_chr,case_bp,case_freq,controlinlines)
            #c_count,c_freq,percent_of,c_sample,c_genotype,c_genotype,c_het,c_hom=get_control_values(case_chr,case_bp,case_freq,controlinlines)
            #newline=caseinline+'\t'+c_count+'\t'+str(c_freq)+'\t'+str(percent_of)+'\t'+c_sample+'\t'+c_genotype+'\t'+c_het+'\t'+c_hom+'\n'
            newline=caseinline+'\t'+str(percent_of)+'\t'+controlinline+'\n'
            sampleoutfile_open.write(newline)
        else:
            newline=caseinline+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t' \
                     +'NA'+'\n'
            sampleoutfile_open.write(newline)

    for controlinline in controlinlines:
        controlinline=controlinline.strip()
        controlsplit=controlinline.split('\t')
        control_chr=controlsplit[CHR_COL]
        control_bp=controlsplit[POS_COL]
        control_count_of=controlsplit[COUNTOF_COL]
        control_count=controlsplit[COUNT_COL]
        control_freq=controlsplit[FREQ_COL]
        control_sample=controlsplit[SAMPLE_COL]
        control_genotype=controlsplit[GENOTYPE_COL]
        control_het=controlsplit[HET_COL]
        control_hom=controlsplit[HOM_COL]

        if (control_chr,control_bp) in case_keys:
            pass
        else:
            newline=control_chr+'\t'+control_bp+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+'NA'+'\t'+controlinline+'\n'
            sampleoutfile_open.write(newline)

    sampleoutfile_open.close()
        


if __name__=='__main__':
    main(casetablefile,controltablefile)
