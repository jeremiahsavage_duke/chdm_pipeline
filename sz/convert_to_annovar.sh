#!/usr/bin/env bash
for f in ~/dsz_all/*.vcf
do
    echo reading "${f}"
    filename=`basename ${f}`
    origname="${filename%.*}"
    echo creating "${origname}.annovar"
    ~/tools/annovar/convert2annovar.pl -format vcf4 -outfile "${origname}.annovar" "${f}" --includeinfo
done
