#!/usr/bin/env python3
import argparse
import csv
import operator
import os
import natsort

parser=argparse.ArgumentParser('filter vcfs by MAF')
parser.add_argument('-f','--vcfinput',required=True)
args=vars(parser.parse_args())

vcfinput=args['vcfinput']


def main(vcfinput):
    with open(vcfinput,'r') as vcffile:
        vcfreader=csv.reader(vcffile,delimiter='\t',quotechar='"')
        list1=sorted(vcfreader,key=operator.itemgetter(21,22))

    origfile,origext=os.path.splitext(os.path.basename(vcfinput))
    vcfoutput=origfile+'_sorted'+origext
    
    with open(vcfoutput,'w') as vcfout:
        vcfwriter=csv.writer(vcfout,delimiter='\t',quotechar='"')
        vcfwriter.writerows(list1)


if __name__=='__main__':
    main(vcfinput)
