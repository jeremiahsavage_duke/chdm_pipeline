#!/usr/bin/env python3
import argparse
import itertools
import os

parser=argparse.ArgumentParser('merge sorted and count freq files')
parser.add_argument('-t','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']

def main(tsvfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    origfile,origext=os.path.splitext(tsvfile)
    outfile=origfile+'_final'+origext
    outfile_open=open(outfile,'w')
    
    for tsvline in tsvlines:
        tsvsplit=tsvline.split('\t')
        del tsvsplit[56] #sample
        del tsvsplit[55] #genotype
        del tsvsplit[54] #format
        del tsvsplit[53] #filter
        del tsvsplit[52] #score
        del tsvsplit[51] #alt
        del tsvsplit[50] #ref
        del tsvsplit[49] #SOMETHING
        del tsvsplit[48] #pos
        del tsvsplit[47] #chr
        #del tsvsplit[44] #alt
        #del tsvsplit[43] #ref
        del tsvsplit[44] #pos
        del tsvsplit[43] #pos
        del tsvsplit[42] #chr
        del tsvsplit[12] #control_count_of
        del tsvsplit[11] #control_bp
        del tsvsplit[10] #control_chr
        del tsvsplit[2] #case_count_of

        newline='\t'.join(tsvsplit)+'\n'
        outfile_open.write(newline)

    outfile_open.close()

if __name__=='__main__':
    main(tsvfile)
