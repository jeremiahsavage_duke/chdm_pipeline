#!/usr/bin/env python3
import argparse
import os
import sys

CHROMOSOME=0
POSITION=1
REF_ALLELE=3
ALT_ALLELE=4
INFO=7
GENOTYPE=9
PATIENT_SAMPLE=10
PATIENT_COUNT_OF=13
CASE_ALLELE_COUNT=14
CASE_ALLELE_FREQ=15
CONTROL_ALLELE_COUNT=16
CONTROL_ALLELE_FREQ=17
PERCENT_OF=18
GENE=19
AA=20
RFG=21
PHYLOP_SCORE=22
PHYLOP_PRED=23
SIFT_SCORE=24
SIFT_PRED=25
POLY2_SCORE=26
POLY2_PRED=27
LRT_SCORE=28
LRT_PRED=29
MUT_TAST_SCORE=30
MUT_TAST_PRED=31
MUT_TAST_FUNC_COMB_SCORE=32
MUT_TAST_FUNC_IMPACT=33
HGMD_TYPE=34
HGMD_CLASS=35
HGMD_DISEASE=36
HGMD_PMID=37
SWISS_PROT_FUNC=38
SWISS_PROT_DIS_ASSOC=39
SWISS_PROT_POST_TRANSL_MOD=40
SWISS_PROT_EXPR=41

parser=argparse.ArgumentParser('consolidate allels to single row')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

        
    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_shrink'+origext    
    tsvoutfile_open=open(tsvoutfile,'w')



    newtsvlines=''

    chr_list=list()
    pos_list=list()
    ref_list=list()
    alt_list=list()
    info_list=list()
    genotype_list=list()
    sample_list=list()
    count_of_list=list()
    allele_count_list=list()
    allele_freq_list=list()
    control_allele_count_list=list()
    control_allele_freq_list=list()
    percent_of_list=list()
    gene_list=list()
    aa_list=list()
    rfg_list=list()
    phylop_score_list=list()
    phylop_pred_list=list()
    sift_score_list=list()
    sift_pred_list=list()
    poly2_score_list=list()
    poly2_pred_list=list()
    lrt_score_list=list()
    lrt_pred_list=list()
    mut_tast_score_list=list()
    mut_tast_pred_list=list()
    mut_tast_func_comb_score_list=list()
    mut_tast_func_impact_list=list()
    hgmd_type_list=list()
    hgmd_class_list=list()
    hgmd_disease_list=list()
    hgmd_pmid_list=list()
    swiss_prot_func_list=list()
    swiss_prot_dis_assoc_list=list()
    swiss_prot_post_trans_mod_list=list()
    swiss_prot_expr_list=list()
    
    for tsvinline in tsvinlines:
        tsvsplit=tsvinline.split('\t')
        if tsvsplit[0]=='chr':
            header='chr'+'\t'+'pos'+'\t'+'ref'+'\t'+'alt'+'\t'+'sample'+'\t'+'genotype'+'\t'+'info'+'\t'+'count_of'+'\t'+'case_allele_count'+'\t'+ \
                'case_allele_freq'+'\t'+'control_allele_count'+'\t'+'control_allele_freq'+'\t'+'percent_of'+'\t'+'gene'+'\t'+'aa'+'\t'+'rfg'+'\t'+'phylop_score'+'\t'+'phylop_pred'+'\t'+ \
                'sift_score'+'\t'+'sift_pred'+'\t'+'polyphen2_score'+'\t'+'polyphen2_pred'+'\t'+'lrt_score'+'\t'+'lrt_pred'+'\t'+ \
                'mut_tast_score'+'\t'+'mut_tast_pred'+'\t'+'mut_taster_functional_impact_combined_score'+'\t'+ \
                'mut_taster_variant_functional_impact'+'\t'+'hgmd_mutation_type'+'\t'+'hgmd_variant_class'+'\t'+'hgmd_disease'+'\t'+ \
                'hgmd_pubmed_id'+'\t'+'swiss_prot_function'+'\t'+'swiss_prot_disease_assoc'+'\t'+ \
                'swiss_prot_post_translational_modifications'+'\t'+'swiss_prot_expression'+'\n'
            newtsvlines+=header
            continue
        this_case_patient_count_of=tsvsplit[PATIENT_COUNT_OF]
        this_case_allele_count=tsvsplit[CASE_ALLELE_COUNT]
        print('this_case_allele_count=%s' % (this_case_allele_count))
        print('this_case_patient_count_of=%s' % (this_case_patient_count_of))

        if int(this_case_allele_count)==1:
            this_case_chr=tsvsplit[CHROMOSOME]
            this_case_pos=tsvsplit[POSITION]
            this_case_ref=tsvsplit[REF_ALLELE]
            this_case_alt=tsvsplit[ALT_ALLELE]
            this_case_info=tsvsplit[INFO]
            this_case_genotype=tsvsplit[GENOTYPE]
            this_case_sample=tsvsplit[PATIENT_SAMPLE]
            this_case_count_of=tsvsplit[PATIENT_COUNT_OF]
            this_case_allele_count=tsvsplit[CASE_ALLELE_COUNT]
            this_case_allele_freq=tsvsplit[CASE_ALLELE_FREQ]
            this_case_control_allele_count=tsvsplit[CONTROL_ALLELE_COUNT]
            this_case_control_allele_freq=tsvsplit[CONTROL_ALLELE_FREQ]
            this_case_percent_of=tsvsplit[PERCENT_OF]
            this_case_gene=tsvsplit[GENE]
            print('\nthis_case_pos=%s' % (this_case_pos))
            print('this_case_info=%s' % (this_case_info))
            print('this_case_gene=%s' % (this_case_gene))
            this_case_aa=tsvsplit[AA]
            this_case_rfg=tsvsplit[RFG]
            this_case_phylop_score=tsvsplit[PHYLOP_SCORE]
            this_case_phylop_pred=tsvsplit[PHYLOP_PRED]
            this_case_sift_score=tsvsplit[SIFT_SCORE]
            this_case_sift_pred=tsvsplit[SIFT_PRED]
            this_case_poly2_score=tsvsplit[POLY2_SCORE]
            this_case_poly2_pred=tsvsplit[POLY2_PRED]
            this_case_lrt_score=tsvsplit[LRT_SCORE]
            this_case_lrt_pred=tsvsplit[LRT_PRED]
            this_case_mut_tast_score=tsvsplit[MUT_TAST_SCORE]
            this_case_mut_tast_pred=tsvsplit[MUT_TAST_PRED]
            this_case_mut_tast_func_comb_score=tsvsplit[MUT_TAST_FUNC_COMB_SCORE]
            this_case_mut_tast_func_impact=tsvsplit[MUT_TAST_FUNC_IMPACT]
            this_case_hgmd_type=tsvsplit[HGMD_TYPE]
            this_case_hgmd_class=tsvsplit[HGMD_CLASS]
            this_case_hgmd_disease=tsvsplit[HGMD_DISEASE]
            this_case_hgmd_pmid=tsvsplit[HGMD_PMID]
            this_case_swiss_func=tsvsplit[SWISS_PROT_FUNC]
            this_case_swiss_disease=tsvsplit[SWISS_PROT_DIS_ASSOC].strip()
            this_case_swiss_post_trans=tsvsplit[SWISS_PROT_POST_TRANSL_MOD].strip()
            this_case_swiss_expr=tsvsplit[SWISS_PROT_EXPR].strip()

            chr_list.append(this_case_chr)
            pos_list.append(this_case_pos)
            ref_list.append(this_case_ref)
            alt_list.append(this_case_alt)

            info_list.append(this_case_info)
            genotype_list.append(this_case_genotype)
            sample_list.append(this_case_sample)
            count_of_list.append(this_case_count_of)
            allele_count_list.append(this_case_allele_count)
            allele_freq_list.append(this_case_allele_freq)
            control_allele_count_list.append(this_case_control_allele_count)
            control_allele_freq_list.append(this_case_control_allele_freq)
            percent_of_list.append(this_case_percent_of)
            gene_list.append(this_case_gene)
            aa_list.append(this_case_aa)
            rfg_list.append(this_case_rfg)
            phylop_score_list.append(this_case_phylop_score)
            phylop_pred_list.append(this_case_phylop_pred)
            sift_score_list.append(this_case_sift_score)
            sift_pred_list.append(this_case_sift_pred)
            poly2_score_list.append(this_case_poly2_score)
            poly2_pred_list.append(this_case_poly2_pred)
            lrt_score_list.append(this_case_lrt_score)
            lrt_pred_list.append(this_case_lrt_pred)
            mut_tast_score_list.append(this_case_mut_tast_score)
            mut_tast_pred_list.append(this_case_mut_tast_pred)
            mut_tast_func_comb_score_list.append(this_case_mut_tast_func_comb_score)
            mut_tast_func_impact_list.append(this_case_mut_tast_func_impact)
            hgmd_type_list.append(this_case_hgmd_type)
            hgmd_class_list.append(this_case_hgmd_class)
            hgmd_disease_list.append(this_case_hgmd_disease)
            hgmd_pmid_list.append(this_case_hgmd_pmid)
            swiss_prot_func_list.append(this_case_swiss_func)
            swiss_prot_dis_assoc_list.append(this_case_swiss_disease)
            swiss_prot_post_trans_mod_list.append(this_case_swiss_post_trans)
            swiss_prot_expr_list.append(this_case_swiss_expr)


            newtsvline='/'.join(chr_list)+'\t'+'/'.join(pos_list)+'\t'+'/'.join(ref_list)+'\t'+'/'.join(alt_list)+'\t'+ \
                         '/'.join(sample_list)+'\t'+';'.join(genotype_list)+'\t'+'###############@@@@@@@@@@@@@@@###############'.join(info_list)+'\t'+ '/'.join(count_of_list)+'\t'+'/'.join(allele_count_list)+'\t'+ \
                         '/'.join(allele_freq_list)+'\t'+'/'.join(control_allele_count_list)+'\t'+'/'.join(control_allele_freq_list)+'\t'+'/'.join(percent_of_list)+'\t'+'/'.join(gene_list)+'\t'+ \
                         '/'.join(aa_list)+'\t'+'/'.join(rfg_list)+'\t'+'/'.join(phylop_score_list)+'\t'+'/'.join(phylop_pred_list)+'\t'+ \
                         '/'.join(sift_score_list)+'\t'+'/'.join(sift_pred_list)+'\t'+'/'.join(poly2_score_list)+'\t'+ \
                         '/'.join(poly2_pred_list)+'\t'+'/'.join(lrt_score_list)+'\t'+'/'.join(lrt_pred_list)+'\t'+ \
                         '/'.join(mut_tast_score_list)+'\t'+'/'.join(mut_tast_pred_list)+'\t'+'/'.join(mut_tast_func_comb_score_list)+'\t'+ \
                         '/'.join(mut_tast_func_impact_list)+'\t'+'/'.join(hgmd_type_list)+'\t'+'/'.join(hgmd_class_list)+'\t'+\
                         '/'.join(hgmd_disease_list)+'\t'+'/'.join(hgmd_pmid_list)+'\t'+'/'.join(swiss_prot_func_list)+'\t'+'/'.join(swiss_prot_dis_assoc_list)+'\t'+'/'.join(swiss_prot_post_trans_mod_list)+'\t'+'/'.join(swiss_prot_expr_list)+'\n'

            #newtsvline=chr_list+'\t'+pos_list+'\t'+ref_list+'\t'+alt_list+'\t'+ \
                         #sample_list+'\t'+count_of_list+'\t'+allele_count_list+'\t'+ \
                         #allele_freq_list+'\t'+percent_of_list+'\t'+gene_list+'\t'+ \
                         #aa_list+'\t'+rfg_list+'\t'+phylop_score_list+'\t'+phylop_pred_list+'\t'+ \
                         #sift_score_list+'\t'+sift_pred_list+'\t'+poly2_score_list+'\t'+ \
                         #poly2_pred_list+'\t'+lrt_score_list+'\t'+lrt_pred_list+'\t'+ \
                         #mut_tast_score_list+'\t'+mut_tast_pred_list+'\t'+mut_tast_func_comb_score_list+'\t'+ \
                         #mut_tast_func_impact_list+'\t'+hgmd_type_list+'\t'+hgmd_class_list+'\t'+\
                         #hgmd_disease_list+'\t'+hgmd_pmid_list+'\n'
            print('\n\nnewtsvline=%s\n\n' % (newtsvline))
            newtsvlines+=newtsvline

            del chr_list[:]
            del pos_list[:]
            del ref_list[:]
            del alt_list[:]
            del info_list[:]
            del genotype_list[:]
            del sample_list[:]
            del count_of_list[:]
            del allele_count_list[:]
            del allele_freq_list[:]
            del control_allele_count_list[:]
            del control_allele_freq_list[:]
            del percent_of_list[:]
            del gene_list[:]
            del aa_list[:]
            del rfg_list[:]
            del phylop_score_list[:]
            del phylop_pred_list[:]
            del sift_score_list[:]
            del sift_pred_list[:]
            del poly2_score_list[:]
            del poly2_pred_list[:]
            del lrt_score_list[:]
            del lrt_pred_list[:]
            del mut_tast_score_list[:]
            del mut_tast_pred_list[:]
            del mut_tast_func_comb_score_list[:]
            del mut_tast_func_impact_list[:]
            del hgmd_type_list[:]
            del hgmd_class_list[:]
            del hgmd_disease_list[:]
            del hgmd_pmid_list[:]
            del swiss_prot_func_list[:]
            del swiss_prot_dis_assoc_list[:]
            del swiss_prot_post_trans_mod_list[:]
            del swiss_prot_expr_list[:]



        elif int(this_case_patient_count_of)==1 and int(this_case_patient_count_of) < int(this_case_allele_count):
            this_case_chr=tsvsplit[CHROMOSOME]
            this_case_pos=tsvsplit[POSITION]
            this_case_ref=tsvsplit[REF_ALLELE]
            this_case_alt=tsvsplit[ALT_ALLELE]
            this_case_info=tsvsplit[INFO]
            this_case_genotype=tsvsplit[GENOTYPE]
            this_case_sample=tsvsplit[PATIENT_SAMPLE]
            this_case_count_of=tsvsplit[PATIENT_COUNT_OF]
            this_case_allele_count=tsvsplit[CASE_ALLELE_COUNT]
            this_case_allele_freq=tsvsplit[CASE_ALLELE_FREQ]
            this_case_control_allele_count=tsvsplit[CONTROL_ALLELE_COUNT]
            this_case_control_allele_freq=tsvsplit[CONTROL_ALLELE_FREQ]
            this_case_percent_of=tsvsplit[PERCENT_OF]
            this_case_gene=tsvsplit[GENE]
            print('\nthis_case_pos=%s' % (this_case_pos))
            print('this_case_info=%s' % (this_case_info))
            print('this_case_gene=%s' % (this_case_gene))
            this_case_aa=tsvsplit[AA]
            this_case_rfg=tsvsplit[RFG]
            this_case_phylop_score=tsvsplit[PHYLOP_SCORE].strip()
            this_case_phylop_pred=tsvsplit[PHYLOP_PRED].strip()
            this_case_sift_score=tsvsplit[SIFT_SCORE].strip()
            this_case_sift_pred=tsvsplit[SIFT_PRED].strip()
            this_case_poly2_score=tsvsplit[POLY2_SCORE].strip()
            this_case_poly2_pred=tsvsplit[POLY2_PRED].strip()
            this_case_lrt_score=tsvsplit[LRT_SCORE].strip()
            this_case_lrt_pred=tsvsplit[LRT_PRED].strip()
            this_case_mut_tast_score=tsvsplit[MUT_TAST_SCORE].strip()
            this_case_mut_tast_pred=tsvsplit[MUT_TAST_PRED].strip()
            this_case_mut_tast_func_comb_score=tsvsplit[MUT_TAST_FUNC_COMB_SCORE].strip()
            this_case_mut_tast_func_impact=tsvsplit[MUT_TAST_FUNC_IMPACT].strip()
            this_case_hgmd_type=tsvsplit[HGMD_TYPE].strip()
            this_case_hgmd_class=tsvsplit[HGMD_CLASS].strip()
            this_case_hgmd_disease=tsvsplit[HGMD_DISEASE].strip()
            this_case_hgmd_pmid=tsvsplit[HGMD_PMID].strip()
            this_case_swiss_func=tsvsplit[SWISS_PROT_FUNC].strip()
            this_case_swiss_disease=tsvsplit[SWISS_PROT_DIS_ASSOC].strip()
            this_case_swiss_post_trans=tsvsplit[SWISS_PROT_POST_TRANSL_MOD].strip()
            this_case_swiss_expr=tsvsplit[SWISS_PROT_EXPR].strip()

            chr_list.append(this_case_chr)
            pos_list.append(this_case_pos)
            ref_list.append(this_case_ref)
            alt_list.append(this_case_alt)
            
            info_list.append(this_case_info)
            genotype_list.append(this_case_genotype)
            sample_list.append(this_case_sample)
            count_of_list.append(this_case_count_of)
            allele_count_list.append(this_case_allele_count)
            allele_freq_list.append(this_case_allele_freq)
            control_allele_count_list.append(this_case_control_allele_count)
            control_allele_freq_list.append(this_case_control_allele_freq)
            percent_of_list.append(this_case_percent_of)
            gene_list.append(this_case_gene)
            aa_list.append(this_case_aa)
            rfg_list.append(this_case_rfg)
            phylop_score_list.append(this_case_phylop_score)
            phylop_pred_list.append(this_case_phylop_pred)
            sift_score_list.append(this_case_sift_score)
            sift_pred_list.append(this_case_sift_pred)
            poly2_score_list.append(this_case_poly2_score)
            poly2_pred_list.append(this_case_poly2_pred)
            lrt_score_list.append(this_case_lrt_score)
            lrt_pred_list.append(this_case_lrt_pred)
            mut_tast_score_list.append(this_case_mut_tast_score)
            mut_tast_pred_list.append(this_case_mut_tast_pred)
            mut_tast_func_comb_score_list.append(this_case_mut_tast_func_comb_score)
            mut_tast_func_impact_list.append(this_case_mut_tast_func_impact)
            hgmd_type_list.append(this_case_hgmd_type)
            hgmd_class_list.append(this_case_hgmd_class)
            hgmd_disease_list.append(this_case_hgmd_disease)
            hgmd_pmid_list.append(this_case_hgmd_pmid)
            swiss_prot_func_list.append(this_case_swiss_func)
            swiss_prot_dis_assoc_list.append(this_case_swiss_disease)
            swiss_prot_post_trans_mod_list.append(this_case_swiss_post_trans)
            swiss_prot_expr_list.append(this_case_swiss_expr)

           
        elif int(this_case_patient_count_of)>1 and int(this_case_patient_count_of) < int(this_case_allele_count):
            this_case_chr=tsvsplit[CHROMOSOME]
            this_case_pos=tsvsplit[POSITION]
            this_case_ref=tsvsplit[REF_ALLELE]
            this_case_alt=tsvsplit[ALT_ALLELE]
            this_case_info=tsvsplit[INFO]
            this_case_genotype=tsvsplit[GENOTYPE]
            this_case_sample=tsvsplit[PATIENT_SAMPLE]
            this_case_count_of=tsvsplit[PATIENT_COUNT_OF]
            this_case_allele_count=tsvsplit[CASE_ALLELE_COUNT]
            this_case_allele_freq=tsvsplit[CASE_ALLELE_FREQ]
            this_case_control_allele_count=tsvsplit[CONTROL_ALLELE_COUNT]
            this_case_control_allele_freq=tsvsplit[CONTROL_ALLELE_FREQ]
            this_case_percent_of=tsvsplit[PERCENT_OF]
            this_case_gene=tsvsplit[GENE]
            print('\nthis_case_pos=%s' % (this_case_pos))
            print('this_case_info=%s' % (this_case_info))
            print('this_case_gene=%s' % (this_case_gene))
            this_case_aa=tsvsplit[AA]
            this_case_rfg=tsvsplit[RFG]
            this_case_phylop_score=tsvsplit[PHYLOP_SCORE]
            this_case_phylop_pred=tsvsplit[PHYLOP_PRED]
            this_case_sift_score=tsvsplit[SIFT_SCORE]
            this_case_sift_pred=tsvsplit[SIFT_PRED]
            this_case_poly2_score=tsvsplit[POLY2_SCORE]
            this_case_poly2_pred=tsvsplit[POLY2_PRED]
            this_case_lrt_score=tsvsplit[LRT_SCORE]
            this_case_lrt_pred=tsvsplit[LRT_PRED]
            this_case_mut_tast_score=tsvsplit[MUT_TAST_SCORE]
            this_case_mut_tast_pred=tsvsplit[MUT_TAST_PRED]
            this_case_mut_tast_func_comb_score=tsvsplit[MUT_TAST_FUNC_COMB_SCORE]
            this_case_mut_tast_func_impact=tsvsplit[MUT_TAST_FUNC_IMPACT]
            this_case_hgmd_type=tsvsplit[HGMD_TYPE]
            this_case_hgmd_class=tsvsplit[HGMD_CLASS]
            this_case_hgmd_disease=tsvsplit[HGMD_DISEASE].strip()
            this_case_hgmd_pmid=tsvsplit[HGMD_PMID].strip()
            this_case_swiss_func=tsvsplit[SWISS_PROT_FUNC].strip()
            this_case_swiss_disease=tsvsplit[SWISS_PROT_DIS_ASSOC].strip()
            this_case_swiss_post_trans=tsvsplit[SWISS_PROT_POST_TRANSL_MOD].strip()
            this_case_swiss_expr=tsvsplit[SWISS_PROT_EXPR].strip()

            #chr_list.append(this_case_chr)
            #pos_list.append(this_case_pos)
            #ref_list.append(this_case_ref)
            #alt_list.append(this_case_alt)
            
            info_list.append(this_case_info)
            genotype_list.append(this_case_genotype)
            sample_list.append(this_case_sample)
            count_of_list.append(this_case_count_of)
            #allele_count_list.append(this_case_allele_count)
            allele_freq_list.append(this_case_allele_freq)
            #control_allele_count_list.append(this_case_control_allele_count)
            #control_allele_freq_list.append(this_case_control_allele_freq)
            percent_of_list.append(this_case_percent_of)
            gene_list.append(this_case_gene)
            aa_list.append(this_case_aa)
            rfg_list.append(this_case_rfg)
            phylop_score_list.append(this_case_phylop_score)
            phylop_pred_list.append(this_case_phylop_pred)
            sift_score_list.append(this_case_sift_score)
            sift_pred_list.append(this_case_sift_pred)
            poly2_score_list.append(this_case_poly2_score)
            poly2_pred_list.append(this_case_poly2_pred)
            lrt_score_list.append(this_case_lrt_score)
            lrt_pred_list.append(this_case_lrt_pred)
            mut_tast_score_list.append(this_case_mut_tast_score)
            mut_tast_pred_list.append(this_case_mut_tast_pred)
            mut_tast_func_comb_score_list.append(this_case_mut_tast_func_comb_score)
            mut_tast_func_impact_list.append(this_case_mut_tast_func_impact)
            hgmd_type_list.append(this_case_hgmd_type)
            hgmd_class_list.append(this_case_hgmd_class)
            hgmd_disease_list.append(this_case_hgmd_disease)
            hgmd_pmid_list.append(this_case_hgmd_pmid)
            #swiss_prot_func_list.append(this_case_swiss_func)
            #swiss_prot_dis_assoc_list.append(this_case_swiss_disease)
            #swiss_prot_post_trans_mod_list.append(this_case_swiss_post_trans)
            #swiss_prot_expr_list.append(this_case_swiss_expr)


        elif int(this_case_patient_count_of)>1 and int(this_case_patient_count_of) == int(this_case_allele_count):
            this_case_chr=tsvsplit[CHROMOSOME]
            this_case_pos=tsvsplit[POSITION]
            this_case_ref=tsvsplit[REF_ALLELE]
            this_case_alt=tsvsplit[ALT_ALLELE]
            this_case_info=tsvsplit[INFO]
            this_case_genotype=tsvsplit[GENOTYPE]
            this_case_sample=tsvsplit[PATIENT_SAMPLE]
            this_case_count_of=tsvsplit[PATIENT_COUNT_OF]
            this_case_allele_count=tsvsplit[CASE_ALLELE_COUNT]
            this_case_allele_freq=tsvsplit[CASE_ALLELE_FREQ]
            this_case_control_allele_count=tsvsplit[CONTROL_ALLELE_COUNT]
            this_case_control_allele_freq=tsvsplit[CONTROL_ALLELE_FREQ]
            this_case_percent_of=tsvsplit[PERCENT_OF]
            this_case_gene=tsvsplit[GENE]
            print('\nthis_case_pos=%s' % (this_case_pos))
            print('this_case_info=%s' % (this_case_info))
            print('this_case_gene=%s' % (this_case_gene))
            this_case_aa=tsvsplit[AA]
            this_case_rfg=tsvsplit[RFG]
            this_case_phylop_score=tsvsplit[PHYLOP_SCORE]
            this_case_phylop_pred=tsvsplit[PHYLOP_PRED]
            this_case_sift_score=tsvsplit[SIFT_SCORE]
            this_case_sift_pred=tsvsplit[SIFT_PRED]
            this_case_poly2_score=tsvsplit[POLY2_SCORE]
            this_case_poly2_pred=tsvsplit[POLY2_PRED]
            this_case_lrt_score=tsvsplit[LRT_SCORE]
            this_case_lrt_pred=tsvsplit[LRT_PRED]
            this_case_mut_tast_score=tsvsplit[MUT_TAST_SCORE]
            this_case_mut_tast_pred=tsvsplit[MUT_TAST_PRED]
            this_case_mut_tast_func_comb_score=tsvsplit[MUT_TAST_FUNC_COMB_SCORE]
            this_case_mut_tast_func_impact=tsvsplit[MUT_TAST_FUNC_IMPACT]
            this_case_hgmd_type=tsvsplit[HGMD_TYPE]
            this_case_hgmd_class=tsvsplit[HGMD_CLASS]
            this_case_hgmd_disease=tsvsplit[HGMD_DISEASE]
            this_case_hgmd_pmid=tsvsplit[HGMD_PMID].strip()
            this_case_swiss_func=tsvsplit[SWISS_PROT_FUNC].strip()
            this_case_swiss_disease=tsvsplit[SWISS_PROT_DIS_ASSOC].strip()
            this_case_swiss_post_trans=tsvsplit[SWISS_PROT_POST_TRANSL_MOD].strip()
            this_case_swiss_expr=tsvsplit[SWISS_PROT_EXPR].strip()

            #chr_list.append(this_case_chr)
            #pos_list.append(this_case_pos)
            #ref_list.append(this_case_ref)
            #alt_list.append(this_case_alt)

            info_list.append(this_case_info)
            genotype_list.append(this_case_genotype)
            sample_list.append(this_case_sample)
            count_of_list.append(this_case_count_of)
            #allele_count_list.append(this_case_allele_count)
            allele_freq_list.append(this_case_allele_freq)
            #control_allele_count_list.append(this_case_control_allele_count)
            #control_allele_freq_list.append(this_case_control_allele_freq)
            percent_of_list.append(this_case_percent_of)
            gene_list.append(this_case_gene)
            aa_list.append(this_case_aa)
            rfg_list.append(this_case_rfg)
            phylop_score_list.append(this_case_phylop_score)
            phylop_pred_list.append(this_case_phylop_pred)
            sift_score_list.append(this_case_sift_score)
            sift_pred_list.append(this_case_sift_pred)
            poly2_score_list.append(this_case_poly2_score)
            poly2_pred_list.append(this_case_poly2_pred)
            lrt_score_list.append(this_case_lrt_score)
            lrt_pred_list.append(this_case_lrt_pred)
            mut_tast_score_list.append(this_case_mut_tast_score)
            mut_tast_pred_list.append(this_case_mut_tast_pred)
            mut_tast_func_comb_score_list.append(this_case_mut_tast_func_comb_score)
            mut_tast_func_impact_list.append(this_case_mut_tast_func_impact)
            hgmd_type_list.append(this_case_hgmd_type)
            hgmd_class_list.append(this_case_hgmd_class)
            hgmd_disease_list.append(this_case_hgmd_disease)
            hgmd_pmid_list.append(this_case_hgmd_pmid)
            #swiss_prot_func_list.append(this_case_swiss_func)
            #swiss_prot_dis_assoc_list.append(this_case_swiss_disease)
            #swiss_prot_post_trans_mod_list.append(this_case_swiss_post_trans)
            #swiss_prot_expr_list.append(this_case_swiss_expr)



            newtsvline='/'.join(chr_list)+'\t'+'/'.join(pos_list)+'\t'+'/'.join(ref_list)+'\t'+'/'.join(alt_list)+'\t'+ \
                         '/'.join(sample_list)+'\t'+';'.join(genotype_list)+'\t'+'###############@@@@@@@@@@@@@@@###############'.join(info_list)+'\t'+'/'.join(count_of_list)+'\t'+'/'.join(allele_count_list)+'\t'+ \
                         allele_freq_list[0]+'\t'+'/'.join(control_allele_count_list)+'\t'+'/'.join(control_allele_freq_list)+'\t'+percent_of_list[0]+'\t'+'/'.join(gene_list)+'\t'+ \
                         '/'.join(aa_list)+'\t'+rfg_list[0]+'\t'+'/'.join(phylop_score_list)+'\t'+'/'.join(phylop_pred_list)+'\t'+ \
                         '/'.join(sift_score_list)+'\t'+'/'.join(sift_pred_list)+'\t'+'/'.join(poly2_score_list)+'\t'+ \
                         '/'.join(poly2_pred_list)+'\t'+'/'.join(lrt_score_list)+'\t'+'/'.join(lrt_pred_list)+'\t'+ \
                         '/'.join(mut_tast_score_list)+'\t'+'/'.join(mut_tast_pred_list)+'\t'+'/'.join(mut_tast_func_comb_score_list)+'\t'+ \
                         '/'.join(mut_tast_func_impact_list)+'\t'+'/'.join(hgmd_type_list)+'\t'+'/'.join(hgmd_class_list)+'\t'+\
                         '/'.join(hgmd_disease_list)+'\t'+'/'.join(hgmd_pmid_list)+'\t'+'/'.join(hgmd_pmid_list)+'\t'+'/'.join(swiss_prot_func_list)+'\t'+'/'.join(swiss_prot_dis_assoc_list)+'\t'+'/'.join(swiss_prot_post_trans_mod_list)+'\t'+'/'.join(swiss_prot_expr_list)+'\n'
            
            print('\n\nnewtsvline=%s\n\n' % (newtsvline))
            newtsvlines+=newtsvline
            
            del chr_list[:]
            del pos_list[:]
            del ref_list[:]
            del alt_list[:]
            del info_list[:]
            del genotype_list[:]
            del sample_list[:]
            del count_of_list[:]
            del allele_count_list[:]
            del allele_freq_list[:]
            del control_allele_count_list[:]
            del control_allele_freq_list[:]
            del percent_of_list[:]
            del gene_list[:]
            del aa_list[:]
            del rfg_list[:]
            del phylop_score_list[:]
            del phylop_pred_list[:]
            del sift_score_list[:]
            del sift_pred_list[:]
            del poly2_score_list[:]
            del poly2_pred_list[:]
            del lrt_score_list[:]
            del lrt_pred_list[:]
            del mut_tast_score_list[:]
            del mut_tast_pred_list[:]
            del mut_tast_func_comb_score_list[:]
            del mut_tast_func_impact_list[:]
            del hgmd_type_list[:]
            del hgmd_class_list[:]
            del hgmd_disease_list[:]
            del hgmd_pmid_list[:]
            del swiss_prot_func_list[:]
            del swiss_prot_dis_assoc_list[:]
            del swiss_prot_post_trans_mod_list[:]
            del swiss_prot_expr_list[:]


        else:
            sys.exit('Game Over')

    for newtsvline in newtsvlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
