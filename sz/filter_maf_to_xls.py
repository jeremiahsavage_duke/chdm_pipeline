#!/usr/bin/env python3
import argparse
import os
import glob
import xlwt

MAF_FREQ_01=0.001
MAF_FREQ_02_LOW=0.001
MAF_FREQ_02_HIGH=0.05
MAF_FREQ_03_LOW=0.01
MAF_FREQ_03_HIGH=0.05
THOUSANDMAFKEY='TEUR'
ESPREFCOUNT='ET'
ESPALTCOUNT='EV'
GENEKEY='GN'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('filter vcfs by MAF')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def get_info_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if datasplit[0].endswith(akey):
            return datasplit[1]

def get_maf_lt(vcflines,mafkey,maf_cutoff):
    keptvalues=''
    keptnulls=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
            keptnulls+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_value=get_info_value(mafkey,vcfinfo)
                gene_value=get_info_value(GENEKEY,vcfinfo)
                if gene_value is None:
                    gene_value=''
                if (maf_value is None) or (maf_value is '.'):
                    newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                    keptnulls+=newvcfline
                else:
                    maf_freq=float(maf_value)
                    if maf_freq <= maf_cutoff:
                        newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                        keptvalues+=newvcfline
    return keptvalues,keptnulls


def get_maf_count_lt(vcflines,maf_ref_key,maf_alt_key,maf_cutoff):
    keptvalues=''
    keptnulls=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
            keptnulls+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_ref_value=get_info_value(maf_ref_key,vcfinfo)
                maf_alt_value=get_info_value(maf_alt_key,vcfinfo)
                gene_value=get_info_value(GENEKEY,vcfinfo)
                if gene_value is None:
                    gene_value=''
                if (maf_ref_value is None) or (maf_ref_value is '.'):
                    newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                    keptnulls+=newvcfline
                else:
                    maf_ref_count=int(maf_ref_value)
                    maf_alt_count=int(maf_alt_value)
                    if maf_ref_count==0: # no divide by zero
                        maf_freq=999999999
                    else:
                        maf_freq=float(maf_alt_count)/float(maf_ref_count)
                    if maf_freq <= maf_cutoff:
                        newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                        keptvalues+=newvcfline
    return keptvalues,keptnulls


def get_maf_range(vcflines,maf_freq_key,maf_low_cutoff,maf_high_cutoff):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_value=get_info_value(maf_freq_key,vcfinfo)
                gene_value=get_info_value(GENEKEY,vcfinfo)
                if gene_value is None:
                    gene_value=''
                if (maf_value is None) or (maf_value is '.'):
                    continue
                else:
                    maf_freq=float(maf_value)
                    if (maf_freq >= maf_low_cutoff) and (maf_freq <= maf_high_cutoff):
                        newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                        keptvalues+=newvcfline
    return keptvalues


def get_maf_count_range(vcflines,maf_ref_key,maf_alt_key,maf_low_cutoff,maf_high_cutoff):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                maf_ref_value=get_info_value(maf_ref_key,vcfinfo)
                maf_alt_value=get_info_value(maf_alt_key,vcfinfo)
                gene_value=get_info_value(GENEKEY,vcfinfo)
                if gene_value is None:
                    gene_value=''
                if (maf_ref_value is None) or (maf_ref_value is '.'):
                    continue
                else:
                    maf_ref_count=int(maf_ref_value)
                    maf_alt_count=int(maf_alt_value)
                    if maf_ref_count==0: # no divide by zero
                        maf_freq=999999999
                    else:
                        maf_freq=float(maf_alt_count)/float(maf_ref_count)
                    if (maf_freq >= maf_low_cutoff) and (maf_freq <= maf_high_cutoff):
                        newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                        keptvalues+=newvcfline
    return keptvalues


def get_maf_gt(vcflines,mafkey,maf_cutoff):
    keptlines=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptlines+=vcfline
    return keptlines


def write_maf_to_xls(vcflines,xlwt_sheet):
    for rownum, vcfline in enumerate(vcflines.split('\n')):
        if vcfline.startswith('#'):
            xlwt_sheet.write(rownum,0,vcfline)
        else:
            vcfsplit=vcfline.split('\t')
            for colnum,vcffield in enumerate(vcfsplit):
                xlwt_sheet.write(rownum,colnum,vcffield)


def filter_thousand(vcffile):
    mafkey=THOUSANDMAFKEY
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    outfile_lt_01=xlwt.Workbook(encoding='utf-8')
    outfile_range_01=xlwt.Workbook(encoding='utf-8')
    outfile_range_02=xlwt.Workbook(encoding='utf-8')
    
    outfile_lt_01_sh01=outfile_lt_01.add_sheet('values')
    outfile_lt_01_sh02=outfile_lt_01.add_sheet('nulls')
    outfile_range_01_sh01=outfile_range_01.add_sheet('values')
    outfile_range_02_sh01=outfile_range_02.add_sheet('values')


    maf_value_lt_val01,maf_value_lt_null01=get_maf_lt(vcflines,mafkey,MAF_FREQ_01)
    maf_value_range_val01=get_maf_range(vcflines,mafkey,MAF_FREQ_02_LOW,MAF_FREQ_02_HIGH)
    maf_value_range_val02=get_maf_range(vcflines,mafkey,MAF_FREQ_03_LOW,MAF_FREQ_03_HIGH)

    write_maf_to_xls(maf_value_lt_val01,outfile_lt_01_sh01)
    write_maf_to_xls(maf_value_lt_null01,outfile_lt_01_sh02)
    write_maf_to_xls(maf_value_range_val01,outfile_range_01_sh01)
    write_maf_to_xls(maf_value_range_val02,outfile_range_02_sh01)


    outfile_lt_01.save(origfile+'_'+mafkey+'_low.xls')
    outfile_range_01.save(origfile+'_'+mafkey+'_range_low.xls')
    outfile_range_02.save(origfile+'_'+mafkey+'_range_high.xls')



def filter_esp(vcffile):
    mafrefcount=ESPREFCOUNT
    mafaltcount=ESPALTCOUNT
    mafkey=mafaltcount

    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    outfile_lt_01=xlwt.Workbook(encoding='utf-8')
    outfile_range_01=xlwt.Workbook(encoding='utf-8')
    outfile_range_02=xlwt.Workbook(encoding='utf-8')
    
    outfile_lt_01_sh01=outfile_lt_01.add_sheet('values')
    outfile_lt_01_sh02=outfile_lt_01.add_sheet('nulls')
    outfile_range_01_sh01=outfile_range_01.add_sheet('values')
    outfile_range_02_sh01=outfile_range_02.add_sheet('values')


    maf_value_lt_val01,maf_value_lt_null01=get_maf_count_lt(vcflines,mafrefcount,mafaltcount,MAF_FREQ_01)
    maf_value_range_val01=get_maf_count_range(vcflines,mafrefcount,mafaltcount,MAF_FREQ_02_LOW,MAF_FREQ_02_HIGH)
    maf_value_range_val02=get_maf_count_range(vcflines,mafrefcount,mafaltcount,MAF_FREQ_03_LOW,MAF_FREQ_03_HIGH)

    write_maf_to_xls(maf_value_lt_val01,outfile_lt_01_sh01)
    write_maf_to_xls(maf_value_lt_null01,outfile_lt_01_sh02)
    write_maf_to_xls(maf_value_range_val01,outfile_range_01_sh01)
    write_maf_to_xls(maf_value_range_val02,outfile_range_02_sh01)


    outfile_lt_01.save(origfile+'_'+mafkey+'_low.xls')
    outfile_range_01.save(origfile+'_'+mafkey+'_range_low.xls')
    outfile_range_02.save(origfile+'_'+mafkey+'_range_high.xls')



def make_clean():
    thousand_list=glob.glob('*_'+THOUSANDMAFKEY+'*_*.xls')
    esp_list=glob.glob('*_'+ESPALTCOUNT+'*_*.xls')
    for afile in thousand_list:
        os.remove(afile)
    for afile in esp_list:
        os.remove(afile)
    
def main(vcfdir):
    #make_clean()
    vcflist=get_vcf_list(vcfdir)
    for vcffile in vcflist:
        filter_thousand(vcffile)
        if 'SNPs' in vcffile:
            filter_esp(vcffile)


if __name__=='__main__':
    main(vcfdir)
