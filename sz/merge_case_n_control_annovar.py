#!/usr/bin/env python3
import argparse
import os
import sys


CASEDATA=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/case.dat')
CONTROLDATA=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/control.dat')
CASEOUTFILE='case.tsv'
CONTROLOUTFILE='control.tsv'
INPUTFILEKEY='.tsv'

parser=argparse.ArgumentParser('merge cases and merge controls')
parser.add_argument('-d','--tsvdirectory',required=True)
args=vars(parser.parse_args())

tsvdir=args['tsvdirectory']


def get_sample_list(filelocation):
    samplefile_open=open(filelocation,'r')
    samplelines=samplefile_open.readlines()
    samplefile_open.close()
    samplelist=samplelines[0].split(',')
    samplelist=list(map(int,samplelist))
    return samplelist


def get_tsv_list(vcfdir,filekey):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith(filekey):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def getpatientkey(tsvfile):
    patientkey=int(os.path.basename(tsvfile).split('_')[0])
    return patientkey


def add_vcflines_to_file(tsvfile,patientkey,appendfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    for tsvline in tsvlines:
        if tsvline.startswith('Func'):
            continue
        else:
            newline=tsvline.strip()+'\t'+str(patientkey)+'\n'
            appendfile.write(newline)


def first_add_vcflines_to_file(tsvfile,patientkey,appendfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    for tsvline in tsvlines:
        if tsvline.startswith('Func'):
            pass
            #newline=tsvline.strip()+'\t'+'pos'+'\t'+'id'+'\t'+'ref'+'\t'+'alt'+'\t'+'qual'+'\t'+'filter'+'\t'+'info'+'\t'+'format'+'\t'+'genotype'+'\t'+'patientkey'+'\n'
            #print('newline=%s' % (newline))
            #appendfile.write(newline)
        else:
            newline=tsvline.strip()+'\t'+str(patientkey)+'\n'
            appendfile.write(newline)


def main(tsvdir):
    filekey=INPUTFILEKEY
    caselist=get_sample_list(CASEDATA)
    controllist=get_sample_list(CONTROLDATA)
    tsvlist=get_tsv_list(tsvdir,filekey)
    mergegene_case=open(CASEOUTFILE,'w')
    mergegene_control=open(CONTROLOUTFILE,'w')

    case_tsvcount=0
    control_tsvcount=0
    for tsvfile in tsvlist:
        patientkey=getpatientkey(tsvfile)
        if patientkey in caselist:
            if case_tsvcount==0:
                case_tsvcount+=1
                first_add_vcflines_to_file(tsvfile,patientkey,mergegene_case)
            else:
                add_vcflines_to_file(tsvfile,patientkey,mergegene_case)
        elif patientkey in controllist:
            if control_tsvcount==0:
                control_tsvcount+=1
                first_add_vcflines_to_file(tsvfile,patientkey,mergegene_control)
            else:
                add_vcflines_to_file(tsvfile,patientkey,mergegene_control)
        else:
            print('patient %s' % (patientkey))
            sys.exit('Sample not in case/control lists!')
    mergegene_case.close()
    mergegene_control.close()



if __name__=='__main__':
    main(tsvdir)
