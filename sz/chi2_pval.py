#!/usr/bin/env python3
import argparse
import os
import sys
from scipy.stats import chi2_contingency
from numpy import array

CASE_HET=7
CASE_HOM=8
CONTROL_HET=17
CONTROL_HOM=18
CASE_COUNT=960
CONTROL_COUNT=960


parser=argparse.ArgumentParser('consolidate allels to single row')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def do_calc(case_count,control_count):
    cc_array=array([case_count,CASE_COUNT-case_count,control_count,CONTROL_COUNT-control_count]).reshape(2,-1)
    result=chi2_contingency(cc_array)
    res_chi2=result[0]
    res_pval=result[1]
    return res_chi2, res_pval
    

def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_pval'+origext    
    tsvoutfile_open=open(tsvoutfile,'w')

    newtsvlines=str()
    for tsvinline in tsvinlines:
        #print('tsvinline=%s' % tsvinline.strip())
        tsvsplit=tsvinline.split('\t')
        case_het=tsvsplit[CASE_HET].strip()
        case_hom=tsvsplit[CASE_HOM].strip()
        control_het=tsvsplit[CONTROL_HET].strip()
        control_hom=tsvsplit[CONTROL_HOM].strip()
        if case_het=='NA':
            case_count=0
            control_count=int(control_het)+2*int(control_hom)
            res_chi2,res_pval=do_calc(case_count,control_count)
            newline=tsvinline.strip()+'\t'+format(res_chi2,'.25f')+'\t'+format(res_pval,'.25f')+'\n'
            newtsvlines+=newline
        elif control_het=='NA':
            case_count=int(case_het)+2*int(case_hom)
            control_count=0
            res_chi2,res_pval=do_calc(case_count,control_count)
            newline=tsvinline.strip()+'\t'+format(res_chi2,'.25f')+'\t'+format(res_pval,'.25f')+'\n'
            newtsvlines+=newline
        else:
            case_count=int(case_het)+2*int(case_hom)
            control_count=int(control_het)+2*int(control_hom)
            res_chi2,res_pval=do_calc(case_count,control_count)
            newline=tsvinline.strip()+'\t'+format(res_chi2,'.25f')+'\t'+format(res_pval,'.25f')+'\n'
            newtsvlines+=newline

    for newtsvline in newtsvlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
