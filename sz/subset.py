#!/usr/bin/env python3
import argparse
import os
import shutil

CASESAMPLES=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/case.dat')
CONTROLSAMPLES=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/control.dat')
INPUTFILEKEY='.vcf'
SUBSETSIZE=15

parser=argparse.ArgumentParser('generate a small subset for work')
parser.add_argument('-d','--vcfdir',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdir']


def get_sample_list(filelocation):
    samplefile_open=open(filelocation,'r')
    samplelines=samplefile_open.readlines()
    samplefile_open.close()
    samplelist=samplelines[0].split(',')
    samplelist=list(map(int,samplelist))
    return samplelist


def get_vcf_list(vcfdir,filekey):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith(filekey):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def getpatientkey(vcffile):
    patientkey=int(os.path.basename(vcffile).split('_')[0])
    return patientkey


def dosubset(vcflist,caselist,controllist,samplecount):
    casecount=0
    caseextra=0
    controlcount=0
    controlextra=0
    for vcffile in vcflist:
        patientkey=getpatientkey(vcffile)
        if patientkey in caselist:
            if casecount>=samplecount:
                caseextra+=1
                continue
            else:
                casecount+=1
                shutil.copy(vcffile,'.')
        elif patientkey in controllist:
            if controlcount>=samplecount:
                controlextra+=1
                continue
            else:
                controlcount+=1
                shutil.copy(vcffile,'.')

def main(tsvdir):
    filekey=INPUTFILEKEY
    caselist=get_sample_list(CASESAMPLES)
    controllist=get_sample_list(CONTROLSAMPLES)
    vcflist=get_vcf_list(vcfdir,filekey)
    dosubset(vcflist,caselist,controllist,SUBSETSIZE)


if __name__=='__main__':
    main(vcfdir)
