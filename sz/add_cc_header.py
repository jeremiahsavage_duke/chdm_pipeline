#!/usr/bin/env python3
import argparse
import itertools
import os

HEADER='case_chr'+'\t'+'case_pos'+'\t'+'case_count'+'\t'+'case_frequency'+'\t'+'case_patient'+'\t'+ \
    'case_genotype'+'\t'+'case_het_count'+'\t'+'case_hom_count'+'\t'+'percent_of'+'\t'+ \
    'control_count'+'\t'+'control_freqency'+'\t'+'control_sample'+'\t'+'control_genotype'+'\t'+ \
    'control_het_count'+'\t'+'control_hom_count'+'\t'+'allele_Chi^2'+'\t'+'allele_pvalue'+'\t'+'Func'+'\t'+'Gene'+'\t'+ \
    'ExonicFunc'+'\t'+'AAChange'+'\t'+'Converved'+'\t'+ \
    'SegDup'+'\t'+'ESP6500si_EA'+'\t'+'1000g2012apr_EUR'+'\t'+'dbSNP138'+'\t'+'AVSIFT'+'\t'+'LJB_PhyloP'+'\t'+'LJB_PhyloP_Pred'+'\t'+ \
    'LJB_SIFT'+'\t'+'LJB_SIFT_Pred'+'\t'+'LJB_PolyPhen2'+'\t'+'LJB_PolyPhen2_Pred'+'\t'+'LJB_LRT'+'\t'+'LJB_LRT_Pred'+'\t'+ \
    'LJB_MutationTaster'+'\t'+'LJB_MutationTaster_Pred'+'\t'+'LJB_GERP++'+'\t'+'Ref'+'\t'+'Obs'+'\t'+'gene_Chi^2'+'\t'+ \
    'gene_pvalue'+'\t'+'gene_variant_count'+'\n'


parser=argparse.ArgumentParser('merge sorted and count freq files')
parser.add_argument('-t','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']


def writeheader(outfile_open):
    outfile_open.write(HEADER)


def main(tsvfile):
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()


    outfile='case_control.tsv'
    outfile_open=open(outfile,'w')
    
    writeheader(outfile_open)
    for tsvline in tsvlines:
        outfile_open.write(tsvline)

    outfile_open.close()

if __name__=='__main__':
    main(tsvfile)
