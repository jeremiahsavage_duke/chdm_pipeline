#!/usr/bin/env python3
import argparse
import os
import sys

GENOTYPECOL=34
SAMPLECOL=35

parser=argparse.ArgumentParser('merge sorted and count freq files')
parser.add_argument('-d','--datafile',required=True)
parser.add_argument('-c','--countfile',required=True)
args=vars(parser.parse_args())

datafile=args['datafile']
countfile=args['countfile']


def is_het(data_genotype):
    genotype=data_genotype.split(':')[0]
    if genotype=='0/1':
        return '1'
    elif genotype=='1/0':
        return '1'
    elif genotype=='1/1':
        return '0'
    elif genotype=='0/0':
        sys.exit('huh?')
    else:
        sys.exit('geno me')


def is_hom(data_genotype):
    genotype=data_genotype.split(':')[0]
    if genotype=='0/1':
        return '0'
    elif genotype=='1/0':
        return '0'
    elif genotype=='1/1':
        return '1'
    elif genotype=='0/0':
        sys.exit('huh')
    else:
        sys.exit('geno me')

def main(datafile,countfile):
    with open(datafile) as datafile_open:
        datalines=datafile_open.readlines()

    with open(countfile,'r') as countfile_open:
        countlines=countfile_open.readlines()


    origfile,origext=os.path.splitext(countfile)
    outfile=origfile+'_data'+origext
    outfile_open=open(outfile,'w')

    newlines=''
    for i,countline in enumerate(countlines):
        dataline=datalines[i]
        datasplit=dataline.split('\t')
        data_genotype=datasplit[GENOTYPECOL].strip()
        data_sample=datasplit[SAMPLECOL].strip()
        het_status=is_het(data_genotype)
        hom_status=is_hom(data_genotype)
        newline=countline.strip('\n')+'\t'+data_sample+'\t'+data_genotype+'\t'+het_status+'\t'+hom_status+'\n'
        newlines+=newline

    for newline in newlines:
        outfile_open.write(newline)

    outfile_open.close()

if __name__=='__main__':
    main(datafile,countfile)
