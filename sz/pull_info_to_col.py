#!/usr/bin/env python3
import argparse
import os

GENEKEY=['GN','IGN']
GENEKEY_TXT='gene'
GIKEY='GI'
RFGKEY='RFG'
RFGKEY_TXT='rfg'
AAKEY='p.'
AAKEY_TXT='aa'
PHYLOPSCORE='NA'
PHYLOPSCORE_TXT='phylop_score'
PHYLOPPRED='NB'
PHYLOPPRED_TXT='phylop_prediction'
SIFTSCORE='NC'
SIFTSCORE_TXT='sift_score'
SIFTPRED='ND'
SIFTPRED_TXT='sift_prediction'
POLY2SCORE='NE'
POLY2SCORE_TXT='polyphen2_score'
POLY2PRED='NF'
POLY2PRED_TXT='polyphen2_prediction'
LRTSCORE='NG'
LRTSCORE_TXT='lrt_score'
LRTPRED='NH'
LRTPRED_TXT='lrt_prediction'
MUTTASTSCORE='NI'
MUTTASTSCORE_TXT='mut_taster_score'
MUTTASTPRED='NJ'
MUTTASTPRED_TXT='mut_taster_prediction'
MUTTASTCOMBSCORE='NK'
MUTTASTCOMBSCORE_TXT='mut_taster_functional_impact_combined_score'
MUTTASTCOMBPRED='NL'
MUTTASTCOMBPRED_TXT='mut_taster_variant_functional_impact'
HGMDTYPE='HT'
HGMDTYPE_TXT='hgmd_mutation_type'
HGMDCLASS='HC'
HGMDCLASS_TXT='hgmd_variant_class'
HGMDDISEASE='HD'
HGMDDISEASE_TXT='hgmd_disease'
HGMDPMID='HP'
HGMDPMID_TXT='hgmd_pubmed_id'
SWPRTFUNC='SF'
SWPRTFUNC_TXT='swiss_prot_function'
SWPRTDISEASE='SD'
SWPRTDISEASE_TXT='swiss_prot_disease_assoc'
SWPRTTRLNMOD='SM'
SWPRTTRLNMOD_TXT='swiss_prot_post_translational_modifications'
SWPRTEXP='SX'
SWPRTEXP_TXT='swiss_prot_expression'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('pull info to separate column')
parser.add_argument('-f','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']


def get_info_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    datavalue=None
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if datasplit[0].endswith(akey):
            datavalue=datasplit[1].strip('"')
            return datavalue
    if datavalue is None:
        datavalue='None'
        return datavalue


def get_splicing_value(vcfinfo):
    gi_info=get_info_value('GI',vcfinfo)
    if ')' in gi_info:
        splicing_value=gi_info.split(')')[0]
    else:
        splicing_value='.'
    return splicing_value


def get_info_field(vcfline):
    vcfsplit=vcfline.split('\t')
    vcfinfo=vcfsplit[VCFINFOCOL].strip('"')
    return vcfinfo

def get_gene_value(vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        datakey=datasplit[0]
        if (datakey=='IGN') or (datakey=='GN'):
            datavalue=datasplit[1]
            return datavalue
    #if no GN
    for vcfinfodata in vcfinfosplit:
        datasplit=vcinfodata.split('=')
        datakey=datasplit[0]
        if (datakey==GIKEY): #intergenic, etc
            datavalue=datasplit[1]
            genevalue=datavalue.split(':')[0]
            return datavalue

        
def get_aa_change(vcfinfo):
    givalue=get_info_value(GIKEY,vcfinfo)
    gisplit=givalue.split(':')
    for gidata in gisplit:
        if gidata.startswith(AAKEY):
            return gidata
    return 'None'


def write_tsv(tsvlines,outfile_open):
    for tsvline in tsvlines:
        outfile_open.write(tsvline)


def get_newheader(tsvline):
    newfields=GENEKEY_TXT+'\t' + AAKEY_TXT+'\t' + RFGKEY_TXT+'\t' + PHYLOPSCORE_TXT+'\t' + PHYLOPPRED_TXT +'\t' + SIFTSCORE_TXT+'\t' + SIFTPRED_TXT+'\t' + POLY2SCORE_TXT+'\t' + POLY2PRED_TXT+'\t' + LRTSCORE_TXT+'\t' + LRTPRED_TXT+'\t' + MUTTASTSCORE_TXT+'\t' + MUTTASTPRED_TXT+'\t' + MUTTASTCOMBSCORE_TXT+'\t' + MUTTASTCOMBPRED_TXT+'\t' + HGMDTYPE_TXT+'\t' + HGMDCLASS_TXT+'\t' + HGMDDISEASE_TXT+'\t' + HGMDPMID_TXT+ '\t' + SWPRTFUNC_TXT+'\t' + SWPRTDISEASE_TXT+'\t' + SWPRTTRLNMOD_TXT+'\t' + SWPRTEXP_TXT+ '\n'
    newline=tsvline.strip('\n')+'\t'+newfields
    return newline


def pull_data(tsvfile):
    #genekeylist=GENEKEY
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvfile))
    outfile_open=open(origfile+'_info'+origext,'w')

    newlines=''
    for tsvline in tsvlines:
        if tsvline.startswith('chr'):
            newheader=get_newheader(tsvline)
            newlines+=newheader
        else:
            vcfinfo=get_info_field(tsvline)
            GENEKEY_value=get_gene_value(vcfinfo)
            AAKEY_value=get_aa_change(vcfinfo)
            RFGKEY_value=get_info_value(RFGKEY,vcfinfo)
            PHYLOPSCORE_value=get_info_value(PHYLOPSCORE,vcfinfo)
            PHYLOPPRED_value=get_info_value(PHYLOPPRED,vcfinfo)
            SIFTSCORE_value=get_info_value(SIFTSCORE,vcfinfo)
            SIFTPRED_value=get_info_value(SIFTPRED,vcfinfo)
            POLY2SCORE_value=get_info_value(POLY2SCORE,vcfinfo)
            POLY2PRED_value=get_info_value(POLY2PRED,vcfinfo)
            LRTSCORE_value=get_info_value(LRTSCORE,vcfinfo)
            LRTPRED_value=get_info_value(LRTPRED,vcfinfo)
            MUTTASTSCORE_value=get_info_value(MUTTASTSCORE,vcfinfo)
            MUTTASTPRED_value=get_info_value(MUTTASTPRED,vcfinfo)
            MUTTASTCOMBSCORE_value=get_info_value(MUTTASTCOMBSCORE,vcfinfo)
            MUTTASTCOMBPRED_value=get_info_value(MUTTASTCOMBPRED,vcfinfo)
            HGMDTYPE_value=get_info_value(HGMDTYPE,vcfinfo)
            HGMDCLASS_value=get_info_value(HGMDCLASS,vcfinfo)
            HGMDDISEASE_value=get_info_value(HGMDDISEASE,vcfinfo)
            HGMDPMID_value=get_info_value(HGMDPMID,vcfinfo)
            SWPRTFUNC_value=get_info_value(SWPRTFUNC,vcfinfo)
            SWPRTDISEASE_value=get_info_value(SWPRTDISEASE,vcfinfo)
            SWPRTTRLNMOD_value=get_info_value(SWPRTTRLNMOD,vcfinfo)
            SWPRTEXP_value=get_info_value(SWPRTEXP,vcfinfo)
            print('\n\nGENEKEY_value=%s' % (GENEKEY_value))
            print('SWPRTFUNC_value=%s' % (SWPRTFUNC_value))
            print('SWPRTDISEASE_value=%s' % (SWPRTDISEASE_value))
            print('SWPRTTRLNMOD_value=%s' % (SWPRTTRLNMOD_value))
            print('SWPRTEXP_value=%s' % (SWPRTEXP_value))
            

            newline=tsvline.strip('\n')+'\t'+GENEKEY_value+'\t' + AAKEY_value+'\t' + RFGKEY_value+'\t' + PHYLOPSCORE_value+'\t' + PHYLOPPRED_value +'\t' + SIFTSCORE_value+'\t' + SIFTPRED_value+'\t' + POLY2SCORE_value+'\t' + POLY2PRED_value+'\t' + LRTSCORE_value+'\t' + LRTPRED_value+'\t' + MUTTASTSCORE_value+'\t' + MUTTASTPRED_value+'\t' + MUTTASTCOMBSCORE_value+'\t' + MUTTASTCOMBPRED_value+'\t' + HGMDTYPE_value+'\t' + HGMDCLASS_value+'\t' + HGMDDISEASE_value+'\t' + HGMDPMID_value+ '\t' + SWPRTFUNC_value+'\t' + SWPRTDISEASE_value+'\t' + SWPRTTRLNMOD_value+'\t' + SWPRTEXP_value+ '\n'
            print('newline=%s\n\n' % (newline))
            newlines+=newline

    write_tsv(newlines,outfile_open)
    outfile_open.close()


    
def main(tsvfile):
    pull_data(tsvfile)


if __name__=='__main__':
    main(tsvfile)
