mkdir -p gene/snp/merge
mkdir -p gene/indel/merge
cd gene
~/chdm_pipeline/sz/filter_gene_to_tsv.py -d ~/danish_schizophrenia/batch1/
~/chdm_pipeline/sz/filter_gene_to_tsv.py -d ~/danish_schizophrenia/batch2/
mv *.SNPs_* snp
mv *.INDELs_* indel
cd snp/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../


~/chdm_pipeline/sz/filter_maf_to_tsv.py -d ./

~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_low_nulls.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_range_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_range_high_values.tsv

~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_low_nulls.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_range_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_range_high_values.tsv


~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_low_nulls_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_range_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_range_high_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_low_nulls_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_range_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_range_high_values_sorted.tsv

~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_low_nulls_sorted_count.tsv -n control_TEUR_low_nulls_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_low_values_sorted_count.tsv -n control_TEUR_low_values_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_range_low_values_sorted_count.tsv -n control_TEUR_range_low_values_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_range_high_values_sorted_count.tsv -n control_TEUR_range_high_values_sorted_count.tsv

~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_low_nulls_sorted.tsv -c case_TEUR_low_nulls_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_low_values_sorted.tsv -c case_TEUR_low_values_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_range_low_values_sorted.tsv -c case_TEUR_range_low_values_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_range_high_values_sorted.tsv -c case_TEUR_range_high_values_sorted_count_v_control.tsv

cd ../../
cd indel/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
~/chdm_pipeline/sz/filter_maf_to_tsv.py -d ./

~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_low_nulls.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_range_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case_TEUR_range_high_values.tsv

~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_low_nulls.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_range_low_values.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control_TEUR_range_high_values.tsv


~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_low_nulls_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_range_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_TEUR_range_high_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_low_nulls_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_range_low_values_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_TEUR_range_high_values_sorted.tsv

~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_low_nulls_sorted_count.tsv -n control_TEUR_low_nulls_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_low_values_sorted_count.tsv -n control_TEUR_low_values_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_range_low_values_sorted_count.tsv -n control_TEUR_range_low_values_sorted_count.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_TEUR_range_high_values_sorted_count.tsv -n control_TEUR_range_high_values_sorted_count.tsv

~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_low_nulls_sorted.tsv -c case_TEUR_low_nulls_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_low_values_sorted.tsv -c case_TEUR_low_values_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_range_low_values_sorted.tsv -c case_TEUR_range_low_values_sorted_count_v_control.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_TEUR_range_high_values_sorted.tsv -c case_TEUR_range_high_values_sorted_count_v_control.tsv




############
cd ~/project_ed/
###all maf###
mkdir maf
cd maf
~/chdm_pipeline/sz/filter_maf_to_tsv.py -d ~/danish_schizophrenia/batch1/
~/chdm_pipeline/sz/filter_maf_to_tsv.py -d ~/danish_schizophrenia/batch2/

mkdir -p maf/ev/snp/low_null/merge
mkdir -p maf/ev/snp/low_value/merge
mkdir -p maf/ev/snp/low_range/merge
mkdir -p maf/ev/snp/high_range/merge
#mkdir -p maf/ev/indel/low_null/merge
#mkdir -p maf/ev/indel/low_value/merge
#mkdir -p maf/ev/indel/low_range/merge
#mkdir -p maf/ev/indel/high_range/merge

mkdir -p maf/teur/snp/low_null/merge
mkdir -p maf/teur/snp/low_value/merge
mkdir -p maf/teur/snp/low_range/merge
mkdir -p maf/teur/snp/high_range/merge
mkdir -p maf/teur/indel/low_null/merge
mkdir -p maf/teur/indel/low_value/merge
mkdir -p maf/teur/indel/low_range/merge
mkdir -p maf/teur/indel/high_range/merge

cd maf
for f in *_EV_* ; do mv $f ev/ ; done
for f in *_TEUR_* ; do mv $f teur/ ; done
#
cd ev
for f in *.SNPs_* ; do mv $f snp/ ; done
#for f in *.INDELs_* ; do mv $f indel/ ; done
cd ../
cd teur
for f in *.SNPs_* ; do mv $f snp/ ; done
for f in *.INDELs_* ; do mv $f indel/ ; done
cd ../
#
cd teur/snp
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/
cd ../

cd indel
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/
cd ../

###
cd snp
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../
###
cd indel
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../../
##########
cd ev
cd snp
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../

##########
cd ~/project_ed/maf/indel/
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/
###
cd ~/project_ed/maf/teur/snp
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../


cd low_value/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../
###############

cd ~/project_ed/maf/ev/snp
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/


cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_value/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../
###############







####################################2014-05-05####################################

############
cd
mkdir dsz_small
cd dsz_small
~/chdm_pipeline/sz/subset.py -d ~/dsz_all/
cd ~/project_ed/subset/
###all maf###
mkdir maf
cd maf
~/chdm_pipeline/sz/filter_maf_to_tsv.py -d ~/dsz_small/
cd ../

mkdir -p maf/ev/snp/low_null/merge
mkdir -p maf/ev/snp/low_value/merge
mkdir -p maf/ev/snp/low_range/merge
mkdir -p maf/ev/snp/high_range/merge
#mkdir -p maf/ev/indel/low_null/merge
#mkdir -p maf/ev/indel/low_value/merge
#mkdir -p maf/ev/indel/low_range/merge
#mkdir -p maf/ev/indel/high_range/merge

mkdir -p maf/teur/snp/low_null/merge
mkdir -p maf/teur/snp/low_value/merge
mkdir -p maf/teur/snp/low_range/merge
mkdir -p maf/teur/snp/high_range/merge
mkdir -p maf/teur/indel/low_null/merge
mkdir -p maf/teur/indel/low_value/merge
mkdir -p maf/teur/indel/low_range/merge
mkdir -p maf/teur/indel/high_range/merge

cd maf
for f in *_EV_* ; do mv $f ev/ ; done
for f in *_TEUR_* ; do mv $f teur/ ; done
#
cd ev
for f in *.SNPs_* ; do mv $f snp/ ; done
#for f in *.INDELs_* ; do mv $f indel/ ; done
cd ../
cd teur
for f in *.SNPs_* ; do mv $f snp/ ; done
for f in *.INDELs_* ; do mv $f indel/ ; done
cd ../
#
cd teur/snp
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/
cd ../

cd indel
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/
cd ../

###
cd snp
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../
###
cd indel
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../../
##########
cd ev
cd snp
cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
cd ../

#####################
cd ../
cd teur/snp/

cd low_value/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../
###############

cd ~/project_ed/maf/ev/snp
mv *range_low_values.tsv low_range/
mv *range_high_values.tsv high_range/
mv *low_nulls.tsv low_null/
mv *low_values.tsv low_value/


cd low_value/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd low_value/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_null/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd low_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd high_range/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../
###############
#################################2014-05-05###############################################


###################2014-05-06####################
cd
mkdir dsz_small
cd dsz_small
~/chdm_pipeline/sz/subset.py -d ~/dsz_all/
cd ~/project_ed/subset/
###all maf###
mkdir maf
cd maf
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths.py -d ~/dsz_small/


###########
cd ~/project_ed/
mkdir comb
cd comb
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths.py -d ~/dsz_all/


mkdir -p snp/null/merge
mkdir -p snp/ultrarare/merge
mkdir -p snp/rare/merge
mkdir -p snp/common/merge
mkdir -p snp/polymorphic/merge

#mkdir -p indel/low_null/merge
#mkdir -p indel/low_value/merge
#mkdir -p indel/low_range/merge
#mkdir -p indel/high_range/merge


for f in *.SNPs_* ; do mv $f snp/ ; done
#for f in *.INDELs_* ; do mv $f indel/ ; done

cd snp
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
#cd ../

#cd indel
#mv *_null.tsv null/
#mv *_polymorphic.tsv polymorphic/
#mv *_ultrarare.tsv ultrarare/
#mv *_rare.tsv rare/
#mv *_common.tsv common/
#cd ../
###################
cd ultrarare/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd rare/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd common/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd null/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../

cd polymorphic/merge
~/chdm_pipeline/sz/merge_case_n_control.py -d ../
cd ../../
####################
cd ultrarare/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
~/chdm_pipeline/sz/consolidate_samples.py -t case_sorted_v_control_exonic_info.tsv
~/chdm_pipeline/sz/consolidate_samples.py -t case_sorted_v_control_intronic_info.tsv
~/chdm_pipeline/sz/keep_alt_prot.py -t case_sorted_v_control_exonic_info_shrink.tsv
~/chdm_pipeline/sz/keep_alt_prot.py -t case_sorted_v_control_intronic_info_shrink.tsv
cd ../../

cd rare/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
~/chdm_pipeline/sz/consolidate_samples.py -t 
~/chdm_pipeline/sz/keep_alt_prot.py -t 
cd ../../

cd common/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd null/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../

cd polymorphic/merge
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/comp_case_control.py -c case_sorted_count.tsv -n control_sorted_count.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_v_control.tsv
~/chdm_pipeline/sz/filter_intron.py -f case_sorted_v_control.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_exonic.tsv
~/chdm_pipeline/sz/pull_info_to_col.py -f case_sorted_v_control_intronic.tsv
cd ../../


~/chdm_pipeline/sz/consolidate_samples.py -t 
~/chdm_pipeline/sz/keep_alt_prot.py -t 



###########2014-05-09################
convert_to_annovar.sh

for f in /home/jeremiah/dsz_annovar/*.annovar ; do echo /home/jeremiah/tools/annovar/summarize_annovar.pl ${f} /home/jeremiah/tools/annovar/humandb/ --buildver hg19 --verdbsnp 138 --ver1000g 1000g2012apr --veresp 6500si ; done | parallel -j+0




############2014-05-12#############

cd  ~/dsz_annovar
for f in *.exome_summary.csv ; do mv ${f} ../dsz_annotate/ ; done
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths_annovar.py -d ./

mkdir -p snp/null/merge
mkdir -p snp/ultrarare/merge
mkdir -p snp/rare/merge
mkdir -p snp/common/merge
mkdir -p snp/polymorphic/merge

#mkdir -p indel/low_null/merge
#mkdir -p indel/low_value/merge
#mkdir -p indel/low_range/merge
#mkdir -p indel/high_range/merge


for f in *.SNPs_* ; do mv $f snp/ ; done
#for f in *.INDELs_* ; do mv $f indel/ ; done

cd snp
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
#cd ../

#cd indel
#mv *_null.tsv null/
#mv *_polymorphic.tsv polymorphic/
#mv *_ultrarare.tsv ultrarare/
#mv *_rare.tsv rare/
#mv *_common.tsv common/
#cd ../
###################
cd ultrarare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd rare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd common/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd null/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd polymorphic/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../
####################
cd ultrarare/merge
~/chdm_pipeline/sz/remove_quote.py -t case.tsv
~/chdm_pipeline/sz/remove_info.py -t case_noquote.tsv
~/chdm_pipeline/sz/remove_quote.py -t control.tsv
~/chdm_pipeline/sz/remove_info.py -t control_noquote.tsv
mv control.tsv control.tsv.orig
mv control_noquote_noinfo.tsv control.tsv
mv case.tsv case.tsv.orig
mv case_noquote_noinfo.tsv case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d case_sorted.tsv -c case_sorted_count.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d control_sorted.tsv -c control_sorted_count.tsv
~/chdm_pipeline/sz/consolidate_control.py -t control_sorted_count_data.tsv
~/chdm_pipeline/sz/comp_case_control_fast.py -c case_sorted_count_data.tsv -n control_sorted_count_data_shrink.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_data_v_control.tsv
~/chdm_pipeline/sz/consolidate_samples_annovar.py -t case_sorted_v_control.tsv
~/chdm_pipeline/sz/remove_cols.py -t case_sorted_v_control_shrink.tsv
cd ../../

cd rare/merge
cd ../../

cd common/merge
cd ../../

cd null/merge
cd ../../

cd polymorphic/merge
cd ../../

~/chdm_pipeline/sz/consolidate_samples.py -t 
~/chdm_pipeline/sz/keep_alt_prot.py -t 


#################2014-05-19#########
cd dsz_annotate
mkdir indel
mv *.INDELs* indel/

mkdir esp
mkdir thousand
mkdir comb

mkdir -p indel/esp/null/merge
mkdir -p indel/esp/ultrarare/merge
mkdir -p indel/esp/rare/merge
mkdir -p indel/esp/common/merge
mkdir -p indel/esp/polymorphic/merge

mkdir -p indel/thousand/null/merge
mkdir -p indel/thousand/ultrarare/merge
mkdir -p indel/thousand/rare/merge
mkdir -p indel/thousand/common/merge
mkdir -p indel/thousand/polymorphic/merge

mkdir -p indel/comb/null/merge
mkdir -p indel/comb/ultrarare/merge
mkdir -p indel/comb/rare/merge
mkdir -p indel/comb/common/merge
mkdir -p indel/comb/polymorphic/merge

cd indel
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths_annovar.py -d ./

for f in *_COMB_* ; do mv ${f}  comb/ ; done
for f in *_ESP_* ; do mv ${f} esp/ ; done
for f in *_THOUSAND_* ; do mv ${f} thousand/ ; done


#######2014-05-20############
cd comb/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

cd esp/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

cd thousand/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

###########
cd ultrarare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd rare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd common/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd null/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd polymorphic/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../
################
 #rare,common,null,polymorphic
cd ultrarare/merge
~/chdm_pipeline/sz/remove_quote.py -t case.tsv
~/chdm_pipeline/sz/remove_info.py -t case_noquote.tsv
~/chdm_pipeline/sz/remove_quote.py -t control.tsv
~/chdm_pipeline/sz/remove_info.py -t control_noquote.tsv
mv control.tsv control.tsv.orig
mv control_noquote_noinfo.tsv control.tsv
mv case.tsv case.tsv.orig
mv case_noquote_noinfo.tsv case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d case_sorted.tsv -c case_sorted_count.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d control_sorted.tsv -c control_sorted_count.tsv
~/chdm_pipeline/sz/consolidate_control.py -t control_sorted_count_data.tsv
~/chdm_pipeline/sz/comp_case_control_fast.py -c case_sorted_count_data.tsv -n control_sorted_count_data_shrink.tsv
~/chdm_pipeline/sz/join_sorted_w_count.py -s case_sorted.tsv -c case_sorted_count_data_v_control.tsv
~/chdm_pipeline/sz/consolidate_samples_annovar.py -t case_sorted_v_control.tsv
~/chdm_pipeline/sz/remove_cols.py -t case_sorted_v_control_shrink.tsv
~/chdm_pipeline/sz/remove_synonymous.py -t case_sorted_v_control_shrink_final.tsv
cd ../../




cd ../snp/
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths_annovar.py -d ./
cd ../
mkdir -p snp/esp/null/merge
mkdir -p snp/esp/ultrarare/merge
mkdir -p snp/esp/rare/merge
mkdir -p snp/esp/common/merge
mkdir -p snp/esp/polymorphic/merge

mkdir -p snp/thousand/null/merge
mkdir -p snp/thousand/ultrarare/merge
mkdir -p snp/thousand/rare/merge
mkdir -p snp/thousand/common/merge
mkdir -p snp/thousand/polymorphic/merge

mkdir -p snp/comb/null/merge
mkdir -p snp/comb/ultrarare/merge
mkdir -p snp/comb/rare/merge
mkdir -p snp/comb/common/merge
mkdir -p snp/comb/polymorphic/merge
cd snp/

for f in *_COMB_* ; do mv ${f}  comb/ ; done
for f in *_ESP_* ; do mv ${f} esp/ ; done
for f in *_THOUSAND_* ; do mv ${f} thousand/ ; done
cd comb/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

cd esp/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

cd thousand/
mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/
cd ../

#############################


##########2014-05-21##########
cd ~/dsz_annotate/snp/esp/
mkdir ultranull/merge -p
cd ../thousand/
mkdir -p ultranull/merge
cd ../comb/
mkdir -p ultranull/merge

cd null/merge/
cp -a *.orig ../../ultranull/merge/
cd ../../ultranull/merge/
mv case.tsv.orig case_null.tsv
mv control.tsv.orig control_null.tsv
cd ../../ultrarare/merge/
gcp -a *.orig ../../ultranull/merge/
cd ../../ultranull/merge/
mv case.tsv.orig case_ultrarare.tsv
mv control.tsv.orig control_ultrarare.tsv
cat case_ultrarare.tsv case_null.tsv > case.tsv
cat control_ultrarare.tsv control_null.tsv > control.tsv

#############2014-05-22##########
#######ALSO COMPARE CONTROL TO CASE#######
cd ultrarare/merge
~/chdm_pipeline/sz/remove_quote.py -t case.tsv
~/chdm_pipeline/sz/remove_info.py -t case_noquote.tsv
~/chdm_pipeline/sz/remove_quote.py -t control.tsv
~/chdm_pipeline/sz/remove_info.py -t control_noquote.tsv
mv control.tsv control.tsv.orig
mv control_noquote_noinfo.tsv control.tsv
mv case.tsv case.tsv.orig
mv case_noquote_noinfo.tsv case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f case.tsv
~/chdm_pipeline/sz/sort_vcf.py -f control.tsv
~/chdm_pipeline/sz/allele_freq.py -f case_sorted.tsv
~/chdm_pipeline/sz/allele_freq.py -f control_sorted.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d case_sorted.tsv -c case_sorted_count.tsv
~/chdm_pipeline/sz/join_count_sample_genotype.py -d control_sorted.tsv -c control_sorted_count.tsv
~/chdm_pipeline/sz/consolidate_all_samples_annovar.py -t control_sorted_count_data.tsv
~/chdm_pipeline/sz/consolidate_all_samples_annovar.py -t case_sorted_count_data.tsv
~/chdm_pipeline/sz/comp_all_case_control_fast.py -c case_sorted_count_data_shrink.tsv -n control_sorted_count_data_shrink.tsv
~/chdm_pipeline/sz/chi2_pval.py -t case_sorted_count_data_shrink_v_control.tsv
grep '^X\|^Y' case_sorted_count_data_shrink_v_control_pval.tsv > allosome.tsv
grep '^[0-9]' case_sorted_count_data_shrink_v_control_pval.tsv > autosome.tsv
~/chdm_pipeline/sz/sort_comp_annovar.py -f allosome.tsv
~/chdm_pipeline/sz/sort_comp_annovar.py -f autosome.tsv
cat autosome_sorted.tsv allosome_sorted.tsv > genome_sorted.tsv
~/chdm_pipeline/sz/join_count_w_case_control.py --case case_sorted.tsv --control control_sorted.tsv --genomecount genome_sorted.tsv
~/chdm_pipeline/sz/remove_cc_synonymous.py -t case_v_control_data.tsv
~/chdm_pipeline/sz/remove_cols.py -t nonsyn.tsv
~/chdm_pipeline/sz/add_cc_header.py -t nonsyn_final.tsv
ls -lh case_control.tsv
cd ../../


########2014-05-23


#######2014-05-27


###2014-06-02###

###2014-07-30###

cd dsz_annotate/snp/
for f in *.exome_summary.csv ; do echo "~/chdm_pipeline/sz/skat/filter_annovar_func.py -a ${f}" ; done | parallel -j+0
mkdir make_vcf
cd make_vcf
~/chdm_pipeline/sz/filter_maf_to_tsv_joint_ev_ths_annovar.py -d ../

mkdir esp

for f in *_ESP_* ; do mv ${f} esp/ ; done
cd esp
mkdir null ultrarare rare common polymorphic ultranull

mv *_null.tsv null/
mv *_polymorphic.tsv polymorphic/
mv *_ultrarare.tsv ultrarare/
mv *_rare.tsv rare/
mv *_common.tsv common/

mkdir -p common/merge
mkdir -p null/merge
mkdir -p polymorphic/merge
mkdir -p rare/merge
mkdir -p ultrarare/merge
mkdir -p ultranull/merge

cd ultrarare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd rare/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd common/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd null/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../

cd polymorphic/merge
~/chdm_pipeline/sz/merge_case_n_control_annovar.py -d ../
cd ../../


~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f case.tsv
~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f control.tsv

for f in *.vcf ; do echo ~/bin/bgzip ${f} ; done  | parallel -j+0
for f in *.vcf.gz ; do echo tabix ${f} ; done  | parallel -j+0

~/bin/vcftools/vcf-merge *.vcf.gz > all.vcf



#java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK.jar -T CombineVariants -R ~/grc37/human_g1k_v37.fasta --variant null.vcf --variant ultrarare.vcf --out ultranull.vcf




~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v all.vcf
mv all_DP.vcf ultrarare.vcf
mkdir new_skat
mv ultrarare.vcf new_skat/
cd new_skat/


pseq all new-project --resources /home/jeremiah/tools/hg19_plink/
pseq all load-vcf --vcf ./ultrarare.vcf
pseq all load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq all write-ped --name all

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f all.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f all.tfam
mv all_new.tfam all.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t all.tped
mv all_new.tped all.tped

~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t all.tped -b /home/jeremiah/tools/cil.bed
mv all.setid plink.setid
plink --tped all.tped --tfam all.tfam

####
cd /home/jeremiah/dsz_annotate/snp/make_vcf_esp/esp/null/merge
cp -a case.tsv ../../ultranull/merge/case_null.tsv
cp -a control.tsv ../../ultranull/merge/control_null.tsv
cd ../../ultrarare/merge/
cp -a case.tsv ../../ultranull/merge/case_ultrarare.tsv
cp -a control.tsv ../../ultranull/merge/control_ultrarare.tsv

cat case_ultrarare.tsv case_null.tsv > case.tsv
cat control_ultrarare.tsv control_null.tsv > control.tsv

~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f case.tsv
~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f control.tsv

for f in *.vcf ; do echo "~/bin/vcftools/vcf-sort ${f} > ${f%.*}_sort.vcf && mv ${f%.*}_sort.vcf ${f}" ; done


for f in *.vcf ; do echo ~/bin/bgzip ${f} ; done  | parallel -j+0
for f in *.vcf.gz ; do echo tabix ${f} ; done  | parallel -j+0

~/bin/vcftools/vcf-merge *.vcf.gz > all.vcf
mv all.vcf ultranull.vcf

####
~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v ultranull.vcf
mv ultranull_DP.vcf ultranull.vcf

pseq un new-project --resources /home/jeremiah/tools/hg19_plink/
pseq un load-vcf --vcf ./ultranull.vcf
pseq un load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq un write-ped --name ultranull

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f ultranull.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f ultranull.tfam
mv ultranull_new.tfam ultranull.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t ultranull.tped
mv ultranull_new.tped ultranull.tped

~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t ultranull.tped -b /home/jeremiah/tools/cil.bed
mv ultranull.setid plink.setid
plink --tped ultranull.tped --tfam ultranull.tfam


####ultranull genome setid
awk -F '\t' '$1="genome"' plink.setid > plink_genome.setid

mkdir genome/
mv plink_genome.setid genome/
cp -a ultranull.t* genome/
cd genome/
mv plink_genome.setid plink.setid
plink --tped ultranull.tped --tfam ultranull.tfam
#R

####rare

~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f case.tsv
~/chdm_pipeline/sz/skat/ann_noinfo2vcf.py -f control.tsv

for f in *.vcf ; do echo ~/bin/bgzip ${f} ; done  | parallel -j+0
for f in *.vcf.gz ; do echo tabix ${f} ; done  | parallel -j+0

~/bin/vcftools/vcf-merge *.vcf.gz > rare.vcf


~/chdm_pipeline/sz/skat/vcf_wt_fake_DP.py -v rare.vcf
mv rare_DP.vcf rare.vcf

pseq rare new-project --resources /home/jeremiah/tools/hg19_plink/
pseq rare load-vcf --vcf ./rare.vcf
pseq rare load-pheno --file ~/chdm_pipeline/sz/skat/sz.phe
pseq rare write-ped --name rare

~/chdm_pipeline/sz/skat/fam_annotate_phe.py -f rare.tfam -p ~/chdm_pipeline/sz/skat/sz.phe
mv rare_new.tfam rare.tfam
~/chdm_pipeline/sz/skat/plink_fam_id.py -f rare.tfam
mv rare_new.tfam rare.tfam
~/chdm_pipeline/sz/skat/tped_varname.py -t rare.tped
mv rare_new.tped rare.tped

~/chdm_pipeline/sz/skat/bed_tped_2_setid.py -t rare.tped -b /home/jeremiah/tools/cil.bed
mv rare.setid plink.setid
plink --tped rare.tped --tfam rare.tfam


####rare genome setid
awk -F '\t' '$1="genome"' plink.setid > plink_genome.setid

mkdir genome/
mv plink_genome.setid genome/
cp -a rare.t* genome/
cd genome/
mv plink_genome.setid plink.setid
plink --tped rare.tped --tfam rare.tfam
#R
