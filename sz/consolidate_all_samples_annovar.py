#!/usr/bin/env python3
import argparse
import os
import sys

CONTROL_CHR=0
CONTROL_BP=1
CONTROL_COUNT_OF=2
CONTROL_COUNT=3
CONTROL_FREQ=4
CONTROL_SAMPLE=5
CONTROL_GENOTYPE=6
CONTROL_HET=7
CONTROL_HOM=8

parser=argparse.ArgumentParser('consolidate allels to single row')
parser.add_argument('-t','--tsvinfile',required=True)
args=vars(parser.parse_args())

tsvinfile=args['tsvinfile']


def main(tsvinfile):
    with open(tsvinfile,'r') as tsvinfile_open:
        tsvinlines=tsvinfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvinfile))
    tsvoutfile=origfile+'_shrink'+origext    
    tsvoutfile_open=open(tsvoutfile,'w')

    newtsvlines=''
    control_count_of_list=list()
    control_sample_list=list()
    control_genotype_list=list()
    control_het_list=list()
    control_hom_list=list()

    lenheader=0
    for tsvinline in tsvinlines:
        tsvsplit=tsvinline.split('\t')

        if len(tsvsplit)<lenheader: ##KLUDGE
            for newtsvline in newtsvlines:
                tsvoutfile_open.write(newtsvline)
            tsvoutfile_open.close()
            sys.exit('What a kludge')

        this_control_count_of=tsvsplit[CONTROL_COUNT_OF]
        this_control_count=tsvsplit[CONTROL_COUNT]

        if int(this_control_count)==1:
            newtsvline=tsvinline
            newtsvlines+=newtsvline

        elif int(this_control_count_of)==1 and int(this_control_count_of) < int(this_control_count):
            this_control_count_of=tsvsplit[CONTROL_COUNT_OF].strip()
            this_control_sample=tsvsplit[CONTROL_SAMPLE].strip()
            this_control_genotype=tsvsplit[CONTROL_GENOTYPE].strip()
            this_control_het=tsvsplit[CONTROL_HET].strip()
            this_control_hom=tsvsplit[CONTROL_HOM].strip()
            control_count_of_list.append(this_control_count_of)
            control_sample_list.append(this_control_sample)
            control_genotype_list.append(this_control_genotype)
            control_het_list.append(this_control_het)
            control_hom_list.append(this_control_hom)

        elif int(this_control_count_of)>1 and int(this_control_count_of) < int(this_control_count):
            this_control_count_of=tsvsplit[CONTROL_COUNT_OF].strip()
            this_control_sample=tsvsplit[CONTROL_SAMPLE].strip()
            this_control_genotype=tsvsplit[CONTROL_GENOTYPE].strip()
            this_control_het=tsvsplit[CONTROL_HET].strip()
            this_control_hom=tsvsplit[CONTROL_HOM].strip()
            control_count_of_list.append(this_control_count_of)
            control_sample_list.append(this_control_sample)
            control_genotype_list.append(this_control_genotype)
            control_het_list.append(this_control_het)
            control_hom_list.append(this_control_hom)

        elif int(this_control_count_of)>1 and int(this_control_count_of) == int(this_control_count):
            this_control_count_of=tsvsplit[CONTROL_COUNT_OF].strip()
            this_control_sample=tsvsplit[CONTROL_SAMPLE].strip()
            this_control_genotype=tsvsplit[CONTROL_GENOTYPE].strip()
            this_control_het=tsvsplit[CONTROL_HET].strip()
            this_control_hom=tsvsplit[CONTROL_HOM].strip()
            control_count_of_list.append(this_control_count_of)
            control_sample_list.append(this_control_sample)
            control_genotype_list.append(this_control_genotype)
            control_het_list.append(this_control_het)
            control_hom_list.append(this_control_hom)


            tsvsplit[CONTROL_COUNT_OF]=';'.join(control_count_of_list)
            tsvsplit[CONTROL_SAMPLE]=';'.join(control_sample_list)
            tsvsplit[CONTROL_GENOTYPE]=';'.join(control_genotype_list)
            total_het=0
            for het in control_het_list:
                total_het+=int(het)
            total_hom=0
            for hom in control_hom_list:
                total_hom+=int(hom)
            tsvsplit[CONTROL_HET]=str(total_het)
            tsvsplit[CONTROL_HOM]=str(total_hom)

            newtsvline='\t'.join(tsvsplit)+'\n'
            newtsvlines+=newtsvline

            del control_count_of_list[:]
            del control_sample_list[:]
            del control_genotype_list[:]
            del control_het_list[:]
            del control_hom_list[:]

        else:
            sys.exit('Game Over')

    for newtsvline in newtsvlines:
        tsvoutfile_open.write(newtsvline)

    tsvoutfile_open.close()

if __name__=='__main__':
    main(tsvinfile)
