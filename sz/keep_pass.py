#!/usr/bin/env python3
import argparse
import os
import glob

VCFFILTERCOL=6

parser=argparse.ArgumentParser('filter annovar vcf by MAF')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def keep_pass(vcffile):
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))
    passfile=origfile+'_PASS'+origext
    passfile_open=open(passfile,'w')

    for vcfline in vcflines:
        if vcfline.startswith('#'):
            passfile_open.write(vcfline)
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            if 'PASS' in vcffilter:
                passfile_open.write(vcfline)

    passfile_open.close()


def main(vcfdir):
    vcflist=get_vcf_list(vcfdir)
    for vcffile in vcflist:
        keep_pass(vcffile)


if __name__=='__main__':
    main(vcfdir)
