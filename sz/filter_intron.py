#!/usr/bin/env python3
import argparse
import os



MUTATIONTYPEKEY=['rfg','irf'] #snp,indel
VCFINFOCOL=7
FILTEROUT=['intronic','intergenic','utr3','utr5','ncrna_intronic','ncrna_exonic','upstream','downstream'] #case insensitive

parser=argparse.ArgumentParser('filter intronic and utr out')
parser.add_argument('-f','--tsvfile',required=True)
args=vars(parser.parse_args())

tsvfile=args['tsvfile']



def filter_intronic(tsvlines,infokeylistkey,filteroutlist):
    intronicstring=''
    exonicstring=''
    for tsvline in tsvlines:
        exonic=True
        tsvsplit=tsvline.split('\t')
        if 'chr' in tsvsplit[0]: #write header
            intronicstring+=tsvline
            exonicstring+=tsvline
            continue
        tsvinfofield=tsvsplit[VCFINFOCOL]
        tsvinfosplit=tsvinfofield.split(';')
        for tsvinfodata in tsvinfosplit:
            datasplit=tsvinfodata.split('=')
            datakey=datasplit[0].lower()
            if datakey in infokeylistkey:
                datavalue=datasplit[1].lower()
                for filteroutvalue in filteroutlist:
                    if filteroutvalue in datavalue:
                        intronicstring+=tsvline
                        exonic=False
                        break
                if exonic:
                    exonicstring+=tsvline
    return exonicstring,intronicstring



def write_tsv_to_file(tsvlines,openfile):
    for tsvline in tsvlines:
        openfile.write(tsvline)



def filter_tsv(tsvfile):
    infokeylist=MUTATIONTYPEKEY
    filteroutlist=FILTEROUT
    
    with open(tsvfile) as tsvfile_open:
        tsvlines=tsvfile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(tsvfile))
    outfile_intronic_open=open(origfile+'_intronic.tsv','w')
    outfile_exonic_open=open(origfile+'_exonic.tsv','w')

    exonicstring,intronicstring=filter_intronic(tsvlines,infokeylist,filteroutlist)
    write_tsv_to_file(exonicstring,outfile_exonic_open)
    write_tsv_to_file(intronicstring,outfile_intronic_open)

    outfile_intronic_open.close()
    outfile_exonic_open.close()



def main(tsvfile):
    filter_tsv(tsvfile)



if __name__=='__main__':
    main(tsvfile)
