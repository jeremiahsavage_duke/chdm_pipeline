#!/usr/bin/env python3
import argparse
import os
import shutil
import sys


CASEDATA=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/case.dat')
CONTROLDATA=os.path.join(os.getenv('HOME'),'chdm_pipeline/sz/control.dat')
CASEDIR=os.path.join(os.getenv('HOME'),'case_dir')
CONTROLDIR=os.path.join(os.getenv('HOME'),'control_dir')
INPUTFILEKEY='.vcf'

parser=argparse.ArgumentParser('merge cases and merge controls')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']


def get_sample_list(filelocation):
    samplefile_open=open(filelocation,'r')
    samplelines=samplefile_open.readlines()
    samplefile_open.close()
    samplelist=samplelines[0].split(',')
    samplelist=list(map(int,samplelist))
    return samplelist


def get_vcf_list(vcfdir,filekey):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith(filekey):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist


def getpatientkey(vcffile):
    patientkey=int(os.path.basename(vcffile).split('_')[0])
    return patientkey


def main(vcfdir):
    filekey=INPUTFILEKEY
    caselist=get_sample_list(CASEDATA)
    controllist=get_sample_list(CONTROLDATA)
    casedir=CASEDIR
    controldir=CONTROLDIR
    vcflist=get_vcf_list(vcfdir,filekey)
    for vcffile in vcflist:
        patientkey=getpatientkey(vcffile)
        if patientkey in caselist:
            shutil.move(vcffile,casedir)
        elif patientkey in controllist:
            shutil.move(vcffile,controldir)
        else:
            print('patient %s' % (patientkey))
            sys.exit('Sample not in case/control lists!')



if __name__=='__main__':
    main(vcfdir)
