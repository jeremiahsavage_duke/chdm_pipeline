#!/usr/bin/env python3
import argparse
import os

parser=argparse.ArgumentParser('count allel frequency')
parser.add_argument('-f','--vcfinput',required=True)
args=vars(parser.parse_args())

vcfinput=args['vcfinput']


def get_sample_count(samplefile):
    samplefile_open=open(samplefile,'r')
    samplelines=samplefile_open.readlines()
    samplefile_open.close()
    samplelist=samplelines[0].split(',')
    sample_count=len(samplelist)
    return sample_count

def write_count_table(curr_chr,curr_bp,allele_count,sample_count,vcfout):
    if curr_chr==0:
        return
    for i in range(allele_count):
        allele_freq=float(allele_count)/float(sample_count)
        vcfout.write('%s\t%s\t%s\t%s\t%s\n' % (curr_chr,curr_bp,i+1,allele_count,allele_freq))

def main(vcfinput):
    with open(vcfinput,'r') as vcffile:
        vcfinlines=vcffile.readlines()

        
    origfile,origext=os.path.splitext(os.path.basename(vcfinput))
    vcfoutput=origfile+'_count'+origext    
    vcfout= open(vcfoutput,'w')

    sample_count=0
    if 'case' in vcfoutput:
        sample_count=get_sample_count(CASESAMPLES)
    elif 'control' in vcfoutput:
        sample_count=get_sample_count(CONTROLSAMPLES)


    curr_chr=0
    curr_bp=0
    allele_count=1
    for vcfinline in vcfinlines:
        vcfsplit=vcfinline.split('\t')
        this_chr=vcfsplit[0]
        this_bp=vcfsplit[1]
        if this_bp != curr_bp:
            write_count_table(curr_chr,curr_bp,allele_count,sample_count,vcfout)
            curr_chr=this_chr
            curr_bp=this_bp
            allele_count=1
        else:
            allele_count+=1

            
    vcfout.close()
        


if __name__=='__main__':
    main(vcfinput)
