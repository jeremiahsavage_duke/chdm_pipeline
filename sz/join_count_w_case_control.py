#!/usr/bin/env python3
import argparse
import os
import sys

GENOTYPECOL=34
SAMPLECOL=35
CASECOUNT_COL=3
CONTROLCOUNT_COL=10
GENOMECOUNT_CHR_COL=0
GENOMECOUNT_POS_COL=1
SAMPLE_CHR_COL=21
SAMPLE_POS_COL=22

parser=argparse.ArgumentParser('merge sorted and count freq files')
parser.add_argument('--casefile',required=True)
parser.add_argument('--controlfile',required=True)
parser.add_argument('--genomecount',required=True)
args=vars(parser.parse_args())

casefile=args['casefile']
controlfile=args['controlfile']
genomecount=args['genomecount']

def is_het(data_genotype):
    genotype=data_genotype.split(':')[0]
    if genotype=='0/1':
        return '1'
    elif genotype=='1/0':
        return '1'
    elif genotype=='1/1':
        return '0'
    elif genotype=='0/0':
        sys.exit('huh?')
    else:
        sys.exit('geno me')


def is_hom(data_genotype):
    genotype=data_genotype.split(':')[0]
    if genotype=='0/1':
        return '0'
    elif genotype=='1/0':
        return '0'
    elif genotype=='1/1':
        return '1'
    elif genotype=='0/0':
        sys.exit('huh')
    else:
        sys.exit('geno me')

def in_case(genomecountsplit):
    if genomecountsplit[CASECOUNT_COL].strip()=='NA':
        return False
    else:
        return True

def get_sampleline(samplelines,genomecount_chr,genomecount_bp):
    #print('genomecount_chr=%s' % genomecount_chr)
    #print('genomecount_bp=%s' % genomecount_bp)
    for sampleline in samplelines:
        sampleline=sampleline.strip()
        #print('sampleline=%s' % sampleline)
        samplesplit=sampleline.split('\t')
        #print('samplesplit=%s' % samplesplit)
        sample_chr=samplesplit[SAMPLE_CHR_COL].strip()
        sample_bp=samplesplit[SAMPLE_POS_COL].strip()
        #print('sample_chr=%s' % sample_chr)
        #print('sample_bp=%s' % sample_bp)
        if ((genomecount_chr==sample_chr) and (genomecount_bp==sample_bp)):
            return sampleline
    sys.exit('Why here?')
        

def main(casefile,controlfile,genomecount):
    with open(casefile) as casefile_open:
        caselines=casefile_open.readlines()

    with open(controlfile,'r') as controlfile_open:
        controllines=controlfile_open.readlines()

    with open(genomecount,'r') as genomecount_open:
        genomecountlines=genomecount_open.readlines()

    outfile='case_v_control_data.tsv'
    outfile_open=open(outfile,'w')

    newlines=''
    for i,genomecountline in enumerate(genomecountlines):
        genomecountline=genomecountline.strip()
        genomecountsplit=genomecountline.split('\t')
        genomecount_chr=genomecountsplit[GENOMECOUNT_CHR_COL]
        genomecount_bp=genomecountsplit[GENOMECOUNT_POS_COL]
        sampleline=str()
        if in_case(genomecountsplit):
            #print('in_case')
            #print('genomecountline=%s' % (genomecountline))
            sampleline=get_sampleline(caselines,genomecount_chr,genomecount_bp)
        else:
            #print('in_control')
            sampleline=get_sampleline(controllines,genomecount_chr,genomecount_bp)
        newline=genomecountline+'\t'+sampleline+'\n'
        #datasplit=caseline.split('\t')
        #data_genotype=datasplit[GENOTYPECOL].strip()
        #data_sample=datasplit[SAMPLECOL].strip()
        #het_status=is_het(data_genotype)
        #hom_status=is_hom(data_genotype)
        #newline=countline.strip('\n')+'\t'+data_sample+'\t'+data_genotype+'\t'+het_status+'\t'+hom_status+'\n'
        newlines+=newline

    for newline in newlines:
        outfile_open.write(newline)

    outfile_open.close()

if __name__=='__main__':
    main(casefile,controlfile,genomecount)
