#!/usr/bin/env python3
import argparse
import os
import glob
import xlwt

GENELIST=['DISC1','PCM1','NOS1','HOOK3']
GENEKEY='GN'
VCFFILTERCOL=6
VCFINFOCOL=7

parser=argparse.ArgumentParser('filter vcfs by GENELIST')
parser.add_argument('-d','--vcfdirectory',required=True)
args=vars(parser.parse_args())

vcfdir=args['vcfdirectory']

def get_vcf_list(vcfdir):
    filelist=os.listdir(vcfdir)
    matchfilelist=list()
    for afile in filelist:
        if afile.endswith('.vcf'):
            matchfilelist.append(os.path.join(vcfdir,afile))
    return matchfilelist



def get_info_value(akey,vcfinfo):
    vcfinfosplit=vcfinfo.split(';')
    for vcfinfodata in vcfinfosplit:
        datasplit=vcfinfodata.split('=')
        if datasplit[0].endswith(akey):
            return datasplit[1]



def get_genes(vcflines,infokey,genelist):
    keptvalues=''
    for vcfline in vcflines:
        if vcfline.startswith('#'):
            keptvalues+=vcfline
        else:
            vcfsplit=vcfline.split('\t')
            vcffilter=vcfsplit[VCFFILTERCOL]
            vcfinfo=vcfsplit[VCFINFOCOL]
            if 'PASS' in vcffilter:
                gene_value=get_info_value(infokey,vcfinfo)
                if gene_value in genelist:
                    newvcfline=vcfline.strip()+'\t'+gene_value+'\n'
                    keptvalues+=newvcfline
    return keptvalues



def write_maf_to_xls(vcflines,xlwt_sheet):
    for rownum, vcfline in enumerate(vcflines.split('\n')):
        if vcfline.startswith('#'):
            xlwt_sheet.write(rownum,0,vcfline)
        else:
            vcfsplit=vcfline.split('\t')
            for colnum,vcffield in enumerate(vcfsplit):
                xlwt_sheet.write(rownum,colnum,vcffield)



def filter_genes(vcffile):
    infokey=GENEKEY
    genelist=GENELIST
    
    with open(vcffile) as vcffile_open:
        vcflines=vcffile_open.readlines()

    origfile,origext=os.path.splitext(os.path.basename(vcffile))

    outfile_lt_01=xlwt.Workbook(encoding='utf-8')    
    outfile_lt_01_sh01=outfile_lt_01.add_sheet('genes')
    #outfile_lt_01_sh02=outfile_lt_01.add_sheet('nulls')
 
    maf_value_lt_val01=get_genes(vcflines,infokey,genelist)

    write_maf_to_xls(maf_value_lt_val01,outfile_lt_01_sh01)
    #write_maf_to_xls(maf_value_lt_null01,outfile_lt_01_sh02)
    outfile_lt_01.save(origfile+'_genes.xls')



def make_clean():
    xls_list=glob.glob('*.xls')
    for afile in xls_list:
        os.remove(afile)



def main(vcfdir):
    #make_clean()
    vcflist=get_vcf_list(vcfdir)
    for vcffile in vcflist:
        filter_genes(vcffile)



if __name__=='__main__':
    main(vcfdir)
