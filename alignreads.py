import os
import subprocess
import multiprocessing
#local imports
import pipeutil

def alignrna(preppedfastqdict,referencedir,referencegenomename,gtffile,numcpus):
    samlist=list()
    gtfpath=os.path.join(referencedir,gtffile)
    for p in preppedfastqdict:
        read1=p
        print("read1=",read1)
        read2=preppedfastqdict[p]
        print("read2=",read2)
        output='tophat_out/accepted_hits.bam'
        samlist.append(output)
        if os.path.exists(output):
            print('tophat processing already completed:',output)
        else:
            if read2 is None:
                subcall='/usr/bin/time tophat --b2-very-sensitive --keep-fasta-order -p '+numcpus+' -r 175 --mate-std-dev 75 -G '+gtfpath+' '+referencegenomename+' '+read1
                print(subcall)
                subprocess.call([subcall],shell=True)
            else:
                subcall='/usr/bin/time tophat --b2-very-sensitive --keep-fasta-order -p '+numcpus+' -r 175 --mate-std-dev 75 -G '+gtfpath+' '+referencegenomename+' '+read1+' '+read2
                print(subcall)
                subprocess.call([subcall],shell=True)
    return samlist

def alignreads(preppedfastqdict,referencedir,referencegenomefasta,lastcall,isrna,gtffile):
    print('alignreads.py::alignreads()::isrna=',isrna)
    numcpus=str(multiprocessing.cpu_count())
    print("numspus=",numcpus)
    print("referencegenomefasta:",referencegenomefasta)
    referencegenomename,refext=os.path.splitext(referencegenomefasta)
    if isrna:
        samlist=alignrna(preppedfastqdict,referencedir,referencegenomename,gtffile,numcpus)
        return samlist
    for p in preppedfastqdict:
        samlist=list()
        read1=p
        print("read1=",read1)
        read2=preppedfastqdict[p]
        print("read2=",read2)
        origname1,origextension=os.path.splitext(p)
        origname1=origname1.replace(lastcall,"")
        output=origname1+".sam"
        samlist.append(output)
        if os.path.exists(output):
            print("bowtie2 processing already completed:",output)
        else:
            if read2 is None:
                subcall='/usr/bin/time bowtie2 -p ' + numcpus + ' -x ' + referencegenomename + ' -U ' + read1 + ' -S ' + output + ' --phred33'
                print(subcall)
                subprocess.call([subcall],shell=True)
            else:
                subcall="/usr/bin/time bowtie2 -p " + numcpus + " -x " + referencegenomename + " -1 " + read1 + " -2 " + read2 + " -S " + output + " --phred33"
                print(subcall)
                subprocess.call([subcall],shell=True)
    return samlist
