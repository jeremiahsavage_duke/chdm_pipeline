import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-m','--mimfile',required=True)
parser.add_argument('-t','--tsvsymbol',required=True)
parser.add_argument('-s','--mimsymbol',required=True)
parser.add_argument('-n','--mimnumber',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
mimfile=args['mimfile']
tsvsymbol=args['tsvsymbol']
mimsymbol=args['mimsymbol']
mimnumber=args['mimnumber']

with open(infile,"r") as tsvinput:
    tsvlines=tsvinput.readlines()

tsvheader=tsvlines[0]
tsvheadernames=tsvheader.split()

with open(mimfile,"r") as mimput:
    mimlines=mimput.readlines()

mimheader=mimlines[0]
mimheadernames=mimheader.split()

tsvsymbolval=0
for field in tsvheadernames:
    if (tsvsymbol==field):
        break
    tsvsymbolval+=1
print("tsvsymbolval=",tsvsymbolval)

mimsymbolval=0
for field in mimheadernames:
    if (mimsymbol==field):
        break
    mimsymbolval+=1
print("mimsymbolval=",mimsymbolval)

mimnumberval=0
for field in mimheadernames:
    if (mimnumber==field):
        break
    mymnumbervale+=1
print("mimnumberval=",mimnumberval)

with open(outfile,"w") as tsvoutput:
    for tsvline in tsvlines:
        if tsvline.startswith('>'):
            tsvoutput.write(tsvline.rstrip('\n')+'\t'+mimnumber+'\n')
        else:
            matched=False
            tsvfields=tsvline.split('\t')
            for mimline in mimlines:
                mimfields=mimline.split('\t')
                print("tsvfields[tsvsymbolval]=",tsvfields[tsvsymbolval])
                print("mimfields[mimsymbolval]=",mimfields[mimsymbolval])
                print()
                print()
                if (tsvfields[tsvsymbolval] == mimfields[mimsymbolval].rstrip('\n')):
                    tsvoutput.write(tsvline.rstrip('\n')+'\t'+mimfields[mimnumberval]+'\n')
                    matched=True
                    break
            if (matched==False):
                tsvoutput.write(tsvline.rstrip('\n')+'\t'+'\n')
