import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-f','--fathercol',required=True)
parser.add_argument('-m','--mothercol',required=True)
parser.add_argument('-p','--probandcol',required=True)
parser.add_argument('--mothergeno',required=True)
parser.add_argument('--fathergeno',required=True)
parser.add_argument('--probandgeno',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
fathercol=args['fathercol']
mothercol=args['mothercol']
probandcol=args['probandcol']
mothergeno=args['mothergeno']
fathergeno=args['fathergeno']
probandgeno=args['probandgeno']

with open(infile,"r") as tsvinput:
    lines=tsvinput.readlines()

for line in lines:
    if line.startswith('>'):
        header=line
        break

headernames=header.split()
fathervalue=0
mothervalue=0
probandvalue=0
for field in headernames:
    if (fathercol==field):
        break
    fathervalue+=1
print("fathervalue=%s" % fathervalue)
for field in headernames:
    if (mothercol==field):
        break
    mothervalue+=1
print("mothervalue=%s" % mothervalue)
for field in headernames:
    if (probandcol==field):
        break
    probandvalue+=1
print("probandvalue=%s" % probandvalue)

print('mothercol=%s,fathercol=%s,probandcol=%s'% (mothercol,fathercol,probandcol))
print('mothergeno=%r,fathergeno=%r,probandgeno=%r'% (mothergeno,fathergeno,probandgeno))
with open(outfile,"w") as tsvoutput:
    for line in lines:
        if line.startswith('>') or line.startswith('#') or line.startswith('\n'):
            tsvoutput.write(line)
        else:
            fields=line.split('\t')
            #print('fields=%s' % fields)
            #print('mothervalue=%s' % mothervalue)
            #print('fathervalue=%s' % fathervalue)
            #print('probandvalue=%s' % probandvalue)
            #print('mother=%s' % fields[mothervalue].rstrip('\n'))
            #print('father=%s' % fields[fathervalue].rstrip('\n'))
            #print('proband=%s' % fields[probandvalue].rstrip('\n'))
            mother=fields[mothervalue].strip()
            father=fields[fathervalue].strip()
            proband=fields[probandvalue].strip()
            if mother == mothergeno and father == fathergeno and proband == probandgeno:
                #print('mother=%r,father=%r,proband=%r'%(mother,father,proband))
                continue
            else:
                tsvoutput.write(line)
