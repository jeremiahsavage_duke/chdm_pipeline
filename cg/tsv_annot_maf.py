import os
import argparse

parser=argparse.ArgumentParser("gvf input file reader")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
parser.add_argument('-v','--vcfdir',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']
vcfdir=args['vcfdir']

def attrdict2string(attributes_dict,headerordata):
    vcfannotstring=""
    keys_list = attributes_dict.keys()
    keys_list.sort()
    for k in keys_list:
        if(headerordata=="data"):
            vcfannotstring += attributes_dict[k]+'\t'
        elif(headerordata=="header"):
            vcfannotstring += k+'\t'
    vcfannotstring = vcfannotstring[:-1] #remove trailing '\t'
    return vcfannotstring

def vcfinfoparse(vcfinfofield,headerordata):
    attributes=vcfinfofield.splint(';')
    attributes_dict=dict()
    for attribute in attributes:
        if('=' in attribute):
            key=attribute.split('=')[0]
            value=attribute.split('=')[1]
            attributes_dict[key]=value
        else:
            attributes_dict[key]='None'
    MAF_eaaaall_list=attributes_dict.pop('MAF').split(',')
    attributes_dict['MAF_EA']=MAF_eaaaall_list[0]
    attributes_dict['MAF_AA']=MAF_eaaaall_list[1]
    attributes_dict['MAF_ALL']=MAF_eaaaall_list[2]
    vcfannotstring=attrdict2string(attributes_dict,headerordata)
    return vcfannotstring


def getvcfdata(chromname,chrompos,vcfgenome_dict):
    vcfdata=vcfgenome_dict[chromname]
    vcfannotstring=""
    for vcfline in vcfdata:
        vcfpos = int(vcfline.split('\t')[1])
        if (vcfpos == chrompos):
            vcfinfofield=vcfline.split('\t')[7]
            vcfannotstring=vcfinfoparse(vcfinfofield,"data")
            break
        if (vcfpos > chrompos):
            vcfannotstring=getnulldata()
    return vcfannotstring


def getvcfheader(vcfgenome_dict):
    achromosome=vcfgenome_dict.keys()[0]
    print("achromosome=",achromosome)
    a,b,c=vcfgenome_dict[achromosome].partition('\n')
    print("vcfgenome_dict[achromosome].partition('\n')=",a)
    print("a.split('\t')=",a.split('\t'))
    vcfinfofield=vcfgenome_dict[achromosome][0].split('\t')[7]
    vcfannotstring=vcfinfoparse(vcfinfofield,"header")


def tsvannot(infile,outfile,vcfgenome_dict):
    with open(infile,"r") as tsvfile:
        tsvlines = tsvfile.readlines()

    tsvoutfile=open(outfile,'w')

    for tsvline in tsvlines:
        if tsvline.startswith("variantId"):
            vcfheader=getvcfheader(vcfgenome_dict)
            tsvoutfile.write(line+'\t'+vcfheader)
        else:
            data=line.split()
            chromname = data[1]
            chrompos = data[3]
            vcfline = getvcfdata(chromname,chrompos,vcfgenome_dict)
            tsvoutfile.write(line)
                
    tsvoutfile.close()


def readvcffiles(vcfdir):
    vcffiles = [f for f in os.listdir(vcfdir) if f.endswith('.vcf')]
    vcffiles.sort()
    genome_dict=dict()
    reallines=""
    for f in vcffiles:
        chromname = f.split('.')[1]
        genome_dict[chromname] = ''
    
    for vcffile in vcffiles:
        with open(os.path.normpath(vcfdir+"/"+vcffile),"r") as vcfinfile:
            vcflines=vcfinfile.readlines()
        for line in vcflines:
            if line.startswith("#"):
                continue
            else:
                reallines += line
        genome_dict[vcffile.split('.')[1]]=reallines
            
    return genome_dict


vcfgenome_dict=readvcffiles(vcfdir)
tsvannot(infile,outfile,vcfgenome_dict)
