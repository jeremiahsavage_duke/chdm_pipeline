import os
import argparse

parser=argparse.ArgumentParser("pseudo-awk")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
parser.add_argument('-f','--fieldname',required=True)
parser.add_argument('-v','--fieldvalue',required=True)
parser.add_argument('-m','--headermark',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']
fieldname=args['fieldname']
fieldvalue=args['fieldvalue']
headermark=args['headermark']

def get_inlines(infile):
    with open(infile,"r") as infile_open:
        inlines=infile_open.readlines()
    return inlines


def get_headerline(inlines,headermark):
    for inline in inlines:
        if inline.startswith(headermark):
            return inline
    raise LookupError('headermark does not exist: %s' % headermark)


def get_headerkey(headerline,headername):
    headerids=headerline.split()
    headerkey=0
    for headerid in headerids:
        if (headername==headerid):
            return headerkey
        headerkey+=1
    raise LookupError('headername does not exist: %s' % headername)


def get_headerlinenumber(inlines,headermark):
    headerlinenumber=0
    for inline in inlines:
        if inline.startswith(headermark):
            headerlinenumber+=1
            return headerlinenumber
        else:
            headerlinenumber+=1
    raise LookupError('headermark does not exist: %s' % headermark)


def get_datalines(inlines,headermark):
    headerlinenumber=get_headerlinenumber(inlines,headermark)
    datalines=list()
    linenumber=0
    for inline in inlines:
        if linenumber<=headerlinenumber:
            linenumber+=1
            continue
        else:
            linenumber+=1
            datalines.append(inline)
    return datalines


def filter_table_lessthan(datalines,headerkey,fieldvalue):
    filtered_data=str()
    for dataline in datalines.splitlines():
        fields=dataline.split('\t')
        i=0
        if (fields[headerkey] == ''):
            filtered_data+=dataline
        elif (float(fields[headervalue]) <= fieldvalue):
            filtered_data+=dataline
    return filtered_data


def filter_table_list(datalines,headerkey,fieldvalues):
    filtered_data=str()
    for dataline in datalines:
        fields=dataline.split('\t')
        i=0
        if (fields[headerkey]==''):
            filtered_data+=dataline
        if fields[headerkey] in fieldvalues:
            filtered_data+=dataline
            #pass
        #else:
            #filtered_data+=dataline
    return filtered_data


def doit(infile,outfile,fieldname,fieldvalue,headermark):
    inlines=get_inlines(infile)
    headerline=get_headerline(inlines,headermark)
    headerkey=get_headerkey(headerline,fieldname)
    datalines=get_datalines(inlines,headermark)
    if isinstance(fieldvalue,(int,float,complex)):
        filter_table_lessthan(datalines,headerkey,float(fieldvalue))
    else:
        fieldvalues=fieldvalue.split()
        filtered_data=filter_table_list(datalines,headerkey,fieldvalues)
    with open(outfile,'w') as outfile_open:
        outfile_open.write(headerline)
        outfile_open.write(filtered_data)

doit(infile,outfile,fieldname,fieldvalue,headermark)
