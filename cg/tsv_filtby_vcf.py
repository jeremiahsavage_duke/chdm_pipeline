import os
import argparse

parser=argparse.ArgumentParser("gvf input file reader")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
parser.add_argument('-v','--vcffilter',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']
vcffilter=args['vcffilter']

def getdblist(dbstring):
    dblist=dbstring.split(';')
    dblist=[]
    #dbdict=dict()
    for record in dblist:
        dbName=record.split(':')[0]
        dbID=record.split(':')[1]
        #dbdict[dbName]=dbID
        dblist.append(dbID)
    return dblist

def tsvparse(infile,outfile,vcflines):
    with open(infile,"r") as tsvfile:
        lines = tsvfile.readlines()

    tsvoutfile=open(outfile,'w')

    for line in lines:
        novelVar=True
        if line.startswith("#"):
            tsvoutfile.write(line)
        else:
            data=line.split()
            if (data[4] == "snp") and (len(data) == 10): #snp w/record
                novelVar=False
                dblist=getdblist(data[7])
            elif (data[4] == "del") and (len(data) == 9):
                novelVar=False
                dblist=getdblist(data[6])
            elif (data[4] == "ins") and (len(data) == 9):
                novelVar=False
                dblist=getdblist(data[6])
            elif (data[4] == "sub") and (len(data) == 10):
                novelVar=False
                dblist=getdblist(data[7])

        if (novelVar==False):
            varFiltFlag=isVarFiltered(dblist,vcflines)
            if (varFiltFlag):
                print("Common var is removed: ", dblist)
            else:
                tsvoutfile.write(line)
        else:
            tsvoutfile.write(line)
                
    tsvoutfile.close()


def isVarFiltered(dblist,vcflines):
    for vcfline in vcflines:
        for dbID in dblist:
            if dbID in vcfline:
                return True
    return False


def readVcfFile(infile):
    with open(infile,"r") as vcfinfile:
        vcflines=vcfinfile.readlines()
    return vcflines


vcflines=readVcfFile(vcffilter)
tsvparse(infile,outfile,vcflines)
