import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-p','--proband',required=True)
parser.add_argument('-g','--genotype',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
proband=args['proband']
genotype=args['genotype']

with open(infile,"r") as tsvinput:
    lines=tsvinput.readlines()

header=lines[0]
headernames=header.split()

probandvalue=0
for field in headernames:
    if (proband==field):
        break
    probandvalue+=1
print("probandvalue=",probandvalue)


with open(outfile,"w") as tsvoutput:
    for line in lines:
        if line.startswith('>'):
            tsvoutput.write(line)
        else:
            fields=line.split('\t')
            if (fields[probandvalue] == genotype):
                continue
            else:
                tsvoutput.write(line)
