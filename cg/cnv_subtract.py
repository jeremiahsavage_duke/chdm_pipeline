import argparse
import os

parser = argparse.ArgumentParser("subtract from proband")
parser.add_argument('-p','--proband',required=True)
parser.add_argument('-m','--mother',required=False)
parser.add_argument('-f','--father',required=False)
args=vars(parser.parse_args())

proband=args['proband']
mother=args['mother']
father=args['father']

def dosubtraction(probanddata,parentdata):
    subresults=str()
    probandlines=probanddata.splitlines()
    parentlines=parentdata.splitlines()
    pbcount=int(0)
    for probandline in probandlines:
        pbsplit=probandline.split('\t')
        pbchr=pbsplit[0]
        pbbegin=pbsplit[1]
        pbend=pbsplit[2]
        pbploidy=pbsplit[5]
        match=False
        for parentline in parentlines:
            parsplit=parentline.split('\t')
            parchr=parsplit[0]
            parbegin=parsplit[1]
            parend=parsplit[2]
            parploidy=parsplit[5]
            if (pbchr==parchr) and (pbbegin==parbegin) and (pbend==parend) and (pbploidy==parploidy):
                print('pbchr=',pbchr)
                print('\tparchr=',parchr)
                print('pbbegin=',pbbegin)
                print('\tparbegin=',parbegin)
                print('pbend=',pbend)
                print('\tparend=',parend)
                print('pbploid=',pbploidy)
                print('\tparploid=',parploidy)
                pbcount+=1
                match=True
                break
        if not match:
            subresults+=probandline
    print('pbcount=',pbcount)
    return subresults

def writeheader(proband,outfile):
    with open(proband,'r') as proband_open:
        probandlines=proband_open.readlines()

    outfile_open=open(outfile,'w')

    for probandline in probandlines:
        if probandline.startswith('#'):
            outfile_open.write(probandline)
        if probandline.startswith('>'):
            outfile_open.write(probandline)
        if probandline == '\n':
            outfile_open.write(probandline)
        else:
            continue

    outfile_open.close()
        

def getdata(person):
    with open (person,'r') as person_open:
        personlines=person_open.readlines()

    persondata=str()
    for personline in personlines:
        if personline.startswith('#'):
            continue
        if personline.startswith('>'):
            continue
        if personline == '\n':
            continue
        else:
            persondata+=personline

    return persondata

def writeresults(subresults,outfile):
    outfile_open=open(outfile,'a')
    outfile_open.write('\n')
    for subresult in subresults:
        outfile_open.write(subresult)
    outfile_open.close()

def subtract(proband,mother,father):
    origfile,origext=os.path.splitext(proband)
    outfile=str()
    if (mother is not None) and (father is not None):
        outfile=origfile+'_submother_subfather'+origext
        writeheader(proband,outfile)

        probanddata=getdata(proband)
        motherdata=getdata(mother)
        fatherdata=getdata(father)

        subresults=dosubtraction(probanddata,motherdata)
        subresults=dosubtraction(subresults,fatherdata)

        writeresults(subresults,outfile)
    elif mother is not None:
        outfile=origfile+'_submother'+origext
        writeheader(proband,outfile)

        probanddata=getdata(proband)
        motherdata=getdata(mother)

        subresults=dosubtraction(probanddata,motherdata)

        writeresults(subresults,outfile)
    elif father is not None:
        outfile=origfile+'_subfather'+origext
        writeheader(proband,outfile)

        probanddata=getdata(proband)
        fatherdata=getdata(father)

        subresults=dosubtraction(probanddata,fatherdata)

        writeresults(subresults,outfile)
    else:
        import sys
        print('Must substract something')
        sys.exit(0)


subtract(proband,mother,father)
