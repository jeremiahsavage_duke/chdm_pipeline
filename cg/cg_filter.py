import os
import argparse

parser=argparse.ArgumentParser("filter (eg ""01 01 11"")")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-f','--filter',required=True)
args=vars(parser.parse_args())

tsvinput=args['input']
tsvfilter=args['filter']
shortname=''.join(tsvfilter.split())
origname,origextension=os.path.splitext(tsvinput)
tsvoutput=origname+"_"+shortname+origextension

with open(tsvinput,"r") as tsvfile:
    lines=tsvfile.readlines()

with open(tsvoutput,"w") as tsvfile:
    tsvfile.write(lines[0])
    for line in lines:
        templine=' '.join(line.split())
        if templine.endswith(tsvfilter):
            tsvfile.write(line)
