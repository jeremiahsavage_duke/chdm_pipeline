import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-f','--fathercol',required=True)
parser.add_argument('-m','--mothercol',required=True)
parser.add_argument('-p','--probandcol',required=True)
parser.add_argument('--mothergeno',required=True)
parser.add_argument('--fathergeno',required=True)
parser.add_argument('--probandgeno',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
fathercol=args['fathercol']
mothercol=args['mothercol']
probandcol=args['probandcol']
mothergeno=int(args['mothergeno'])
fathergeno=int(args['fathergeno'])
probandgeno=int(args['probandgeno'])

with open(infile,"r") as tsvinput:
    lines=tsvinput.readlines()

header=lines[8]
headernames=header.split()
fathervalue=0
mothervalue=0
probandvalue=0
for field in headernames:
    if (fathercol==field):
        break
    fathervalue+=1
print("fathervalue=",fathervalue)
for field in headernames:
    if (mothercol==field):
        break
    mothervalue+=1
print("mothervalue=",mothervalue)
for field in headernames:
    if (probandcol==field):
        break
    probandvalue+=1
print("probandvalue=",probandvalue)

print('mothergeno=%r,fathergeno=%r,probandgeno=%r'% (mothergeno,fathergeno,probandgeno))
with open(outfile,"w") as tsvoutput:
    for line in lines:
        if line.startswith('>') or line.startswith('#') or line.startswith('\n'):
            tsvoutput.write(line)
        else:
            fields=line.split('\t')
            mother=int(fields[mothervalue].rstrip('\n'))
            father=int(fields[fathervalue].rstrip('\n'))
            proband=int(fields[probandvalue].rstrip('\n'))
            if mother == mothergeno and father == fathergeno and proband == probandgeno:
                tsvoutput.write(line)
