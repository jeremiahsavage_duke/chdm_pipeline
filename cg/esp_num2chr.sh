INFILE=$1
for i in {1..22}
do
    echo i = ${i}
    sed -i "s/^${i}\t/chr${i}\t/g" ${INFILE}
done

sed -i 's/^X/chrX/g' ${INFILE}
sed -i 's/^Y/chrY/g' ${INFILE}
sed -i 's/^M/chrM/g' ${INFILE}
