import os
import argparse

parser=argparse.ArgumentParser("gvf input file reader")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
parser.add_argument('-f','--field',required=True)
parser.add_argument('-c','--cutoff',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']
infofield=args['field']
cutoff=args['cutoff']

def getgmaf(popfreqstring):
    popfreqlist=popfreqstring.split(',')
    na=0
    c1=0
    for vals in popfreqlist:
        na += int(vals.split('(')[1].split('/')[0])
        c1 += int(vals.split('(')[2].split('/')[0])
    if (int(c1) > 0):
        gmafval = float(na)/float(c1)
    else:
        gmafval=0
    return gmafval

def getinfoval(info,field):
    attributes=info.split(';')
    attributesdict=dict()
    for attribute in attributes:
        if ('=' in attribute):
            key=attribute.split('=')[0]
            value=attribute.split('=')[1]
            attributesdict[key]=value
        else:
            attributesdict[attribute]=None
    #if ("GMAF" == field):
    if (field in info):
        return attributesdict[field]
    #    else:
    #        gmafval = getgmaf(attributesdict['POPFREQ'])
    #        return gmafval
    else:
        return 0

def VCFparse(infile,outfile,infofield,cutoff):
    with open(infile,"r") as vcfinfile:
        lines = vcfinfile.readlines()

    vcfoutfile = open(outfile,'w')

    for line in lines:
        if line.startswith("#"):
            vcfoutfile.write(line)
        else:
            data=line.split()
            contig=data[0]
            position=data[1]
            dbID=data[2]
            referenceSeq=data[3]
            variantSeq=data[4]
            quality=data[5]
            vcffilter=data[6]
            info=data[7]
        
            infoval = getinfoval(info,infofield)
            if (float(infoval) >= float(cutoff)):
                vcfoutfile.write(line)

    vcfoutfile.close()

VCFparse(infile,outfile,infofield,cutoff)
