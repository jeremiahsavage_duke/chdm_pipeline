import argparse
import os

parser = argparse.ArgumentParser("subtract from proband")
parser.add_argument('-p','--proband',required=True)
parser.add_argument('-m','--mother',required=False)
parser.add_argument('-f','--father',required=False)
args=vars(parser.parse_args())

proband=args['proband']
mother=args['mother']
father=args['father']

def getsmallerchr(firstchr,secondchr):
    if firstchr.isdigit():
        firstnum=int(firstchr)
        if secondchr.isdigit():
            secondnum=int(secondchr)
            if firstnum==secondnum:
                return 'equal'
            elif firstnum<secondnum:
                return 'first'
            elif secondnum<firstnum:
                return 'second'
        else:
            return 'first'
    elif secondchr.isdigit():
        return 'second'
    else:
        if firstchr == secondchr:
            return 'equal'
        if firstchr == 'X':
            if secondchr == 'Y':
                return 'first'
            elif secondchr == 'M':
                return 'first'
        elif firstchr == 'Y':
            if secondchr == 'M':
                return 'first'
        else:
            return 'second'
            
def getsmallerpos(firstpos,secondpos):
    if firstpos==secondpos:
        return 'equal'
    elif firstpos<secondpos:
        return 'first'
    elif secondpos<firstpos:
        return 'second'


def getsmallerploidy(firstploidy,secondploidy):
    #print('getsmallerploidy()::firstploidy=',firstploidy)
    #print('getsmallerploidy()::secondploidy=',secondploidy)
    if firstploidy.isdigit() and secondploidy.isdigit():
        firstploidynum=int(firstploidy)
        secondploidynum=int(secondploidy)
        if firstploidynum==secondploidynum:
            return 'equal'
        elif firstploidynum<secondploidynum:
            return 'first'
        elif secondploidynum<firstploidynum:
            return 'second'
    elif firstploidy.isdigit():
        return 'first'
    elif secondploidy.isdigit():
        return 'second'
    elif firstploidy==secondploidy:
        return 'equal'

def getsmallerpair(firstname,firstdata,firstpos,secondname,seconddata,secondpos):
    #print('firstdata=',firstdata.split('\n')[firstpos])
    #print('seconddata=',seconddata.split('\n')[secondpos])
    firstchr=firstdata.split('\n')[firstpos].split('\t')[0].lstrip('chr')
    firstbegin=int(firstdata.split('\n')[firstpos].split('\t')[1])
    firstend=int(firstdata.split('\n')[firstpos].split('\t')[2])
    firstploidy=firstdata.split('\n')[firstpos].split('\t')[5]
    secondchr=seconddata.split('\n')[secondpos].split('\t')[0].lstrip('chr')
    secondbegin=int(seconddata.split('\n')[secondpos].split('\t')[1])
    secondend=int(seconddata.split('\n')[secondpos].split('\t')[2])
    secondploidy=seconddata.split('\n')[secondpos].split('\t')[5]

    smallerchr=getsmallerchr(firstchr,secondchr)
    #print('getsmallerpair()::smallerchr=',smallerchr)
    if smallerchr=='first':
        return firstname
    elif smallerchr=='second':
        return secondname
    elif smallerchr=='equal':
        smallerbegin=getsmallerpos(firstbegin,secondbegin)
        #print('getsmallerpair()::smallerbegin=',smallerbegin)
        if smallerbegin=='first':
            return firstname
        elif smallerbegin=='second':
            return secondname
        elif smallerbegin=='equal':
            smallerend=getsmallerpos(firstend,secondend)
            #print('getsmallerpair()::smallerend=',smallerend)
            if smallerend=='first':
                return firstname
            elif smallerend=='second':
                return secondname
            elif smallerend=='equal':
                smallerploidy=getsmallerploidy(firstploidy,secondploidy)
                #print('getsmallerpair()::smallerploidy=',smallerploidy)
                if smallerploidy=='first':
                    return firstname
                elif smallerploidy=='second':
                    return secondname
                elif smallerploidy=='equal':
                    return 'equal'

# 1=1 m
# 2=2 f
# 3=3 p
# 4=1,2 m,f
# 5=1,3 m,p
# 6=2,3 f,p 
# 7=1,2,3 m,f,p
def getsmallestoftrio(motherdata,fatherdata,probanddata,motherpos,fatherpos,probandpos):
    firsttest=getsmallerpair('mother',motherdata,motherpos,'father',fatherdata,fatherpos)
    #print('getsmallestoftrio()::firsttest=',firsttest)
    if firsttest=='equal':
        secondtest=getsmallerpair('mother',motherdata,motherpos,'proband',probanddata,probandpos)
        #print('getsmallestoftrio()::secondtest=',secondtest)
        if secondtest=='equal':
            return 7
        if secondtest=='mother':
            return 4
        if secondtest=='proband':
            return 3
    if firsttest=='mother':
        secondtest=getsmallerpair('mother',motherdata,motherpos,'proband',probanddata,probandpos)
        if secondtest=='equal':
            return 5
        if secondtest=='mother':
            return 1
        if secondtest=='proband':
            return 3
    if firsttest=='father':
        secondtest=getsmallerpair('father',motherdata,motherpos,'proband',probanddata,probandpos)
        if secondtest=='equal':
            return 6
        if secondtest=='father':
            return 2
        if secondtest=='proband':
            return 3

def getnextcnv(motherdata,fatherdata,probanddata,motherpos,fatherpos,probandpos):
    smallestoftrio=getsmallestoftrio(motherdata,fatherdata,probanddata,motherpos,fatherpos,probandpos)
    #print('getnextcnv()::smallestoftrio=',smallestoftrio)
    #print('getnextcnv()::motherpos=',motherpos)
    #print('getnextcnv()::motherdata.split(\'\n\')[motherpos]=',motherdata.split('\n')[motherpos])
    cnvrecord=str()
    if smallestoftrio==7:
        cnvrecord=motherdata.split('\n')[motherpos]
        motherpos+=1
        fatherpos+=1
        probandpos+=1
    elif smallestoftrio==6:
        cnvrecord=fatherdata.split('\n')[fatherpos]
        fatherpos+=1
        probandpos+=1
    elif smallestoftrio==5:
        cnvrecord=motherdata.split('\n')[motherpos]
        motherpos+=1
        probandpos+=1
    elif smallestoftrio==4:
        cnvrecord=motherdata.split('\n')[motherpos]
        motherpos+=1
        fatherpos+=1
    elif smallestoftrio==3:
        cnvrecord=probanddata.split('\n')[probandpos]
        probandpos+=1
    elif smallestoftrio==2:
        cnvrecord=fatherdata.split('\n')[fatherpos]
        fatherpos+=1
    elif smallestoftrio==1:
        cnvrecord=motherdata.split('\n')[motherpos]
        motherpos+=1
    #print('getnextcnv()::cnvrecord=',cnvrecord)
    #print('getnextcnv()::smallestoftrio=',smallestoftrio)
    return cnvrecord,motherpos,fatherpos,probandpos


def isrecordinindividual(cnvrecord,individualdata):
    #print('isrecordinindividual()::cnvrecord=%s' % cnvrecord)
    cnvchr=cnvrecord.split('\t')[0].lstrip('chr')
    cnvbegin=int(cnvrecord.split('\t')[1])
    cnvend=int(cnvrecord.split('\t')[2])
    cnvploidy=cnvrecord.split('\t')[5]
    #print('cnvchr=%s, cnvbegin=%r, cnvend=%s, cnvploidy=%s' % (cnvchr, cnvbegin, cnvend, cnvploidy))
    for dataline in individualdata.split('\n'):
        if len(dataline)==0:
            continue
        indchr=dataline.split('\t')[0].lstrip('chr')
        indbegin=int(dataline.split('\t')[1])
        indend=int(dataline.split('\t')[2])
        indploidy=dataline.split('\t')[5]
        if cnvchr==indchr and cnvbegin==indbegin and cnvend==indend and cnvploidy==indploidy:
            return True
    return False

def gettriorecord(cnvrecord,motherdata,fatherdata,probanddata):
    #print('gettriorecord()::cnvrecord=',cnvrecord)
    triorecord=cnvrecord.strip()
    #print('gettriorecord()::triorecord=',triorecord)
    if isrecordinindividual(cnvrecord,motherdata):
        triorecord+='\t1'
    else:
        triorecord+='\t0'
    if isrecordinindividual(cnvrecord,fatherdata):
        triorecord+='\t1'
    else:
        triorecord+='\t0'
    if isrecordinindividual(cnvrecord,probanddata):
        triorecord+='\t1'
    else:
        triorecord+='\t0'
    return triorecord

def dotrio(motherdata,fatherdata,probanddata):
    trioresults=str()
    motherpos=int(0)
    fatherpos=int(0)
    probandpos=int(0)
    while True:
        cnvrecord,motherpos,fatherpos,probandpos=getnextcnv(motherdata,fatherdata,probanddata,motherpos,fatherpos,probandpos)
        triorecord=gettriorecord(cnvrecord,motherdata,fatherdata,probanddata)
        print('%s' % triorecord)
    
    return trioresults

def getshortname(individual):
    shortname=individual.rstrip('-ASM').lstrip('GS0000')
    return shortname


def writeheader(mother,father,proband,outfile):
    with open(proband,'r') as proband_open:
        probandlines=proband_open.readlines()
    outfile_open=open(outfile,'w')
    for probandline in probandlines:
        if probandline.startswith('#'):
            outfile_open.write(probandline)
        if probandline.startswith('>'):
            mothershort=getshortname(mother)
            fathershort=getshortname(father)
            probandshort=getshortname(proband)
            newline=probandline.strip()+'\t'+mothershort+'\t'+fathershort+'\t'+probandshort+'\n'
            outfile_open.write(newline)
        if probandline == '\n':
            outfile_open.write(probandline)
        else:
            continue
    outfile_open.close()
        

def getdata(person):
    with open (person,'r') as person_open:
        personlines=person_open.readlines()
    persondata=str()
    for personline in personlines:
        if personline.startswith('#'):
            continue
        if personline.startswith('>'):
            continue
        if personline == '\n':
            continue
        else:
            persondata+=personline
    return persondata

def writeresults(subresults,outfile):
    outfile_open=open(outfile,'a')
    outfile_open.write('\n')
    for subresult in subresults:
        outfile_open.write(subresult)
    outfile_open.close()

def trioit(mother,father,proband):
    origfile,origext=os.path.splitext(proband)
    outfile=origfile+'_trio'+origext
    writeheader(mother,father,proband,outfile)

    probanddata=getdata(proband)
    motherdata=getdata(mother)
    fatherdata=getdata(father)

    trioresults=dotrio(motherdata,fatherdata,probanddata)

    writeresults(trioresults,outfile)


trioit(mother,father,proband)
