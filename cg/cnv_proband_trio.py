import argparse
import os

parser = argparse.ArgumentParser("subtract from proband")
parser.add_argument('-p','--proband',required=True)
parser.add_argument('-m','--mother',required=False)
parser.add_argument('-f','--father',required=False)
args=vars(parser.parse_args())

proband=args['proband']
mother=args['mother']
father=args['father']


def isrecordinindividual(cnvrecord,individualdata):
    cnvchr=cnvrecord.split('\t')[0].lstrip('chr')
    cnvbegin=int(cnvrecord.split('\t')[1])
    cnvend=int(cnvrecord.split('\t')[2])
    cnvploidy=cnvrecord.split('\t')[5]
    for dataline in individualdata.split('\n'):
        indchr=dataline.split('\t')[0].lstrip('chr')
        indbegin=int(dataline.split('\t')[1])
        indend=int(dataline.split('\t')[2])
        indploidy=dataline.split('\t')[5]
        if cnvchr==indchr and cnvbegin==indbegin and cnvend==indend and cnvploidy==indploidy:
            return True
    return False


def gettriorecord(cnvrecord,motherdata,fatherdata):
    triorecord=cnvrecord
    if isrecordinindividual(cnvrecord,motherdata):
        triorecord+='\t1'
    else:
        triorecord+='\t0'
    if isrecordinindividual(cnvrecord,fatherdata):
        triorecord+='\t1'
    else:
        triorecord+='\t0'
    triorecord+='\t1'
    return triorecord


def dotrio(motherdata,fatherdata,probanddata):
    trioresults=str()
    for cnvrecord in probanddata.split('\n'):
        trioresult=gettriorecord(cnvrecord,motherdata,fatherdata)
        trioresults+=trioresult+'\n'
        print('%s' % trioresult)    
    return trioresults


def getshortname(individual):
    shortname=os.path.basename(individual).rstrip('-ASM.tsv').lstrip('cnvSegmentsDiploidBeta-GS0000')
    return shortname


def writeheader(mother,father,proband,outfile):
    with open(proband,'r') as proband_open:
        probandlines=proband_open.readlines()
    outfile_open=open(outfile,'w')
    for probandline in probandlines:
        if probandline.startswith('#'):
            outfile_open.write(probandline)
        if probandline.startswith('>'):
            mothershort=getshortname(mother)
            fathershort=getshortname(father)
            probandshort=getshortname(proband)
            newline=probandline.strip()+'\t'+mothershort+'\t'+fathershort+'\t'+probandshort+'\n'
            outfile_open.write(newline)
        if probandline == '\n':
            outfile_open.write(probandline)
        else:
            continue
    outfile_open.flush()
    outfile_open.close()
        

def getdata(person):
    with open (person,'r') as person_open:
        personlines=person_open.readlines()
    persondata=str()
    for personline in personlines:
        if personline.startswith('#'):
            continue
        if personline.startswith('>'):
            continue
        if personline == '\n':
            continue
        else:
            persondata+=personline
    return persondata


def writeresults(trioresults,outfile):
    outfile_open=open(outfile,'a')
    for trioresult in trioresults:
        outfile_open.write(trioresult)
    outfile_open.close()


def trioit(mother,father,proband):
    origfile,origext=os.path.splitext(proband)
    outfile=origfile+'_trio'+origext
    writeheader(mother,father,proband,outfile)

    probanddata=getdata(proband).rstrip('\n')
    motherdata=getdata(mother).rstrip('\n')
    fatherdata=getdata(father).rstrip('\n')

    trioresults=dotrio(motherdata,fatherdata,probanddata)
    writeresults(trioresults,outfile)


trioit(mother,father,proband)
