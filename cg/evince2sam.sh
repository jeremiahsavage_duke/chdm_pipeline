#!/bin/sh
for i in {1..22}; do
    cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013595-ASM.tsv.bz2 --output ~/complete_genomics/output/sam/GS000013595_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr
done
