import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
parser.add_argument('-m','--matchfile',required=True)
parser.add_argument('-f','--incol',required=True)
parser.add_argument('-s','--matchcol',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
matchfile=args['matchfile']
incol=args['incol']
matchcol=args['matchcol']


with open(infile,"r") as infile_f:
    inlines=infile_f.readlines()

with open(matchfile,"r") as matchfile_f:
    matchlines=matchfile_f.readlines()


incolval=0
inheader=inlines[0]
inheadernames=inheader.split('\t')
for field in inheadernames:
    if (incol==field.rstrip('\n')):
        break
    incolval+=1
print("incolval=",incolval)

matchcolval=0
matchheader=matchlines[0]
matchheadernames=matchheader.split('\t')
for field in matchheadernames:
    if (matchcol==field.rstrip('\n')):
        break
    matchcolval+=1
print("matchcolval=",matchcolval)
matchheader=matchlines[0]
matchlines=matchlines[1:]

with open(outfile,"w") as outfile_f:
    linum=0
    for inline in inlines:
        if (linum==0):
            outfile_f.write(inline.rstrip('\n')+'\t'+matchheader)
        else:
            matched=False
            infields=inline.split('\t')
            inval=infields[incolval].rstrip('\n')
            print("linum=",linum)
            print("inval",inval)
            numfields=len(infields)
            for matchline in matchlines:
                matchfields=matchline.split('\t')
                matchval=matchfields[matchcolval]
                if ((inval == matchval) and (inval != "")):
                    print("matchval",matchval)
                    print("YESSS")
                    outfile_f.write(inline.rstrip('\n')+'\t'+matchline)
                    matched=True
                    break
            if (matched==False):
                i=0
                extratabs=""
                while(i < numfields):
                    extratabs+='\t'
                    i+=1
                outfile_f.write(inline.rstrip('\n')+extratabs+'\n')
        linum+=1
