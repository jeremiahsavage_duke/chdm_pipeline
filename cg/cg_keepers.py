import os
import argparse

parser=argparse.ArgumentParser("what to keep")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

with open(infile,"r") as infile_f:
    lines=infile_f.readlines()

keepers=[":CDS:","\tCDS\t",":FRAMSHIFT","\tFRAMESHIFT\t",":NONSENSE","\tNONSENSE\t",":DISRUPT","\tDISRUPT\t",":ACCEPTOR","\tACCEPTOR\t",":DONOR","\tDONOR\t",":MISSENSE","\tMISSENSE\t",":DELETE","\tDELETE",":INSERT","\tINSERT",":NONSYNONYMOUS","\tNONSYNONYMOUS"]

with open(outfile,"w") as outfile_f:
    for i in range(0,20):
        outfile_f.write(lines[i])

    for line in lines:
        for keeper in keepers:
            if keeper in line:
                outfile_f.write(line)
                break;
