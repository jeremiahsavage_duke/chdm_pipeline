import os
import argparse

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']
OVERLAPPINGGENE=9
KNOWNCNV=10
with open(infile,"r") as tsvinput:
    lines=tsvinput.readlines()


with open(outfile,"w") as tsvoutput:
    for line in lines:
        if line.startswith('>') or line.startswith('#') or line.startswith('\n'):
            tsvoutput.write(line)
        else:
            fields=line.split('\t')
            gene=fields[OVERLAPPINGGENE].strip('\t')
            knowncnv=fields[KNOWNCNV].strip('\t')
            if len(gene)>0 and len(knowncnv)==0:
                tsvoutput.write(line)
