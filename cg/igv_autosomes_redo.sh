new
genome hg19
snapshotDirectory /home/jeremiah/complete_genomics/output/sam/green_redo/
load /home/jeremiah/complete_genomics/output/sam/GS000013595_chr4_sort.bam
goto chr4:85547320
sort quality
snapshot
new
genome hg19
snapshotDirectory /home/jeremiah/complete_genomics/output/sam/orange_redo/
load /home/jeremiah/complete_genomics/output/sam/GS000013595_chr19_sort.bam
goto chr19:48258636
sort quality
snapshot
new
genome hg19
snapshotDirectory /home/jeremiah/complete_genomics/output/sam/orange_redo/
load /home/jeremiah/complete_genomics/output/sam/GS000013595_chr7_sort.bam
goto chr7:43201317
sort quality
snapshot
new
genome hg19
snapshotDirectory /home/jeremiah/complete_genomics/output/sam/orange_redo/
load /home/jeremiah/complete_genomics/output/sam/GS000013595_chr17_sort.bam
goto chr17:43333215
sort quality
snapshot
new
genome hg19
snapshotDirectory /home/jeremiah/complete_genomics/output/sam/orange_redo/
load /home/jeremiah/complete_genomics/output/sam/GS000013595_chr6_sort.bam
goto chr6:31083800
sort quality
snapshot
