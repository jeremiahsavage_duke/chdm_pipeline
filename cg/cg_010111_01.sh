#make listvar
time cgatools listvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --output listvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

#make testvar
time cgatools testvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --input listvar_trio.tsv --output testvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2




###START HERE###

#grep 01 01 11
time python ~/chdm_pipeline/cg_filter.py -i testvar_trio.tsv -f "01 01 11"

#filter "common"
time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_010111.tsv -o testvar_trio_010111_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

#annotate with gene info
time cgatools join --beta --input testvar_trio_010111_0.01.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_010111_annot.tsv --match chromosome:chromosome --select 'a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam' --overlap begin,end:begin,end --overlap-mode allow-abutting-points

#annotate with quality
time cgatools join --beta --input testvar_trio_010111_annot.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_010111_annot_qual.tsv --match chromosome:chromosome --select 'a.*,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality' --overlap begin,end:begin,end --overlap-mode allow-abutting-points

#remove duplicates
sort -u -t, -k1,1n testvar_trio_010111_annot.tsv > testvar_trio_010111_annot_uniq.tsv
sort -u -t, -k1,1n testvar_trio_010111_annot_qual.tsv > testvar_trio_010111_annot_qual_uniq.tsv

#count hits
tail -n +2  testvar_trio_010111_0.01.tsv | wc -l # num of vars
tail -n +2 testvar_trio_010111_annot_uniq.tsv | wc -l # num of annotated vars
tail -n +2 testvar_trio_010111_annot_qual_uniq.tsv | wc -l # num of annotated quality vars

#form file with header
rm testvar_trio_010111_annot_uniq_header.tsv
head testvar_trio_010111_annot.tsv -n 14 >> testvar_trio_010111_annot_qual_uniq_header.tsv
tail -n +2 testvar_trio_010111_annot_qual_uniq.tsv >> testvar_trio_010111_annot_qual_uniq_header.tsv

cp testvar_trio_010111_annot_qual_uniq_header.tsv testvar_trio_010111_annot_qual_uniq_header_VQLOWHIGH.tsv
#
tail -n +2  testvar_trio_010111_0.01.tsv | wc -l # num of vars
tail -n +2 testvar_trio_010111_annot_uniq.tsv | wc -l # num of annotated vars
tail -n +2 testvar_trio_010111_annot_qual_uniq.tsv | wc -l # num of annotated quality vars

sed -i '/VQLOW/d' testvar_trio_010111_annot_qual_uniq_header.tsv
tail -n +15 testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l

ack "\tTSS-UPSTREAM\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tACCEPTOR\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tINTRON\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR3\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR5\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR\t" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l

ack "\tdbsnp" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tsnp" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tsub" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tins" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tdel" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l




ack "\tVQLOW\tVQLOW" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tVQLOW\tVQHIGH" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tVQHIGH\tVQLOW" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l
ack "\tVQHIGH\tVQHIGH" testvar_trio_010111_annot_qual_uniq_header.tsv | wc -l

cp testvar_trio_010111_annot_qual_uniq_header.tsv testvar_trio_010111_annot_exon.tsv

time sed -i "/INTRON/d" testvar_trio_010111_annot_exon.tsv
time sed -i "/TSS-UPSTREAM/d" testvar_trio_010111_annot_exon.tsv
time sed -i "/UTR3/d" testvar_trio_010111_annot_exon.tsv
time sed -i "/UTR5/d" testvar_trio_010111_annot_exon.tsv
time sed -i "/UTR/d" testvar_trio_010111_annot_exon.tsv

