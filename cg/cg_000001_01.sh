#make listvar
time cgatools listvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --output listvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

#make testvar
time cgatools testvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --input listvar_trio.tsv --output testvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2




###START HERE###

#grep 00 00 11
time python ~/chdm_pipeline/cg_filter.py -i testvar_trio.tsv -f "00 00 01"

#filter "common"
time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_000001.tsv -o testvar_trio_000001_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

#annotate with gene info
time cgatools join --beta --input testvar_trio_000001_0.01.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_000001_annot.tsv --match chromosome:chromosome --select 'a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam' --overlap begin,end:begin,end --overlap-mode allow-abutting-points

#annotate with quality
time cgatools join --beta --input testvar_trio_000001_annot.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_000001_annot_qual.tsv --match chromosome:chromosome --select 'a.*,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality' --overlap begin,end:begin,end --overlap-mode allow-abutting-points

#remove duplicates
sort -u -t, -k1,1n testvar_trio_000001_annot.tsv > testvar_trio_000001_annot_uniq.tsv
sort -u -t, -k1,1n testvar_trio_000001_annot_qual.tsv > testvar_trio_000001_annot_qual_uniq.tsv

#count hits
tail -n +2  testvar_trio_000001_0.01.tsv | wc -l # num of vars
tail -n +2 testvar_trio_000001_annot_uniq.tsv | wc -l # num of annotated vars
tail -n +2 testvar_trio_000001_annot_qual_uniq.tsv | wc -l # num of annotated quality vars

#form file with header
rm testvar_trio_000001_annot_uniq_header.tsv
head testvar_trio_000001_annot.tsv -n 14 >> testvar_trio_000001_annot_qual_uniq_header.tsv
tail -n +2 testvar_trio_000001_annot_qual_uniq.tsv >> testvar_trio_000001_annot_qual_uniq_header.tsv

#
ack "\tdbsnp" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tsnp" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tsub" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tins" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tdel" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tEXON\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tINTRON\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tTSS-UPSTREAM\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR3\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR5\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tACCEPTOR\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tUTR\t" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tVQLOW\tVQLOW" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tVQLOW\tVQHIGH" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tVQHIGH\tVQLOW" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l
ack "\tVQHIGH\tVQHIGH" testvar_trio_000001_annot_qual_uniq_header.tsv | wc -l

cp testvar_trio_000001_annot_qual_uniq_header.tsv testvar_trio_000001_annot_exon.tsv

time sed -i '/INTRON/d' testvar_trio_000001_annot_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_000001_annot_exon.tsv
time sed -i '/UTR3/d' testvar_trio_000001_annot_exon.tsv
time sed -i '/UTR5/d' testvar_trio_000001_annot_exon.tsv
time sed -i '/ACCEPTOR/d' testvar_trio_000001_annot_exon.tsv
time sed -i '/UTR/d' testvar_trio_000001_annot_exon.tsv


##########2012-09-28
cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr1-GS000013595-ASM.tsv.bz2 --output e2s_chr1.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr6-GS000013595-ASM.tsv.bz2 --output e2s_chr6.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr8-GS000013595-ASM.tsv.bz2 --output e2s_chr8.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr9-GS000013595-ASM.tsv.bz2 --output e2s_chr9.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr10-GS000013595-ASM.tsv.bz2 --output e2s_chr10.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr11-GS000013595-ASM.tsv.bz2 --output e2s_chr11.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr12-GS000013595-ASM.tsv.bz2 --output e2s_chr12.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr13-GS000013595-ASM.tsv.bz2 --output e2s_chr13.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr15-GS000013595-ASM.tsv.bz2 --output e2s_chr15.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr16-GS000013595-ASM.tsv.bz2 --output e2s_chr16.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

cgatools evidence2sam --beta --evidence-dnbs ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr17-GS000013595-ASM.tsv.bz2 --output e2s_chr17.sam --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr &

samtools view -bS e2s_chr1.sam > e2s_chr1.bam
samtools view -bS e2s_chr6.sam > e2s_chr6.bam &
samtools view -bS e2s_chr8.sam > e2s_chr8.bam &
samtools view -bS e2s_chr9.sam > e2s_chr9.bam &
samtools view -bS e2s_chr10.sam > e2s_chr10.bam &
samtools view -bS e2s_chr11.sam > e2s_chr11.bam &
samtools view -bS e2s_chr12.sam > e2s_chr12.bam &
samtools view -bS e2s_chr13.sam > e2s_chr13.bam &
samtools view -bS e2s_chr15.sam > e2s_chr15.bam &
samtools view -bS e2s_chr16.sam > e2s_chr16.bam &
samtools view -bS e2s_chr17.sam > e2s_chr17.bam &

samtools sort e2s_chr1.bam e2s_chr1_sort.bam &
samtools sort e2s_chr6.bam e2s_chr6_sort.bam &
samtools sort e2s_chr8.bam e2s_chr8_sort.bam &
samtools sort e2s_chr9.bam e2s_chr9_sort.bam &
samtools sort e2s_chr10.bam e2s_chr10_sort.bam &
samtools sort e2s_chr11.bam e2s_chr11_sort.bam &
samtools sort e2s_chr12.bam e2s_chr12_sort.bam &
samtools sort e2s_chr13.bam e2s_chr13_sort.bam &
samtools sort e2s_chr15.bam e2s_chr15_sort.bam &
samtools sort e2s_chr16.bam e2s_chr16_sort.bam &
samtools sort e2s_chr17.bam e2s_chr17_sort.bam &

samtools index e2s_chr1_sort.bam &
samtools index e2s_chr6_sort.bam &
samtools index e2s_chr8_sort.bam &
samtools index e2s_chr9_sort.bam &
samtools index e2s_chr10_sort.bam &
samtools index e2s_chr11_sort.bam &
samtools index e2s_chr12_sort.bam &
samtools index e2s_chr13_sort.bam &
samtools index e2s_chr15_sort.bam &
samtools index e2s_chr16_sort.bam &
samtools index e2s_chr17_sort.bam &


