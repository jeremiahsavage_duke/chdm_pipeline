FILE=$1
sed -i 's/chr\([[:print:]]\)/\1/g' $FILE
sed -i '/apd_hap1/d' $FILE
sed -i '/cox_hap2/d' $FILE
sed -i '/dbb_hap3/d' $FILE
sed -i '/mcf_hap5/d' $FILE
sed -i '/qbl_hap6/d' $FILE
sed -i '/ctg9_hap1/d' $FILE
sed -i '/mann_hap4/d' $FILE
sed -i '/ssto_hap7/d' $FILE
sed -i '/Un_gl000.*/d' $FILE
sed -i '/ctg5_hap1/d' $FILE
sed -i '/gl000.*_random/d' $FILE
