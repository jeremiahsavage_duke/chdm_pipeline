#!/bin/bash

nextera_i5=("TAGATCGC" "CTCTCTAT" "TATCCTCT" "AGAGTAGA" "GTAAGGAG" "ACTGCATA" "AAGGAGTA" "CTAAGCCT")
nextera_i7=("TCGCCTTA" "CTAGTACG" "TTCTGCCT" "GCTCAGGA" "AGGAGTCC" "CATGCCTA" "GTAGAGAG" "CCTCTCTG" "AGCGTAGC" "CAGCCTCG" "TGCCTCTT" "TCCTCTAC")

#for var in "${nextera_i5[@]}" ; do echo "${var}" ; done
#for var in "${nextera_i7[@]}" ; do echo "${var}" ; done

D500=("AATGATACGGCGACCACCGAGATCTACACNNNNNNNNACACTCTTTCCCTACACGACGCTCTTCCGATCT" "TCTAGCCTTCTCGCAGCACATCCCTTTCTCACANNNNNNNNCACATCTAGAGCCACCAGCGGCATAGTAA" "TTACTATGCCGCTGGTGGCTCTAGATGTGNNNNNNNNTGTGAGAAAGGGATGTGCTGCGAGAAGGCTAGA" "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTNNNNNNNNGTGTAGATCTCGGTGGTCGCCGTATCATT")

D700=("GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNNNATCTCGTATGCCGTCTTCTGCTTG" "GTTCGTCTTCTGCCGTATGCTCTANNNNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG" "CTAGCCTTCTCGTGTGCAGACTTGAGGTCAGTGNNNNNNNNTAGAGCATACGGCAGAAGACGAAC" "CAAGCAGAAGACGGCATACGAGATNNNNNNNNGTGACTGGAGTTCAGACGTGTGCTCTTCCGATC")

cutadapt -b AATGATACGGCGACCACCGAGATCTACACNNNNNNNNACACTCTTTCCCTACACGACGCTCTTCCGATCT -b TCTAGCCTTCTCGCAGCACATCCCTTTCTCACANNNNNNNNCACATCTAGAGCCACCAGCGGCATAGTAA -b TTACTATGCCGCTGGTGGCTCTAGATGTGNNNNNNNNTGTGAGAAAGGGATGTGCTGCGAGAAGGCTAGA -b AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTNNNNNNNNGTGTAGATCTCGGTGGTCGCCGTATCATT -b GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNNNATCTCGTATGCCGTCTTCTGCTTG -b GTTCGTCTTCTGCCGTATGCTCTANNNNNNNNCACTGACCTCAAGTCTGCACACGAGAAGGCTAG -b CTAGCCTTCTCGTGTGCAGACTTGAGGTCAGTGNNNNNNNNTAGAGCATACGGCAGAAGACGAAC -b CAAGCAGAAGACGGCATACGAGATNNNNNNNNGTGACTGGAGTTCAGACGTGTGCTCTTCCGATC -b AATGATACGGCGACCACCGAGATCTACACNNNNNNNNTCGTCGGCAGCGTC -b CAAGCAGAAGACGGCATACGAGATNNNNNNNNGTCTCGTGGGCTCGG input1.fastq -o all_o.fastq --info-file all_i.fastq --match-read-wildcards

cp 248002_CGAGGCTG-TATCCTCT_L002_R1_001.fastq input1.fastq
END=${#D500[@]}
for ((i=1;i<=END;i++))
do
    echo 
    echo ${i}
    if [[ "${i}" == "1" ]]
    then
	echo "First Run"
	j=`expr ${i} - 1`
	cutadapt -b "${D500[j]}" input1.fastq --wildcard-file t${i}_w.fastq -o t${i}_o.fastq --info-file t${i}_i.fastq --match-read-wildcards
    elif [[ "${i}" < "${END}" ]]
    then
	echo "Other Run"
	j=`expr ${i} - 1`
	cutadapt -b "${D500[j]}" t${j}_o.fastq --wildcard-file t${i}_w.fastq -o t${i}_o.fastq --info-file t${i}_i.fastq --match-read-wildcards
    elif [[ "${i}" == "${END}" ]]
    then
	echo "Last Run"
	lastfile=t${i}_o.fastq
	j=`expr ${i} - 1`
	cutadapt -b "${D500[j]}" t${j}_o.fastq --wildcard-file t${i}_w.fastq -o ${lastfile} --info-file t${i}_i.fastq --match-read-wildcards
    fi
done

echo ${lastfile}

END=${#D700[@]}
tempv="b"
for ((i=1;i<=END;i++))
do
    echo 
    echo ${i}
    if [[ "${i}" == "1" ]]
    then
	echo "First Run"
	j=`expr ${i} - 1`
	cutadapt -b "${D700[j]}" ${lastfile} --wildcard-file t${i}${tempv}_w.fastq -o t${i}${tempv}_o.fastq --info-file t${i}${tempv}_i.fastq --match-read-wildcards
    elif [[ "${i}" < "${END}" ]]
    then
	echo "Other Run"
	j=`expr ${i} - 1`
	cutadapt -b "${D700[j]}" t${j}_o.fastq --wildcard-file t${i}${tempv}_w.fastq -o t${i}${tempv}_o.fastq --info-file t${i}${tempv}_i.fastq --match-read-wildcards
    elif [[ "${i}" == "${END}" ]]
    then
	echo "Last Run"
	lastfile=t${i}${tempv}_o.fastq
	j=`expr ${i} - 1`
	cutadapt -b "${D700[j]}" t${j}_o.fastq --wildcard-file t${i}${tempv}_w.fastq -o ${lastfile} --info-file t${i}${tempv}_i.fastq --match-read-wildcards
    fi
done

echo $lastfile
