import argparse
from datetime import date
import os
import sys
from natsort import natsorted
from collections import OrderedDict

CILCOLNO=22
CNTLCOLNO=24

parser = argparse.ArgumentParser("rotate sample-genes")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def getsampleset(inlines,colno):
    sampleset=set()
    lineno=0
    for inline in inlines:
        lineno+=1
        if lineno==1:
            continue
        ils=inline.split('\t')
        samplenames=ils[colno].lstrip().rstrip()
        samplelist=samplenames.split(';')
        for sample in samplelist:
            name=sample.split(',')[0].lstrip().rstrip()
            if name != '':
                sampleset.add(name)
    return sampleset


def writeheader(outfile_open,sampleset):
    samplestring='\t'.join(sampleset).lstrip().rstrip()
    td=date.today()
    vcfdate=str(td.year)+str(td.month)+str(td.day)
    header='##fileformat=VCFv4.1\n'
    header += '##fileDate='+vcfdate+'\n'
    header += '#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t'+samplestring+'\n'
    outfile_open.write(header)


def getinfo(ils):
    ift=ils[0].lstrip().rstrip()
    gene=ils[1].lstrip().rstrip()
    strand=ils[2].lstrip().rstrip()
    espmaf=ils[8].lstrip().rstrip()
    mrna=ils[9].lstrip().rstrip()
    dnavar=ils[10].lstrip().rstrip()
    refaa=ils[11].lstrip().rstrip()
    altaa=ils[13].lstrip().rstrip()
    numaa=ils[12].lstrip().rstrip()
    case='True' if ils[14] == 'X' else 'False'
    control='True' if ils[15] == 'X' else 'False'
    vartype=ils[16].lstrip().rstrip()
    jatd=(ils[17] if ils[17].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    mks=(ils[18] if ils[18].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    bbs=(ils[19] if ils[19].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    nphp=(ils[20] if ils[20].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    cil=(ils[21] if ils[21].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    ctrl=(ils[23] if ils[23].lstrip().rstrip() != '' else 'N/A').lstrip().rstrip()
    info='IFT='+ift+';GENE='+gene+';STRAND='+strand+';ESPMAF='+espmaf+';mRNA='+mrna+';DNAvar='+dnavar+';AAnum='+numaa+';AAref='+refaa+';AAalt='+altaa+';CASE='+case+';CONTROL='+control+';VARTYPE='+vartype+';JATD='+jatd+';MKS='+mks+';BBS='+bbs+';NPHP='+nphp+';CIL='+cil+';CTRL='+ctrl
    return info


def getvcfline(ils):
    rawchrom=ils[3].lstrip().rstrip()
    chrom='chr'+str(int(ils[3].replace('chr',''))).lstrip().rstrip()
    pos=ils[4].lstrip().rstrip()
    snpid=(ils[5] if ils[5] != '0' else '.').lstrip().rstrip()
    ref=ils[6].lstrip().rstrip()
    alt=ils[7].lstrip().rstrip()
    qual='.'
    filt='.'
    form='GT'
    infoline=getinfo(ils)
    vcfline=chrom+'\t'+pos+'\t'+snpid+'\t'+ref+'\t'+alt+'\t'+qual+'\t'+filt+'\t'+infoline+'\t'+form
    return vcfline

def getsampleline(ils,colno,sampledict):
    samplenames=ils[colno].lstrip().rstrip()
    samplelist=samplenames.split(';')
    sampleset=set()
    zygdict=dict()
    for sample in samplelist:
        print('getsampleline()::sample=',sample)
        name=sample.split(',')[0].lstrip().rstrip()
        if name != '':
            sampleset.add(name)
            zygdict[name]=sample.split(',')[1].lstrip().rstrip()
    sortednamelist=natsorted(sampleset)
    tablist=list()
    for sortedname in sortednamelist:
        tablist.append(sampledict[sortedname])
    gtlist=list()
    for name in sortednamelist:
        zyg=zygdict[name]
        print('zyg=',zyg)
        if zyg=='het':
            gtlist.append('0/1')
        elif zyg=='hom':
            gtlist.append('1/1')
        elif zyg=='NA':
            gtlist.append('?/?')
        else:
            print('game over, man')
            sys.exit()
    gtstring=''
    accumtab=0
    for idx,tabno in enumerate(tablist):
        print('tablist=',tablist)
        if accumtab==0:
            gtstring+='0/0\t'*(tabno-accumtab+1)
        else:
            gtstring+='0/0\t'*(tabno-accumtab)
        print('idx=',idx)
        print('tabno=',tabno)
        print('accumtab=',accumtab)
        print('tabno-accumtab=',tabno-accumtab)
        print('gtlist[idx]=',gtlist[idx])
        gtstring+=gtlist[idx]
        accumtab+=tabno-accumtab
    return gtstring

        
def writeoutline(ils,samplecolno,sampledict,outfile_open):
    vcfline=getvcfline(ils)
    #sampleline=getsampleline(ils,samplecolno,sampledict)
    newline=vcfline+'\n'#sampleline+'\n'
    outfile_open.write(newline)


def vcfit(infile):
    origfile,origext = os.path.splitext(infile)
    #outfile_cil = origfile+'_cilopathy.vcf'
    #outfile_cntl = origfile+'_control.vcf'
    outfile=origfile+'.vcf'

    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()

    outfile_open = open(outfile,'w')
    #outfilecntl_open = open(outfile_cntl,'w')

    print('CIL')
    cilsamplelist=natsorted(getsampleset(inlines,CILCOLNO))
    cilsampledict=dict((value,idx) for idx,value in enumerate(cilsamplelist))
    print('CNTL')
    cntlsamplelist=natsorted(getsampleset(inlines,CNTLCOLNO))
    cntlsampledict=dict((value,idx) for idx,value in enumerate(cntlsamplelist))

    lineno=0
    for inline in inlines:
        print('lineno=',lineno)
        lineno+=1
        if lineno==1:
            lineno+=1
            writeheader(outfile_open,cilsamplelist)
            #writeheader(outfilecntl_open,cntlsamplelist)
            continue
        ils=inline.split('\t')
        cilsample=ils[CILCOLNO].lstrip().rstrip()
        cntlsample=ils[CNTLCOLNO].lstrip().rstrip()
        #if (cilsample != ''):
        writeoutline(ils,CILCOLNO,cilsampledict,outfile_open)
        #if (cntlsample != ''):
        #    writeoutline(ils,CNTLCOLNO,cntlsampledict,outfile_open)

    outfile_open.close()
    #outfilecntl_open.close()

vcfit(infile)
