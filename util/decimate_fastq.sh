#make
fastqfiles=$1

for f in ${fastqfiles}
do
    echo f = $f
    numlinesorig=`wc -l $f`
    echo numlinesorig = $numlinesorig
    numlinesorig=($numlinesorig)
    echo numlinesorig = $numlinesorig
    decamt=`expr ${numlinesorig} / 10`
    echo decamt = $decamt
    remainder=`expr $decamt % 4`
    decamt=`expr $decamt + 4 - $remainder`
    echo newdecamt =  $decamt
    for i in {1..10}
    do
	echo i = $i
	base=${f%.*}
	ext=${f##*.}
	newname=${base}_${i}.${ext}
	numlinesnew=`expr ${decamt} \* ${i}`
	echo numlinesnew = $numlinesnew
	if [ $i -eq "10" ]
	then
	    cp ${f} ${newname}
	fi
	echo head -n ${numlinesnew} $f > ${newname}
	head -n ${numlinesnew} $f > ${newname}
    done
done
