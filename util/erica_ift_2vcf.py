import argparse
import csv
import os

parser = argparse.ArgumentParser("cull tsv file")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']


def culltsv(infile):
    origfile = os.path.splitext(infile)[0]
    tsvfile = origfile+".vcf"
    print(tsvfile)
    with open(infile,"r") as source:
        rdr = csv.reader(source, delimiter='\t')
        with open(tsvfile,"w") as outfile:
            wrt = csv.writer(outfile, delimiter='\t')
            for r in rdr:
                if (r[0] == "IFT"):
                    wrt.writerow(("##fileformat=VCFv4.1",))
                    wrt.writerow(("#CHROM","POS","ID","REF","ALT","QUAL","FILTER","INFO","FORMAT",origfile))
                else:
                    wrt.writerow((r[3], r[4], r[5], r[6], r[7], '.', '.', "IFT="+r[0]+";GENE="+r[1]+";STRAND="+r[2]+";ESP6500SI="+r[8]+";HG19MRNA="+r[9]+";REFAA="+r[11]+";ALTAA="+r[13]+";CASE="+r[14]+";CONTROL="+r[15]+";CONSTRUCT="+r[16]+";8/19INVIVOPATH="+r[17]+";VARTYPE="+r[18]+";CILSAMP="+''.join(r[19].replace(";",":").split())+";CONSAMP="+''.join(r[25].replace(";",":").split())+";JATD="+r[20]+";MKS="+r[21]+";BBS="+r[22]+";NPHP="+r[23]+";CIL="+r[24]+";CTRL="+r[26],"GT"))

culltsv(infile)
