import argparse
import csv
import os

parser = argparse.ArgumentParser("convert csv to tab")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def csv2tab(infile,outfile):
    csvlines=""
    with open(infile,'r') as csvfile:
        csvreader = csv.reader(csvfile,delimiter=',', quotechar='"')
        for row in csvreader:
            csvlines += '\t'.join(row)+'\n'

    with open(outfile,'w') as tabfile:
        for csvline in csvlines:
            tabfile.write(csvline)

csv2tab(infile,outfile)
