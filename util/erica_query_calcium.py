import argparse
import os
import urllib2

parser = argparse.ArgumentParser('get go by keyword')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-k','--keyword',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

infile=args['infile']
keyword=args['keyword']
outfile=args['outfile']

def get_keyword_list(uniprotid):
    keyword_list=list()
    try:
        response=urllib2.urlopen('http://www.ebi.ac.uk/QuickGO/GAnnotation?protein='+uniprotid+'&format=tsv')
        html=response.read()
    except urllib2.HTTPError:
        print('There was an error with the request:%s' % uniprotid)
        return ['urllib2.HTTPerror',uniprotid]
    html_lines = html.split('\n')
    for html_line in html_lines:
        if html_line.startswith('DB') or html_line=='':
            continue
        html_line_split=html_line.split('\t')
        go_name=html_line_split[7]
        if keyword in go_name:
            print('html_line=%s' % html_line)
            print('go_name=%s' % go_name)
            go_id=html_line_split[6]
            keyword_list.append(go_id+';'+go_name)
    return keyword_list

def getgokeyword(infile,outfile,keyword):

    with open(infile,'r') as infile_open:
        infile_lines = infile_open.readlines()

    outfile_open=open(outfile,'w')

    for infile_line in infile_lines:
        if infile_line.startswith('Gene'):
            outfile_open.write(infile_line.strip('\n')+'\t'+'ontology'+'\n')
            continue
        uniprotid=infile_line.split('\t')[0]

        keyword_list=get_keyword_list(uniprotid)
        keyword_line='\t'.join(keyword_list)
        outfile_line=infile_line.strip('\n')+'\t'+keyword_line+'\n'
        outfile_open.write(outfile_line)

    outfile_open.close()

getgokeyword(infile,outfile,keyword)
