import argparse
import csv
import os

parser = argparse.ArgumentParser("cull tsv file")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']


def culltsv(infile):
    tsvfile = os.path.splitext(infile)[0]
    tsvfile += "_cull.tsv"
    print(tsvfile)
    with open(infile,"r") as source:
        rdr = csv.reader(source, delimiter='\t')
        with open(tsvfile,"w") as outfile:
            wrt = csv.writer(outfile, delimiter='\t')
            for r in rdr:
                #print(r)
                wrt.writerow((r[0], r[1], r[2], r[3], r[5], r[6], r[7], r[8]))

culltsv(infile)
