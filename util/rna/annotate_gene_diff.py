#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys


parser=argparse.ArgumentParser('annotate gene_diff with gene name')
parser.add_argument('-r','--gene_results',required=True)
parser.add_argument('-s','--gene_source',required=True)
args=vars(parser.parse_args())

results=args['results']
source=args['source']


def main(results,source):
    with open(source,'r') as source_open:
        sourcelines=source_open.readlines()

    with open(results,'r') as results_open:
        resultlines=results_open.readlines()

    origfile,origext=os.path.splitext(results)
    outfile=origfile+'_annot'+origext
    outfile_open=open(outfile)

    id_gene_dict=dict()
    id_locus_dict=dict()
    for sourceline in sourcelines:
        sourcesplit=sourceline.split('\t')
        test_id=sourcesplit[0]
        gene_id=sourcesplit[1]
        gene=sourcesplit[2]
        locus=sourcesplit[3]
        id_gene_dict[gene_id]=gene
        id_locus_dict[gene_id]=locus

    for resultline in resultlines:
        resultsplit=resultline.split('\t')
        if resultsplit[0]=='gene_id':
            resultsplit.insert(1,'gene')
            resultsplit.insert(2,'locus')
            newline='\t'.join(resultsplit)
            outfile_open.write(newline)
        else:
            res_gene_id=resultsplit[0]
            res_gene=id_gene_dict[res_gene_id]
            res_locus=id_locus_dict[res_gene_id]
            resultsplit.insert(1,res_gene)
            resultsplit.insert(2,res_locus)
            newline='\t'.join(resultsplit)
            outfile_open.write(newline)
            
    outfile_open.close()


if __name__=='__main__':
    main(results,source)
