#!/usr/bin/env python3

import argparse
import glob
import os
import re

parser=argparse.ArgumentParser('automated concatenation')
parser.add_argument('-d','--directory',required=False)
args=vars(parser.parse_args())

directory=args['directory']

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs

def get_lanes(adir):
    print('adir=%s' % adir)
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2])_(\d+).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    lane_set=set()
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==5:
            this_lane=fastq_match.groups()[2]
            lane_set.add(this_lane)
    print('lane_set=%s' % lane_set)
    return lane_set

def get_files_to_concat(adir,lane):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2])_(\d+).fastq')
    concat_list=[os.path.basename(fastq) for fastq in (glob.glob(os.path.join(adir,'*.fastq')))]
    fastq_dict=dict()
    for afile in concat_list:
        fastq_match=fastqfilere.match(afile)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==5:
            this_sample=fastq_match.groups()[0]
            this_tag=fastq_match.groups()[1]
            this_lane=fastq_match.groups()[2]
            this_read=fastq_match.groups()[3]
            this_number=fastq_match.groups()[4]
            if lane==this_lane and this_read=='R1':
                out_R1=this_sample+'_'+this_tag+'_'+this_lane+'_'+this_read+'.fastq'
                fastq_dict[afile]=out_R1
            if lane==this_lane and this_read=='R2':
                out_R2=this_sample+'_'+this_tag+'_'+this_lane+'_'+this_read+'.fastq'
                fastq_dict[afile]=out_R2
    return fastq_dict
    

def get_source_list(fastq_dict,outfile):
    print('fastq_dict=%s' % fastq_dict)
    fastq_source_list=list()
    for key,value in fastq_dict.items():
        if outfile==value:
            fastq_source_list.append(key)
    fastq_source_list=sorted(fastq_source_list)
    print('fastq_source_list=%s' % fastq_source_list)
    return fastq_source_list
    
#Debug this >;)
def concatfiles(adir,fastq_dict):
    outfile_set=set(fastq_dict.values())
    print('outfile_set=%s' % outfile_set)
    for outfile in outfile_set:
        print('outfile=%s' % outfile)
        fastq_source_list=get_source_list(fastq_dict,outfile)
        with open(os.path.join(adir,outfile),'w') as outfile_open:
            for fastq_source in fastq_source_list:
                print('fastq_source=%s' % fastq_source)
                if fastq_dict[fastq_source]==outfile:
                    with open(os.path.join(adir,fastq_source),'r') as fastq_source_open:
                        for line in fastq_source_open:
                            outfile_open.write(line)

def main():
    alldirs=get_dirs()
    for adir in alldirs:
        laneset=get_lanes(adir)
        for lane in laneset:
            fastq_dict=get_files_to_concat(adir,lane)
            concatfiles(adir,fastq_dict)
        

if __name__=='__main__':
    main()
