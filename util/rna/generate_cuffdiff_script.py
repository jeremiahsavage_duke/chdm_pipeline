#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs


def get_lanes(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    lane_set=set()
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_lane=fastq_match.groups()[2]
            lane_set.add(this_lane)
    return lane_set


def get_sample(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_sample=fastq_match.groups()[0]
            return this_sample
    sys.exit('Problems')


def write_files_to_cuffdiff(adir,sample,lane_list,outfile_open):
    #source_dirs=list()
    accepted_bam_list=list()
    for lane in lane_list:
        source_dir=os.path.join(adir,sample+'_'+lane)
        if os.path.isdir(source_dir):
            accepted_bam=os.path.join(source_dir,'accepted_hits.bam')
            if os.path.exists(accepted_bam):
                accepted_bam_list.append(accepted_bam)
            else:
                sys.exit('Needed bam file does NOT exist: %s' % accepted_bam)
        else:
            sys.exit('Needed source dir does NOT exist: %s' % source_dir)
    cuffdiff_out_dir=os.path.join(adir,sample+'_'+'_'.join(lane_list))
    outdir=os.path.join(adir,this_sample+'_'+lane)
    nohupout=os.path.join(adir,lane+'_nohup.out')
    align_command='nohup cuffdiff -p 24 -o '+outdir+ \
        ' -b ~/GRCh37.75/rna/Homo_sapiens.GRCh37.75.fa' + \
        ' -u ~/GRCh37.75/rna/Homo_sapiens.GRCh37.75.gtf '+ \
        ','.join(accepted_bam_list) + \
        ' > '+nohupout+' &\n'
    outfile_open.write(align_command)

def cleanup(adir,sample,lane_list):
    import shutil
    outdir=os.path.join(adir,sample+'_'+'_'.join(lane_list))
    nohupfile=os.path.join(adir,sample+'_'.join(lane_list)+'_cuffdiff_nohup.out')
    if os.path.isfile(nohupfile):
        os.remove(nohupfile)
    if os.path.isdir(outdir):
        shutil.rmtree(outdir)

def main():
    alldirs=get_dirs()
    outfile_open=open('cuffdiff.sh','w')
    outfile_open.write('#!/bin/sh\n')
    for adir in alldirs:
        lane_list=sorted(list(get_lanes(adir)))
        sample=get_sample(adir)
        cleanup(adir,sample,lane_list)
        write_files_to_cuffdiff(adir,sample,lane_list,outfile_open)
    outfile_open.close()

if __name__=='__main__':
    main()
