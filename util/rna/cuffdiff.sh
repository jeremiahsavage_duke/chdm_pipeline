cd Project_Willer_1536_140703A1

1. Concat lane reads:
python3 ~/chdm_pipeline/util/rna/concat_rnaseq_lanes.py

2. Generate tophat script and run it
python3 ~/chdm_pipeline/util/rna/generate_tophat_script.py
sh ./tophat2.sh

3. Merge bam file of each lane
python3 ~/chdm_pipeline/util/rna/generate_bammerge_script.py
sh ./bammerge.sh


----optional--not used--cufflinks steps----
4. python3 ~/chdm_pipeline/util/rna/generate_cufflinks_script.py
./cufflink.sh

------------------------------------------


mkdir affected_v_female_control/
cd affected_v_female_control/
nohup cuffdiff -p 24 -L Affected,FemControl -v ~/GRCh37.75/cuffcmp.combined.gtf ../Sample_JW0824/JW0824_L007_L008_bammerge/merged_sorted.bam,../Sample_JW0827/JW0827_L007_L008_bammerge/merged_sorted.bam,../Sample_JW23528/JW23528_L007_L008_bammerge/merged_sorted.bam,../Sample_JW2386/JW2386_L007_L008_bammerge/merged_sorted.bam,../Sample_JW24192/JW24192_L007_L008_bammerge/merged_sorted.bam ../Sample_JW13917/JW13917_L007_L008_bammerge/merged_sorted.bam &

mkdir female_carrier_v_control/
cd female_carrier_v_control/
nohup cuffdiff -p 24 -L Carrier,Control -v ~/GRCh37.75/cuffcmp.combined.gtf ../Sample_JW0825/JW0825_L007_L008_bammerge/merged_sorted.bam,../Sample_JW10073/JW10073_L007_L008_bammerge/merged_sorted.bam ../Sample_JW13917/JW13917_L007_L008_bammerge/merged_sorted.bam &

mkdir affected_v_control/
cd affected_v_control/
nohup cuffdiff -p 24 -L Affected,Control -v ~/GRCh37.75/cuffcmp.combined.gtf ../Sample_JW0824/JW0824_L007_L008_bammerge/merged_sorted.bam,../Sample_JW0827/JW0827_L007_L008_bammerge/merged_sorted.bam,../Sample_JW23528/JW23528_L007_L008_bammerge/merged_sorted.bam,../Sample_JW2386/JW2386_L007_L008_bammerge/merged_sorted.bam,../Sample_JW24192/JW24192_L007_L008_bammerge/merged_sorted.bam ../Sample_JW18007/JW18007_L007_L008_bammerge/merged_sorted.bam,../Sample_JW18644/JW18644_L007_L008_bammerge/merged_sorted.bam &



R commands
-----------
$ cd affected_v_control/
$ R
> library(cummeRbund)
> cuff_data  <- readCufflinks('./')
> png('csDensity-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csDensity(genes(cuff_data))
> dev.off()
>
> png('csScatter-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csScatter(genes(cuff_data),'Affected','Control')
> dev.off()
>
> png('csVolcano-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csVolcano(genes(cuff_data),'Affected','Control')
> dev.off()
>
> gene_diff_data <- diffData(genes(cuff_data))
> sig_gene_data <- subset(gene_diff_data, (significant == 'yes'))
> nrow(sig_gene_data) # optional
> write.table(sig_gene_data,'sig_gene_data.txt',sep='\t',row.names= F, col.names = T, quote = F)
>
> quit()
n

~/chdm_pipeline/util/rna/annotate_gene_diff.py -r sig_gene_data.txt -s gene_exp.diff



$ cd female_carrier_v_control/
$ R
> library(cummeRbund)
> cuff_data  <- readCufflinks('./')
> png('csDensity-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csDensity(genes(cuff_data))
> dev.off()
>
> png('csScatter-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csScatter(genes(cuff_data),'Carrier','Control')
> dev.off()
>
> png('csVolcano-genes-cuff_data.png',width=10,height=10,units="in",res=300)
> csVolcano(genes(cuff_data),'Carrier','Control')
> dev.off()
>
> gene_diff_data <- diffData(genes(cuff_data))
> sig_gene_data <- subset(gene_diff_data, (significant == 'yes'))
> nrow(sig_gene_data) # optional
> write.table(sig_gene_data,'sig_gene_data.txt',sep='\t',row.names= F, col.names = T, quote = F)
>
> quit()
n

~/chdm_pipeline/util/rna/annotate_gene_diff.py -r sig_gene_data.txt -s gene_exp.diff
