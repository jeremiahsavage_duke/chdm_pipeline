#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs


def get_lanes(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    lane_set=set()
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_lane=fastq_match.groups()[2]
            lane_set.add(this_lane)
    return lane_set


def get_sample(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_sample=fastq_match.groups()[0]
            return this_sample
    sys.exit('Problems')


def write_files_to_bammerge(adir,sample,lane_list,outfile_open):
    accepted_hits_bam_list=list()
    for lane in lane_list:
        source_dir=os.path.join(adir,sample+'_'+lane)
        if os.path.isdir(source_dir):
            accepted_hits_bam=os.path.join(source_dir,'accepted_hits.bam')
            if os.path.exists(accepted_hits_bam):
                accepted_hits_bam_list.append(accepted_hits_bam)
            else:
                sys.exit('Needed bam file does NOT exist: %s' % accepted_hits_bam)
        else:
            sys.exit('Needed source dir does NOT exist: %s' % source_dir)
            
    print('accepted_hits_bam_list=%s' % accepted_hits_bam_list)
    
    bammerge_out_dir=os.path.join(adir,sample+'_'+'_'.join(lane_list)+'_bammerge')

    if not os.path.exists(bammerge_out_dir):
        os.makedirs(bammerge_out_dir)
    
    nohupout=os.path.join(bammerge_out_dir,'nohup.out')
    
    merge_command='nohup samtools merge '+os.path.join(bammerge_out_dir,'merged.bam ') + \
        ' '.join(accepted_hits_bam_list) + ' && ' + \
        'samtools sort '+os.path.join(bammerge_out_dir,'merged.bam ') + \
        os.path.join(bammerge_out_dir,'merged_sorted') + ' && ' + \
        'nohup samtools index '+os.path.join(bammerge_out_dir,'merged_sorted.bam') + \
        ' > ' + nohupout + ' &\n\n'
    outfile_open.write(merge_command)

def cleanup(adir,sample,lane_list):
    import shutil
    bammerge_out_dir=os.path.join(adir,sample+'_'+'_'.join(lane_list)+'_bammerge')
    print('checking to remove: %s' % bammerge_out_dir)
    if os.path.isdir(bammerge_out_dir):
        print('removing directory: %s' % bammerge_out_dir)
        shutil.rmtree(bammerge_out_dir)

def main():
    alldirs=get_dirs()
    outfile_open=open('bammerge.sh','w')
    outfile_open.write('#!/bin/sh\n')
    for adir in alldirs:
        lane_list=sorted(list(get_lanes(adir)))
        sample=get_sample(adir)
        cleanup(adir,sample,lane_list)
        write_files_to_bammerge(adir,sample,lane_list,outfile_open)
    outfile_open.close()

if __name__=='__main__':
    main()
