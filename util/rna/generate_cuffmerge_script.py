#!/usr/bin/env python3

import argparse
import glob
import os
import re
import sys

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs


def get_lanes(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    lane_set=set()
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_lane=fastq_match.groups()[2]
            lane_set.add(this_lane)
    return lane_set


def get_sample(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_sample=fastq_match.groups()[0]
            return this_sample
    sys.exit('Problems')


def write_files_to_cuffmerge(adir,sample,lane_list,outfile_open):
    transcripts_gtf_list=list()
    for lane in lane_list:
        source_dir=os.path.join(adir,sample+'_'+lane+'_cufflink')
        if os.path.isdir(source_dir):
            transcripts_gtf=os.path.join(source_dir,'transcripts.gtf')
            if os.path.exists(transcripts_gtf):
                transcripts_gtf_list.append(transcripts_gtf)
            else:
                sys.exit('Needed transcript file does NOT exist: %s' % transcripts_gtf)
        else:
            sys.exit('Needed source dir does NOT exist: %s' % source_dir)
            

    cuffmerge_out_dir=os.path.join(adir,sample+'_'+'_'.join(lane_list)+'_cuffmerge')

    if not os.path.exists(cuffmerge_out_dir):
        os.makedirs(cuffmerge_out_dir)
    
    assembled_transcript_txt=os.path.join(cuffmerge_out_dir,'assembly_GTF_list.txt')
    assembled_transcript_txt_open=open(assembled_transcript_txt,'w')
    for transcripts_gtf in transcripts_gtf_list:
        assembled_transcript_txt_open.write(transcripts_gtf+'\n')
    assembled_transcript_txt_open.close()

    nohupout=os.path.join(cuffmerge_out_dir,'nohup.out')
    align_command='nohup cuffmerge -p 24 -o '+cuffmerge_out_dir + \
        ' -g ~/GRCh37.75/Homo_sapiens.GRCh37.75.gtf ' + \
        assembled_transcript_txt + ' > '+nohupout+' &\n'
    outfile_open.write(align_command)

def cleanup(adir,sample,lane_list):
    import shutil
    cuffmerge_out_dir=os.path.join(adir,sample+'_'+'_'.join(lane_list)+'_cuffmerge')
    print('checking to remove: %s' % cuffmerge_out_dir)
    if os.path.isdir(cuffmerge_out_dir):
        print('removing directory: %s' % cuffmerge_out_dir)
        shutil.rmtree(cuffmerge_out_dir)

def main():
    alldirs=get_dirs()
    outfile_open=open('cuffmerge.sh','w')
    outfile_open.write('#!/bin/sh\n')
    for adir in alldirs:
        lane_list=sorted(list(get_lanes(adir)))
        sample=get_sample(adir)
        cleanup(adir,sample,lane_list)
        write_files_to_cuffmerge(adir,sample,lane_list,outfile_open)
    outfile_open.close()

if __name__=='__main__':
    main()
