#!/usr/bin/env python3

import argparse
import glob
import os
import re

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs


def get_lanes(adir):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in glob.glob(os.path.join(adir,'*.fastq'))]
    lane_set=set()
    for fastq_file in fastq_list:
        fastq_match=fastqfilere.match(fastq_file)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_lane=fastq_match.groups()[2]
            lane_set.add(this_lane)
    return lane_set


def write_files_to_align(adir,lane,outfile_open):
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2]).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in (glob.glob(os.path.join(adir,'*.fastq')))]
    for afile in fastq_list:
        fastq_match=fastqfilere.match(afile)
        if fastq_match is None:
            continue
        if len(fastq_match.groups())==4:
            this_sample=fastq_match.groups()[0]
            this_tag=fastq_match.groups()[1]
            this_lane=fastq_match.groups()[2]
            this_read=fastq_match.groups()[3]
            if lane==this_lane and this_read=='R1':
                fastq_read1=os.path.join(adir,afile)
                fastq_read2=fastq_read1.replace('_R1','_R2')
                outdir=os.path.join(adir,this_sample+'_'+lane)
                nohupout=os.path.join(adir,lane+'_nohup.out')
                align_command='nohup tophat2 -p 24 -o '+outdir+ \
                    ' --transcriptome-index=/home/jeremiah/GRCh37.75/rna/Homo_sapiens.GRCh37.75' + \
                    ' ~/GRCh37.75/rna/Homo_sapiens.GRCh37.75 '+fastq_read1+' '+fastq_read2+ \
                    ' > '+nohupout+' &\n'
                outfile_open.write(align_command)

def cleanup(adir,lane):
    import shutil
    fastqfilere=re.compile('([A-Z][A-Z]\d+)_([ATCG]+)_(L\d+)_(R[1,2])_(\d+).fastq')
    fastq_list=[os.path.basename(fastq) for fastq in (glob.glob(os.path.join(adir,'*.fastq')))]
    afile=fastq_list[0]
    fastq_match=fastqfilere.match(afile)
    this_sample=fastq_match.groups()[0]
    outdir=os.path.join(adir,this_sample+'_'+lane)
    nohupfile=os.path.join(adir,lane+'_nohup.out')
    if os.path.isfile(nohupfile):
        os.remove(nohupfile)
    if os.path.isdir(outdir):
        shutil.rmtree(outdir)

def main():
    alldirs=get_dirs()
    outfile_open=open('tophat2.sh','w')
    outfile_open.write('#!/bin/sh\n')
    for adir in alldirs:
        lane_list=sorted(list(get_lanes(adir)))
        for lane in lane_list:
            cleanup(adir,lane)
            write_files_to_align(adir,lane,outfile_open)
    outfile_open.close()

if __name__=='__main__':
    main()
