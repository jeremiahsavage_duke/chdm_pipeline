import argparse
import os
import sys


parser = argparse.ArgumentParser("bed2interval")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def doit(infile):
    with open(infile,'r') as infile_open:
        inlines=infile_open.readlines()

    origname,origext=os.path.splitext(infile)
    outfile=origname+'.txt'

    outfile_open=open(outfile,'w')

    counter=int(1)
    for inline in inlines:
        ils=inline.strip().split('\t')
        ils.append('+')
        ils.append('interval_'+str(counter))
        counter+=1
        newline=('\t').join(ils)
        outfile_open.write(newline+'\n')

doit(infile)
