import argparse
import csv
import os

parser = argparse.ArgumentParser("convert tab to NM_* values")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def tab2nm(infile,outfile):

    with open(infile,'r') as tabfile:
        tablines = tabfile.readlines()

    with open(outfile,'w') as tabfile:
        for tabline in tablines:
            data = tabline.split()
            nmval = data[2].split(',')[0]
            if ("_" in nmval):
                tabfile.write(nmval+'\n')

tab2nm(infile,outfile)
