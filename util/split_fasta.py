import argparse
import os

parser = argparse.ArgumentParser("split fasta file to files")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def splitfasta(infile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()

    firstfile = inlines[0].rstrip('\n')[1:] + ".fa"
    print("firstfile=",firstfile)
    outfile = open(firstfile,"w")
    for line in inlines:
        if line.startswith(">"):
            newfile = line.rstrip('\n')
            newfile = newfile[1:]
            newfile += ".fa"
            print(newfile)
            outfile.close()
            outfile = open(newfile,"w")
            outfile.write(line)
        else:
            outfile.write(line)
    outfile.close()


splitfasta(infile)
