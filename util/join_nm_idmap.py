import argparse
import csv
import os

parser = argparse.ArgumentParser("convert tab to NM_* values")
parser.add_argument('-n','--nmfile',required=True)
parser.add_argument('-i','--idmapfile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

nm_file=args['nmfile']
idmap_file=args['idmapfile']
out_file=args['outfile']

def joinnmid(nm_file,idmap_file,out_file):

    with open(nm_file,'r') as nmfile:
        nmlines = nmfile.readlines()

    with open(idmap_file,'r') as idmapfile:
        idmaplines = idmapfile.readlines()

    with open(out_file,'w') as outfile:
        for nmline in nmlines:
            nmline_clean=nmline.strip().upper()
            print('nmline_clean=%s' % nmline_clean)
            for idmapline in idmaplines:
                idmapline_clean=idmapline.strip().upper()
                #print('idmapline.upper=%s' % idmapline.upper())
                if (nmline_clean in idmapline_clean):
                    #print('idmapline=%s' % idmapline)
                    outfile.write(nmline_clean+'\t'+idmapline_clean+'\n')
                    break;

joinnmid(nm_file,idmap_file,out_file)
