import argparse
import os
from Bio import SeqIO

parser = argparse.ArgumentParser("cull vcf file on ratios")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-f','--fastagenome',required=True)
args=vars(parser.parse_args())

infile=args['input']
fasta=args['fastagenome']

def parsefile(infile,fasta):
    fasta_genome = SeqIO.to_dict(SeqIO.parse(fasta,"fasta"))
    origfile = os.path.splitext(infile)[0]
    vcffile = origfile+"_refalle.vcf"
    print(vcffile)
    with open(infile,"r") as source:
        inlines = source.readlines()

    outfile=open(vcffile,'w')

    for line in inlines:
        if (not line.startswith("#")):
            chrm = line.split('\t')[0]
            pos = line.split('\t')[1]
            refallele = str(fasta_genome[chrm].seq)[int(pos)-1]
            linearray = line.split('\t')
            linearray[3] = refallele
            newline = '\t'.join(linearray)
            outfile.write(newline)
        elif (line.startswith("#")):
            outfile.write(line)

    outfile.close()

parsefile(infile,fasta)
