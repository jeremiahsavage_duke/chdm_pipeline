#!/bin/bash
for fq in *.fastq
do
    echo "gzip -c ${fq} > ${fq}.gz"
done | parallel -j12
