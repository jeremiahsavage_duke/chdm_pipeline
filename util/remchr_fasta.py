import argparse
import os

parser = argparse.ArgumentParser("convert csv to tab")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def chr2vcf(infile,outfile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()

    out_file=open(outfile,'w')
    remchr = False
    for inline in inlines:
        if inline.startswith(">chr"):
            keepchr = True
            out_file.write(inline)
        elif inline.startswith(">Zv9"):
            keepchr = False
        else:
            if (keepchr):
                out_file.write(inline)
    out_file.close()

chr2vcf(infile,outfile)
