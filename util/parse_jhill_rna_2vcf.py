import argparse
import csv
import os

parser = argparse.ArgumentParser("cull vcf file on ratios")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def parsefile(infile):
    origfile = os.path.splitext(infile)[0]
    vcffile = origfile+".vcf"
    print(vcffile)
    with open(infile,"r") as source:
        inlines = source.readlines()

    outfile=open(vcffile,'w')
    outfile.write("##fileformat=VCFv4.1\n")
    outfile.write("#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	jhill_rnaseq\n")
    for line in inlines:
        if line.startswith("ENSDARG"):
            print("ENSDARG")
            ensdarg = line.split('\t')[0]
            ensdart = line.split('\t')[1]
            chrm = line.split('\t')[2]
            print("ensdarg=",ensdarg)
            print("ensdart=",ensdart)
            print("chrm=",chrm)
        elif line.split('\t')[1].isdigit():
            startallele = line.split('\t')[1]
            endallele = line.split('\t')[2]
            altallele = line.split('\t')[3]
            coverage = line.split('\t')[4].split(' ')[0]
            coverage += line.split('\t')[4].split(' ')[1]
            coverage += line.split('\t')[4].split(' ')[2].rstrip('\n')
            print("startallele=",startallele)
            print("endallele=",endallele)
            print("altallele=",altallele)
            print("coverage=",coverage)
        elif (line.split('\t')[2].split(' ')[0].isalpha()):
            print("2")
            print(line.split('\t')[2].split(' '))
            annotation = line.split('\t')[2].rstrip('\n')
            annotation = '-'.join(annotation.split())
            print("annotation=",annotation)
        elif (line.split('\t')[3].split(' ')[0].split('-')[0].isalpha()):
            print("1")
            effect = line.split('\t')[3].rstrip('\n')
            effect = ''.join(effect.split())
            outfile.write(chrm+'\t'+endallele+'\t'+'.'+'\t'+'.'+'\t'+altallele+'\t'+'.'+'\t'+"PASS"+'\t'+"ANNOTATION="+annotation+";COVERAGE="+coverage+";EFFECT="+effect+";ENSDARG="+ensdarg+";ENSDART="+ensdart+'\t'+"GT"+'\n')

    outfile.close()

parsefile(infile)
