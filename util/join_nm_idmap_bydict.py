import argparse
import csv
import os
import pickle

parser = argparse.ArgumentParser("convert tab to NM_* values")
parser.add_argument('-n','--nmfile',required=True)
parser.add_argument('-i','--idmapfile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

nm_file=args['nmfile']
idmap_file=args['idmapfile']
out_file=args['outfile']

def joinnmid(nm_file,idmap_file,out_file):

    with open(nm_file,'r') as nmfile:
        nmlines = nmfile.readlines()

    mapfile,mapextention=os.path.splitext(idmap_file)
    dictfile=mapfile+".dict"
    if (os.path.exists(dictfile)):
        dictinput=open(dictfile,'rb')
        idmapdict=pickle.load(dictinput)
        dictinput.close()
    else:
        with open(idmap_file,'r') as idmapfile:
            idmaplines = idmapfile.readlines()
        idmapdict=dict()
        for idmapline in idmaplines:
            if (("NM_" in idmapline.split()[2]) or ("NC_" in idmapline.split()[2])):
                nmval=idmapline.split()[2]
                nmval=nmval.split('.')[0]
                nmval=nmval.rstrip('\n')
                idmapdict[nmval]=idmapline.rstrip('\n')
        dictoutput=open(dictfile,'wb')
        pickle.dump(idmapdict,dictoutput)
        dictoutput.close()

    with open(out_file,'w') as outfile:
        ANUM=0
        for nmline in nmlines:
            nmval=nmline.split()[2].split(',')[0]
            if nmval in idmapdict:
                outfile.write(nmline.rstrip('\n')+'\t'+idmapdict[nmval]+'\n')
            else:
                outfile.write(nmline.rstrip('\n')+'\t'+"NOUNIPROT"+str(ANUM)+'\n')
                ANUM+=1

joinnmid(nm_file,idmap_file,out_file)
