import argparse
import os

parser = argparse.ArgumentParser("cull tsv file")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def culltsv(infile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()

    tsvfile = os.path.splitext(infile)[0]
    tsvfile += ".tsv"
    outfile = open(tsvfile,"w")
    for line in inlines:
        firstcol = line.split('\t')[0]
        print("firstcol = ",firstcol)
        if firstcol.isdigit():
            outfile.write(line)
        elif (firstcol =="Chr"):
            outfile.write(line)
    outfile.close()

culltsv(infile)
