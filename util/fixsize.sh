#!/bin/sh
if [ "$#" -ne 4 ]; then
    echo "Usage: $0 input.wig input.chrom.sizes out.bw output.chrom.sizes" >&2
    exit 1
fi
if ! [ -e "$1" ]; then
    echo "$1 not found" >&2
    exit 1
fi
if ! [ -e "$2" ]; then
    echo "$2 not found" >&2
    exit 1
fi
INPUTWIG=$1
INPUTSIZES=$2
OUTPUTBIGWIG=$3
OUTPUTSIZES=$4

wigToBigWig $INPUTWIG $INPUTSIZES $OUTPUTBIGWIG 2>err.log
if [[ "${PIPESTATUS[*]}" = "0" ]]; then
    echo "$2 file has correct sizes"
    echo "BigWig output to $3"
    exit 0
fi

cp ${INPUTSIZES} ${OUTPUTSIZES}
echo wigToBigWig $INPUTWIG $INPUTSIZES $OUTPUTBIGWIG 2>err.log
wigToBigWig $INPUTWIG $INPUTSIZES $OUTPUTBIGWIG 2>err.log
while [[ ${PIPESTATUS[*]} != 0 ]]
do
    IFS=' '
    ERR=`cat err.log`
    err_split=($ERR)
    CHROM=${err_split[5]}
    echo "CHROM=${CHROM}"
    SIZEFROM=${err_split[7]}
    echo "SIZEFROM=${SIZEFROM}"
    SIZETO=${err_split[13]}
    echo "SIZETO=${SIZETO}"
    echo "sed 's/${CHROM}\(\[[:space:]]\)${SIZEFROM}/${CHROM}\1 ${SIZETO}/' ${OUTPUTSIZES} > TeMp.sizes"
    sed "s/${CHROM}\(\s\)${SIZEFROM}/${CHROM}\1${SIZETO}/" ${OUTPUTSIZES} > TeMp.sizes
    echo mv TeMp.sizes ${OUTPUTSIZES}
    mv TeMp.sizes ${OUTPUTSIZES}
    rm err.log

    echo wigToBigWig $INPUTWIG $OUTPUTSIZES $OUTPUTBIGWIG 2>err.log
    wigToBigWig $INPUTWIG $OUTPUTSIZES $OUTPUTBIGWIG 2>err.log
done
