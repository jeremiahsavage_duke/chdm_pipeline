import argparse
import csv
import os

parser = argparse.ArgumentParser("remove commas")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

in_file=args['infile']
out_file=args['outfile']

def joinnmid(in_file,out_file):

    with open(in_file,'r') as infile:
        inlines = infile.readlines()


    with open(out_file,'w') as outfile:
        for inline in inlines:
            print('inline=%s' % inline)
            newline = inline.replace(',','')
            outfile.write(newline)

joinnmid(in_file,out_file)
