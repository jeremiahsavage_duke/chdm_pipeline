import argparse
import csv
import os

parser = argparse.ArgumentParser("rotate sample-genes")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']


def rotatetsv(infile):
    origfile,origext = os.path.splitext(infile)
    outfile = origfile+'_rotate'+origext
    print('reading '+infile)

    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()

    ciloutfile = open('ciliopathy.tsv','w')
    controloutfile = open('control.tsv','w')

    lineno=1
    for inline in inlines:
        print()
        print()
        if lineno==1:
            ils = inline.split('\t')
            print('ils=',ils)
            header = 'SampleName'+'\t'+'Zygosity'+'\t'+ils[0]+'\t'+ils[1]+'\t'+ils[2]+'\t'+ils[3]+'\t'+ils[4]+'\t'+ils[5]+'\t'+ils[6]+'\t'+ils[7]+'\t'+ils[8]+'\t'+ils[9]+'\t'+ils[10]+'\t'+ils[11]+'\t'+ils[12]+'\t'+ils[13]+'\t'+ils[14]+'\t'+ils[15]+'\t'+ils[16]+'\t'+ils[17]+'\t'+ils[18]+'\t'+ils[19]+'\t'+ils[20]+'\t'+ils[21]+'\t'+ils[23]+'\n'
            ciloutfile.write(header)
            lineno+=1
            continue
        ils = inline.split('\t')
        print('ils=',ils)
        cilsample=ils[22]
        cilsplit = cilsample.split(';')
        print('sample=',cilsample)
        print('samplesplit=',cilsplit)
        for cil in cilsplit:
            cil=cil.lstrip()
            cil=cil.rstrip()
            print('     cil=',cil)
            subcil=cil.split(',')
            print('     subcil=',subcil)
            if len(subcil)==2:
                cilname=subcil[0]
                cilzyg=subcil[1]
            elif len(subcil[0])>1:
                cilname=subcil[0]
                cilzyg='NA'
            elif len(subcil)==1:
                cilname=''
                cilzyg=''
            newline = cilname+'\t'+cilzyg+'\t'+ils[0]+'\t'+ils[1]+'\t'+ils[2]+'\t'+ils[3]+'\t'+ils[4]+'\t'+ils[5]+'\t'+ils[6]+'\t'+ils[7]+'\t'+ils[8]+'\t'+ils[9]+'\t'+ils[10]+'\t'+ils[11]+'\t'+ils[12]+'\t'+ils[13]+'\t'+ils[14]+'\t'+ils[15]+'\t'+ils[16]+'\t'+ils[17]+'\t'+ils[18]+'\t'+ils[19]+'\t'+ils[20]+'\t'+ils[21]+'\t'+ils[23]+'\n'
            ciloutfile.write(newline)



    lineno=1
    for inline in inlines:
        print()
        print()
        if lineno==1:
            ils = inline.split('\t')
            print('ils=',ils)
            header = 'SampleName'+'\t'+'Zygosity'+'\t'+ils[0]+'\t'+ils[1]+'\t'+ils[2]+'\t'+ils[3]+'\t'+ils[4]+'\t'+ils[5]+'\t'+ils[6]+'\t'+ils[7]+'\t'+ils[8]+'\t'+ils[9]+'\t'+ils[10]+'\t'+ils[11]+'\t'+ils[12]+'\t'+ils[13]+'\t'+ils[14]+'\t'+ils[15]+'\t'+ils[16]+'\t'+ils[17]+'\t'+ils[18]+'\t'+ils[19]+'\t'+ils[20]+'\t'+ils[21]+'\t'+ils[23]+'\n'
            controloutfile.write(header)
            lineno+=1
            continue
        ils = inline.split('\t')
        print('ils=',ils)
        cilsample=ils[24]
        cilsplit = cilsample.split(';')
        print('sample=',cilsample)
        print('samplesplit=',cilsplit)
        for cil in cilsplit:
            cil=cil.lstrip()
            cil=cil.rstrip()
            print('     cil=',cil)
            subcil=cil.split(',')
            print('     subcil=',subcil)
            if len(subcil)==2:
                cilname=subcil[0]
                cilzyg=subcil[1]
            elif len(subcil[0])>1:
                cilname=subcil[0]
                cilzyg='NA'
            elif len(subcil)==1:
                cilname=''
                cilzyg=''
            newline = cilname+'\t'+cilzyg+'\t'+ils[0]+'\t'+ils[1]+'\t'+ils[2]+'\t'+ils[3]+'\t'+ils[4]+'\t'+ils[5]+'\t'+ils[6]+'\t'+ils[7]+'\t'+ils[8]+'\t'+ils[9]+'\t'+ils[10]+'\t'+ils[11]+'\t'+ils[12]+'\t'+ils[13]+'\t'+ils[14]+'\t'+ils[15]+'\t'+ils[16]+'\t'+ils[17]+'\t'+ils[18]+'\t'+ils[19]+'\t'+ils[20]+'\t'+ils[21]+'\t'+ils[23]+'\n'
            controloutfile.write(newline)


    ciloutfile.close()
    controloutfile.close()

rotatetsv(infile)
