#!/bin/bash
declare -a fqfiles=(`find . -iname *.fastq`)
for fqfile in "${fqfiles[@]}"
do
    echo "gzip -c "${fqfile}" > "${fqfile}".gz"
done | parallel -j12
