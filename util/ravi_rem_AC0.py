import argparse
import os

parser = argparse.ArgumentParser("remove +AC0")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def remAC0(infile,outfile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()
    out_file=open(outfile,'w')
    for inline in inlines:
        newline = inline.replace("+AC0","")
        out_file.write(newline)
    out_file.close()

remAC0(infile,outfile)
