 #!/usr/bin/env Rscript
library(ggplot2)
library(plyr)
args <- commandArgs(TRUE)
input <- args[1]
outdir <- args[2]
print(c('input=',input))
print(c('outdir=',outdir))
outfile <- paste(outdir,'sharedvariants.png',sep='/')
vals <- scan(input)
vals <- as.integer(vals)
coords <- rep(0:(length(vals)-1))
df1 <- data.frame(x=coords,vals)
df2 <- ddply(df1, .(x), summarize,y=length(x))
theme_set(theme_grey(base_size = 8))
p <- ggplot(df1,aes(x,y=vals)) + geom_bar(stat="identity") + geom_text(aes(label=vals), vjust=0,size=2) +  xlab("Number of samples") + ylab("Number of variants")
ggsave(plot=p,filename=outfile,width=5,height=5,units='in')
