import os
import re
import subprocess

string1="time java -d64 -Xmx4g -jar ~/local/bin/GenomeAnalysisTK.jar -T BaseRecalibrator -I Dir17_Mut_CTTGTA_R1_fixmateinformation.bam -R /home/jeremiah/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa -knownSites /home/jeremiah/zv9/Danio_rerio.Zv9.70.vcf -o fixvcf.grp 2> out.err"

while 1:
    s=""
    subprocess.call([string1],shell=True)
    with open('out.err', 'r') as f:
        lines = f.readlines()
    for l in lines:
        if "malformed" in l:
            s=l
    malformedline=s.split()
    linenum=malformedline[13]
    linenum=linenum.replace(":","")
    print("malformedline:",linenum)
    string2="sed -i -e '"+linenum+"d' ~/zv9/Danio_rerio.Zv9.70.vcf"
    print("string2:",string2)
    subprocess.call([string2],shell=True)
