#!/usr/bin/env python

import os

def get_dirs():
    dirlist=os.listdir()
    dirlistdirs=list()
    for adir in dirlist:
        if os.path.isdir(adir):
            dirlistdirs.append(adir)
    return dirlistdirs

def get_files_to_alter(adir):
    alterfilelist=list()
    dirfiles=os.listdir(adir)
    for afile in dirfiles:
        if afile.endswith('.fastq'):
            continue
        else:
            alterfilelist.append(afile)
    return alterfilelist
    
def prepend_dir_name(adir,alterfilelist):
    for alterfile in alterfilelist:
        newname=adir+'_'+alterfile
        fullcurrpath=os.path.join(adir,alterfile)
        fullnewpath=os.path.join(adir,newname)
        os.rename(fullcurrpath,fullnewpath)
        print('move %s to %s' % (fullcurrpath, fullnewpath))

def main():
    alldirs=get_dirs()
    for adir in alldirs:
        alterfilelist=get_files_to_alter(adir)
        prepend_dir_name(adir,alterfilelist)
        

if __name__=='__main__':
    main()
