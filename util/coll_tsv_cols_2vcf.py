import argparse
import csv
import os

parser = argparse.ArgumentParser("cull tsv file")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']


def culltsv(infile):
    origfile = os.path.splitext(infile)[0]
    tsvfile = origfile+".vcf"
    print(tsvfile)
    with open(infile,"r") as source:
        rdr = csv.reader(source, delimiter='\t')
        with open(tsvfile,"w") as outfile:
            wrt = csv.writer(outfile, delimiter='\t')
            for r in rdr:
                if (r[0] == "Chr"):
                    wrt.writerow(("##fileformat=VCFv4.1",))
                    wrt.writerow(("#CHROM","POS","ID","REF","ALT","QUAL","FILTER","INFO","FORMAT",origfile))
                else:
                    wrt.writerow(("chr"+r[0], r[1], ".", r[2], r[3], ".", ".", "ENSDARG="+r[5] + ";ENSDART="+r[6]+";MRN="+r[7] +";CRN=" + r[8],"GT"))

culltsv(infile)
