import argparse
import csv
import os
import sys

parser=argparse.ArgumentParser('join uniprot and go lists')
parser.add_argument('-u','--uniprot',required=True)
parser.add_argument('-g','--golist',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

uniprot_file=args['uniprot']
go_file=args['golist']
outfile=args['outfile']

def writeheader(outfile_open,uniprot_file,go_file):
    with open(uniprot_file,'r') as uniprot_open:
        uniprot_lines=uniprot_open.readlines()

    with open(go_file,'r') as go_open:
        go_lines=go_open.readlines()

    uniprot_header=uniprot_lines[0].strip('\n')
    go_header=go_lines.split('\n')[0].strip('\n')

    outfile_header=uniprot_header+'\t'+go_header
    outfile_open.write(outfile_header)

def joinit(uniprot_file,go_file,outfile):
    outfile_open=open(outfile,'w')
    writeheader(outfile_open,uniprot_file,go_file)

    with open(uniprot_file,'r') as uniprot_open:
        uniprot_lines=uniprot_open.readlines()

    with open(go_file,'r') as go_open:
        go_lines=go_open.readlines()
    
    for uniprot_line in uniprot_lines:
        if uniprot_line.startswith('Gene'):
            continue
        else:
            outline=uniprot_line.strip('\n')
            outfile_open.write(outline)

            uniprot_id=uniprot_line.split('\t')[6]
            for go_line in go_lines:
                if go_line.startswith('DB'):
                    continue
                else:
                    go_id=go_line.split('\t')[1]
                    if uniprot_id==go_id:
                        outfile_open.write(go_line)
                        break
            outfile_open.write('\n')

    outfile_open.close()
            



joinit(uniprot_file,go_file,outfile)
