#!/bin/sh
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input.wig output.wig" >&2
    exit 1
fi
if ! [ -e "$1" ]; then
    echo "$1 not found" >&2
    exit 1
fi

sed 's/chrom=\([[:digit:]]\)/chrom=chr\1/g' ${1} > TeMp1.wig
sed 's/chrom=MT/chrom=chrM/' TeMp1.wig > ${2}
rm TeMp1.wig
