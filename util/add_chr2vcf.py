import argparse
import os

parser = argparse.ArgumentParser("convert csv to tab")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def chr2vcf(infile,outfile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()

    out_file=open(outfile,'w')
    for inline in inlines:
        firstfield = inline.split('\t')[0]
        if (firstfield.isdigit()):
            out_file.write("chr" + inline)
        else:
            out_file.write(inline)
    out_file.close()

chr2vcf(infile,outfile)
