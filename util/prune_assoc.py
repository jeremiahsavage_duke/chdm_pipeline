import argparse
import csv
import os
import sys
KEEPCOLS=[1,2,5,6,7]
parser=argparse.ArgumentParser('prune quickGO association file')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']


def doit(infile,outfile):
    outfile_open=open(outfile,'w')

    with open(infile,'r') as infile_open:
        infile_lines=infile_open.readlines()

    for infile_line in infile_lines:
        ils=infile_line.split('\t')
        outstr=str()
        for keepcol in KEEPCOLS:
            outstr+=ils[keepcol]+'\t'
        outstr.strip('\t')
        outfile_open.write(outstr+'\n')
    
    outfile_open.close()


doit(infile,outfile)
