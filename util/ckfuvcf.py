import os
import re
import subprocess

string1="time java -d64 -Xmx4g -jar ~/local/bin/GenomeAnalysisTK.jar -T BaseRecalibrator -I CHC1_TAGCTT_R1_fixmateinformation.bam -R /home/jeremiah/home2/Danio_rerio.Zv9.68.dna.toplevel/Danio_rerio.Zv9.68.dna.toplevel.fa -knownSites /home/jeremiah/home2/Danio_rerio.Zv9.68.dna.toplevel/Danio_rerio.vcf -o input.CHC1_TAGCTT_R1_fixmateinformation.grp 2> out.err"

while 1:
    s=""
    subprocess.call([string1],shell=True)
    with open('out.err', 'r') as f:
        lines = f.readlines()
    for l in lines:
        if "malformed" in l:
            s=l
    malformedline=s.split()
    linenum=malformedline[13]
    linenum=linenum.replace(":","")
    print("malformedline:",linenum)
    string2="sed -i -e '"+linenum+"d' ~/home2/Danio_rerio.Zv9.68.dna.toplevel/Danio_rerio.vcf"
    print("string2:",string2)
    subprocess.call([string2],shell=True)
