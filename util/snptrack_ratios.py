import argparse
import csv
import os

parser = argparse.ArgumentParser("cull vcf file on ratios")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def mutantphase(mrn):
    mutantref = float(mrn.split('/')[0])
    mutantalt = float(mrn.split('/')[1])
    keep = False
    if (mutantalt > 50):
        if ( (mutantref/mutantalt) <= 0.04):
            keep = True
        else:
            keep = False
    elif (mutantalt == 0):
        keep = False
    else:
        if ( (mutantref/mutantalt) <= 0.1):
            keep = True
        else:
            keep = False
    return keep

def controlphase(crn):
    controlref = int(crn.split('/')[0])
    controlalt = int(crn.split('/')[1])
    keep = False
    if ( abs(controlref - controlalt) <= 5 ):
        keep = True
    if (controlalt == 0):
        keep = True
    elif (float(controlref) / float(controlalt) >= 0.7):
        keep = True
    return keep

def cullvcf(infile):
    origfile = os.path.splitext(infile)[0]
    vcffile = origfile+"_ratio.vcf"
    print(vcffile)
    with open(infile,"r") as source:
        rdr = csv.reader(source, delimiter='\t')
        with open(vcffile,"w") as outfile:
            wrt = csv.writer(outfile, delimiter='\t')
            for r in rdr:
                if (r[0].startswith('#')):
                    wrt.writerow(r)
                else:
                    infofield = r[7]
                    crn = infofield.split(';')[0].split('=')[1]
                    mrn = infofield.split(';')[3].split('=')[1]
                    keepmut = mutantphase(mrn)
                    keepcon = controlphase(crn)
                    if (keepmut and keepcon):
                        wrt.writerow(r)


cullvcf(infile)
