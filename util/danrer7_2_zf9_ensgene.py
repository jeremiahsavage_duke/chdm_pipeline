import argparse
import os

parser = argparse.ArgumentParser("convert csv to tab")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

input=args['input']
output=args['output']

def ucscfix(input,output):

    with open(input,'r') as infile:
        inlines = infile.readlines()

    outfile=open(output,'w')

    for line in inlines:
        linesplit=line.split('\t')
        chrom=linesplit[2]
        if chrom.startswith('chr'):
            newchrom=chrom.replace('chr','')
            if newchrom=='MT':
                newchrom='M'
            newsplit=linesplit
            newsplit[2]=newchrom
            newline='\t'.join(newsplit)
            outfile.write(newline)
        else:
            outfile.write(line)

    outfile.close()

ucscfix(input,output)
