import argparse
import csv
import os

parser = argparse.ArgumentParser("cull tsv file")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']


def counttranscripts(infile):
    origfile = os.path.splitext(infile)[0]
    countfile = origfile+".count"
    print(countfile)
    with open(infile,"r") as source:
        rdr = csv.reader(source, delimiter='\t')
        with open(countfile,"w") as outfile:
            wrt = csv.writer(outfile, delimiter='\t')
            for r in rdr:
                transcripts = r[2]
                transcripts_components = transcripts.split(':')
                genename = transcripts_components[0]
                num_variants = 0
                for item in transcripts_components:
                    if (origfile.startswith("ref")):
                        if item.startswith("NM_"):
                            num_variants += 1
                    if (origfile.startswith("ens")):
                        if item.startswith("ENST"):
                            num_variants += 1
                    if (origfile.startswith("known")):
                        if item.startswith("uc"):
                            num_variants += 1
                wrt.writerow((r[3],"num_var="+str(num_variants),genename,r[4],r[5],r[6],r[7],r[15]))

counttranscripts(infile)
