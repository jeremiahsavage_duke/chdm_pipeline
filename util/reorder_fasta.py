import argparse
import os

parser = argparse.ArgumentParser("convert csv to tab")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']

def chr2vcf(infile,outfile):

    with open(infile,'r') as in_file:
        inlines = in_file.readlines()

    out_file=open(outfile,'w')
    keepchr = False
    for i in range(25):
        print(">chr"+str(i+1)+'\t'+"hi")
        for inline in inlines:
            if inline.startswith(">chr"+str(i+1)+'\n'):
                keepchr = True
                out_file.write(inline)
            elif inline.startswith(">"):
                keepchr = False
            elif (keepchr):
                out_file.write(inline)
    out_file.close()

chr2vcf(infile,outfile)
