import argparse
import os
import sys


parser = argparse.ArgumentParser("unmess bed")
parser.add_argument('-i','--input',required=True)
args=vars(parser.parse_args())

infile=args['input']

def doit(infile):
    with open(infile,'r') as infile_open:
        inlines=infile_open.readlines()

    origname,origext=os.path.splitext(infile)
    outfile=origname+'_fix'+origext

    outfile_open=open(outfile,'w')

    for inline in inlines:
        if inline.startswith('chr'):
            newline=inline.strip()
            outfile_open.write(newline+'\n')
    

doit(infile)
