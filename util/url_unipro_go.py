import argparse
import os
import urllib2

parser = argparse.ArgumentParser("get uniprot GO data")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
#parser.add_argument('-a','--goaspect',required=True)
args=vars(parser.parse_args())

in_file=args['infile']
#goaspect=args['goaspect']
out_file=args['outfile']

def getuniprotgo(in_file,out_file):

    with open(in_file,'r') as infile:
        inlines = infile.readlines()

    outfile=open(out_file,'w')

    uninmdict=dict()
    uninmlist=list()
    for inline in inlines:
        uniprotval=inline.split()[1]
        uninmdict[uniprotval]=inline
        uninmlist.append(uniprotval)

    unikeylist=uninmdict.keys()
    for uniprot in uninmlist:
        if ("NOUNIPROT" not in uniprot):
            print('uniprot=%s' % uniprot)
            #print('http://www.ebi.ac.uk/QuickGO/GAnnotation?protein='+unikey+'&format=tsv&aspect='+goaspect)
            response=urllib2.urlopen('http://www.ebi.ac.uk/QuickGO/GAnnotation?protein='+uniprot+'&format=tsv') #&aspect='+goaspect)
            html=response.read()
            htmlstring=uniprot.rstrip('\n')+'\t'
            #htmlstring=uninmdict[unikey].rstrip('\n')+'\t'+unikey+'\t'
            linecount=0
            htmllines = html.split('\n')
            for htmlline in htmllines:
                print("htmlline=",htmlline)
                if(htmlline==''):
                    continue
                if(linecount==0):
                    linecount+=1
                    continue
                if(linecount==1):
                    htmlstring+=htmlline.split('\t')[3]+'\t' #symbol
                    print("symbol=",htmlline.split('\t')[3])
                htmlstring+=htmlline.split('\t')[11]+'\t' #GO Aspect
                print("GOAspect=",htmlline.split('\t')[11])
                htmlstring+=htmlline.split('\t')[7]+'\t' #GO Name
                print("GOName=",htmlline.split('\t')[7])
                htmlstring+=htmlline.split('\t')[9]+'\t'#Evidence
                print("Evidence=",htmlline.split('\t')[9])
                linecount+=1
            htmlstring.rstrip('\t')
            htmlstring+='\n'
            outfile.write(htmlstring)
            continue
        else:
            outfile.write(uniprot)
        outfile.write(uniprot+'\n')

    outfile.close()
getuniprotgo(in_file,out_file)
