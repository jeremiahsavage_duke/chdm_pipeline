import inspect
import os

def buildpairedenddict(fastqlist):
    print('pipeutil::buildpairedendict::fastqlist=',fastqlist)
    pairedenddict=dict()
    for f in fastqlist:
        if '_R1' in f:
            print('f w/R1')
            r2name=f.replace('_R1','_R2')
            if os.path.exists(r2name):
                pairedenddict[f]=f.replace('_R1','_R2')
            else:
                pairedenddict[f]=None
        if ('_R1' not in f) and ('_R2' not in f):
            print('f w/o R1 or R2')
            pairedenddict.update({f:None})
    print('pipeutil::buildpairedendict::pairenddict=',pairedenddict)
    return pairedenddict


def tagdict2fqlist(fastqtagdict):
    fname=inspect.stack()[0][3]
    print('pipeutil::tagdict2fqlist::fastqtagdict=',fastqtagdict)
    fqlist=list()
    for f in fastqtagdict:
        print('pipeutil::tagdict2fqlist::f=',f)
        fqlist.append(f)
    print('pipeutil::tagdict2fqlist::fqlist=',fqlist)
    return fqlist,fname


def list2pairedenddict(fastqlist):
    pairedenddict={}
    for f in fastqlist:
        if "_R1_" in f:
            pairedenddict[f]=f.replace("_R1_","_R2_")
    return pairedenddict

def fastqtagdict2adapterslist(fastqtagdict,templatefile):
    adapterlist=[]
    for f in fastqtagdict:
        tag=fastqtagdict[f]
        adapterfile=tag2adapterfile(tag,templatefile)
        adapterslist.append(adapterfile)
    return adapterlist

def tag2adapterfile(tag,templatefile):
    print("templatefile=",templatefile)
    print("tag=",tag)
    templatename,templateextension=os.path.splitext(templatefile)
    templatename=os.path.basename(templatename)
    adapterfile=templatename+"_"+tag+templateextension
    return adapterfile

def pathlastdirname(apath):
    apath=os.path.normpath(apath)
    dirlist=apath.split(os.sep)
    dirlistlen=len(dirlist)
    lastdir=dirlist[dirlistlen-1]
    return lastdir

def addquote(astring):
    newstring= "\""+ astring + "\""
    return newstring
