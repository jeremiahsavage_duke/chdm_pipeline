import ast
import inspect
import os
import re
import argparse
#project imports
import prepinput
import trimfilter
import alignreads
import sam2bam
import prepreferencegenome
import rungatk
import variant_filtration
parser=argparse.ArgumentParser("run the pipe")
parser.add_argument('-s','--truseqtemplate', required=True)
parser.add_argument('-i','--indexlist', required=True)
parser.add_argument('-d','--dictfile', required=True)
parser.add_argument('-n','--stringreplaced', required=True)
parser.add_argument('-r','--referencedir', required=True)
#parser.add_argument('-c','--chromolist', required=True)
parser.add_argument('-g','--genomesource', required=True)
parser.add_argument('-b','--baitfile', required=False)
parser.add_argument('-t','--targetfile', required=False)
parser.add_argument('-v','--vcffile', required=False)
parser.add_argument('-m','--genomename', required=True)
parser.add_argument('-p','--poolsize',required=True)
parser.add_argument('-z','--mutagenized',required=True)
parser.add_argument('--snptrack_chromosome',required=False)
parser.add_argument('--snptrack_startpeak',required=False)
parser.add_argument('--snptrack_endpeak',required=False)
parser.add_argument('--isrna',required=False)
parser.add_argument('--gtffile',required=False)
parser.add_argument('--skipfilter',required=False)
parser.add_argument('--filterdir',required=False)
args=vars(parser.parse_args())

truseqtemplate=args['truseqtemplate']
tagindexlist=''.join(args['indexlist'].split()).split(',')
fastqtagdictfile=args['dictfile']
replacestring=args['stringreplaced']
referencedir=args['referencedir']
genomename=args['genomename']
#chromosomefastalist=''.join(args['chromolist'].split()).split(',')
genomesource=args['genomesource']
baitfile=args['baitfile']
targetfile=args['targetfile']
vcffile=args['vcffile']
mutagenized=bool(args['mutagenized'])
poolsize=args['poolsize']
snptrack_chromosome=args['snptrack_chromosome']
snptrack_startpeak=args['snptrack_startpeak']
snptrack_endpeak=args['snptrack_endpeak']
sampleploidy = int(poolsize) * 2
sampleploidy = str(sampleploidy)
filterdir = args['filterdir']
isrna=ast.literal_eval(args['isrna'])
print('pipeline.py::args[\'isrna\']=',args['isrna'])
print('pipeline.py::isrna=',isrna)
gtffile=args['gtffile']
skipfilter=args['skipfilter']
if skipfilter is None:
    skipfilterlist=list()
else:
    skipfilterlist=list(skipfilter)
### need a variable for fastq format (sanger, illumina, phred, solexa)

referencedir += os.sep
currdir=os.getcwd()
callerframerecord = inspect.stack()[0]
frame = callerframerecord[0]
info = inspect.getframeinfo(frame)

referencegenomefasta=prepreferencegenome.prepreferencegenome(referencedir,genomesource,genomename)
fastqtagdict=prepinput.prepinput(fastqtagdictfile,currdir,tagindexlist,truseqtemplate,replacestring)
print(info.filename+'::'+info.function+'::'+str(info.lineno)+'::fastqtagdict=',fastqtagdict)
preppedfastqdict,lastcall=trimfilter.trimfilter(fastqtagdict,truseqtemplate)
print(info.filename+'::'+info.function+'::'+str(info.lineno)+'::preppedfastqdict=',preppedfastqdict)
samlist=alignreads.alignreads(preppedfastqdict,referencedir,referencegenomefasta,lastcall,isrna,gtffile)
bamlist,lastcall=sam2bam.sam2bamrmdup(samlist,referencegenomefasta,isrna)
vcflist,lastcall=rungatk.rungatk(bamlist,referencegenomefasta,baitfile,targetfile,vcffile,lastcall,sampleploidy,mutagenized)
print("vcflist=",vcflist)
print("snptrack_chromosome=",snptrack_chromosome)
print("snptrack_startpeak=",snptrack_startpeak)
print("snptrack_endpeak=",snptrack_endpeak)
variant_filtration.variant_filtration(vcflist,snptrack_chromosome,snptrack_startpeak,snptrack_endpeak,referencegenomefasta,skipfilterlist,filterdir)
