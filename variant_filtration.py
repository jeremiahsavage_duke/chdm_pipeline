import glob
import inspect
import os
import shutil
import subprocess
import multiprocessing # .cpu_count()
import sys #exit
#local
homedir=os.path.expanduser("~/")
sys.path.append(os.path.normpath(os.path.join(homedir,'chdm_pipeline','vcf')))
import vcf_split_numfilt

def get_homologous_region(vcflist,snptrack_chromosome,snptrack_startpeak,snptrack_endpeak,referencegenomefasta):
    fname=inspect.stack()[0][3]
    outlist=list()
    origname,origextension=os.path.splitext(vcflist[0])
#    outname=origname.replace(lastcall,fname)+origextension
    #outfile = "chr"+str(snptrack_chromosome)+'_'+str(snptrack_startpeak)+'_'+str(snptrack_endpeak)+'.vcf'
    outfile='region.vcf'
    fulloutfile=os.path.join('filter',outfile)
    if os.path.exists(fulloutfile):
        print("SelectVariants already run: ",outfile)
    else:
        subcall="/usr/bin/time java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar " + " -R " + referencegenomefasta + " -T SelectVariants --variant " + vcflist[0] + " -o " + fulloutfile + " -L " + str(snptrack_chromosome)+':'+str(snptrack_startpeak)+'-'+str(snptrack_endpeak)
        print(fname+" subcall: ",subcall)
        subprocess.call([subcall],shell=True)
    return fulloutfile,fname

def make_filtration_dir(snptrack_chromosome):
    dirnamelist=['filter','allelefilt','nonallelefilt']
    for dirname in dirnamelist:
        if (not os.path.exists(dirname)):
            os.makedirs(dirname)
    return dirnamelist

def get_filtername(sampledir):
    if (sampledir.startswith('Sample_')):
        filtername = sampledir.replace('Sample_','')
        if (filtername.startswith('Zf-')):
            filtername = filtername.replace('Zf-','')
        if ('-' in filtername):
            filtername = filtername.split('-')[0]
        if ('_' in filtername):
            filtername = filtername.split('_')[0]
        return filtername
    else:
        return sampledir

def filter_it(vcffile,sampledir,referencegenomefasta,subdir,filterdir):
    print('variant_filtration.py::filter_it()::vcffile=',vcffile)
    print('variant_filtration.py::filter_it()::sampledir=',sampledir)
    print('variant_filtration.py::filter_it()::referencegenomefasta=',referencegenomefasta)
    print('variant_filtration.py::filter_it()::subdir=',subdir)
    print('variant_filtration.py::filter_it()::filterdir=',filterdir)
    vcfdir=os.path.join(filterdir,sampledir)
    print('vcfdir=%s' % vcfdir)
    
    #print('variant_filtration.py::filter_it()::sampledir=',os.path.join(os.pardir,sampledir))

    fname=inspect.stack()[0][3]
    outlist=list()
    filtername = get_filtername(sampledir)
    print('filtername=',filtername)
    import glob
    print("glob=,",glob.glob(os.path.join(vcfdir,'*.vcf')))
    filtervcf = glob.glob(os.path.join(vcfdir,'*.vcf'))
    print('filtervcf=%s' % filtervcf)
    if (len(filtervcf) == 0):
        import sys
        print('where\'s your head at?!')
        sys.exit(0)
    filtervcf = filtervcf[0]
    inname='varfilter.vcf'
    infile=os.path.join(subdir,inname)
    outname='varfilter_out.vcf'
    outfile=os.path.join(subdir,outname)
    print('infile=',infile)
    print('outfile=',outfile)
    if not os.path.exists(infile):
        import shutil
        print('copy ' + vcffile + ' to ' + infile)
        shutil.copyfile(vcffile,infile)

    subcall="/usr/bin/time java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar " + " -R " + referencegenomefasta + " -T VariantFiltration --variant " + infile + " --mask " + filtervcf + " --out " + outfile + " --maskName \"" + filtername + "\""
    print(fname+" subcall: ",subcall)
    subprocess.call([subcall],shell=True)        

    if os.path.isfile(outfile):
        import shutil
        shutil.move(outfile,infile)
        indexfile1=infile+'.idx'
        indexfile2=outfile+'.idx'
        if os.path.exists(indexfile1):
            os.remove(indexfile1)
        if os.path.exists(indexfile2):
            os.remove(indexfile2)
    else:
        import sys
        print('Game Over, Man')
        sys.exit(0)
    return infile,fname

def getfilteridlist(infile):
    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()
    filteridlist=list()
    for inline in inlines:
        if inline.startswith('##FILTER=<ID='):
            filtername=inline.split('=')[2].split(',')[0]
            if (filtername != 'LowQual'):
                filteridlist.append(filtername)
    return filteridlist


def alreadyfiltered(vcffile,newlist,subdir):
    infile=os.path.join(subdir,'varfilter.vcf')
    if not os.path.exists(infile):
        print('alreadyfiltered()::no file:',infile)
        return False
    else:
        currfiltered=getfilteridlist(infile)
        print('alreadyfiltered::currfiltered=',currfiltered)
        if (len(currfiltered) != len(newlist)):
            print('alreadyfiltered()::wronglength:',infile)
            return False
        else:
            print('alreadyfiltered()::yes!:',infile)
            return True


def filter_against_others(vcffile,lastcall,referencegenomefasta,skipfilterlist,subdir,filterdir):
    fname=inspect.stack()[0][3]
    print('variant_filtration.py::filter_against_others()::vcffile=',vcffile)
    sample_dirs = os.listdir(filterdir)
    current_sample = os.getcwd().split('/')[4]
    print("current_sample=",current_sample)
    print("samples_dirs=",sample_dirs)
    sample_dirs.remove(current_sample)
    for toskip in skipfilterlist:
        for sampledir in sample_dirs:
            if toskip in sampledir:
                sample_dirs.remove(sampledir)
    filterlist = [sample for sample in sample_dirs if not 'Wt' in sample]
    newlist = [os.path.join(item,'all') if 'Mut' in item else item for item in filterlist]
    print("filterlist=",filterlist)
    print()
    print('newlist=',newlist)
    if not alreadyfiltered(vcffile,newlist,subdir):
        for sampledir in newlist:
            filteredvcf,lastfiltercall=filter_it(vcffile,sampledir,referencegenomefasta,subdir,filterdir)
    else:
        filteredvcf=os.path.join(subdir,'varfilter.vcf')
    return filteredvcf,fname

def annotate(vcffiltlist):
    print('annotate::vcffiltlist=',vcffiltlist)
    for filtvcf in vcffiltlist:
        origfile,origext=os.path.splitext(filtvcf)
        outfile1=origfile+'_ucsc'+origext
        outfile2=origfile+'_ucsc'+'.annovar'
        print('filtvcf=',filtvcf)
        print('outfile1=',outfile1)
        print('outfile2=',outfile2)
        if not os.path.exists(outfile2):
            subcall1='python ~/chdm_pipeline/vcf/vcf_zv2danr.py -i ' + filtvcf
            subcall2='perl ~/tools/annovar/convert2annovar.pl ' + outfile1 + ' -i --format vcf4 > ' + outfile2
            subcall3='perl ~/tools/annovar/annotate_variation.pl --geneanno -splicing_threshold 18 -buildver danRer7 ' + outfile2 + ' --dbtype ensGene ~/harris/orig/zebradb'
            subprocess.call([subcall1],shell=True)
            subprocess.call([subcall2],shell=True)
            subprocess.call([subcall3],shell=True)
        else:
            print('annotated already exists: ', outfile2)

def combine_mutwtvcf(mutvcf,wtvcf,referencegenomefasta):
    print('variant_filtration.py::combine_mutwtvcf()::mutvcf=',mutvcf)
    print('variant_filtration.py::combine_mutwtvcf()::wtvcf=',wtvcf)
    fname=inspect.stack()[0][3]
    print('variant_filtration::combinemutwtvcf()::mutvcf=',mutvcf)
    outdir=os.path.dirname(mutvcf)
    print('variant_filtration::combinemutwtvcf()::outdir=',outdir)
    outfile=os.path.join(outdir,'combine.vcf')
    if os.path.exists(outfile):
        print(mutvcf+' and '+wtvcf+' already combined')
    else:
        subcall='java -Xmx4G -d64 -jar ~/.local/bin/GenomeAnalysisTK.jar -T CombineVariants -R ' + referencegenomefasta + ' --variant ' + mutvcf + ' --variant ' + wtvcf + ' -o ' + outfile + ' --genotypemergeoption UNIQUIFY'
        print(fname+" subcall: ",subcall)
        subprocess.call([subcall],shell=True)
    return outfile,fname

def combineallmutwt(vcflist,referencegenomefasta):
    fname=inspect.stack()[0][3]
    print('variant_filtration.py::combineallmutwt()::vcflist=',vcflist)
    mutvcf=vcflist[0]
    mutdir = os.getcwd()
    print('variant_filtration.py::combineallmutwt()::mutdir=',mutdir)
    wtdir=mutdir.replace('Mut','Wt')
    print('variant_filtration.py::combineallmutwt()::wtdir=',wtdir)

    wtvcf=glob.glob(os.path.join(wtdir,'*.vcf'))[0]
    if not os.path.isdir('all'):
        os.makedirs('all')
    outfile='all/mutwt.vcf'
    if os.path.exists(outfile):
        print(outfile +' already combined')
    else:
        subcall='java -Xmx4G -d64 -jar ~/.local/bin/GenomeAnalysisTK.jar -T CombineVariants -R ' + referencegenomefasta + ' --variant ' + mutvcf + ' --variant ' + wtvcf + ' -o ' + outfile + ' --genotypemergeoption UNIQUIFY'
        print(fname+" subcall: ",subcall)
        subprocess.call([subcall],shell=True)
    return outfile,fname

def filter_allele_ratios(vcffile,referencegenomefasta,filtration_dir):
    fname=inspect.stack()[0][3]
    currdir = os.getcwd()
    if ('Mut' in currdir):
        mutdir = currdir
        print('variant_filtration.py::filter_allele_ratios()::mutdir=',mutdir)
        wtdir=mutdir.replace('Mut','Wt')
        print('variant_filtration.py::filter_allele_ratios()::wtdir=',wtdir)
        mutvcf=os.path.join(mutdir,vcffile)
        print('mutvcf=',mutvcf)
        wtvcf=os.path.join(wtdir,vcffile)
        print('wtvcf=',wtvcf)
        combinedvcf,lastcall=combine_mutwtvcf(mutvcf,wtvcf,referencegenomefasta)
        outfile=os.path.join('allelefilt','alleleratio.vcf')
        print('outfile=',outfile)
        if os.path.exists(outfile):
            print(outfile+' already filtered by allele ratio')
        else:
            subcall='python ~/chdm_pipeline/vcf/unifiedgenotyper_allele_ratio.py --combined '+ combinedvcf + ' --control ' + wtvcf + ' --mutant ' + mutvcf + ' --output ' + outfile
            print(fname+" subcall: ",subcall)
            subprocess.call([subcall],shell=True)
    else:
        outfile='None'
    return outfile,fname

def listtofile(binlist,outfile):
    outstr=str()
    for item in binlist:
        outstr+=' '+str(item)
    outstr=outstr.strip()
    outfile_open=open(outfile,'w')
    outfile_open.write(outstr)
    outfile_open.close()

def plothistogram(datafile):
    fname=inspect.stack()[0][3]
    outdir=os.path.dirname(datafile)
    subcall='/usr/bin/time Rscript ~/chdm_pipeline/util/plot_binlist.R ' + datafile + ' ' + outdir
    print(fname+" subcall: ",subcall)
    subprocess.call([subcall],shell=True)


def histogram(vcffile):
    datafile=os.path.join(os.path.dirname(vcffile),'binlist.txt')
    binlist,vcffiltlist=vcf_split_numfilt.splitnumfilt(vcffile)
    listtofile(binlist,datafile)
    plothistogram(datafile)
    return binlist,vcffiltlist

def variant_filtration(vcflist,snptrack_chromosome,snptrack_startpeak,snptrack_endpeak,referencegenomefasta,skipfilterlist,filterdir):
    print('variant_filtration(filterdir)=%s\n' % (filterdir))
    if filterdir is None:
        filterdir=os.path.abspath(os.path.join(os.getcwd(),os.pardir))
    print('filterdir=%s' % filterdir)
    print("variant_filtration::make_filtration_dir")
    print('\n\n\n')
    filtration_dirlist=make_filtration_dir(snptrack_chromosome)
    print('\n\n\n')
    print("variant_filtration::get_homologous_region")
    vcffile,lastcall = get_homologous_region(vcflist,snptrack_chromosome,snptrack_startpeak,snptrack_endpeak,referencegenomefasta)
    print('\n\n\n')
    allelevcffile,lastcall = filter_allele_ratios(vcffile,referencegenomefasta,'allelefilt')
    print('\n\n\n')
    print("variant_filtration::filter_against_others")
    combinevcffile,lastcall=combineallmutwt(vcflist,referencegenomefasta)
    print('\n\n\n')
    print('call filter_against_others(allelefilt)')
    vcffileallele,lastcall=filter_against_others(allelevcffile,lastcall,referencegenomefasta,skipfilterlist,'allelefilt',filterdir)
    print('call filter_against_others(nonallelefilt)')
    vcffilenonallele,lastcall=filter_against_others(vcffile,lastcall,referencegenomefasta,skipfilterlist,'nonallelefilt',filterdir)
    print('\n\n\n')
    print('vcffileallel=',vcffileallele)
    print('vcffilenonallele=',vcffilenonallele)
    binlist_alle,vcffiltlist_alle=histogram(vcffileallele)
    binlist_nonalle,vcffiltlist_nonalle=histogram(vcffilenonallele)
    print('vcffile=',vcffile)
    annotate(vcffiltlist_alle)
    annotate(vcffiltlist_nonalle)
