###2012-11-21
#clean
rm qc*_*_*.fastq

#make
for f in qc*.fastq
do
    echo f = $f
    numlinesorig=`wc -l $f`
    echo numlinesorig = $numlinesorig
    numlinesorig=($numlinesorig)
    echo numlinesorig = $numlinesorig
    decamt=`expr ${numlinesorig} / 10`
    echo decamt = $decamt
    remainder=`expr $decamt % 4`
    decamt=`expr $decamt + 4 - $remainder`
    echo newdecamt =  $decamt
    for i in {1..10}
    do
	echo i = $i
	base=${f%.*}
	ext=${f##*.}
	newname=${base}_${i}.${ext}
	numlinesnew=`expr ${decamt} \* ${i}`
	echo numlinesnew = $numlinesnew
	if [ $i -eq "10" ]
	then
	    cp ${f} ${newname}
	else
	    echo head -n ${numlinesnew} $f > ${newname}
	    head -n ${numlinesnew} $f > ${newname}
	fi
    done
done

mkdir zv9
cd zv9
wget ftp://ftp.ensembl.org/pub/release-69/fasta/danio_rerio/dna/Danio_rerio.Zv9.69.dna.toplevel.fa.gz
wget ftp://ftp.ensembl.org/pub/release-69/gtf/danio_rerio/Danio_rerio.Zv9.69.gtf.gz
gunzip Danio_rerio.Zv9.69.gtf.gz
gunzip Danio_rerio.Zv9.69.dna.toplevel.fa.gz
cuffcompare -s Danio_rerio.Zv9.69.dna.toplevel.fa -CG -r Danio_rerio.Zv9.69.gtf Danio_rerio.Zv9.69.gtf
