 library(RnaSeqTutorial)

aln3 <- readGappedAlignments("accepted_hits.bam",format="BAM")

ensembl_mart <- useMart("ensembl",dataset="drerio_gene_ensembl")

exon.annotation <- getBM(c("ensembl_gene_id","strand","ensembl_transcript_id","chromosome_name","ensembl_exon_id","exon_chrom_start","exon_chrom_end"),mart=ensembl_mart,filters="chromosome_name",values=c(as.character(seq(1,25)),"MT"))

exon.annotation$chromosome <- paste("chr",exon.annotation$chromosome_name,sep="")

exon.range <- RangedData(IRanges(start=exon.annotation$exon_chrom_start,end=exon.annotation$exon_chrom_end),space=exon.annotation$chromosome,strand=exon.annotation$strand,transcript=exon.annotation$ensembl_transcript_id,gene=exon.annotation$ensembl_gene_id,exon=exon.annotation$ensembl_exon_id,universe = "Zv9")


########
library(GenomicFeatures)
zv9.refGene <- makeTranscriptDbFromUCSC(genome="danRer7",tablename="refGene")
zv9.ensGene <- makeTranscriptDbFromUCSC(genome="danRer7",tablename="ensGene")
#zv9.knownGene <- makeTranscriptDbFromUCSC(genome="danRer7",tablename="knownGene")
zv9.ensExon <- exons(zv9.ensGene)
zv9.refExon <- exons(zv9.refGene)
########





###################
library(BSgenome.Drerio.UCSC.danRer7)
chrsizes <- seqlengths(Drerio)
#names(chrsizes) <- seq_along(names(chrsizes))
chrnums <- sapply(names(chrsizes), function(x) substring(x,4,))


chrnums <- unname(chrnums)
names(chrsizes) <- chrnums
names(chrsizes)[26] <- "MT"
###################




seqlevels(aln3) #remove all ^Zv9_
#substring(names(chrsizes)[[3]],4,)

gr <- granges(aln3)
singles <- split(gr,seqnames(gr))

#keep <- lapply(seq(1:25), function(x) singles$"x")
keep <- lapply(seq(1:25), function(x) singles[[x]])
aln3.24 <- lapply(seq(1:25), function(x) aln3[seqnames(aln3) == "x"])
cover <- coverage(aln3,width=chrsizes)


library(genomeIntervals)
gInterval <- readGff3("Danio_rerio.Zv9.70.gtf")
library(IRanges)
exon.range2 <- RangedData(
 IRanges(
 start=gInterval[,1],
 end=gInterval[,2]),
 space=gInterval$seq_name,
 strand=gInterval$strand,
 transcript=as.vector(
 getGffAttribute(gInterval,"Parent")),
 gene=as.vector(
 getGffAttribute(gInterval,"Name")),
 exon=as.vector(
 getGffAttribute(gInterval,"ID")),
 universe = "Zv9"
 )



genes.table <- easyRNASeq(
    organism="danRer7",
#    chr.sizes=as.list(seqlengths(Drerio)),
    readLength=50L,
    annotationMethod="gtf",
    annotationFile="/home/jeremiah/zv9/Danio_rerio.Zv9.70.gtf",
    format="bam",
    count="genes",
    summarization="geneModels",
    conditions=c("jhc1.bam"="control","jhc2.bam"="control","jhw1.bam"="exp","jhw2.bam"="exp"),
    gapped="TRUE",
    chr.size="auto",
    #normalize="TRUE",
    nbCore=16,
    filesDirectory="/home/jeremiah/Project_Wang10496/accepted_hits",
    filenames=c("jhc1.bam","jhc2.bam","jhw1.bam","jhw2.bam"))


genes.table.rnaseq <- easyRNASeq(
    organism="danRer7",
#    chr.sizes=as.list(seqlengths(Drerio)),
    readLength=50L,
    annotationMethod="gtf",
    annotationFile="/home/jeremiah/zv9/Danio_rerio.Zv9.70.gtf",
    format="bam",
    count="genes",
    summarization="geneModels",
    conditions=c("jhc1.bam"="control1","jhc2.bam"="control2","jhw1.bam"="exp1","jhw2.bam"="exp2"),
    gapped="TRUE",
    chr.size="auto",
    #normalize="TRUE",
    nbCore=16,
    filesDirectory="/home/jeremiah/Project_Wang10496/accepted_hits",
    filenames=c("jhc1.bam","jhc2.bam","jhw1.bam","jhw2.bam"),
    ouputFormat="RNAseq")

Checking arguments... 
Fetching annotations... 
Read 876021 records
Computing gene models... 
Summarizing counts... 
Preparing output 
Warning messages:
1: In easyRNASeq(organism = "danRer7", readLength = 50L, annotationMethod = "gtf",  :
  Your organism has no mapping defined to perform the validity check for the UCSC compliance of the chromosome name.
Defined organism\'s mapping can be listed using the 'knownOrganisms' function.
To benefit from the validity check, you can provide a 'chr.map' to your 'easyRNASeq' function call.
As you did not do so, 'validity.check' is turned off
2: In .Method(..., deparse.level = deparse.level) :
  number of columns of result is not a multiple of vector length (arg 1)
3: In easyRNASeq(organism = "danRer7", readLength = 50L, annotationMethod = "gtf",  :
  There are 2590 synthetic exons as determined from your annotation that overlap! This implies that some reads will be counted more than once! Is that really what you want?


rnaSeq.genes <- easyRNASeq(system.file(
	"extdata",
	package="RnaSeqTutorial"),
    organism="Dmelanogaster",
    readLength=30L,
    annotationMethod="rda",
    annotationFile=system.file(
	"data",
	"gAnnot.rda",
	package="RnaSeqTutorial"),
    count="genes",
    summarization="geneModels",
    nbCore=16,
    pattern="[A,C,T,G]{6}\\.bam$",
    outputFormat="RNAseq")


gInterval <- readGff3("/home/jeremiah/zv9/Danio_rerio.Zv9.70.gtf")

exon.range2 <- RangedData(
    IRanges(start=gInterval[,1],
	end=gInterval[,2]),
    space=gInterval$seq_name,
    strand=gInterval$strand,
    transcript=as.vector(
	getGffAttribute(gInterval,"Parent")),
    gene=as.vector(
	getGffAttribute(gInterval,"Name")),
    exon=as.vector(
	getGffAttribute(gInterval,"ID")),
    universe = "Zv9"
)

feature.size = width(exon.range2)
names(feature.size) = exon.range2$exon
feature.size <- feature.size[!duplicated(names(feature.size))]



library(pasilla)

datafile = system.file( "extdata/pasilla_gene_counts.tsv", package="pasilla" )

pasillaDesign = data.frame(
    row.names = colnames( pasillaCountTable ),
    condition = c( "untreated", "untreated", "untreated",
	"untreated", "treated", "treated", "treated" ),
    libType = c( "single-end", "single-end", "paired-end",
	"paired-end", "single-end", "paired-end", "paired-end" ) )

pairedSamples = pasillaDesign$libType == "paired-end"
countTable = pasillaCountTable[ , pairedSamples ]
condition = pasillaDesign$condition[ pairedSamples ]
condition.factor = factor( c( "untreated", "untreated", "treated", "treated" ) )
cds = newCountDataSet( countTable, condition )
cds.esf = estimateSizeFactors( cds )
cds.esd = estimateDispersions( cds.esf )
str( fitInfo(cds.esd) )
plotDispEsts( cds.esd )
 
