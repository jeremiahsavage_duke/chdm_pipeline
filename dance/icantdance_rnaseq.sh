####2013-02-20
cat endo_secreted.txt | cut -f1 --complement | most

cat endo_secreted.txt | cut -f1 --complement > endo_secreted.txt.new
cat whole_heart_secreted.txt | cut -f1 --complement > whole_heart_secreted.txt.new
cat fin_secreted.txt | cut -f1 --complement > fin_secreted.txt.new
cat *.new > secreted.txt

python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a cmlc2Trap_7dpa_vs_uninjured.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c endocardium_7dpa_v_Control_0.1.tsv -f 1

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a filterASet -b ~/Heart_RNA_Seq_Results/Secreted/secreted.txt -f 0 -s 0 -o testA.out -w secreted

python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i Fin_Regenerate_v_uninjured_0.1.tsv &
python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i Spinal_Regenerate_v_uninjured_0.1.tsv &
python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i endocardium_7dpa_v_Control_0.1.tsv &
python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i Whole_heart_7dpa_v_Control_0.1.tsv &
python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i epicardium_7dpa_v_Control.tsv &
python ~/chdm_pipeline/rnaseq/uniprot_grab.py -i cmlc2Trap_7dpa_vs_uninjured.tsv &

###2013-02-22
python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a cmlc2Trap_7dpa_vs_uninjured.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c endocardium_7dpa_v_Control_0.1.tsv -f 1
python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a cmlc2Trap_7dpa_vs_uninjured.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c endocardium_7dpa_v_Control_0.1.tsv -f -1
python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a cmlc2Trap_7dpa_vs_uninjured.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c endocardium_7dpa_v_Control_0.1.tsv -f 0

python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a Fin_Regenerate_v_uninjured_0.1.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c Spinal_Regenerate_v_uninjured_0.1.tsv -f 1
python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a Fin_Regenerate_v_uninjured_0.1.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c Spinal_Regenerate_v_uninjured_0.1.tsv -f -1
python ~/chdm_pipeline/rnaseq/venn_triple_singlefileout.py -a Fin_Regenerate_v_uninjured_0.1.tsv -b Whole_heart_7dpa_v_Control_0.1.tsv -c Spinal_Regenerate_v_uninjured_0.1.tsv -f 0

cat *_uniprot.tsv > uniprot.tsv
cat uniprot.tsv | cut -f1,21  > uniprot_zfin.tsv

cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_1.0.tsv
cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_-1.0.tsv
cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_0.0.tsv
Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_1.0.tsv
Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_-1.0.tsv
Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_0.0.tsv

python ~/chdm_pipeline/rnaseq/join_wheader.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_1.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_1.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_-1.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_0.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_0.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_1.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_1.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_-1.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_0.0.tsv -b uniprot_zfin.tsv -f 0 -s 0 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_0.0.tsv &

python ~/chdm_pipeline/rnaseq/join_wheader.py -a -b -f -s -o 


####
match_wheader_addword.py
go0001071_nucacid_txn_factor.txt
python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_1.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_-1.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_0.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_0.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_1.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_-1.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_0.0.tsv -b go0001071_nucacid_txn_factor.txt -f 4 -s 1 -w txn_factor_go0001071 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_0.0.tsv &



go0004872_receptor_activity.txt


python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_1.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_recpt_1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_-1.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_recpt_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_0.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_recpt_0.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_1.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_-1.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_-1.0.tsv &

python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_txn_0.0.tsv -b go0004872_receptor_activity.txt -f 4 -s 1 -w receptor_activity_go0004872 -o Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_0.0.tsv &


python ~/chdm_pipeline/rnaseq/match_wheader_addword.py -a -b -f -s -w -o


###digestible
cp Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_sec_0.0.tsv fin_wholeheart_spinal_nocutoff.tsv
cp Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_sec_-1.0.tsv fin_wholeheart_spinal_logN1.tsv
cp Fin_Regenerate_v_uninjured_0.1_Whole_heart_7dpa_v_Control_0.1_Spinal_Regenerate_v_uninjured_0.1_zfin_recpt_sec_1.0.tsv fin_wholeheart_spinal_logP1.tsv
cp cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_recpt_sec_0.0.tsv trap_wholeheart_endocardium_nocutoff.tsv
cp cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_recpt_sec_sec_-1.0.tsv trap_wholeheart_endocardium_logN1.tsv
cp cmlc2Trap_7dpa_vs_uninjured_Whole_heart_7dpa_v_Control_0.1_endocardium_7dpa_v_Control_0.1_zfin_txn_recpt_sec_1.0.tsv trap_wholeheart_endocardium_logP1.tsv
####### join
cat uniprot.tsv | cut -f21 --complement > uniprot_nozfin.tsv
python ~/chdm_pipeline/rnaseq/join_wheader.py -a trap_wholeheart_endocardium_logP1.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o trap_wholeheart_endocardium_logP1_annot.tsv &
python ~/chdm_pipeline/rnaseq/join_wheader.py -a trap_wholeheart_endocardium_logN1.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o trap_wholeheart_endocardium_logN1_annot.tsv &
python ~/chdm_pipeline/rnaseq/join_wheader.py -a trap_wholeheart_endocardium_nocutoff.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o trap_wholeheart_endocardium_nocutoff_annot.tsv &
python ~/chdm_pipeline/rnaseq/join_wheader.py -a fin_wholeheart_spinal_logP1.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o fin_wholeheart_spinal_logP1_annot.tsv &
python ~/chdm_pipeline/rnaseq/join_wheader.py -a fin_wholeheart_spinal_logN1.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o fin_wholeheart_spinal_logN1_annot.tsv &
python ~/chdm_pipeline/rnaseq/join_wheader.py -a fin_wholeheart_spinal_nocutoff.tsv -b uniprot_nozfin.tsv -f 0 -s 0 -o fin_wholeheart_spinal_nocutoff_annot.tsv &



###GO INFO
MF
nucleic acid binding transcription factor activity
GO:0001071

MF
protein binding transcription factor activity
GO:0000988

CC
extracellular region
GO:0005576
##
Note that this term is intended to annotate gene products that are not attached to the cell surface. For gene products from multicellular organisms which are secreted from a cell but retained within the organism (i.e. released into the interstitial fluid or blood), consider the cellular component term
extracellular space
GO:0005615

GO:0005634 (nucleus)

GO:0016020 (membrane)

GO:0005886 (plasma membrane

MF
GO:0004872 receptor activity

