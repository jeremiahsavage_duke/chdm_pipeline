#clean
rm qc*_*_*.fastq

#make
for f in qc*.fastq
do
    echo f = $f
    numlinesorig=`wc -l $f`
    echo numlinesorig = $numlinesorig
    numlinesorig=($numlinesorig)
    echo numlinesorig = $numlinesorig
    decamt=`expr ${numlinesorig} / 10`
    echo decamt = $decamt
    remainder=`expr $decamt % 4`
    decamt=`expr $decamt + 4 - $remainder`
    echo newdecamt =  $decamt
    for i in {1..10}
    do
	echo i = $i
	base=${f%.*}
	ext=${f##*.}
	newname=${base}_${i}.${ext}
	numlinesnew=`expr ${decamt} \* ${i}`
	echo numlinesnew = $numlinesnew
	if [ $i -eq "10" ]
	then
	    cp ${f} ${newname}
	fi
	echo head -n ${numlinesnew} $f > ${newname}
	head -n ${numlinesnew} $f > ${newname}
    done
done



##dragons
for f in qc_*B_*.fastq
do
    echo fileA = $f
    base=${f%.*}
    ext=${f##.}
    echo base = $base
    echo ext = $ext
    leading=`echo $base | tr "_" " " | cut -d " " -f 1`
    experiment=`echo $base | tr "_" " " | cut -d " " -f 2`
    tenth=`echo $base | tr "_" " " | cut -d " " -f 3`
    newdir=${experiment%?}_${tenth}
    echo mkdir ${newdir}
    mkdir ${newdir}
    mate=${leading}_${newdir}B_${tenth}.${ext}
    echo mv ${f} ${mate} ${newdir}
    mv ${f} ${mate} ${newdir}
done

zftemplate="#!/bin/bash
#$ -S /bin/bash -cwd
#$ -o qsub.out -j y
#$ -l mem_free=40G
NUMCPU=\`cat /proc/cpuinfo  | grep processor | wc -l\`
echo NUMCPU=\$NUMCPU"


## create qsub files
cd ~/ravi/Data/
dirs=`find *_* -type d`
for d in $dirs
do
    echo $d
    experiment=`echo $d | tr "_" " " | cut -d " " -f 1`
    tenth=`echo $d | tr "_" " " | cut -d " " -f 2`
    mate1=qc_${experiment}A_${tenth}.fastq
    mate2=qc_${experiment}B_${tenth}.fastq
    qsubstring=$(printf "${zftemplate}\nREAD1=${mate1}\nREAD2=${mate2}\ncd ~/ravi/Data/${d}\n/usr/bin/time -v tophat -p \$NUMCPU -r 175 --mate-std-dev 75 -G ~/zv9/cuffcmp.combined.gtf ~/zv9/Danio_rerio.Zv9.69.dna.toplevel ${mate1} ${mate2}")
    printf '%s' "qsubstring = $qsubstring"
    mkdir -p ~/ravi/Data/qsub/
    printf '%s' "$qsubstring" > qsub/q${d}.q
    echo
    echo
done

## create raw files
cd ~/ravi/Data/
dirs=`find *_* -type d`
for d in $dirs
do
    echo $d
    experiment=`echo $d | tr "_" " " | cut -d " " -f 1`
    tenth=`echo $d | tr "_" " " | cut -d " " -f 2`
    mate1=qc_${experiment}A_${tenth}.fastq
    mate2=qc_${experiment}B_${tenth}.fastq
    qsubstring=$(printf "${zftemplate}\nREAD1=${mate1}\nREAD2=${mate2}\ncd ~/ravi/Data/${d}\n/usr/bin/time -v tophat -p \$NUMCPU -r 175 --mate-std-dev 75 -G ~/zv9/cuffcmp.combined.gtf ~/zv9/Danio_rerio.Zv9.69.dna.toplevel ${mate1} ${mate2}")
    printf '%s' "qsubstring = $qsubstring"
    mkdir -p ~/ravi/Data/raw/
    printf '%s' "$qsubstring" > raw/${d}.sh
    echo
    echo
done



##submit qsub files


##serial raw calls
buildthechain=""
i=0
for f in *_*.sh
do
    if [ ${i} -eq "0" ]
    then
	buildthechain=$(printf "sh $f")
    else
	buildthechain=$(printf "$buildthechain && sh $f")
    fi
    i=$((i+1))
done
printf '%s' "$buildthechain" > chain.sh
