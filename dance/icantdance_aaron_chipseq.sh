##2013-05-24

samplelinereadsheadlines
JAG1_4_TGACCA_R1.fastq  1003289202508223025754628
JAG1_10_GATCTG_R1.fastq  25754628 643865725754628

JAG1_3_TTAGGC_R1.fastq   34251856 856296425754628
JAG1_9_CGATGT_R1.fastq  3029595687573989225754628

JAG1_1_CGTGAT_R1.fastq  1259456603148641525754628
JAG1_2_ACATCG_R1.fastq   617945361544863425754628


4a10
head -n 25754628 JAG1_4_TGACCA_R1.fastq > JAG1_4_TGACCA_R1_crop.fastq
mv JAG1_4_TGACCA_R1_crop.fastq JAG1_4_TGACCA_R1.fastq
cat JAG1_4_TGACCA_R1.fastq JAG1_10_GATCTG_R1.fastq > 4a10.fastq
mv 4a10.fastq JAG1_4_TGACCA_R1.fastq
rm JAG1_10_GATCTG_R1.fastq

3a9
head -n 25754628 JAG1_3_TTAGGC_R1.fastq > JAG1_3_TTAGGC_R1_crop.fastq
head -n 25754628 JAG1_9_CGATGT_R1.fastq > JAG1_9_CGATGT_R1_crop.fastq
cat JAG1_3_TTAGGC_R1_crop.fastq JAG1_9_CGATGT_R1_crop.fastq > 3a9.fastq
mv 3a9.fastq JAG1_3_TTAGGC_R1.fastq
rm *crop* JAG1_9_CGATGT_R1.fastq

1a2
head -n 25754628 JAG1_1_CGTGAT_R1.fastq > JAG1_1_CGTGAT_R1_crop.fastq
head -n 25754628 JAG1_2_ACATCG_R1.fastq > JAG1_2_ACATCG_R1_crop.fastq
cat JAG1_1_CGTGAT_R1_crop.fastq JAG1_1_CGTGAT_R1_crop.fastq > 1a2.fastq
mv 1a2.fastq JAG1_1_CGTGAT_R1.fastq
rm *crop* JAG1_2_ACATCG_R1.fastq



nohup macs14 -t JAG1_4_TGACCA_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 9v2 -g 1.412e+9 &

nohup macs14 -t JAG1_1_CGTGAT_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 9v2 -g 1.412e+9 &


##2013-06-05 non-normalized
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_1
cp JAG1_1_CGTGAT_R1_scythe.fastq ../unnormalized/1a2/
cp fastqfiletag.dict ../unnormalized/1a2/
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_2
cp JAG1_2_ACATCG_R1_scythe.fastq ../unnormalized/1a2/
cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/1a2
cat JAG1_1_CGTGAT_R1_scythe.fastq JAG1_2_ACATCG_R1_scythe.fastq > 1a2.fastq
rm JAG1_1_CGTGAT_R1_scythe.fastq JAG1_2_ACATCG_R1_scythe.fastq
mv 1a2.fastq JAG1_1_CGTGAT_R1_scythe.fastq
touch JAG1_1_CGTGAT_R1.fastq fastqfiletag.dict JAG1_1_CGTGAT_L007_R1_001.fastq JAG1_1_CGTGAT_L007_R1_001.fastq.gz
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_3
cp JAG1_3_TTAGGC_R1_scythe.fastq ../unnormalized/3a9/
cp fastqfiletag.dict ../unnormalized/3a9/
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_9
cp JAG1_9_CGATGT_R1_scythe.fastq ../unnormalized/3a9/
cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/3a9
cat JAG1_3_TTAGGC_R1_scythe.fastq JAG1_9_CGATGT_R1_scythe.fastq > 3a9.fastq
rm JAG1_3_TTAGGC_R1_scythe.fastq JAG1_9_CGATGT_R1_scythe.fastq
mv 3a9.fastq JAG1_3_TTAGGC_R1_scythe.fastq
touch JAG1_3_TTAGGC_R1.fastq fastqfiletag.dict JAG1_3_TTAGGC_L007_R1_001.fastq JAG1_3_TTAGGC_L007_R1_001.fastq.gz
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_4
cp JAG1_4_TGACCA_R1_scythe.fastq ../unnormalized/4a10/
cp fastqfiletag.dict ../unnormalized/4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_10
cp JAG1_10_GATCTG_R1_scythe.fastq ../unnormalized/4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/4a10
cat JAG1_4_TGACCA_R1_scythe.fastq  JAG1_10_GATCTG_R1_scythe.fastq > 4a10.fastq
rm JAG1_4_TGACCA_R1_scythe.fastq  JAG1_10_GATCTG_R1_scythe.fastq
mv 4a10.fastq JAG1_4_TGACCA_R1_scythe.fastq
touch JAG1_4_TGACCA_R1.fastq JAG1_4_TGACCA_L007_R1_001.fastq JAG1_4_TGACCA_L007_R1_001.fastq.gz
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized
mkdir 1a2v3a9
mkdir 4a10v3a9
mkdir 4a10v3a9_rev
mkdir 1a2v3a9_rev
mkdir 3a9v8
mkdir 3a9v8_rev

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_8
cp JAG1_8_ATCACG_R1_fixmateinformation.ba* ../unnormalized/3a9v8/
cp JAG1_8_ATCACG_R1_fixmateinformation.ba* ../unnormalized/3a9v8_rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/1a2
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../1a2v3a9/
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../1a2v3a9_rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/3a9
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../1a2v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../1a2v3a9_rev/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../4a10v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../4a10v3a9_rev/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../3a9v8/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../3a9v8_rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/4a10
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../4a10v3a9/
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../4a10v3a9_rev/


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/1a2v3a9/
nohup macs14 -t JAG1_1_CGTGAT_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 1a2v3a9 -g 1.412e+9 &


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/1a2v3a9_rev/
nohup macs14 -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_1_CGTGAT_R1_fixmateinformation.bam -Sw --call-subpeaks -n 1a2v3a9-rev -g 1.412e+9 &


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/3a9v8/
nohup macs14 -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_8_ATCACG_R1_fixmateinformation.bam -Sw --call-subpeaks -n 3a9v8 -g 1.412e+9 &


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/3a9v8_rev/
nohup macs14 -t JAG1_8_ATCACG_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 3a9v8-rev -g 1.412e+9 &


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/4a10v3a9/
nohup macs14 -t JAG1_4_TGACCA_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 4a10v3a9 -g 1.412e+9 &


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/4a10v3a9_rev/
nohup macs14 -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_4_TGACCA_R1_fixmateinformation.bam -Sw --call-subpeaks -n 4a10v3a9-rev -g 1.412e+9 &



### back to normalized
cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized
mkdir 8
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_8
cp JAG1_8_ATCACG_R1_scythe.fastq fastqfiletag.dict ../normalized/8/
cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/8
touch JAG1_8_ATCACG_R1.fastq JAG1_8_ATCACG_L007_R1_001.fastq JAG1_8_ATCACG_L007_R1_001.fastq.gz
head -n 5150924 JAG1_8_ATCACG_R1_scythe.fastq > 8norm.fastq
mv 8norm.fastq JAG1_8_ATCACG_R1_scythe.fastq
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &


cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized
mkdir 3a9v8
mkdir 3a9v8_rev

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/3a9
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../3a9v8/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../3a9v8_rev/
cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/8
cp JAG1_8_ATCACG_R1_fixmateinformation.ba* ../3a9v8/
cp JAG1_8_ATCACG_R1_fixmateinformation.ba* ../3a9v8_rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/3a9v8

nohup macs14 -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_8_ATCACG_R1_fixmateinformation.bam -Sw --call-subpeaks -n 3a9v8 -g 1.412e+9 &

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/3a9v8_rev/

nohup macs14 -t JAG1_8_ATCACG_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 3a9v8-rev -g 1.412e+9 &


###2013-06-06 BROAD PEAKS
cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/
mkdir broad-4a10v3a9
mkdir broad-4a10v3a9-rev
mkdir broad-1a2v3a9
mkdir broad-1a2v3a9-rev


cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/1a2
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../broad-1a2v3a9/
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../broad-1a2v3a9-rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/3a9
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-1a2v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-1a2v3a9-rev/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-4a10v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-4a10v3a9-rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/4a10
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../broad-4a10v3a9/
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../broad-4a10v3a9-rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/broad-4a10v3a9
nohup macs2 callpeak -t JAG1_4_TGACCA_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -B -n b4a10v3a9 -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/broad-4a10v3a9-rev
nohup macs2 callpeak -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_4_TGACCA_R1_fixmateinformation.bam -B -n b4a10v3a9-rev -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/broad-1a2v3a9
nohup macs2 callpeak -t JAG1_1_CGTGAT_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -B -n b1a2v3a9 -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/normalized/broad-1a2v3a9-rev
nohup macs2 callpeak -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_1_CGTGAT_R1_fixmateinformation.bam -B -n b1a2v3a9-rev -g 1.412e+9 --broad &


####
####

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/
mkdir broad-4a10v3a9
mkdir broad-4a10v3a9-rev
mkdir broad-1a2v3a9
mkdir broad-1a2v3a9-rev

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/1a2
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../broad-1a2v3a9/
cp JAG1_1_CGTGAT_R1_fixmateinformation.ba* ../broad-1a2v3a9-rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/3a9
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-1a2v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-1a2v3a9-rev/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-4a10v3a9/
cp JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../broad-4a10v3a9-rev/

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/4a10
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../broad-4a10v3a9/
cp JAG1_4_TGACCA_R1_fixmateinformation.ba* ../broad-4a10v3a9-rev/


cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/broad-4a10v3a9
nohup macs2 callpeak -t JAG1_4_TGACCA_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -B -n b4a10v3a9 -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/broad-4a10v3a9-rev
nohup macs2 callpeak -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_4_TGACCA_R1_fixmateinformation.bam -B -n b4a10v3a9-rev -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/broad-1a2v3a9
nohup macs2 callpeak -t JAG1_1_CGTGAT_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -B -n b1a2v3a9 -g 1.412e+9 --broad &

cd /home/jeremiah/Project_Poss10673_13042B1_new/unnormalized/broad-1a2v3a9-rev
nohup macs2 callpeak -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_1_CGTGAT_R1_fixmateinformation.bam -B -n b1a2v3a9-rev -g 1.412e+9 --broad &


##2013-06-21
cd /home/jeremiah/Project_Poss10673_13042B1_nwe
mkdir maxnorm
cd maxnorm
mkdir 4a10
mkdir 3a9


#4&10
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_4
wc -l JAG1_4_TGACCA_L007_R1_*.fastq
  64000000 JAG1_4_TGACCA_L007_R1_001.fastq
  36328920 JAG1_4_TGACCA_L007_R1_002.fastq
 100328920 total

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_10
wc -l JAG1_10_GATCTG_L007_R1_*.fastq
  25754628 JAG1_10_GATCTG_L007_R1_001.fastq

126083548 total 4&10 lines
 31520887 total 4&10 reads

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_4
cp JAG1_4_TGACCA_R1_scythe.fastq ../maxnorm/4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_10
cp JAG1_10_GATCTG_R1_scythe.fastq ../maxnorm/4a10/

cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/4a10
cat JAG1_10_GATCTG_R1_scythe.fastq JAG1_4_TGACCA_R1_scythe.fastq > 4a10.fastq
mv 4a10.fastq JAG1_4_TGACCA_R1_scythe.fastq
rm JAG1_10_GATCTG_R1_scythe.fastq
touch JAG1_4_TGACCA_L007_R1_001.fastq JAG1_4_TGACCA_L007_R1_002.fastq JAG1_4_TGACCA_L007_R1_002.fastq.gz JAG1_4_TGACCA_L007_R1_001.fastq.gz JAG1_4_TGACCA_R1.fastq JAG1_4_TGACCA_R1_scythediscarded.fastq aaronravi_adapters_TGACCA.fasta
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_4
cp fastqfiletag.dict ../maxnorm/4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/4a10
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &

#3&9
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_3
wc -l JAG1_3_TTAGGC_L007_R1_001.fastq
  34251856 JAG1_3_TTAGGC_L007_R1_001.fastq

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_9
wc -l JAG1_9_CGATGT_L007_R1_*.fastq
   64000000 JAG1_9_CGATGT_L007_R1_001.fastq
   64000000 JAG1_9_CGATGT_L007_R1_002.fastq
   64000000 JAG1_9_CGATGT_L007_R1_003.fastq
   64000000 JAG1_9_CGATGT_L007_R1_004.fastq
   46959568 JAG1_9_CGATGT_L007_R1_005.fastq
  302959568 total

337211424 total 3&9 lines
 84302856 total 3&9 reads

cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_3
cp JAG1_3_TTAGGC_R1_scythe.fastq ../maxnorm/3a9/
cd /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_9
cp JAG1_9_CGATGT_R1_scythe.fastq ../maxnorm/3a9/

cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/3a9
cat JAG1_3_TTAGGC_R1_scythe.fastq JAG1_9_CGATGT_R1_scythe.fastq > 3a9.fastq
head -n 126083548 3a9.fastq > JAG1_3_TTAGGC_R1_scythe.fastq
rm JAG1_9_CGATGT_R1_scythe.fastq
rm 3a9.fastq
touch JAG1_3_TTAGGC_L007_R1_001.fastq.gz JAG1_3_TTAGGC_L007_R1_001.fastq JAG1_3_TTAGGC_R1.fastq
cp /home/jeremiah/Project_Poss10673_13042B1_new/Sample_JAG1_3
cp fastqfiletag.dict ../maxnorm/3a9/
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/3a9
python ~/chdm_pipeline/chen/ravi/regchip.py
nohup python ~/chdm_pipeline/chen/ravi/regchip.py &


### wait for completion
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm
mkdir 3a9v4a10
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/3a9
cp -a JAG1_3_TTAGGC_R1_fixmateinformation.ba* ../3a9v4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/4a10
cp -a JAG1_4_TGACCA_R1_fixmateinformation.ba* ../3a9v4a10/
cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm/3a9v4a10
nohup macs14 -t JAG1_4_TGACCA_R1_fixmateinformation.bam -c JAG1_3_TTAGGC_R1_fixmateinformation.bam -Sw --call-subpeaks -n 4a10v3a9-maxnorm -g 1.412e+9 &

cd /home/jeremiah/Project_Poss10673_13042B1_new/maxnorm
mkdir 3a9v4a10-rev
cd cd 3a9v4a10
cp -a JAG1_*.ba* ../3a9v4a10-rev/
cd ../3a9v4a10-rev/
nohup macs14 -t JAG1_3_TTAGGC_R1_fixmateinformation.bam -c JAG1_4_TGACCA_R1_fixmateinformation.bam -Sw --call-subpeaks -n 4a10v3a9-maxnorm-rev -g 1.412e+9 &



