for f in {1..25} ; do echo $f ; time sed -i "s/^$f\([[:space:]]\)/chr$f\1/g" Danio_rerio.gvf ; done

for f in {1..25} ; do echo $f ; time sed -i "s/^##sequence-region\([[:space:]]\)$f\([[:space:]]\)/##sequence-region\1chr$f\2/g" Danio_rerio.gvf ; done
export PERL5LIB=~/bin/
perl ~/bin/gvf2vcf.pl Danio_rerio.Zv9.68.dna.toplevel.fa Danio_rerio.gvf > Danio_rerio.vcf 2>gvf2vcf.STDERR
time perl ~/bin/gvf2vcf.pl danRer7.fa Danio_rerio.gvf > Danio_rerio.vcf 2>gvf2vcf.STDERR

###################
samtools view -H CHC1_TAGCTT_R1_printreads.bam > CHC1.header
samtools view -H CHC2_GGCTAC_R1_printreads.bam > CHC2.header

time java -d64 -Xmx4g -jar ~/bin/picard176/ReplaceSamHeader.jar INPUT=CHC1_TAGCTT_R1_printreads.bam HEADER=CHC1.header OUTPUT=CHC1_TAGCTT_chr.bam

time java -d64 -Xmx4g -jar ~/bin/picard176/ReplaceSamHeader.jar INPUT=CHC2_GGCTAC_R1_printreads.bam HEADER=CHC2.header OUTPUT=CHC2_GGCTAC_chr.bam
for f in {1..25} ; do echo $f ; time sed -i 's/chr$f/$f/g' danRer7_repeatmask_chr$f.bed ; done

###################
#for f in {1..25} ; do echo $f ; samtools view -b CHC1_TAGCTT_R1_printreads.bam chr"$f" -o CHC1_chr"$f".bam ; done
for f in {5..25} ; do echo time samtools view -b CHC1_TAGCTT_R1_printreads.bam chr"$f" -o CHC1_chr"$f".bam ; done | parallel -j+0
#for f in {1..25} ; do echo $f ; time samtools index CHC1_chr"$f".bam ; done
for f in {1..25} ; do echo time samtools index CHC1_chr"$f".bam ; done | parallel -j+0

#for f in {1..25} ; do echo $f ; samtools view -b CHC2_GGCTAC_R1_printreads.bam chr"$f" -o CHC2_chr"$f".bam ; done
for f in {5..25} ; do echo time samtools view -b CHC2_GGCTAC_R1_printreads.bam chr"$f" -o CHC2_chr"$f".bam ; done | parallel -j+0
#for f in {1..25} ; do echo $f ; time samtools index CHC2_chr"$f".bam ; done
for f in {1..25} ; do echo time samtools index CHC2_chr"$f".bam ; done | parallel -j+0

mv CHC1_chr*.bam* ../../split_chr/

mv CHC2_chr*.bam* ../../split_chr/


for f in {1..25} ; do echo "time samtools mpileup -ugf /home/jeremiah/home2/danRer7/danRer7.fa *_chr"$f".bam | bcftools view -vcg - > chr"$f".vcf" ; done | nohup parallel -j+0 &

##control
nohup time samtools mpileup -ugf /home/jeremiah/home2/danRer7/danRer7.fa AB_chr8.bam T*_chr8.bam WK*_chr8.bam wdd_chr8.bam| bcftools view -vcg - > control_chr8.vcf > nohup_pile.out &
time grep -v INDEL control_chr8.vcf > control_chr8_noINDEL.vcf

time java -jar ~/bin/GenomeAnalysisTK.jar -T VariantFiltration -R /home/jeremiah/home2/danRer7/danRer7.fa --variant control_chr8_noINDEL.vcf --out control_chr8_noINDEL_filtered.vcf --mask /home/jeremiah/home2/harris/orig/repeat_mask/danRer7_repeatmask_chr8.bed --maskName RepeatMask --clusterWindowSize 10 --filterExpression 'QUAL < 30' --filterName "QualFilter" --filterExpression 'DP < 8' --filterName "LowDepth" --filterExpression 'DP > 60' --filterName "HighDepth" --filterExpression 'MQ < 40' --filterName "MapQual"

perl ~/home2/harris/orig/perl/classifySNPs.pl control_chr8_noINDEL_filtered.vcf NVO TU WK > chr8_NVO_EK_WK.cn
##/control

for f in {1..25} ; do echo "grep -v INDEL chr"$f".vcf > chr"$f"_noINDEL.vcf" ; done | parallel --max-procs=4


qfor f in {1..25} ; do echo "time java -jar ~/bin/GenomeAnalysisTK.jar -T VariantFiltration -R /home/jeremiah/home2/danRer7/danRer7.fa --variant chr"$f"_noINDEL.vcf --out chr"$f"_noINDEL_filtered.vcf --mask /home/jeremiah/home2/harris/orig/repeat_mask/danRer7_repeatmask_chr"$f".bed --maskName RepeatMask --clusterWindowSize 10 --filterExpression 'QUAL < 30' --filterName "QualFilter" --filterExpression 'DP < 8' --filterName "LowDepth" --filterExpression 'DP > 60' --filterName "HighDepth" --filterExpression 'MQ < 40' --filterName "MapQual"" ; done | nohup parallel --max-procs=3 &


for f in {1..25} ; do echo "perl ~/home2/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf CHC2_GGCTAC_R1 CHC1_TAGCTT_R1 WK > chr"$f"_mutant1_EK_WK.cn" ; done | nohup parallel -j+0 &


for f in {1..25} ; do echo "perl ~/home2/harris/orig/perl/convert_bp_to_cM.pl ~/tools/data/scriptsandtext/MGH_map/chr"$f"_cM chr"$f"_mutant1_EK_WK.cn > chr"$f"_mutant1_EK_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/home2/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant1_EK_WK_cM.cn 20 > chr"$f"_mutant1_EK_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant1_EK_WK_mappingscore_20cM chr2_mutant1_EK_WK_mappingscore_20cM chr3_mutant1_EK_WK_mappingscore_20cM chr4_mutant1_EK_WK_mappingscore_20cM chr5_mutant1_EK_WK_mappingscore_20cM chr6_mutant1_EK_WK_mappingscore_20cM chr7_mutant1_EK_WK_mappingscore_20cM chr8_mutant1_EK_WK_mappingscore_20cM chr9_mutant1_EK_WK_mappingscore_20cM chr10_mutant1_EK_WK_mappingscore_20cM chr11_mutant1_EK_WK_mappingscore_20cM chr12_mutant1_EK_WK_mappingscore_20cM chr13_mutant1_EK_WK_mappingscore_20cM chr14_mutant1_EK_WK_mappingscore_20cM chr15_mutant1_EK_WK_mappingscore_20cM chr16_mutant1_EK_WK_mappingscore_20cM chr17_mutant1_EK_WK_mappingscore_20cM chr18_mutant1_EK_WK_mappingscore_20cM chr19_mutant1_EK_WK_mappingscore_20cM chr20_mutant1_EK_WK_mappingscore_20cM chr21_mutant1_EK_WK_mappingscore_20cM chr22_mutant1_EK_WK_mappingscore_20cM chr23_mutant1_EK_WK_mappingscore_20cM chr24_mutant1_EK_WK_mappingscore_20cM chr25_mutant1_EK_WK_mappingscore_20cM > mutant1_EK_WK_mappingscore20cMall

########ANALYSIS CHR16
java -d64 -Xmx1G -jar ~/tools/IGV_2.1.21/igv.jar chr16_mutant1_EK_WK.cn

awk '($2 >= 42800000)&&($2 <= 57400000)' chr16_noINDEL_filtered.vcf_CHC2_GGCTAC_R1_homnovelSNPs > chr16_homnovelSNPs_linked
########ANALYSIS CHR7
java -d64 -Xmx1G -jar ~/tools/IGV_2.1.21/igv.jar chr7_mutant1_EK_WK.cn

awk '($2 >= 0)&&($2 <= 13000000)' chr7_noINDEL_filtered.vcf_CHC2_GGCTAC_R1_homnovelSNPs > chr7_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr7_homnovelSNPs_linked > chr7_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr7_homnovelSNPs_linked_annovar /home/jeremiah/home2/harris/orig/zebradb

perl `which annotate_variation.pl` chr7_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb
########ANALYSIS CHR11
java -d64 -Xmx1G -jar ~/tools/IGV_2.1.21/igv.jar chr11_mutant1_EK_WK.cn

awk '($2 >= 22500000)&&($2 <= 37200000)' chr11_noINDEL_filtered.vcf_CHC2_GGCTAC_R1_homnovelSNPs > chr11_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr11_homnovelSNPs_linked > chr11_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr11_homnovelSNPs_linked_annovar /home/jeremiah/home2/harris/orig/zebradb

perl `which annotate_variation.pl` chr11_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb
########ANNOVAR
mkdir danrerdb

annotate_variation.pl -downdb --buildver danRer7 gene danrerdb/

annotate_variation.pl --buildver danRer7 --downdb seq danrerdb/danRer7_seq

retrieve_seq_from_fasta.pl danrerdb/danRer7_refGene.txt -seqdir danrerdb/danRer7_seq -format refGene -outfile danrerdb/danRer7_refGeneMrna.fa

perl `which convert2annovar.pl` -format vcf4 chr16_homnovelSNPs_linked > chr16_homnovelSNPs_linked_annovar


perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr16_homnovelSNPs_linked_annovar /home/jeremiah/home2/harris/orig/zebradb

cp chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered_harris
cp chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered_inhouse

perl `which annotate_variation.pl` chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered_harris -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb

perl `which annotate_variation.pl` chr16_homnovelSNPs_linked_annovar.danRer7_generic_filtered_inhouse -buildver danRer7 /home/jeremiah/tools/annovar/annovar/danrerdb

########
for f in {1..25} ; do echo $f ; samtools view -h -o CHC1_chr"$f".sam CHC1_chr"$f".bam

########
for i in {1..25} ; do echo $i ; for f in {1..25} ; do echo $f ; time sed -i "s/SN:"$i"/SN:"chr$i"/g" CHC1_chr"$f".sam ; done ; done

for i in {1..25} ; do echo $i ; for f in {1..25} ; do echo $f ; time sed -i "s/SN:"$i"/SN:chr"$i"/g" CHC2_chr"$f".sam ; done ; done
########
for f in {1..25} ; do echo $f ; time samtools index CHC1_chr"$f".bam ; done &

for f in {1..25} ; do echo time samtools index CHC2_chr"$f".bam ; done | parallel -j+0 &
########
#########
mv WK* ~/home2/Project_Chen/split_chr/

for f in {1..25} ; do echo $f ; samtools mpileup -ugf /home/jeremiah/home2/Danio_rerio.Zv9.68.dna.toplevel/Danio_rerio.Zv9.68.dna.toplevel.fa *_chr"$f".bam | bcftools view -vcg - > chr"$f".vcf ; done


##########
grep -v INDEL chr1.vcf > chr1_noINDEL.vcf
grep -v INDEL chr2.vcf > chr2_noINDEL.vcf
grep -v INDEL chr3.vcf > chr3_noINDEL.vcf


for f in {1..25} ; do echo $f ; grep -v INDEL chr"$f".vcf > chr"$f"_noINDEL.vcf ; done


#####################

for f in {1..25} ; do echo $f ; time /home/jeremiah/tools/RepeatMasker/RepeatMasker -s -pa 16 "$f".fasta ; done

#####################

for f in {1..25} ; do echo $f ; time java -jar ~/bin/GenomeAnalysisTK.jar -T VariantFiltration -R /home/jeremiah/home2/Danio_rerio.Zv9.68.dna.toplevel/Danio_rerio.Zv9.68.dna.toplevel.fa --variant chr"$f"_noINDEL.vcf --out chr"$f"_noINDEL_filtered.vcf --mask /home/jeremiah/home2/harris/orig/repeat_mask/danRer7_repeatmask_chr"$f".bed --maskName RepeatMask --clusterWindowSize 10 --filterExpression "QUAL < 30" --filterName "QualFilter" --filterExpression "DP < 8" --filterName "LowDepth" --filterExpression "DP > 60" --filterName "HighDepth" --filterExpression "MQ < 40" --filterName "MapQual" ; done

######################

perl ~/home2/harris/orig/perl/classifySNPs.pl chr1_noINDEL_filtered.vcf CHC2_GGCTAC_R1 CHC1_TAGCTT_R1 WK > 1_mutant1_EK_WK.cn


#####R ME UP##########
roughmap <- read.table("mutant1_EK_WK_mappingscore20cMall")
colnames(roughmap) <- c("chromosome","bp","mapping_score","homo_het_snp","reference_mapping_alleles")
require(gtools)
roughmap <- within(roughmap,chromosome <- factor(chromosome,levels=mixedsort(unique(roughmap$chromosome))))

#p <- ggplot(roughmapB, aes(bp,mapping_score,facets=.~chromosome))
#p <- p + layer(geom="line")

combo_mapscr <- ggplot(roughmap, aes(bp,mapping_score,color=chromosome))
combo_mapscr <- combo_mapscr + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma) 
ggsave(filename="combo_mapscr.png",plot=combo_mapscr,dpi=200,width=16,height=10,units=c("in"))

combo_homhet <- ggplot(roughmap, aes(bp,homo_het_snp,color=chromosome))
combo_homhet <- combo_homhet + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma) 
ggsave(filename="combo_homet.png",plot=combo_homhet,dpi=200,width=16,height=10,units=c("in"))

combo_refmap <- ggplot(roughmap, aes(bp,reference_mapping_alleles,color=chromosome))
combo_refmap <- combo_refmap + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma) 
ggsave(filename="combo_refmap.png",plot=combo_refmap,dpi=200,width=16,height=10,units=c("in"))


homhet <- qplot(bp, homo_het_snp, data=roughmap, facets = .~chromosome, geom="line")
homhet <- homhet + theme(axis.text.x=element_text(angle=-90,hjust = 0))
homhet <- homhet + scale_x_continuous(labels = comma)
ggsave(filename="homhet.png",plot=homhet,dpi=200,width=16,height=10,units=c("in"))

refmap <- qplot(bp, reference_mapping_alleles, data=roughmap, facets = .~chromosome, geom="line")
refmap <- refmap + theme(axis.text.x=element_text(angle=-90,hjust = 0))
refmap <- refmap + scale_x_continuous(labels = comma)
ggsave(filename="refmap.png",plot=refmap,dpi=200,width=16,height=10,units=c("in"))

mapscr <- qplot(bp, mapping_score, data=roughmap, facets = .~chromosome, geom="line")
mapscr <- mapscr + theme(axis.text.x=element_text(angle=-90,hjust = 0))
mapscr <- mapscr + scale_x_continuous(labels = comma)
ggsave(filename="mapscr.png",plot=mapscr,dpi=200,width=16,height=10,units=c("in"))

##########2012-10-26
for f in {1..25} ; do echo "perl ~/home2/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant1_EK_WK_cM.cn 10 > chr"$f"_mutant1_EK_WK_mappingscore_10cM" ; done | parallel -j+0

cat chr1_mutant1_EK_WK_mappingscore_10cM chr2_mutant1_EK_WK_mappingscore_10cM chr3_mutant1_EK_WK_mappingscore_10cM chr4_mutant1_EK_WK_mappingscore_10cM chr5_mutant1_EK_WK_mappingscore_10cM chr6_mutant1_EK_WK_mappingscore_10cM chr7_mutant1_EK_WK_mappingscore_10cM chr8_mutant1_EK_WK_mappingscore_10cM chr9_mutant1_EK_WK_mappingscore_10cM chr10_mutant1_EK_WK_mappingscore_10cM chr11_mutant1_EK_WK_mappingscore_10cM chr12_mutant1_EK_WK_mappingscore_10cM chr13_mutant1_EK_WK_mappingscore_10cM chr14_mutant1_EK_WK_mappingscore_10cM chr15_mutant1_EK_WK_mappingscore_10cM chr16_mutant1_EK_WK_mappingscore_10cM chr17_mutant1_EK_WK_mappingscore_10cM chr18_mutant1_EK_WK_mappingscore_10cM chr19_mutant1_EK_WK_mappingscore_10cM chr10_mutant1_EK_WK_mappingscore_10cM chr21_mutant1_EK_WK_mappingscore_10cM chr22_mutant1_EK_WK_mappingscore_10cM chr23_mutant1_EK_WK_mappingscore_10cM chr24_mutant1_EK_WK_mappingscore_10cM chr25_mutant1_EK_WK_mappingscore_10cM > mutant1_EK_WK_mappingscore10cMall

install.packages("gtools")
install.packages("ggplot2")
roughmap <- read.table("mutant1_EK_WK_mappingscore10cMall")
colnames(roughmap) <- c("chromosome","bp","mapping_score","homo_het_snp","reference_mapping_alleles")
library(gtools)
library(ggplot2)
roughmap <- within(roughmap,chromosome <- factor(chromosome,levels=mixedsort(unique(roughmap$chromosome))))

#p <- ggplot(roughmapB, aes(bp,mapping_score,facets=.~chromosome))
#p <- p + layer(geom="line")

combo_mapscr <- ggplot(roughmap, aes(bp,mapping_score,color=chromosome))
combo_mapscr <- combo_mapscr + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_mapscr_10cM.png",plot=combo_mapscr,dpi=200,width=16,height=10,units=c("in"))

combo_homhet <- ggplot(roughmap, aes(bp,homo_het_snp,color=chromosome))
combo_homhet <- combo_homhet + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_homet_10cM.png",plot=combo_homhet,dpi=200,width=16,height=10,units=c("in"))

combo_refmap <- ggplot(roughmap, aes(bp,reference_mapping_alleles,color=chromosome))
combo_refmap <- combo_refmap + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_refmap_10cM.png",plot=combo_refmap,dpi=200,width=16,height=10,units=c("in"))


homhet <- qplot(bp, homo_het_snp, data=roughmap, facets = .~chromosome, geom="line")
homhet <- homhet + theme(axis.text.x=element_text(angle=-90,hjust = 0))
homhet <- homhet + scale_x_continuous(labels = comma)
ggsave(filename="homhet_10cM.png",plot=homhet,dpi=200,width=16,height=10,units=c("in"))

refmap <- qplot(bp, reference_mapping_alleles, data=roughmap, facets = .~chromosome, geom="line")
refmap <- refmap + theme(axis.text.x=element_text(angle=-90,hjust = 0))
refmap <- refmap + scale_x_continuous(labels = comma)
ggsave(filename="refmap_10cM.png",plot=refmap,dpi=200,width=16,height=10,units=c("in"))

mapscr <- qplot(bp, mapping_score, data=roughmap, facets = .~chromosome, geom="line")
mapscr <- mapscr + theme(axis.text.x=element_text(angle=-90,hjust = 0))
mapscr <- mapscr + scale_x_continuous(labels = comma)
ggsave(filename="mapscr_10cM.png",plot=mapscr,dpi=200,width=16,height=10,units=c("in"))

####2012-11-01
perl `which annotate_variation.pl`  -filter --vcfdbfile WT_SNP_set.vcf --buildver danRer7 --dbtype vcf chr11_homnovelSNPs_linked_annovar /home/jeremiah/home2/tools/

perl `which annotate_variation.pl`  -filter --vcfdbfile WT_SNP_set.vcf --buildver danRer7 --dbtype vcf chr16_homnovelSNPs_linked_annovar /home/jeremiah/home2/tools/

perl `which annotate_variation.pl`  -filter --vcfdbfile WT_SNP_set.vcf --buildver danRer7 --dbtype vcf chr7_homnovelSNPs_linked_annovar /home/jeremiah/home2/tools/

perl `which annotate_variation.pl` chr7_homnovelSNPs_linked_annovar.danRer7_vcf_filtered -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb

perl `which annotate_variation.pl` chr11_homnovelSNPs_linked_annovar.danRer7_vcf_filtered -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb

perl `which annotate_variation.pl` chr16_homnovelSNPs_linked_annovar.danRer7_vcf_filtered -buildver danRer7 /home/jeremiah/home2/harris/orig/zebradb


########2012-11-05
mkdir DIR4
mkdir MM3
mkdir WIR6

cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Dir4-Mut/
cp -a *printreads* ../DIR4/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Dir4-Wt/
cp -a *printreads* ../DIR4/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-MM3-Mut/
cp -a *printreads* ../MM3/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-MM3-Wt/
cp -a *printreads* ../MM3/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Wir6-Mut/
cp -a *printreads* ../WIR6/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Wir6-Wt/
cp -a *printreads* ../WIR6/


###!!!! SWITCH MUT->WT as +==MUT and -==WT
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/
for f in {1..25} ; do echo time samtools view -b Zf-Dir4-Mut_CTTGTA_R1_printreads.bam chr"$f" -o Dir4-Wt_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Dir4-Wt_chr"$f".bam ; done | parallel -j+0

cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/
for f in {1..25} ; do echo time samtools view -b Zf-Dir4-Wt_GCCAAT_R1_printreads.bam chr"$f" -o Dir4-Mut_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Dir4-Mut_chr"$f".bam ; done | parallel -j+0



cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/
for f in {1..25} ; do echo time samtools view -b Zf-Wir6-Mut_CTTGTA_R1_printreads.bam chr"$f" -o Wir6-Wt_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Wir6-Wt_chr"$f".bam ; done | parallel -j+0

cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/
for f in {1..25} ; do echo time samtools view -b Zf-Wir6-Wt_GCCAAT_R1_printreads.bam chr"$f" -o Wir6-Mut_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Wir6-Mut_chr"$f".bam ; done | parallel -j+0



cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/
for f in {1..25} ; do echo time samtools view -b Zf-MM3-Mut_CTTGTA_R1_printreads.bam chr"$f" -o MM3-Wt_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index MM3-Wt_chr"$f".bam ; done | parallel -j+0

cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/
for f in {1..25} ; do echo time samtools view -b Zf-MM3-Wt_GCCAAT_R1_printreads.bam chr"$f" -o MM3-Mut_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index MM3-Mut_chr"$f".bam ; done | parallel -j+0


####TODO: NEED TO INCORPORATE EK/CHC1
for f in {1..25} ; do echo "time samtools mpileup -ugf ~/danRer7/danRer7.fa *_chr$f.bam ~/harris/orig/bam/*_chr$f.bam ~/danRer7/EK/CHC1_chr$f.bam | bcftools view -vcg - > chr"$f".vcf" ; done | nohup parallel -j+0 &

###2012-11-07
for f in {1..25} ; do echo "grep -v INDEL chr"$f".vcf > chr"$f"_noINDEL.vcf" ; done | parallel --max-procs=4
for f in {1..25} ; do echo "grep INDEL chr"$f".vcf > chr"$f"_INDEL.vcf" ; done | parallel --max-procs=4

for f in {1..25} ; do echo "time java -jar ~/local/bin/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --variant chr"$f"_noINDEL.vcf --out chr"$f"_noINDEL_filtered.vcf --mask ~/harris/orig/repeat_mask/danRer7_repeatmask_chr"$f".bed --maskName RepeatMask --clusterWindowSize 10 --filterExpression 'QUAL < 30' --filterName "QualFilter" --filterExpression 'DP < 8' --filterName "LowDepth" --filterExpression 'DP > 60' --filterName "HighDepth" --filterExpression 'MQ < 40' --filterName "MapQual"" ; done | nohup parallel --max-procs=3 &


##
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-Dir4-Wt_GCCAAT_R1 CHC1_TAGCTT_R1 WK > chr"$f"_mutant_EK_WK.cn" ; done | nohup parallel -j+0 &


for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant_EK_WK.cn > chr"$f"_mutant_EK_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant_EK_WK_cM.cn 20 > chr"$f"_mutant_EK_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant_EK_WK_mappingscore_20cM chr2_mutant_EK_WK_mappingscore_20cM chr3_mutant_EK_WK_mappingscore_20cM chr4_mutant_EK_WK_mappingscore_20cM chr5_mutant_EK_WK_mappingscore_20cM chr6_mutant_EK_WK_mappingscore_20cM chr7_mutant_EK_WK_mappingscore_20cM chr8_mutant_EK_WK_mappingscore_20cM chr9_mutant_EK_WK_mappingscore_20cM chr10_mutant_EK_WK_mappingscore_20cM chr11_mutant_EK_WK_mappingscore_20cM chr12_mutant_EK_WK_mappingscore_20cM chr13_mutant_EK_WK_mappingscore_20cM chr14_mutant_EK_WK_mappingscore_20cM chr15_mutant_EK_WK_mappingscore_20cM chr16_mutant_EK_WK_mappingscore_20cM chr17_mutant_EK_WK_mappingscore_20cM chr18_mutant_EK_WK_mappingscore_20cM chr19_mutant_EK_WK_mappingscore_20cM chr20_mutant_EK_WK_mappingscore_20cM chr21_mutant_EK_WK_mappingscore_20cM chr22_mutant_EK_WK_mappingscore_20cM chr23_mutant_EK_WK_mappingscore_20cM chr24_mutant_EK_WK_mappingscore_20cM chr25_mutant_EK_WK_mappingscore_20cM > mutant_EK_WK_mappingscore20cMall

##
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-MM3-Wt_GCCAAT_R1 TLF WK > chr"$f"_mutant_TLF_WK.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant_TLF_WK.cn > chr"$f"_mutant_TLF_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant_TLF_WK_cM.cn 20 > chr"$f"_mutant_TLF_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant_TLF_WK_mappingscore_20cM chr2_mutant_TLF_WK_mappingscore_20cM chr3_mutant_TLF_WK_mappingscore_20cM chr4_mutant_TLF_WK_mappingscore_20cM chr5_mutant_TLF_WK_mappingscore_20cM chr6_mutant_TLF_WK_mappingscore_20cM chr7_mutant_TLF_WK_mappingscore_20cM chr8_mutant_TLF_WK_mappingscore_20cM chr9_mutant_TLF_WK_mappingscore_20cM chr10_mutant_TLF_WK_mappingscore_20cM chr11_mutant_TLF_WK_mappingscore_20cM chr12_mutant_TLF_WK_mappingscore_20cM chr13_mutant_TLF_WK_mappingscore_20cM chr14_mutant_TLF_WK_mappingscore_20cM chr15_mutant_TLF_WK_mappingscore_20cM chr16_mutant_TLF_WK_mappingscore_20cM chr17_mutant_TLF_WK_mappingscore_20cM chr18_mutant_TLF_WK_mappingscore_20cM chr19_mutant_TLF_WK_mappingscore_20cM chr20_mutant_TLF_WK_mappingscore_20cM chr21_mutant_TLF_WK_mappingscore_20cM chr22_mutant_TLF_WK_mappingscore_20cM chr23_mutant_TLF_WK_mappingscore_20cM chr24_mutant_TLF_WK_mappingscore_20cM chr25_mutant_TLF_WK_mappingscore_20cM > mutant_TLF_WK_mappingscore20cMall


##
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-Wir6-Wt_GCCAAT_R1 WK CHC1_TAGCTT_R1 > chr"$f"_mutant_WK_EK.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant_WK_EK.cn > chr"$f"_mutant_WK_EK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant_WK_EK_cM.cn 20 > chr"$f"_mutant_WK_EK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant_WK_EK_mappingscore_20cM chr2_mutant_WK_EK_mappingscore_20cM chr3_mutant_WK_EK_mappingscore_20cM chr4_mutant_WK_EK_mappingscore_20cM chr5_mutant_WK_EK_mappingscore_20cM chr6_mutant_WK_EK_mappingscore_20cM chr7_mutant_WK_EK_mappingscore_20cM chr8_mutant_WK_EK_mappingscore_20cM chr9_mutant_WK_EK_mappingscore_20cM chr10_mutant_WK_EK_mappingscore_20cM chr11_mutant_WK_EK_mappingscore_20cM chr12_mutant_WK_EK_mappingscore_20cM chr13_mutant_WK_EK_mappingscore_20cM chr14_mutant_WK_EK_mappingscore_20cM chr15_mutant_WK_EK_mappingscore_20cM chr16_mutant_WK_EK_mappingscore_20cM chr17_mutant_WK_EK_mappingscore_20cM chr18_mutant_WK_EK_mappingscore_20cM chr19_mutant_WK_EK_mappingscore_20cM chr20_mutant_WK_EK_mappingscore_20cM chr21_mutant_WK_EK_mappingscore_20cM chr22_mutant_WK_EK_mappingscore_20cM chr23_mutant_WK_EK_mappingscore_20cM chr24_mutant_WK_EK_mappingscore_20cM chr25_mutant_WK_EK_mappingscore_20cM > mutant_WK_EK_mappingscore20cMall


############2012-11-08
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4
$R
roughmap <- read.table("mutant_EK_WK_mappingscore20cMall")

cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3
$R
roughmap <- read.table("mutant_TLF_WK_mappingscore20cMall")

cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6
$R
roughmap <- read.table("mutant_WK_EK_mappingscore20cMall")


#COMMON
colnames(roughmap) <- c("chromosome","bp","mapping_score","homo_het_snp","reference_mapping_alleles")
library(gtools)
library(ggplot2)
library(scales)
roughmap <- within(roughmap,chromosome <- factor(chromosome,levels=mixedsort(unique(roughmap$chromosome))))

#p <- ggplot(roughmapB, aes(bp,mapping_score,facets=.~chromosome))
#p <- p + layer(geom="line")

combo_mapscr <- ggplot(roughmap, aes(bp,mapping_score,color=chromosome))
combo_mapscr <- combo_mapscr + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_mapscr_20cM.png",plot=combo_mapscr,dpi=200,width=16,height=10,units=c("in"))

combo_homhet <- ggplot(roughmap, aes(bp,homo_het_snp,color=chromosome))
combo_homhet <- combo_homhet + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_homet_20cM.png",plot=combo_homhet,dpi=200,width=16,height=10,units=c("in"))

combo_refmap <- ggplot(roughmap, aes(bp,reference_mapping_alleles,color=chromosome))
combo_refmap <- combo_refmap + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo_refmap_20cM.png",plot=combo_refmap,dpi=200,width=16,height=10,units=c("in"))


homhet <- qplot(bp, homo_het_snp, data=roughmap, facets = .~chromosome, geom="line")
homhet <- homhet + theme(axis.text.x=element_text(angle=-90,hjust = 0))
homhet <- homhet + scale_x_continuous(labels = comma)
ggsave(filename="homhet_20cM.png",plot=homhet,dpi=200,width=16,height=10,units=c("in"))

refmap <- qplot(bp, reference_mapping_alleles, data=roughmap, facets = .~chromosome, geom="line")
refmap <- refmap + theme(axis.text.x=element_text(angle=-90,hjust = 0))
refmap <- refmap + scale_x_continuous(labels = comma)
ggsave(filename="refmap_20cM.png",plot=refmap,dpi=200,width=16,height=10,units=c("in"))

mapscr <- qplot(bp, mapping_score, data=roughmap, facets = .~chromosome, geom="line")
mapscr <- mapscr + theme(axis.text.x=element_text(angle=-90,hjust = 0))
mapscr <- mapscr + scale_x_continuous(labels = comma)
ggsave(filename="mapscr_20cM.png",plot=mapscr,dpi=200,width=16,height=10,units=c("in"))



###SWITCH_MUT_WT
##SWITCH1
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-Dir4-Mut_CTTGTA_R1 CHC1_TAGCTT_R1 WK > chr"$f"_mutant2_EK_WK.cn" ; done | nohup parallel -j+0 &


for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant2_EK_WK.cn > chr"$f"_mutant2_EK_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant2_EK_WK_cM.cn 20 > chr"$f"_mutant2_EK_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant2_EK_WK_mappingscore_20cM chr2_mutant2_EK_WK_mappingscore_20cM chr3_mutant2_EK_WK_mappingscore_20cM chr4_mutant2_EK_WK_mappingscore_20cM chr5_mutant2_EK_WK_mappingscore_20cM chr6_mutant2_EK_WK_mappingscore_20cM chr7_mutant2_EK_WK_mappingscore_20cM chr8_mutant2_EK_WK_mappingscore_20cM chr9_mutant2_EK_WK_mappingscore_20cM chr10_mutant2_EK_WK_mappingscore_20cM chr11_mutant2_EK_WK_mappingscore_20cM chr12_mutant2_EK_WK_mappingscore_20cM chr13_mutant2_EK_WK_mappingscore_20cM chr14_mutant2_EK_WK_mappingscore_20cM chr15_mutant2_EK_WK_mappingscore_20cM chr16_mutant2_EK_WK_mappingscore_20cM chr17_mutant2_EK_WK_mappingscore_20cM chr18_mutant2_EK_WK_mappingscore_20cM chr19_mutant2_EK_WK_mappingscore_20cM chr20_mutant2_EK_WK_mappingscore_20cM chr21_mutant2_EK_WK_mappingscore_20cM chr22_mutant2_EK_WK_mappingscore_20cM chr23_mutant2_EK_WK_mappingscore_20cM chr24_mutant2_EK_WK_mappingscore_20cM chr25_mutant2_EK_WK_mappingscore_20cM > mutant2_EK_WK_mappingscore20cMall

$R
library(gtools)
library(ggplot2)
library(scales)
roughmap <- read.table("mutant2_EK_WK_mappingscore20cMall")
colnames(roughmap) <- c("chromosome","bp","mapping_score","homo_het_snp","reference_mapping_alleles")
roughmap <- within(roughmap,chromosome <- factor(chromosome,levels=mixedsort(unique(roughmap$chromosome))))

combo_mapscr <- ggplot(roughmap, aes(bp,mapping_score,color=chromosome))
combo_mapscr <- combo_mapscr + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo2_mapscr_20cM.png",plot=combo_mapscr,dpi=200,width=16,height=10,units=c("in"))

combo_homhet <- ggplot(roughmap, aes(bp,homo_het_snp,color=chromosome))
combo_homhet <- combo_homhet + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo2_homet_20cM.png",plot=combo_homhet,dpi=200,width=16,height=10,units=c("in"))

combo_refmap <- ggplot(roughmap, aes(bp,reference_mapping_alleles,color=chromosome))
combo_refmap <- combo_refmap + layer(geom="line") + theme(axis.text.x=element_text(angle=-90,hjust = 0)) + scale_x_continuous(labels = comma)
ggsave(filename="combo2_refmap_20cM.png",plot=combo_refmap,dpi=200,width=16,height=10,units=c("in"))


homhet <- qplot(bp, homo_het_snp, data=roughmap, facets = .~chromosome, geom="line")
homhet <- homhet + theme(axis.text.x=element_text(angle=-90,hjust = 0))
homhet <- homhet + scale_x_continuous(labels = comma)
ggsave(filename="homhet2_20cM.png",plot=homhet,dpi=200,width=16,height=10,units=c("in"))

refmap <- qplot(bp, reference_mapping_alleles, data=roughmap, facets = .~chromosome, geom="line")
refmap <- refmap + theme(axis.text.x=element_text(angle=-90,hjust = 0))
refmap <- refmap + scale_x_continuous(labels = comma)
ggsave(filename="refmap2_20cM.png",plot=refmap,dpi=200,width=16,height=10,units=c("in"))

mapscr <- qplot(bp, mapping_score, data=roughmap, facets = .~chromosome, geom="line")
mapscr <- mapscr + theme(axis.text.x=element_text(angle=-90,hjust = 0))
mapscr <- mapscr + scale_x_continuous(labels = comma)
ggsave(filename="mapscr2_20cM.png",plot=mapscr,dpi=200,width=16,height=10,units=c("in"))


##SWITCH2
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-MM3-Mut_GCCAAT_R1 TLF WK > chr"$f"_mutant2_TLF_WK.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant2_TLF_WK.cn > chr"$f"_mutant2_TLF_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant2_TLF_WK_cM.cn 20 > chr"$f"_mutant2_TLF_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant2_TLF_WK_mappingscore_20cM chr2_mutant2_TLF_WK_mappingscore_20cM chr3_mutant2_TLF_WK_mappingscore_20cM chr4_mutant2_TLF_WK_mappingscore_20cM chr5_mutant2_TLF_WK_mappingscore_20cM chr6_mutant2_TLF_WK_mappingscore_20cM chr7_mutant2_TLF_WK_mappingscore_20cM chr8_mutant2_TLF_WK_mappingscore_20cM chr9_mutant2_TLF_WK_mappingscore_20cM chr10_mutant2_TLF_WK_mappingscore_20cM chr11_mutant2_TLF_WK_mappingscore_20cM chr12_mutant2_TLF_WK_mappingscore_20cM chr13_mutant2_TLF_WK_mappingscore_20cM chr14_mutant2_TLF_WK_mappingscore_20cM chr15_mutant2_TLF_WK_mappingscore_20cM chr16_mutant2_TLF_WK_mappingscore_20cM chr17_mutant2_TLF_WK_mappingscore_20cM chr18_mutant2_TLF_WK_mappingscore_20cM chr19_mutant2_TLF_WK_mappingscore_20cM chr20_mutant2_TLF_WK_mappingscore_20cM chr21_mutant2_TLF_WK_mappingscore_20cM chr22_mutant2_TLF_WK_mappingscore_20cM chr23_mutant2_TLF_WK_mappingscore_20cM chr24_mutant2_TLF_WK_mappingscore_20cM chr25_mutant2_TLF_WK_mappingscore_20cM > mutant2_TLF_WK_mappingscore20cMall


##SWITCH3
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-Wir6-Mut_GCCAAT_R1 WK CHC1_TAGCTT_R1 > chr"$f"_mutant2_WK_EK.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant2_WK_EK.cn > chr"$f"_mutant2_WK_EK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant2_WK_EK_cM.cn 20 > chr"$f"_mutant2_WK_EK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant2_WK_EK_mappingscore_20cM chr2_mutant2_WK_EK_mappingscore_20cM chr3_mutant2_WK_EK_mappingscore_20cM chr4_mutant2_WK_EK_mappingscore_20cM chr5_mutant2_WK_EK_mappingscore_20cM chr6_mutant2_WK_EK_mappingscore_20cM chr7_mutant2_WK_EK_mappingscore_20cM chr8_mutant2_WK_EK_mappingscore_20cM chr9_mutant2_WK_EK_mappingscore_20cM chr10_mutant2_WK_EK_mappingscore_20cM chr11_mutant2_WK_EK_mappingscore_20cM chr12_mutant2_WK_EK_mappingscore_20cM chr13_mutant2_WK_EK_mappingscore_20cM chr14_mutant2_WK_EK_mappingscore_20cM chr15_mutant2_WK_EK_mappingscore_20cM chr16_mutant2_WK_EK_mappingscore_20cM chr17_mutant2_WK_EK_mappingscore_20cM chr18_mutant2_WK_EK_mappingscore_20cM chr19_mutant2_WK_EK_mappingscore_20cM chr20_mutant2_WK_EK_mappingscore_20cM chr21_mutant2_WK_EK_mappingscore_20cM chr22_mutant2_WK_EK_mappingscore_20cM chr23_mutant2_WK_EK_mappingscore_20cM chr24_mutant2_WK_EK_mappingscore_20cM chr25_mutant2_WK_EK_mappingscore_20cM > mutant2_WK_EK_mappingscore20cMall

java -d64 -Xmx1G -jar ~/tools/IGV_2.1.21/igv.jar chr16_mutant1_EK_WK.cn

awk '($2 >= 42800000)&&($2 <= 57400000)' chr16_noINDEL_filtered.vcf_CHC2_GGCTAC_R1_homnovelSNPs > chr16_homnovelSNPs_linked
########ANALYSIS WIR6_CHR17
java -d64 -Xmx1G -jar ~/tools/IGV_2.1.25/igv.jar chr17_mutant_WK_EK.cn

#small SNP
awk '($2 >= 2250000)&&($2 <= 4200000)' chr17_noINDEL_filtered.vcf_Zf-Wir6-Wt_GCCAAT_R1_homnovelSNPs > chr17_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr17_homnovelSNPs_linked > chr17_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr17_homnovelSNPs_linked_annovar /home/jeremiah/harris/orig/zebradb

perl `which annotate_variation.pl` chr17_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/harris/orig/zebradb

#big SNP
awk '($2 >= 2250000)&&($2 <= 10000000)' chr17_noINDEL_filtered.vcf_Zf-Wir6-Wt_GCCAAT_R1_homnovelSNPs > chr17_homnovelSNPs_linked_big

perl `which convert2annovar.pl` -format vcf4 chr17_homnovelSNPs_linked_big > chr17_homnovelSNPs_linked_big_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr17_homnovelSNPs_linked_big_annovar /home/jeremiah/harris/orig/zebradb

perl `which annotate_variation.pl` chr17_homnovelSNPs_linked_big_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/harris/orig/zebradb

#small INDEL
awk '($2 >= 2250000)&&($2 <= 4200000)' chr17_INDEL.vcf > chr17_INDEL.vcf_linked

perl ~/harris/orig/perl/novelINDELS.pl chr17_INDEL.vcf_linked 1 > mutant_chr17_INDEL.vcf_linked
perl `which convert2annovar.pl` -format vcf4 mutant_chr17_INDEL.vcf_linked > mutant_chr17_INDEL.vcf_linked_annovar
perl `which annotate_variation.pl` mutant_chr17_INDEL.vcf_linked_annovar -buildver danRer7 /home/jeremiah/harris/orig/zebradb

#big INDEL
awk '($2 >= 2250000)&&($2 <= 10000000)' chr17_INDEL.vcf > chr17_INDEL.vcf_linked_big
perl ~/harris/orig/perl/novelINDELS.pl chr17_INDEL.vcf_linked_big 1 > mutant_chr17_INDEL.vcf_linked_big
perl `which convert2annovar.pl` -format vcf4 mutant_chr17_INDEL.vcf_linked_big > mutant_chr17_INDEL.vcf_linked_big_annovar
perl `which annotate_variation.pl` mutant_chr17_INDEL.vcf_linked_big_annovar -buildver danRer7 /home/jeremiah/harris/orig/zebradb


##########ANALYSIS DIR4_CHR24
#SNP
awk '($2 >= 1500000)&&($2 <= 8750000)' chr24_noINDEL_filtered.vcf_Zf-Dir4-Wt_GCCAAT_R1_homnovelSNPs > chr24_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr24_homnovelSNPs_linked > chr24_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr24_homnovelSNPs_linked_annovar /home/jeremiah/harris/orig/zebradb

perl `which annotate_variation.pl` chr24_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/harris/orig/zebradb

##########ANALYSIS DIR4_CHR12
#SNP
awk '($2 >= 5000000)&&($2 <= 30000000)' chr12_noINDEL_filtered.vcf_Zf-Dir4-Wt_GCCAAT_R1_homnovelSNPs > chr12_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr12_homnovelSNPs_linked > chr12_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr12_homnovelSNPs_linked_annovar /home/jeremiah/harris/orig/zebradb

perl `which annotate_variation.pl` chr12_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/harris/orig/zebradb



####MM3 is really ref in AB || TU and (crossed briefly TLF) mapped in WIK
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3
for f in {1..25} ; do echo "perl ~/harris/orig/perl/classifySNPs.pl chr"$f"_noINDEL_filtered.vcf Zf-MM3-Mut_GCCAAT_R1 AB WK > chr"$f"_mutant3_AB_WK.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/convert_bp_to_cM.pl ~/harris/orig/MGH_map/chr"$f"_cM chr"$f"_mutant3_AB_WK.cn > chr"$f"_mutant3_AB_WK_cM.cn" ; done | nohup parallel -j+0 &

for f in {1..25} ; do echo "perl ~/harris/orig/perl/mapping_score_updateRatio.pl chr"$f"_mutant3_TLF_WK_cM.cn 20 > chr"$f"_mutant3_AB_WK_mappingscore_20cM" ; done | parallel -j+0

cat chr1_mutant3_AB_WK_mappingscore_20cM chr2_mutant3_AB_WK_mappingscore_20cM chr3_mutant3_AB_WK_mappingscore_20cM chr4_mutant3_AB_WK_mappingscore_20cM chr5_mutant3_AB_WK_mappingscore_20cM chr6_mutant3_AB_WK_mappingscore_20cM chr7_mutant3_AB_WK_mappingscore_20cM chr8_mutant3_AB_WK_mappingscore_20cM chr9_mutant3_AB_WK_mappingscore_20cM chr10_mutant3_AB_WK_mappingscore_20cM chr11_mutant3_AB_WK_mappingscore_20cM chr12_mutant3_AB_WK_mappingscore_20cM chr13_mutant3_AB_WK_mappingscore_20cM chr14_mutant3_AB_WK_mappingscore_20cM chr15_mutant3_AB_WK_mappingscore_20cM chr16_mutant3_AB_WK_mappingscore_20cM chr17_mutant3_AB_WK_mappingscore_20cM chr18_mutant3_AB_WK_mappingscore_20cM chr19_mutant3_AB_WK_mappingscore_20cM chr20_mutant3_AB_WK_mappingscore_20cM chr21_mutant3_AB_WK_mappingscore_20cM chr22_mutant3_AB_WK_mappingscore_20cM chr23_mutant3_AB_WK_mappingscore_20cM chr24_mutant3_AB_WK_mappingscore_20cM chr25_mutant3_AB_WK_mappingscore_20cM > mutant3_AB_WK_mappingscore20cMall


###2012-11-09
#add AB
cd /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4
for f in {1..25} ; do echo "time samtools mpileup -ugf ~/danRer7/danRer7.fa *_chr$f.bam ~/harris/orig/bam/*_chr$f.bam ~/danRer7/EK/CHC1_chr$f.bam | bcftools view -vcg - > chr"$f".vcf" ; done | nohup parallel -j+0 &


#project Stuby
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Stuby-Wt/
cp -a *printreads* ../STBY/
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/Sample_Zf-Stuby-Mut
cp -a *printreads* ../STBY/

cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/
for f in {1..25} ; do echo time samtools view -b Zf-Stuby-Wt_GCCAAT_R1_printreads.bam chr"$f" -o Stby-Mut_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools view -b Zf-Stuby-Mut_CTTGTA_R1_printreads.bam chr"$f" -o Stby-Mut_chr"$f".bam ; done | parallel -j+0


for f in {1..25} ; do echo time samtools index Stby-Wt_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Stby-Mut_chr"$f".bam ; done | parallel -j+0

for f in {1..25} ; do echo "time samtools mpileup -ugf ~/danRer7/danRer7.fa *_chr$f.bam ~/harris/orig/bam/*_chr$f.bam ~/danRer7/EK/CHC1_chr$f.bam | bcftools view -vcg - > chr"$f".vcf" ; done | nohup parallel -j+0 &


###2012-11-13
##########ANALYSIS DIR4_CHR2
#SNP
awk '($2 >= 3000000)&&($2 <= 6000000)' chr2_noINDEL_filtered.vcf_Zf-Dir4-Wt_GCCAAT_R1_homnovelSNPs > chr2_homnovelSNPs_linked

perl `which convert2annovar.pl` -format vcf4 chr2_homnovelSNPs_linked > chr2_homnovelSNPs_linked_annovar

perl `which annotate_variation.pl`  -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr2_homnovelSNPs_linked_annovar /home/jeremiah/harris/orig/zebradb

perl `which annotate_variation.pl` chr2_homnovelSNPs_linked_annovar.danRer7_generic_filtered -buildver danRer7 /home/jeremiah/harris/orig/zebradb




###2012-12-20
#make STBY vcf files
cd ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/
for f in {1..25} ; do echo time samtools view -b Zf-Stuby-Wt_GCCAAT_R1_printreads.bam chr"$f" -o Stby-Mut_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools view -b Zf-Stuby-Mut_CTTGTA_R1_printreads.bam chr"$f" -o Stby-Wt_chr"$f".bam ; done | parallel -j+0


for f in {1..25} ; do echo time samtools index Stby-Wt_chr"$f".bam ; done | parallel -j+0
for f in {1..25} ; do echo time samtools index Stby-Mut_chr"$f".bam ; done | parallel -j+0

for f in {1..25} ; do echo "time samtools mpileup -ugf ~/danRer7/danRer7.fa *_chr$f.bam ~/harris/orig/bam/*_chr$f.bam ~/danRer7/EK/CHC1_chr$f.bam | bcftools view -vcg - > chr"$f".vcf" ; done q| nohup parallel -j+0 &

for f in {1..25} ; do echo "time samtools mpileup -ugf ~/danRer7/danRer7.fa *_chr$f.bam ~/harris/orig/bam/*_chr$f.bam ~/danRer7/EK/CHC1_chr$f.bam | bcftools view -vcg - > chr"$f".vcf" ; done | nohup parallel -j+0 &


#FIRST DIR4-Mut

#filter against harris
java -d64 -Xmx4G -jar GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/harris/orig/vcf/chr${i}_parental_PASS.vcf
 --variant input.vcf -o output1.vcf --maskName chr${i}_parPASS


#filter against WIR6
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1-wir6.vcf --maskName wir6

#filter against MM3
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1-wir6.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1-wir6-mm3.vcf --maskName mm3
q
#filter against STBY


#filter against CHC1


#filter against CHC2


#filter against SNPUNIVERSE


####2013-01-09
vtools select variant "pos >= 1505 and pos <= 1961" --samples "sample_name like 'AB%'" --output variant_id bin chr pos ref alt  --limit 20

vtools select variant "pos >= 1505 and pos <= 1961" --samples "sample_name like 'AB%'"  --count

vtools select variant "pos >= 1505 and pos <= 1961" --samples "sample_name = 'TLF'" -t testTLF

vtools execute 'select * from testTLF'


vtools update variant --from_file chr1.vcf --geno_info GT

vtools select variant "pos >= 38000000 and pos <= 57000000" --samples "sample_name = 'Zf-Dir4-Wt_GCCAAT_R1'" -t Dir4WtChr1

vtools select variant "pos >= 38000000 and pos <= 57000000" --samples "sample_name = 'TLF'" -t TlfWtChr1

vtools update variant --from_stat 'total=#(GT)' 'num=#(alt)' 'hom=#(hom)' 'het=#(het)' 'other=#(other)'

vtools output variant chr pos ref alt total num hom het other -l5

vtools select variant "pos >= 38000000 and pos <= 57000000" --samples "sample_name = 'TLF'" -t TlfWtChr

vtools select variant "pos >= 38000000 and pos <= 57000000 and hom=1" --samples "sample_name = 'TLF'" -t TlfWtChr
=======
###2013-01-10
#DIR4 18000000 to 37000000 492016 (- 97472) to 886559 of 1511057

head -n 886559 chr1.vcf | tail -n 394544 | most
head -n886559 chr1.vcf | tail -n 394544 > chr1_region.vcf
wc -l chr1_region.vcf
  394544

#filter against WIR6
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_wir6.vcf --maskName wir6

#chr1_region_wir6.vcf 493152 (- 98609) to 887695

head -n 887695 chr1_region_wir6.vcf | tail -n 394544 | most
head -n 887695 chr1_region_wir6.vcf | tail -n 394544 > chr1_region_wir6_crop.vcf
wc -l chr1_region_wir6_crop.vcf
  394544

ack -i wir6 chr1_region_wir6_crop.vcf | wc -l
  382379

  394544-382379=12165

ack -i pass chr1_region_wir6_crop.vcf > chr1_region_wir6_crop_pass.vcf

head -n 1163 chr1_region_wir6.vcf > chr1_region_wir6_crop_pass.vcf.temp

cat chr1_region_wir6_crop_pass.vcf >> chr1_region_wir6_crop_pass.vcf.temp

mv chr1_region_wir6_crop_pass.vcf.temp chr1_region_wir6_crop_pass.vcf

###against mm3
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_wir6_crop_pass.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_wir6_mm3.vcf --maskName mm3

ack -i mm3 chr1_region_wir6_mm3.vcf | wc -l
  2443

ack -i pass chr1_region_wir6_mm3.vcf | wc -l
  9724

cp chr1_region_wir6_mm3.vcf chr1_region_subwir6_submm3.vcf 
sed -i '/mm3/d' chr1_region_subwir6_submm3.vcf 

###against stby
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_stby.vcf --maskName stby

ack stby chr1_region_subwir6_submm3_stby.vcf | wc -l
  1010


ack -i pass chr1_region_subwir6_submm3_stby.vcf | wc -l
  8715

cp chr1_region_subwir6_submm3_stby.vcf chr1_region_subwir6_submm3_substby.vcf 
sed -i '/stby/d'  chr1_region_subwir6_submm3_substby.vcf


###against CHC
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_chc12.vcf --maskName chc12

ack chc12 chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l
  4902


ack -i pass chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l
  3814

cp chr1_region_subwir6_submm3_substby_chc12.vcf chr1_region_subwir6_submm3_substby_subchc12.vcf 
sed -i '/chc12/d'  chr1_region_subwir6_submm3_substby_subchc12.vcf


###against harris
/home/jeremiah/harris/orig/vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/harris/orig/vcf/chr1_parental_PASS.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_har.vcf --maskName harris

ack harris chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l



























=======
###2013-01-11
#DIR4 38556239 to 57006567. 915160 (- 97472 - 301836) to 1431011 of 1511057

head -n 1431011 chr1.vcf | tail -n 515852 | most

head -n 1163 chr1.vcf > chr1_region.vcf
head -n 1431011 chr1.vcf | tail -n 515852 >> chr1_region.vcf
wc -l chr1_region.vcf
  517015 - 1163 = 515852

#filter against WIR6
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_wir6.vcf --maskName wir6


ack wir6 chr1_region_wir6.vcf | wc -l
  498277
ack PASS chr1_region_wir6.vcf | wc -l
  18713

cp chr1_region_wir6.vcf chr1_region_subwir6.vcf
sed -i '/wir6/d' chr1_region_subwir6.vcf

###against mm3
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_mm3.vcf --maskName mm3

ack  mm3 chr1_region_subwir6_mm3.vcf | wc -l
  3157

ack PASS chr1_region_subwir6_mm3.vcf | wc -l
  15558

cp chr1_region_subwir6_mm3.vcf chr1_region_subwir6_submm3.vcf 
sed -i '/mm3/d' chr1_region_subwir6_submm3.vcf 

###against stby
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_stby.vcf --maskName stby

ack stby chr1_region_subwir6_submm3_stby.vcf | wc -l
  1434


ack PASS chr1_region_subwir6_submm3_stby.vcf | wc -l
  14126

cp chr1_region_subwir6_submm3_stby.vcf chr1_region_subwir6_submm3_substby.vcf 
sed -i '/stby/d'  chr1_region_subwir6_submm3_substby.vcf


###against CHC
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_chc12.vcf --maskName chc12

ack chc12 chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l
  6137


ack PASS chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l
  7991

cp chr1_region_subwir6_submm3_substby_chc12.vcf chr1_region_subwir6_submm3_substby_subchc12.vcf 
sed -i '/chc12/d'  chr1_region_subwir6_submm3_substby_subchc12.vcf


###against harris
/home/jeremiah/harris/orig/vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/harris/orig/vcf/chr1_parental_PASS.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_har.vcf --maskName harris

ack harris chr1_region_subwir6_submm3_substby_chc12.vcf | wc -l



###ano
convert2annovar.pl -format vcf4 chr1_region_subwir6_submm3_substby_subchc12.vcf > chr1_region_subwir6_submm3_substby_subchc12.annovar

annotate_variation.pl -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr1_region_subwir6_submm3_substby_subchc12.annovar ~/harris/orig/zebradb/
##drop 11 -> 7980

annotate_variation.pl chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered -buildver danRer7 ~/harris/orig/zebradb/

wc -l chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function
  73

ack nonsyn chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function | wc -l
  21

###
###-


cat ~/snpuniverseheader.vcf > proper_universe.vcf
cat WT_SNP_set.vcf >> proper_universe.vcf
java -d64 -Xmx4G -jar ~/tools/IGVTools/igvtools.jar sort proper_universe.vcf proper_universe_sorted.vcf
cp proper_universe_sorted.vcf proper_universe_sorted_fix.vcf

python ~/chdm_pipeline/util/add_chr2vcf.py -i proper_universe_sorted.vcf -o proper_universe_sorted_fix.vcf
java -d64 -Xmx4G -jar ~/tools/IGVTools/igvtools.jar sort  chr1_region_subwir6_submm3_substby_subchc12.vcf chr1_region_subwir6_submm3_substby_subchc12_sort.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_sort.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_univ.vcf --maskName universe



sed -i '/Zv9_NA/d' proper_universe_sorted_fix.vcf
sed -i '/Zv9_sca/d' proper_universe_sorted_fix.vcf
python ~/chdm_pipeline/util/remchr_fasta.py -i danRer7.fa -o danRer7_fixed.fa
python ~/chdm_pipeline/util/reorder_fasta.py -i danRer7_fixed.fa -o danRer7_fixed_crop.fa

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_sort.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_univ.vcf --maskName universe

cp chr1_region_subwir6_submm3_substby_subchc12_univ.vcf chr1_region_subwir6_submm3_substby_subchc12_subuniv.vcf
sed -i '/universe/d' chr1_region_subwir6_submm3_substby_subchc12_subuniv.vcf
###ano
convert2annovar.pl -format vcf4 chr1_region_subwir6_submm3_substby_subchc12_subuniv.vcf > chr1_region_subwir6_submm3_substby_subchc12_subuniv.annovar

annotate_variation.pl -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr1_region_subwir6_submm3_substby_subchc12_subuniv.annovar ~/harris/orig/zebradb/
##drop 11 -> 7980

annotate_variation.pl chr1_region_subwir6_submm3_substby_subchc12_subuniv.annovar.danRer7_generic_filtered -buildver danRer7 ~/harris/orig/zebradb/

wc -l chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function
  73

ack nonsyn chr1_region_subwir6_submm3_substby_subchc12_subuniv.annovar.danRer7_generic_filtered.exonic_variant_function | wc -l
  21


















=======
###2013-01-18
#STBY 26000000 to 41000000. lines 535508 (-343392) = 192116/878900 to 875220

head -n 985221 chr1.vcf | tail -n 302116 | most


head -n 27 chr12.vcf > chr12_region.vcf
head -n 885000 chr12.vcf > int1.vcf
tail -n 350000 int1.vcf >> chr12_region.vcf
wc -l chr12_region.vcf
  350027 - 27 = 350000

#filter against WIR6
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr12.vcf --variant chr12_region.vcf -o chr12_region_wir6.vcf --maskName wir6


ack wir6 chr12_region_wir6.vcf | wc -l
  340452
ack PASS chr12_region_wir6.vcf | wc -l
  9550

cp chr12_region_wir6.vcf chr12_region_subwir6.vcf
sed -i '/wir6/d' chr12_region_subwir6.vcf

###against mm3
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr12.vcf --variant chr12_region_subwir6.vcf -o chr12_region_subwir6_mm3.vcf --maskName mm3

ack  mm3 chr12_region_subwir6_mm3.vcf | wc -l
  2452

ack PASS chr12_region_subwir6_mm3.vcf | wc -l
  7100

cp chr12_region_subwir6_mm3.vcf chr12_region_subwir6_submm3.vcf 
sed -i '/mm3/d' chr12_region_subwir6_submm3.vcf 

###against dir4
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr12.vcf --variant chr12_region_subwir6_submm3.vcf -o chr12_region_subwir6_submm3_dir4.vcf --maskName dir4

ack dir4 chr12_region_subwir6_submm3_dir4.vcf | wc -l
  665


ack PASS chr12_region_subwir6_submm3_dir4.vcf | wc -l
  6437

cp chr12_region_subwir6_submm3_dir4.vcf chr12_region_subwir6_submm3_subdir4.vcf 
sed -i '/dir4/d'  chr12_region_subwir6_submm3_subdir4.vcf


###against CHC
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant chr12_region_subwir6_submm3_subdir4.vcf -o chr12_region_subwir6_submm3_subdir4_chc12.vcf --maskName chc12

ack chc12 chr12_region_subwir6_submm3_subdir4_chc12.vcf | wc -l
  382


ack PASS chr12_region_subwir6_submm3_subdir4_chc12.vcf | wc -l
  6057

cp chr12_region_subwir6_submm3_subdir4_chc12.vcf chr12_region_subwir6_submm3_subdir4_subchc12.vcf 
sed -i '/chc12/d'  chr12_region_subwir6_submm3_subdir4_subchc12.vcf


###against harris
/home/jeremiah/harris/orig/vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/harris/orig/vcf/chr12_parental_PASS.vcf --variant chr12_region_subwir6_submm3_subdir4_subchc12.vcf -o chr1_region_subwir6_submm3_subdir4_subchc12_har.vcf --maskName harris

ack harris chr12_region_subwir6_submm3_subdir4_chc12.vcf | wc -l



###ano
convert2annovar.pl -format vcf4 chr1_region_subwir6_submm3_substby_subchc12.vcf > chr1_region_subwir6_submm3_substby_subchc12.annovar

annotate_variation.pl -filter --genericdbfile danRer7_dbSNPs.txt --buildver danRer7 --dbtype generic chr1_region_subwir6_submm3_substby_subchc12.annovar ~/harris/orig/zebradb/
##drop 11 -> 7980

annotate_variation.pl chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered -buildver danRer7 ~/harris/orig/zebradb/

wc -l chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function
  73

ack nonsyn chr1_region_subwir6_submm3_substby_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function | wc -l
  21

###
###-


cat ~/snpuniverseheader.vcf > proper_universe.vcf
cat WT_SNP_set.vcf >> proper_universe.vcf
java -d64 -Xmx4G -jar ~/tools/IGVTools/igvtools.jar sort proper_universe.vcf proper_universe_sorted.vcf
cp proper_universe_sorted.vcf proper_universe_sorted_fix.vcf

python ~/chdm_pipeline/util/add_chr2vcf.py -i proper_universe_sorted.vcf -o proper_universe_sorted_fix.vcf
java -d64 -Xmx4G -jar ~/tools/IGVTools/igvtools.jar sort  chr1_region_subwir6_submm3_substby_subchc12.vcf chr1_region_subwir6_submm3_substby_subchc12_sort.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_sort.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_univ.vcf --maskName universe



sed -i '/Zv9_NA/d' proper_universe_sorted_fix.vcf
sed -i '/Zv9_sca/d' proper_universe_sorted_fix.vcf
python ~/chdm_pipeline/util/remchr_fasta.py -i danRer7.fa -o danRer7_fixed.fa
python ~/chdm_pipeline/util/reorder_fasta.py -i danRer7_fixed.fa -o danRer7_fixed_crop.fa

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant chr1_region_subwir6_submm3_subdir4_subchc12_sort.vcf -o ~/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr1_region_subwir6_submm3_substby_subchc12_univ.vcf --maskName universe

cp chr1_region_subwir6_submm3_substby_subchc12_univ.vcf chr1_region_subwir6_submm3_substby_subchc12_subuniv.vcf
sed -i '/universe/d' chr1_region_subwir6_submm3_substby_subchc12_subuniv.vcf
###ano
convert2annovar.pl -format vcf4 chr12_region_subwir6_submm3_subdir4_subchc12.vcf > chr12_region_subwir6_submm3_subdir4_subchc12.annovar

annotate_variation.pl -filter --genericdbfile danRer7_dbSNPs.txt --buildver ensGene danRer7 --dbtype generic chr12_region_subwir6_submm3_subdir4_subchc12.annovar ~/harris/orig/zebradb/
##drop 45 -> 7980

annotate_variation.pl chr12_region_subwir6_submm3_subdir4_subchc12.annovar.danRer7_generic_filtered -buildver ensGene danRer7 ~/harris/orig/zebradb/

wc -l chr1_region_subwir6_submm3_subdir4_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function
  73

ack nonsyn chr12_region_subwir6_submm3_subdir4_subchc12.annovar.danRer7_generic_filtered.exonic_variant_function | wc -l
  13


##########2013-01-18
cd Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/

time java -d64 -Xmx4g -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Stby-Mut_chr12.bam -R ~/danRer7/danRer7.fa -o chr12_mut_ug.vcf -metrics mut_snps.metrics --output_mode EMIT_VARIANTS_ONLY --sample_ploidy 36 -L "chr12:34000000-35000000"

time java -d64 -Xmx4g -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Stby-Wt_chr12.bam -R ~/danRer7/danRer7.fa -o chr12_wt_ug.vcf -metrics wt_snps.metrics --output_mode EMIT_VARIANTS_ONLY --sample_ploidy 36 -L "chr12:34000000-35000000"


cd Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/

time java -d64 -Xmx4g -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Stby-Mut_chr12.bam -R ~/danRer7/danRer7.fa -o chr12_mut_ug.vcf -metrics mut_snps.metrics --output_mode EMIT_VARIANTS_ONLY --sample_ploidy 36 -L "chr1:38556239-57006567"


####2013-01-23
for f in *.csv ; do python ~/chdm_pipeline/util/cull_tsv.py -i $f ; done

#python ~/chdm_pipeline/util/coll_tsv_cols_2vcf.py -i 20130122-dir-4-emsenbl-all-ns.tsv
for f in *.tsv ; do python ~/chdm_pipeline/util/coll_tsv_cols_2vcf.py -i $f ; done

###>>>1<<<###  CHR_1
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant 20130122-dir-4-emsenbl-all-ns.vcf -o dir4_ens_ns_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby dir4_ens_ns_stby.vcf | wc -l
#400
ack PASS dir4_ens_ns_stby.vcf | wc -l
#869
cp dir4_ens_ns_stby.vcf dir4_ens_ns_substby.vcf
sed -i '/stby/d' dir4_ens_ns_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant dir4_ens_ns_substby.vcf -o dir4_ens_ns_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 dir4_ens_ns_substby_mm3.vcf | wc -l
#138
ack PASS dir4_ens_ns_substby_mm3.vcf | wc -l
#733

cp dir4_ens_ns_substby_mm3.vcf dir4_ens_ns_substby_submm3.vcf
sed -i '/mm3/d' dir4_ens_ns_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant dir4_ens_ns_substby_submm3.vcf -o dir4_ens_ns_substby_submm3_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 dir4_ens_ns_substby_submm3_wir6.vcf | wc -l
#48
ack PASS dir4_ens_ns_substby_submm3_wir6.vcf | wc -l
#687
cp dir4_ens_ns_substby_submm3_wir6.vcf dir4_ens_ns_substby_submm3_subwir6.vcf
sed -i '/wir6/d' dir4_ens_ns_substby_submm3_subwir6.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant dir4_ens_ns_substby_submm3_subwir6.vcf -o dir4_ens_ns_substby_submm3_subwir6_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 dir4_ens_ns_substby_submm3_subwir6_chc12.vcf | wc -l
#30
ack PASS dir4_ens_ns_substby_submm3_subwir6_chc12.vcf | wc -l
#659
cp dir4_ens_ns_substby_submm3_subwir6_chc12.vcf dir4_ens_ns_substby_submm3_subwir6_subchc12.vcf
sed -i '/chc12/d' dir4_ens_ns_substby_submm3_subwir6_subchc12.vcf

sed -i '/contig/d' dir4_ens_ns_substby_submm3_subwir6_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant dir4_ens_ns_substby_submm3_subwir6_subchc12.vcf -o dir4_ens_ns_substby_submm3_subwir6_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe dir4_ens_ns_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#5
ack PASS dir4_ens_ns_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#656
cp dir4_ens_ns_substby_submm3_subwir6_subchc12_univ.vcf dir4_ens_ns_substby_submm3_subwir6_subchc12_subuniv.vcf
sed -i '/universe/d' dir4_ens_ns_substby_submm3_subwir6_subchc12_subuniv.vcf



###>>>2<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant 20130122-dir-4-emsenbl-all-stop.vcf -o dir4_ens_stop_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby dir4_ens_stop_stby.vcf | wc -l
#4
ack PASS dir4_ens_stop_stby.vcf | wc -l
#13
cp dir4_ens_stop_stby.vcf dir4_ens_stop_substby.vcf
sed -i '/stby/d' dir4_ens_stop_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant dir4_ens_stop_substby.vcf -o dir4_ens_stop_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 dir4_ens_stop_substby_mm3.vcf | wc -l
#3
ack PASS dir4_ens_stop_substby_mm3.vcf | wc -l
#12

cp dir4_ens_stop_substby_mm3.vcf dir4_ens_stop_substby_submm3.vcf
sed -i '/mm3/d' dir4_ens_stop_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant dir4_ens_stop_substby_submm3.vcf -o dir4_ens_stop_substby_submm3_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 dir4_ens_stop_substby_submm3_wir6.vcf | wc -l
#
ack PASS dir4_ens_stop_substby_submm3_wir6.vcf | wc -l
#
cp dir4_ens_stop_substby_submm3_wir6.vcf dir4_ens_stop_substby_submm3_subwir6.vcf
sed -i '/wir6/d' dir4_ens_stop_substby_submm3_subwir6.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant dir4_ens_stop_substby_submm3_subwir6.vcf -o dir4_ens_stop_substby_submm3_subwir6_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 dir4_ens_stop_substby_submm3_subwir6_chc12.vcf | wc -l
#
ack PASS dir4_ens_stop_substby_submm3_subwir6_chc12.vcf | wc -l
#
cp dir4_ens_stop_substby_submm3_subwir6_chc12.vcf dir4_ens_stop_substby_submm3_subwir6_subchc12.vcf
sed -i '/chc12/d' dir4_ens_stop_substby_submm3_subwir6_subchc12.vcf

sed -i '/contig/d' dir4_ens_stop_substby_submm3_subwir6_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant dir4_ens_stop_substby_submm3_subwir6_subchc12.vcf -o dir4_ens_stop_substby_submm3_subwir6_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe dir4_ens_stop_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#2
ack PASS dir4_ens_stop_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#12
cp dir4_ens_stop_substby_submm3_subwir6_subchc12_univ.vcf dir4_ens_stop_substby_submm3_subwir6_subchc12_subuniv.vcf
sed -i '/universe/d' dir4_ens_stop_substby_submm3_subwir6_subchc12_subuniv.vcf





###>>>3<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant 20130122-dir-4-snpeff-all-ns.vcf -o dir4_snpeff_ns_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING
htop
ack stby dir4_snpeff_ns_stby.vcf | wc -l
#36
ack PASS dir4_snpeff_ns_stby.vcf | wc -l
#57
cp dir4_snpeff_ns_stby.vcf dir4_snpeff_ns_substby.vcf
sed -i '/stby/d' dir4_snpeff_ns_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant dir4_snpeff_ns_substby.vcf -o dir4_snpeff_ns_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 dir4_snpeff_ns_substby_mm3.vcf | wc -l
#6
ack PASS dir4_snpeff_ns_substby_mm3.vcf | wc -l
#53

cp dir4_snpeff_ns_substby_mm3.vcf dir4_snpeff_ns_substby_submm3.vcf
sed -i '/mm3/d' dir4_snpeff_ns_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant dir4_snpeff_ns_substby_submm3.vcf -o dir4_snpeff_ns_substby_submm3_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 dir4_snpeff_ns_substby_submm3_wir6.vcf | wc -l
#5
ack PASS dir4_snpeff_ns_substby_submm3_wir6.vcf | wc -l
#50
cp dir4_snpeff_ns_substby_submm3_wir6.vcf dir4_snpeff_ns_substby_submm3_subwir6.vcf
sed -i '/wir6/d' dir4_snpeff_ns_substby_submm3_subwir6.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant dir4_snpeff_ns_substby_submm3_subwir6.vcf -o dir4_snpeff_ns_substby_submm3_subwir6_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 dir4_snpeff_ns_substby_submm3_subwir6_chc12.vcf | wc -l
#8
ack PASS dir4_snpeff_ns_substby_submm3_subwir6_chc12.vcf | wc -l
#44
cp dir4_snpeff_ns_substby_submm3_subwir6_chc12.vcf dir4_snpeff_ns_substby_submm3_subwir6_subchc12.vcf
sed -i '/chc12/d' dir4_snpeff_ns_substby_submm3_subwir6_subchc12.vcf

sed -i '/contig/d' dir4_snpeff_ns_substby_submm3_subwir6_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant dir4_snpeff_ns_substby_submm3_subwir6_subchc12.vcf -o dir4_snpeff_ns_substby_submm3_subwir6_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe dir4_snpeff_ns_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#2
ack PASS dir4_snpeff_ns_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#44
cp dir4_snpeff_ns_substby_submm3_subwir6_subchc12_univ.vcf dir4_snpeff_ns_substby_submm3_subwir6_subchc12_subuniv.vcf
sed -i '/universe/d' dir4_snpeff_ns_substby_submm3_subwir6_subchc12_subuniv.vcf




###>>>4<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr1.vcf --variant 20130122-dir-4-snpeff-all-stop.vcf -o dir4_snpeff_stop_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby dir4_snpeff_stop_stby.vcf | wc -l
#15
ack PASS dir4_snpeff_stop_stby.vcf | wc -l
#26
cp dir4_snpeff_stop_stby.vcf dir4_snpeff_stop_substby.vcf
sed -i '/stby/d' dir4_snpeff_stop_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr1.vcf --variant dir4_snpeff_stop_substby.vcf -o dir4_snpeff_stop_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 dir4_snpeff_stop_substby_mm3.vcf | wc -l
#5
ack PASS dir4_snpeff_stop_substby_mm3.vcf | wc -l
#23

cp dir4_snpeff_stop_substby_mm3.vcf dir4_snpeff_stop_substby_submm3.vcf
sed -i '/mm3/d' dir4_snpeff_stop_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr1.vcf --variant dir4_snpeff_stop_substby_submm3.vcf -o dir4_snpeff_stop_substby_submm3_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 dir4_snpeff_stop_substby_submm3_wir6.vcf | wc -l
#5
ack PASS dir4_snpeff_stop_substby_submm3_wir6.vcf | wc -l
#20
cp dir4_snpeff_stop_substby_submm3_wir6.vcf dir4_snpeff_stop_substby_submm3_subwir6.vcf
sed -i '/wir6/d' dir4_snpeff_stop_substby_submm3_subwir6.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr1.vcf --variant dir4_snpeff_stop_substby_submm3_subwir6.vcf -o dir4_snpeff_stop_substby_submm3_subwir6_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 dir4_snpeff_stop_substby_submm3_subwir6_chc12.vcf | wc -l
#2
ack PASS dir4_snpeff_stop_substby_submm3_subwir6_chc12.vcf | wc -l
#20
cp dir4_snpeff_stop_substby_submm3_subwir6_chc12.vcf dir4_snpeff_stop_substby_submm3_subwir6_subchc12.vcf
sed -i '/chc12/d' dir4_snpeff_stop_substby_submm3_subwir6_subchc12.vcf

sed -i '/contig/d' dir4_snpeff_stop_substby_submm3_subwir6_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant dir4_snpeff_stop_substby_submm3_subwir6_subchc12.vcf -o dir4_snpeff_stop_substby_submm3_subwir6_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe dir4_snpeff_stop_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#2
ack PASS dir4_snpeff_stop_substby_submm3_subwir6_subchc12_univ.vcf | wc -l
#20
cp dir4_snpeff_stop_substby_submm3_subwir6_subchc12_univ.vcf dir4_snpeff_stop_substby_submm3_subwir6_subchc12_subuniv.vcf
sed -i '/universe/d' dir4_snpeff_stop_substby_submm3_subwir6_subchc12_subuniv.vcf






####2013-01-24
###>>>5<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr21.vcf --variant 20130122-wir-6-snpeff-all-stop.vcf -o wir6_snpeff_stop_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby wir6_snpeff_stop_stby.vcf | wc -l
#16
ack PASS wir6_snpeff_stop_stby.vcf | wc -l
#11
cp wir6_snpeff_stop_stby.vcf wir6_snpeff_stop_substby.vcf
sed -i '/stby/d' wir6_snpeff_stop_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr21.vcf --variant wir6_snpeff_stop_substby.vcf -o wir6_snpeff_stop_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 wir6_snpeff_stop_substby_mm3.vcf | wc -l
#4
ack PASS wir6_snpeff_stop_substby_mm3.vcf | wc -l
#9

cp wir6_snpeff_stop_substby_mm3.vcf wir6_snpeff_stop_substby_submm3.vcf
sed -i '/mm3/d' wir6_snpeff_stop_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr21.vcf --variant wir6_snpeff_stop_substby_submm3.vcf -o wir6_snpeff_stop_substby_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 wir6_snpeff_stop_substby_submm3_dir4.vcf | wc -l
#2
ack PASS wir6_snpeff_stop_substby_submm3_dir4.vcf | wc -l
#9
cp wir6_snpeff_stop_substby_submm3_dir4.vcf wir6_snpeff_stop_substby_submm3_subdir4.vcf
sed -i '/dir4/d' wir6_snpeff_stop_substby_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr21.vcf --variant wir6_snpeff_stop_substby_submm3_subdir4.vcf -o wir6_snpeff_stop_substby_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 wir6_snpeff_stop_substby_submm3_subdir4_chc12.vcf | wc -l
#3
ack PASS wir6_snpeff_stop_substby_submm3_subdir4_chc12.vcf | wc -l
#8
cp wir6_snpeff_stop_substby_submm3_subdir4_chc12.vcf wir6_snpeff_stop_substby_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' wir6_snpeff_stop_substby_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' wir6_snpeff_stop_substby_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant wir6_snpeff_stop_substby_submm3_subdir4_subchc12.vcf -o wir6_snpeff_stop_substby_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe wir6_snpeff_stop_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS wir6_snpeff_stop_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#8
cp wir6_snpeff_stop_substby_submm3_subdir4_subchc12_univ.vcf wir6_snpeff_stop_substby_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' wir6_snpeff_stop_substby_submm3_subdir4_subchc12_subuniv.vcf





###>>>6<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr21.vcf --variant 20130122-wir-6-snpeff-all-ns.vcf -o wir6_snpeff_ns_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby wir6_snpeff_ns_stby.vcf | wc -l
#236
ack PASS wir6_snpeff_ns_stby.vcf | wc -l
#156
cp wir6_snpeff_ns_stby.vcf wir6_snpeff_ns_substby.vcf
sed -i '/stby/d' wir6_snpeff_ns_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr21.vcf --variant wir6_snpeff_ns_substby.vcf -o wir6_snpeff_ns_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 wir6_snpeff_ns_substby_mm3.vcf | wc -l
#5
ack PASS wir6_snpeff_ns_substby_mm3.vcf | wc -l
#153

cp wir6_snpeff_ns_substby_mm3.vcf wir6_snpeff_ns_substby_submm3.vcf
sed -i '/mm3/d' wir6_snpeff_ns_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr21.vcf --variant wir6_snpeff_ns_substby_submm3.vcf -o wir6_snpeff_ns_substby_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 wir6_snpeff_ns_substby_submm3_dir4.vcf | wc -l
#19
ack PASS wir6_snpeff_ns_substby_submm3_dir4.vcf | wc -l
#136
cp wir6_snpeff_ns_substby_submm3_dir4.vcf wir6_snpeff_ns_substby_submm3_subdir4.vcf
sed -i '/dir4/d' wir6_snpeff_ns_substby_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr21.vcf --variant wir6_snpeff_ns_substby_submm3_subdir4.vcf -o wir6_snpeff_ns_substby_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 wir6_snpeff_ns_substby_submm3_subdir4_chc12.vcf | wc -l
#12
ack PASS wir6_snpeff_ns_substby_submm3_subdir4_chc12.vcf | wc -l
#126
cp wir6_snpeff_ns_substby_submm3_subdir4_chc12.vcf wir6_snpeff_ns_substby_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' wir6_snpeff_ns_substby_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' wir6_snpeff_ns_substby_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant wir6_snpeff_ns_substby_submm3_subdir4_subchc12.vcf -o wir6_snpeff_ns_substby_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe wir6_snpeff_ns_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#5
ack PASS wir6_snpeff_ns_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#123
cp wir6_snpeff_ns_substby_submm3_subdir4_subchc12_univ.vcf wir6_snpeff_ns_substby_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' wir6_snpeff_ns_substby_submm3_subdir4_subchc12_subuniv.vcf






###>>>7<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr21.vcf --variant 20130122-wir-6-emsenbl-all-ns.vcf -o wir6_ensembl_ns_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby wir6_ensembl_ns_stby.vcf | wc -l
#179
ack PASS wir6_ensembl_ns_stby.vcf | wc -l
#117
cp wir6_ensembl_ns_stby.vcf wir6_ensembl_ns_substby.vcf
sed -i '/stby/d' wir6_ensembl_ns_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr21.vcf --variant wir6_ensembl_ns_substby.vcf -o wir6_ensembl_ns_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 wir6_ensembl_ns_substby_mm3.vcf | wc -l
#4
ack PASS wir6_ensembl_ns_substby_mm3.vcf | wc -l
#115

cp wir6_ensembl_ns_substby_mm3.vcf wir6_ensembl_ns_substby_submm3.vcf
sed -i '/mm3/d' wir6_ensembl_ns_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr21.vcf --variant wir6_ensembl_ns_substby_submm3.vcf -o wir6_ensembl_ns_substby_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 wir6_ensembl_ns_substby_submm3_dir4.vcf | wc -l
#12
ack PASS wir6_ensembl_ns_substby_submm3_dir4.vcf | wc -l
#105
cp wir6_ensembl_ns_substby_submm3_dir4.vcf wir6_ensembl_ns_substby_submm3_subdir4.vcf
sed -i '/dir4/d' wir6_ensembl_ns_substby_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant wir6_ensembl_ns_substby_submm3_subdir4.vcf -o wir6_ensembl_ns_substby_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 wir6_ensembl_ns_substby_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS wir6_ensembl_ns_substby_submm3_subdir4_chc12.vcf | wc -l
#105
cp wir6_ensembl_ns_substby_submm3_subdir4_chc12.vcf wir6_ensembl_ns_substby_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' wir6_ensembl_ns_substby_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' wir6_ensembl_ns_substby_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant wir6_ensembl_ns_substby_submm3_subdir4_subchc12.vcf -o wir6_ensembl_ns_substby_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe wir6_ensembl_ns_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#6
ack PASS wir6_ensembl_ns_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#101
cp wir6_ensembl_ns_substby_submm3_subdir4_subchc12_univ.vcf wir6_ensembl_ns_substby_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' wir6_ensembl_ns_substby_submm3_subdir4_subchc12_subuniv.vcf






###>>>8<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/STBY/chr21.vcf --variant 20130122-wir-6-emsenbl-all-stop.vcf -o wir6_ensembl_stop_stby.vcf --maskName stby -U LENIENT_VCF_PROCESSING

ack stby wir6_ensembl_stop_stby.vcf | wc -l
#2
ack PASS wir6_ensembl_stop_stby.vcf | wc -l
#2
cp wir6_ensembl_stop_stby.vcf wir6_ensembl_stop_substby.vcf
sed -i '/stby/d' wir6_ensembl_stop_substby.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr21.vcf --variant wir6_ensembl_stop_substby.vcf -o wir6_ensembl_stop_substby_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 wir6_ensembl_stop_substby_mm3.vcf | wc -l
#2
ack PASS wir6_ensembl_stop_substby_mm3.vcf | wc -l
#2

cp wir6_ensembl_stop_substby_mm3.vcf wir6_ensembl_stop_substby_submm3.vcf
sed -i '/mm3/d' wir6_ensembl_stop_substby_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr21.vcf --variant wir6_ensembl_stop_substby_submm3.vcf -o wir6_ensembl_stop_substby_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 wir6_ensembl_stop_substby_submm3_dir4.vcf | wc -l
#2
ack PASS wir6_ensembl_stop_substby_submm3_dir4.vcf | wc -l
#2
cp wir6_ensembl_stop_substby_submm3_dir4.vcf wir6_ensembl_stop_substby_submm3_subdir4.vcf
sed -i '/dir4/d' wir6_ensembl_stop_substby_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr21.vcf --variant wir6_ensembl_stop_substby_submm3_subdir4.vcf -o wir6_ensembl_stop_substby_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 wir6_ensembl_stop_substby_submm3_subdir4_chc12.vcf | wc -l
#3
ack PASS wir6_ensembl_stop_substby_submm3_subdir4_chc12.vcf | wc -l
#1
cp wir6_ensembl_stop_substby_submm3_subdir4_chc12.vcf wir6_ensembl_stop_substby_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' wir6_ensembl_stop_substby_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' wir6_ensembl_stop_substby_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant wir6_ensembl_stop_substby_submm3_subdir4_subchc12.vcf -o wir6_ensembl_stop_substby_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe wir6_ensembl_stop_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS wir6_ensembl_stop_substby_submm3_subdir4_subchc12_univ.vcf | wc -l
#1
cp wir6_ensembl_stop_substby_submm3_subdir4_subchc12_univ.vcf wir6_ensembl_stop_substby_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' wir6_ensembl_stop_substby_submm3_subdir4_subchc12_subuniv.vcf






###>>>9<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr12.vcf --variant 20130122-stubby-snpeff-all-stop.vcf -o stubby_snpeff_stop_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 stubby_snpeff_stop_wir6.vcf | wc -l
#2
ack PASS stubby_snpeff_stop_wir6.vcf | wc -l
#4
cp stubby_snpeff_stop_wir6.vcf stubby_snpeff_stop_subwir6.vcf
sed -i '/wir6/d' stubby_snpeff_stop_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr12.vcf --variant stubby_snpeff_stop_subwir6.vcf -o stubby_snpeff_stop_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 stubby_snpeff_stop_subwir6_mm3.vcf | wc -l
#2
ack PASS stubby_snpeff_stop_subwir6_mm3.vcf | wc -l
#4

cp stubby_snpeff_stop_subwir6_mm3.vcf stubby_snpeff_stop_subwir6_submm3.vcf
sed -i '/mm3/d' stubby_snpeff_stop_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr12.vcf --variant stubby_snpeff_stop_subwir6_submm3.vcf -o stubby_snpeff_stop_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 stubby_snpeff_stop_subwir6_submm3_dir4.vcf | wc -l
#2
ack PASS stubby_snpeff_stop_subwir6_submm3_dir4.vcf | wc -l
#4
cp stubby_snpeff_stop_subwir6_submm3_dir4.vcf stubby_snpeff_stop_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' stubby_snpeff_stop_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant stubby_snpeff_stop_subwir6_submm3_subdir4.vcf -o stubby_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 stubby_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS stubby_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#4
cp stubby_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf -o stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#4
cp stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' stubby_snpeff_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>10<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr12.vcf --variant 20130122-stubby-snpeff-all-ns.vcf -o stubby_snpeff_ns_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 stubby_snpeff_ns_wir6.vcf | wc -l
#80
ack PASS stubby_snpeff_ns_wir6.vcf | wc -l
#86
cp stubby_snpeff_ns_wir6.vcf stubby_snpeff_ns_subwir6.vcf
sed -i '/wir6/d' stubby_snpeff_ns_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr12.vcf --variant stubby_snpeff_ns_subwir6.vcf -o stubby_snpeff_ns_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 stubby_snpeff_ns_subwir6_mm3.vcf | wc -l
#13
ack PASS stubby_snpeff_ns_subwir6_mm3.vcf | wc -l
#75

cp stubby_snpeff_ns_subwir6_mm3.vcf stubby_snpeff_ns_subwir6_submm3.vcf
sed -i '/mm3/d' stubby_snpeff_ns_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr12.vcf --variant stubby_snpeff_ns_subwir6_submm3.vcf -o stubby_snpeff_ns_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 stubby_snpeff_ns_subwir6_submm3_dir4.vcf | wc -l
#11
ack PASS stubby_snpeff_ns_subwir6_submm3_dir4.vcf | wc -l
#66
cp stubby_snpeff_ns_subwir6_submm3_dir4.vcf stubby_snpeff_ns_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' stubby_snpeff_ns_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant stubby_snpeff_ns_subwir6_submm3_subdir4.vcf -o stubby_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 stubby_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS stubby_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#66
cp stubby_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf -o stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#5
ack PASS stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#63
cp stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' stubby_snpeff_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>11<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr12.vcf --variant 20130122-stubby-emsenbl-all-ns.vcf -o stubby_ensembl_ns_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 stubby_ensembl_ns_wir6.vcf | wc -l
#76
ack PASS stubby_ensembl_ns_wir6.vcf | wc -l
#83
cp stubby_ensembl_ns_wir6.vcf stubby_ensembl_ns_subwir6.vcf
sed -i '/wir6/d' stubby_ensembl_ns_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr12.vcf --variant stubby_ensembl_ns_subwir6.vcf -o stubby_ensembl_ns_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 stubby_ensembl_ns_subwir6_mm3.vcf | wc -l
#13
ack PASS stubby_ensembl_ns_subwir6_mm3.vcf | wc -l
#72

cp stubby_ensembl_ns_subwir6_mm3.vcf stubby_ensembl_ns_subwir6_submm3.vcf
sed -i '/mm3/d' stubby_ensembl_ns_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr12.vcf --variant stubby_ensembl_ns_subwir6_submm3.vcf -o stubby_ensembl_ns_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 stubby_ensembl_ns_subwir6_submm3_dir4.vcf | wc -l
#11
ack PASS stubby_ensembl_ns_subwir6_submm3_dir4.vcf | wc -l
#63
cp stubby_ensembl_ns_subwir6_submm3_dir4.vcf stubby_ensembl_ns_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' stubby_ensembl_ns_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant stubby_ensembl_ns_subwir6_submm3_subdir4.vcf -o stubby_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 stubby_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS stubby_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#63
cp stubby_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf -o stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#5
ack PASS stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#60
cp stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' stubby_ensembl_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf








###>>>12<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr12.vcf --variant 20130122-stubby-emsenbl-all-stop.vcf -o stubby_ensembl_stop_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 stubby_ensembl_stop_wir6.vcf | wc -l
#2
ack PASS stubby_ensembl_stop_wir6.vcf | wc -l
#3
cp stubby_ensembl_stop_wir6.vcf stubby_ensembl_stop_subwir6.vcf
sed -i '/wir6/d' stubby_ensembl_stop_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr12.vcf --variant stubby_ensembl_stop_subwir6.vcf -o stubby_ensembl_stop_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 stubby_ensembl_stop_subwir6_mm3.vcf | wc -l
#2
ack PASS stubby_ensembl_stop_subwir6_mm3.vcf | wc -l
#3

cp stubby_ensembl_stop_subwir6_mm3.vcf stubby_ensembl_stop_subwir6_submm3.vcf
sed -i '/mm3/d' stubby_ensembl_stop_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr12.vcf --variant stubby_ensembl_stop_subwir6_submm3.vcf -o stubby_ensembl_stop_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 stubby_ensembl_stop_subwir6_submm3_dir4.vcf | wc -l
#2
ack PASS stubby_ensembl_stop_subwir6_submm3_dir4.vcf | wc -l
#3
cp stubby_ensembl_stop_subwir6_submm3_dir4.vcf stubby_ensembl_stop_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' stubby_ensembl_stop_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr12.vcf --variant stubby_ensembl_stop_subwir6_submm3_subdir4.vcf -o stubby_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 stubby_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS stubby_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#3
cp stubby_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf -o stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#3
cp stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' stubby_ensembl_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>13<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr20.vcf --variant 20130122-MM-3-emsenbl-all-stop.vcf -o mm3_ensembl_stop_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 mm3_ensembl_stop_wir6.vcf | wc -l
#3
ack PASS mm3_ensembl_stop_wir6.vcf | wc -l
#2
cp mm3_ensembl_stop_wir6.vcf mm3_ensembl_stop_subwir6.vcf
sed -i '/wir6/d' mm3_ensembl_stop_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr20.vcf --variant mm3_ensembl_stop_subwir6.vcf -o mm3_ensembl_stop_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 mm3_ensembl_stop_subwir6_mm3.vcf | wc -l
#2
ack PASS mm3_ensembl_stop_subwir6_mm3.vcf | wc -l
#2

cp mm3_ensembl_stop_subwir6_mm3.vcf mm3_ensembl_stop_subwir6_submm3.vcf
sed -i '/wir6/d' mm3_ensembl_stop_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr20.vcf --variant mm3_ensembl_stop_subwir6_submm3.vcf -o mm3_ensembl_stop_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 mm3_ensembl_stop_subwir6_submm3_dir4.vcf | wc -l
#2
ack PASS mm3_ensembl_stop_subwir6_submm3_dir4.vcf | wc -l
#2
cp mm3_ensembl_stop_subwir6_submm3_dir4.vcf mm3_ensembl_stop_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' mm3_ensembl_stop_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr20.vcf --variant mm3_ensembl_stop_subwir6_submm3_subdir4.vcf -o mm3_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 mm3_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS mm3_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
cp mm3_ensembl_stop_subwir6_submm3_subdir4_chc12.vcf mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12.vcf -o mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
cp mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_univ.vcf mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' mm3_ensembl_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>14<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr20.vcf --variant 20130122-MM-3-emsenbl-all-ns.vcf -o mm3_ensembl_ns_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 mm3_ensembl_ns_wir6.vcf | wc -l
#60
ack PASS mm3_ensembl_ns_wir6.vcf | wc -l
#133
cp mm3_ensembl_ns_wir6.vcf mm3_ensembl_ns_subwir6.vcf
sed -i '/wir6/d' mm3_ensembl_ns_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr20.vcf --variant mm3_ensembl_ns_subwir6.vcf -o mm3_ensembl_ns_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 mm3_ensembl_ns_subwir6_mm3.vcf | wc -l
#47
ack PASS mm3_ensembl_ns_subwir6_mm3.vcf | wc -l
#88

cp mm3_ensembl_ns_subwir6_mm3.vcf mm3_ensembl_ns_subwir6_submm3.vcf
sed -i '/wir6/d' mm3_ensembl_ns_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr20.vcf --variant mm3_ensembl_ns_subwir6_submm3.vcf -o mm3_ensembl_ns_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 mm3_ensembl_ns_subwir6_submm3_dir4.vcf | wc -l
#40
ack PASS mm3_ensembl_ns_subwir6_submm3_dir4.vcf | wc -l
#57
cp mm3_ensembl_ns_subwir6_submm3_dir4.vcf mm3_ensembl_ns_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' mm3_ensembl_ns_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr20.vcf --variant mm3_ensembl_ns_subwir6_submm3_subdir4.vcf -o mm3_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 mm3_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#6
ack PASS mm3_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#53
cp mm3_ensembl_ns_subwir6_submm3_subdir4_chc12.vcf mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12.vcf -o mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#53
cp mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_univ.vcf mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' mm3_ensembl_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>15<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr20.vcf --variant 20130122-MM-3-snpeff-all-stop.vcf -o mm3_snpeff_stop_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 mm3_snpeff_stop_wir6.vcf | wc -l
#6
ack PASS mm3_snpeff_stop_wir6.vcf | wc -l
#3
cp mm3_snpeff_stop_wir6.vcf mm3_snpeff_stop_subwir6.vcf
sed -i '/wir6/d' mm3_snpeff_stop_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr20.vcf --variant mm3_snpeff_stop_subwir6.vcf -o mm3_snpeff_stop_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 mm3_snpeff_stop_subwir6_mm3.vcf | wc -l
#2
ack PASS mm3_snpeff_stop_subwir6_mm3.vcf | wc -l
#3

cp mm3_snpeff_stop_subwir6_mm3.vcf mm3_snpeff_stop_subwir6_submm3.vcf
sed -i '/wir6/d' mm3_snpeff_stop_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr20.vcf --variant mm3_snpeff_stop_subwir6_submm3.vcf -o mm3_snpeff_stop_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 mm3_snpeff_stop_subwir6_submm3_dir4.vcf | wc -l
#4
ack PASS mm3_snpeff_stop_subwir6_submm3_dir4.vcf | wc -l
#1
cp mm3_snpeff_stop_subwir6_submm3_dir4.vcf mm3_snpeff_stop_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' mm3_snpeff_stop_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr20.vcf --variant mm3_snpeff_stop_subwir6_submm3_subdir4.vcf -o mm3_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 mm3_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#2
ack PASS mm3_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf | wc -l
#1
cp mm3_snpeff_stop_subwir6_submm3_subdir4_chc12.vcf mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12.vcf -o mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#1
cp mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_univ.vcf mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' mm3_snpeff_stop_subwir6_submm3_subdir4_subchc12_subuniv.vcf




###>>>16<<<###
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/WIR6/chr20.vcf --variant 20130122-MM-3-snpeff-all-ns.vcf -o mm3_snpeff_ns_wir6.vcf --maskName wir6 -U LENIENT_VCF_PROCESSING

ack wir6 mm3_snpeff_ns_wir6.vcf | wc -l
#64
ack PASS mm3_snpeff_ns_wir6.vcf | wc -l
#140
cp mm3_snpeff_ns_wir6.vcf mm3_snpeff_ns_subwir6.vcf
sed -i '/wir6/d' mm3_snpeff_ns_subwir6.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/MM3/chr20.vcf --variant mm3_snpeff_ns_subwir6.vcf -o mm3_snpeff_ns_subwir6_mm3.vcf --maskName mm3 -U LENIENT_VCF_PROCESSING

ack mm3 mm3_snpeff_ns_subwir6_mm3.vcf | wc -l
#51
ack PASS mm3_snpeff_ns_subwir6_mm3.vcf | wc -l
#91

cp mm3_snpeff_ns_subwir6_mm3.vcf mm3_snpeff_ns_subwir6_submm3.vcf
sed -i '/wir6/d' mm3_snpeff_ns_subwir6_submm3.vcf

java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen_DIR4_MM3_WIR6_STUBBY/DIR4/chr20.vcf --variant mm3_snpeff_ns_subwir6_submm3.vcf -o mm3_snpeff_ns_subwir6_submm3_dir4.vcf --maskName dir4 -U LENIENT_VCF_PROCESSING

ack dir4 mm3_snpeff_ns_subwir6_submm3_dir4.vcf | wc -l
#43
ack PASS mm3_snpeff_ns_subwir6_submm3_dir4.vcf | wc -l
#57
cp mm3_snpeff_ns_subwir6_submm3_dir4.vcf mm3_snpeff_ns_subwir6_submm3_subdir4.vcf
sed -i '/dir4/d' mm3_snpeff_ns_subwir6_submm3_subdir4.vcf


java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7.fa --mask /home/jeremiah/Project_Chen/split_chr/chr20.vcf --variant mm3_snpeff_ns_subwir6_submm3_subdir4.vcf -o mm3_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf --maskName chc12 -U LENIENT_VCF_PROCESSING

ack chc12 mm3_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#6
ack PASS mm3_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf | wc -l
#53
cp mm3_snpeff_ns_subwir6_submm3_subdir4_chc12.vcf mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf
sed -i '/chc12/d' mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf

sed -i '/contig/d' mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf
java -d64 -Xmx4G -jar ~/tools/GenomeAnalysisTK-2.3-2-g65db1dd/GenomeAnalysisTK.jar -T VariantFiltration -R ~/danRer7/danRer7_fixed_crop.fa --mask /home/jeremiah/tools/megamapper_universe/proper_universe_sorted_fix.vcf --variant mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12.vcf -o mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf --maskName universe -U LENIENT_VCF_PROCESSING

ack universe mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#2
ack PASS mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf | wc -l
#53
cp mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_univ.vcf mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf
sed -i '/universe/d' mm3_snpeff_ns_subwir6_submm3_subdir4_subchc12_subuniv.vcf



####2013-01-28


###2013-02-21
/usr/bin/time java -d64 -Xmx4g -jar ~/local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -I Stby-Wt_chr12.bam -R ~/danRer7/danRer7.fa --out chr12_wt_ugb.vcf --output_mode EMIT_VARIANTS_ONLY -L "chr12:34710000-34720000" --annotateNDA --sample_ploidy 12 -mbq 1

/usr/bin/time java -d64 -Xmx4g -jar ~/local/bin/GenomeAnalysisTK.jar -T VariantsToVCF -R ~/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa -o Danio_rerio.Zv9.70.vcf --variant Danio_rerio.Zv9.70.gtf

util/queryRepeatDatabase.pl -species zebrafish -stat #works!
#xsmall
/usr/bin/time ~/tools/RepeatMasker/RepeatMasker Danio_rerio.Zv9.70.dna.toplevel.fa -species zebrafish  -s -parallel 12 -xsmall -no_is
#N
/usr/bin/time ~/tools/RepeatMasker/RepeatMasker Danio_rerio.Zv9.70.dna.toplevel.fa -species zebrafish  -s -parallel 12



###2013-03-06
#test pre-baserecalibration and post-baserecalibration
/usr/bin/time java -d64 -Xmx4g -jar ~/local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -I Zf-Stuby-Mut_CTTGTA_R1_fixmateinformation.bam -R ~/danRer7/danRer7.fa --out Zf-Stuby-Mut_CTTGTA_R1_fixmateinformation.vcf --output_mode EMIT_VARIANTS_ONLY -L "chr12:34710000-34720000" --annotateNDA --sample_ploidy 12 --min_base_quality_score 1 -nt 24


###2013-03-07
#too slow
 /usr/bin/time java -d64 -Xmx70G -jar ~/local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Dir6_WT_CGATGT_R1_printreads.bam -R /home/jeremiah/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa -o Dir6_WT_CGATGT_R1_printreads_unifiedgenotyper.vcf -metrics snps.metrics --output_mode EMIT_ALL_SITES --sample_ploidy 56 -nt 24 --min_base_quality_score 2

#good speed
/usr/bin/time java -d64 -Xmx70G -jar ~/local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Dir6_WT_CGATGT_R1_printreads.bam -R /home/jeremiah/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa -o Dir6_WT_CGATGT_R1_printreads_unifiedgenotyper.vcf -metrics snps.metrics --output_mode EMIT_ALL_SITES -nt 24 --min_base_quality_score 2

#did not seem to matter
/usr/bin/time java -d64 -Xmx70G -jar ~/local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I Dir6_WT_CGATGT_R1_printreads.bam -R /home/jeremiah/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa -o Dir6_WT_CGATGT_R1_printreads_unifiedgenotyper.vcf -metrics snps.metrics --output_mode EMIT_ALL_SITES --sample_ploidy 56 -nt 24 --min_base_quality_score 2 --max_alternate_alleles 1



###2013-06-18
wir6 - recall
37607532
38157432

wir6 - oldcall
37607514
38157401
