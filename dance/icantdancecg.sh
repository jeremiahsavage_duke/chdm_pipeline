time cgatools listvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --output listvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

time cgatools testvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --input listvar_trio.tsv --output testvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

python ~/chdm_pipeline/cg_010111.py -i testvar_trio.tsv

cp testvar_trio_010111.tsv testvar_trio_010111_nodbsnp.tsv

sed -i '/dbsnp/d' testvar_trio_010111_nodbsnp.tsv

cgatools join --beta --input testvar_trio_010111_nodbsnp.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_010111_nodbsnp_annot2.tsv --match chromosome:chromosome --select 'a.*,b.*' --overlap begin,end:begin,end --match alleleSeq:call

#####
time python ~/chdm_pipeline/cg_filter.py -i testvar_trio.tsv -f "00 00 01"

cp testvar_trio_000001.tsv testvar_trio_000001_nodbsnp.tsv


time sed -i '/dbsnp/d' testvar_trio_000001_nodbsnp.tsv

time cgatools join --beta --input testvar_trio_000001_nodbsnp.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_000001_nodbsnp_annot.tsv --match chromosome:chromosome --select 'a.*,b.*' --overlap begin,end:begin,end --match alleleSeq:call

cp testvar_trio_000001_nodbsnp_annot.tsv testvar_trio_000001_nodbsnp_annot_exon.tsv

time sed -i '/INTRON/d' testvar_trio_000001_nodbsnp_annot_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_000001_nodbsnp_annot_exon.tsv
time sed -i '/UTR3/d' testvar_trio_000001_nodbsnp_annot_exon.tsv
time sed -i '/UTR5/d' testvar_trio_000001_nodbsnp_annot_exon.tsv
time sed -i '/ACCEPTOR/d' testvar_trio_000001_nodbsnp_annot_exon.tsv
time sed -i '/UTR/d' testvar_trio_000001_nodbsnp_annot_exon.tsv

time sed -i '/sub/d' testvar_trio_000001_nodbsnp_snp.tsv
time sed -i '/del/d' testvar_trio_000001_nodbsnp_snp.tsv
-k 2,2

cp testvar_trio_000001_nodbsnp_annot_exon.tsv testvar_trio_000001_nodbsnp_annot_exon_unique.tsv

time sort -u -t, -k1,1 testvar_trio_000001_nodbsnp_annot_exon.tsv > testvar_trio_000001_nodbsnp_annot_exon_unique.tsv

sort  -k 1,1 testvar_trio_000001_nodbsnp_annot_exon_unique.tsv > testvar_trio_000001_nodbsnp_annot_exon_unique.tsv

###############2012-09-25
time python ~/chdm_pipeline/vcf_infocut.py  -i common_all.vcf -o common_all_0.02.vcf -f "GMAF" -c "0.02" > vcf.out

###############2012-09-26
for f in {1..22} ; do echo $f ; ack MAF ESP6500.chr"$f".snps.vcf | wc -l ; done

time python ~/chdm_pipeline/vcf_infocut.py  -i 00-All.vcf -o 00-All_0.01.vcf -f "GMAF" -c "0.01" > vcf.out
time python ~/chdm_pipeline/vcf_infocut.py  -i 00-All.vcf -o 00-All_0.02.vcf -f "GMAF" -c "0.02" > vcf.out
time python ~/chdm_pipeline/vcf_infocut.py  -i 00-All.vcf -o 00-All_0.005.vcf -f "GMAF" -c "0.005" > vcf.out

time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_010111.tsv -o testvar_trio_010111_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_000011.tsv -o testvar_trio_010111_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

time python ~/chdm_pipeline/cg_filter.py -i testvar_trio.tsv -f "00 00 11"

time cgatools join --beta --input testvar_trio_000011.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_000011_annot.tsv --match chromosome:chromosome --select 'a.*,b.*' --overlap begin,end:begin,end --match alleleSeq:call

cp testvar_trio_000011_annot.tsv testvar_trio_000011_annot_exon.tsv

time sed -i '/INTRON/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR3/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR5/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/ACCEPTOR/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR/d' testvar_trio_000011_annot_exon.tsv

cp testvar_trio_000011_annot.tsv testvar_trio_000011_annot_novel.tsv
time sed -i '/dbsnp/d' testvar_trio_000011_annot_novel.tsv

##recapture header
sort -u -t, -k1,1n testvar_trio_000011_annot.tsv > testvar_trio_000011_annot_uniq.tsv

head testvar_trio_000011_annot.tsv -n 14 >> testvar_trio_000011_annot_uniq_header.tsv
tail -n +2 testvar_trio_000011_annot_uniq.tsv >> testvar_trio_000011_annot_uniq_header.tsv

time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_000011.tsv -o testvar_trio_000011_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

ack "\tsnp" testvar_trio_000011_annot_uniq_header_0.01.tsv | wc -l
ack "\tsub" testvar_trio_000011_annot_uniq_header_0.01.tsv | wc -l
ack "\tins" testvar_trio_000011_annot_uniq_header_0.01.tsv | wc -l
ack "\tdel" testvar_trio_000011_annot_uniq_header_0.01.tsv | wc -l


time python ~/chdm_pipeline/vcf_infocut.py  -i 00-All.vcf -o 00-All_0.01.vcf -f "GMAF" -c "0.01" > vcf.out
##################2012-09-27
##
time cgatools listvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --output listvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

time cgatools testvariants --beta --reference ~/home2/complete_genomics/data/ReferenceFiles/build37.crr --input listvar_trio.tsv --output testvar_trio.tsv --variants ~/home2/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

time python ~/chdm_pipeline/cg_filter.py -i testvar_trio.tsv -f "00 00 11"

time python ~/chdm_pipeline/tsv_filtby_vcf.py -i testvar_trio_000011.tsv -o testvar_trio_000011_0.01.tsv -v ~/home2/hg19broad/00-All_0.01.vcf > some.err

time cgatools join --beta --input testvar_trio_000011.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_000011_annot3.tsv --match chromosome:chromosome --select 'a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam' --overlap begin,end:begin,end --overlap-mode allow-abutting-points


time cgatools join --beta --input testvar_trio_000011_annot3.tsv ~/home2/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_000011_annot4.tsv --match chromosome:chromosome --select 'a.*,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality' --overlap begin,end:begin,end --overlap-mode allow-abutting-points

sort -u -t, -k1,1n testvar_trio_000011_annot3.tsv > testvar_trio_000011_annot_uniq.tsv
#sort -u -k1n testvar_trio_000011_annot3.tsv > testvar_trio_000011_annot_uniq.tsv

tail -n +14  testvar_trio_000011_annot3.tsv | wc -l # num of vars
tail -n +2 testvar_trio_000011_annot_uniq.tsv | wc -l # num of annotated vars
rm testvar_trio_000011_annot_uniq_header.tsv
head testvar_trio_000011_annot.tsv -n 14 >> testvar_trio_000011_annot_uniq_header.tsv
tail -n +2 testvar_trio_000011_annot_uniq.tsv >> testvar_trio_000011_annot_uniq_header.tsv

ack "\tsnp" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tsub" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tins" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tdel" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tINTRON\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tTSS-UPSTREAM\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tUTR3\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tUTR5\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tACCEPTOR\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l
ack "\tUTR\t" testvar_trio_000011_annot_uniq_header.tsv | wc -l

cp testvar_trio_000011_annot_uniq.tsv testvar_trio_000011_annot_exon.tsv

time sed -i '/INTRON/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR3/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR5/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/ACCEPTOR/d' testvar_trio_000011_annot_exon.tsv
time sed -i '/UTR/d' testvar_trio_000011_annot_exon.tsv

#######2012-10-09
cat ESP6500.chr1.snps.vcf ESP6500.chr2.snps.vcf ESP6500.chr3.snps.vcf ESP6500.chr4.snps.vcf ESP6500.chr5.snps.vcf ESP6500.chr6.snps.vcf ESP6500.chr7.snps.vcf ESP6500.chr8.snps.vcf ESP6500.chr9.snps.vcf ESP6500.chr10.snps.vcf ESP6500.chr11.snps.vcf ESP6500.chr12.snps.vcf ESP6500.chr13.snps.vcf ESP6500.chr14.snps.vcf ESP6500.chr15.snps.vcf ESP6500.chr16.snps.vcf ESP6500.chr17.snps.vcf ESP6500.chr18.snps.vcf ESP6500.chr19.snps.vcf ESP6500.chr20.snps.vcf ESP6500.chr21.snps.vcf ESP6500.chr22.snps.vcf > ESP6500.snps.vcf


####2012-12-04
./annotate_variation.pl -buildver hg19 -downdb esp6500si_all humandb/

mkdir 121204
/usr/bin/time -v cgatools listvariants --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --output listvar_trio.tsv --variants ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2

/usr/bin/time -v cgatools testvariants --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --input listvar_trio.tsv --output testvar_trio.tsv --variants ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/var-GS000013593-ASM.tsv.bz2 ~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/var-GS000013594-ASM.tsv.bz2 ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/var-GS000013595-ASM.tsv.bz2


/usr/bin/time -v cgatools join --beta --input testvar_trio.tsv ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/gene-GS000013593-ASM.tsv. --output testvar_trio_gene.tsv --match chromosome:chromosome --select 'a.*, b.geneId, b.mrnaAcc, b.proteinAcc, b.symbol, b.orientation, b.component, b.componentIndex, b.hasCodingRegion, b.impact, b.nucleotidePos, b.proteinPos, b.annotationRefSequence, b.sampleSequence, b.genomeRefSequence, b.pfam' --overlap begin,end:begin,end --overlap-mode allow-abutting-points


/usr/bin/time -v cgatools join --beta --input testvar_trio_gene.tsv ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/masterVarBeta-GS000013593-ASM.tsv.bz2 --output testvar_trio_gene_masterVar.tsv --match chromosome:chromosome --overlap begin,end:begin,end --overlap-mode allow-abutting-points --select 'a.*,b.locus,b.ploidy,b.zygosity,b.varType,b.reference,b.allele1Seq,b.allele2Seq,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality,b.allele1HapLink,b.allele2HapLink,b.allele1XRef,b.allele2XRef,b.evidenceIntervalId,b.allele1ReadCount,b.allele2ReadCount,b.referenceAlleleReadCount,b.totalReadCount,b.allele1Gene,b.allele2Gene,b.pfam,b.miRBaseId,b.repeatMasker,b.segDupOverlap,b.relativeCoverageDiploid,b.calledPloidy,b.relativeCoverageNondiploid,b.calledLevel'

/usr/bin/time -v cgatools mkvcf --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --genome-root ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ --genome-root ~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ --genome-root ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ --output trio.vcf --calibration-root ~/complete_genomics/data/ReferenceFiles/var-calibration-v2/ --source-names masterVar,CNV



####2012-12-05
/usr/bin/time -v cgatools mkvcf --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --genome-root ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ --output GS000013593.vcf --calibration-root ~/complete_genomics/data/ReferenceFiles/var-calibration-v2/ --source-names masterVar

/usr/bin/time -v cgatools mkvcf --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --genome-root ~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ --output GS000013594.vcf --calibration-root ~/complete_genomics/data/ReferenceFiles/var-calibration-v2/ --source-names masterVar

/usr/bin/time -v cgatools mkvcf --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --genome-root ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ --output GS000013595.vcf --calibration-root ~/complete_genomics/data/ReferenceFiles/var-calibration-v2/ --source-names masterVar


grep GL trio.vcf > trio_gl.vcf 
sed -i 's/GS000013593-ASM/p13593/g' trio_gl.vcf
sed -i 's/GS000013594-ASM/p13594/g' trio_gl.vcf
sed -i 's/GS000013595-ASM/p13595/g' trio_gl.vcf

vtools 


/usr/bin/time -v polymutt --in_vcf trio_gl.vcf --nthread 24 -p dm041_input.ped -d dm041_input.dat --out_vcf dm041_trio.vcf --chrX X --chrY Y --MT M

cd ~/data/dm041/trio/
vtools init dm041
vtools import trio_gl.vcf --sample_name p13593 p13594 p13595 --build hg19 --format vcf

cd ~/data/dm041/tsv
vtools init dm041
vtools import --format CGA masterVarBeta-GS000013593-ASM.tsv.bz2 --sample_name GS13593 --build hg19
vtools import --format CGA masterVarBeta-GS000013594-ASM.tsv.bz2 --sample_name GS13594 --build hg19
vtools import --format CGA masterVarBeta-GS000013595-ASM.tsv.b2z --sample_name GS13595 --build hg19

vtools select variant --samples "sample_name like '%GS13593'" --to_table GS13593
vtools select variant --samples "sample_name like '%GS13594'" --to_table GS13594
vtools select variant --samples "sample_name like '%GS13595'" --to_table GS13595


cd ~/data/dm041/ind
vtools init dm041
vtools import --format vcf GS000013593.vcf --sample_name GS13593 --build hg19
vtools import --format vcf GS000013594.vcf --sample_name GS13594 --build hg19
vtools import --format vcf GS000013595.vcf --sample_name GS13595 --build hg19

vtools select variant --samples "sample_name like '%GS13593'" --to_table GS13593
vtools select variant --samples "sample_name like '%GS13594'" --to_table GS13594
vtools select variant --samples "sample_name like '%GS13595'" --to_table GS13595


/usr/bin/time -v cgatools mkvcf --beta --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --genome-root ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ --genome-root ~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ --genome-root ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ --output trio_mv.vcf --calibration-root ~/complete_genomics/data/ReferenceFiles/var-calibration-v2/ --source-names masterVar


bgzip -c GS000013593.vcf > GS000013593.vcf.gz
bgzip -c GS000013594.vcf > GS000013594.vcf.gz
bgzip -c GS000013595.vcf > GS000013595.vcf.gz

tabix -p vcf GS000013593.vcf.gz
tabix -p vcf GS000013594.vcf.gz
tabix -p vcf GS000013595.vcf.gz

vcf-compare GS000013593.vcf.gz GS000013594.vcf.gz GS000013595.vcf.gz


mv GS000013593.vcf GS000013593-ASM.vcf
mv GS000013594.vcf GS000013594-ASM.vcf
mv GS000013595.vcf GS000013595-ASM.vcf


bgzip -c GS000013593-ASM.vcf > GS000013593-ASM.vcf.gz
bgzip -c GS000013594-ASM.vcf > GS000013594-ASM.vcf.gz
bgzip -c GS000013595-ASM.vcf > GS000013595-ASM.vcf.gz
tabix -p vcf GS000013593-ASM.vcf.gz
tabix -p vcf GS000013594-ASM.vcf.gz
tabix -p vcf GS000013595-ASM.vcf.gz

vcf-isec -c GS000013595-ASM.vcf.gz GS000013593-ASM.vcf.gz GS000013594-ASM.vcf.gz > denovo.vcf


vcf-contrast -n +GS000013595-ASM -GS000013593-ASM,GS000013594-ASM trio_mv.vcf > denovo.vcf


###2012-12-06
cd ~/tools/
wget http://downloads.sourceforge.net/project/snpeff/snpEff_latest_core.zip?r=http%3A%2F%2Fsnpeff.sourceforge.net%2Fdownload.html&ts=1354814116&use_mirror=heanet
mv snpEff_latest_core.zip\?r\=http\:%2F%2Fsnpeff.sourceforge.net%2Fdownload.html snpEff_latest_core.zip
mv snpEff_3_1/ snpEff/

sed -i 's/data_dir = ~\/snpEff\/data\//data_dir = ~\/tools\/snpEff\/data\//g' ~/tools/snpEff/snpEff.config
/usr/bin/time -v java -d64 -Xmx4G -jar ~/tools/snpEff/snpEff.jar download -v GRCh37.69 -c ~/tools/snpEff/snpEff.config

cd ~/data/dm041/trio/gem
/usr/bin/time -v java -d64 -Xmx10G -jar ~/tools/snpEff/snpEff.jar -c ~/tools/snpEff/snpEff.config -v GRCh37.69 trio_mv.vcf > trio_mv_eff.vcf

cd ~/tools/
wget https://github.com/arq5x/gemini/archive/master.zip
mv master.zip gemini_2012-12-06.zip
cd ~/tools/gemini-master
python setup.py install --prefix=~/local
~/tools/gemini-master/gemini/install-data.py ~/local/share/
~/local/bin/gemini load -v trio_mv.vcf -p ../dm041.ped -t snpEff trio.db


###end gemini due to cyvcf not being pyvcf and choking
#2012-12-07
/usr/bin/time -v sh esp_num2chr.sh hg19_esp6500si_all.txt
insert >chromosome begin end ref alleleSeq MAF_esp6500si
/usr/bin/time -v sh esp_num2chr.sh hg19_ALL.sites.2012_04.txt
insert >chromosome begin ref alleleSeq MAF_1000g rsID
cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

time awk ' !x[$1]++' testvar_trio_gene_masterVar_exon_nonsyn.tsv > testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv

cp testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv.bak

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv --match chromosome:chromosome --match end:begin --select 'a.*, b.MAF_esp6500si' --overlap-mode allow-abutting-points


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File estvar_trio_gene_masterVar_esp6500.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

##--2012-12-08
/usr/bin/time -v cgatools join --beta --input testvar_trio.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene.tsv --match chromosome:chromosome --select a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam --overlap begin,end:begin,end --overlap-mode allow-abutting-points

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene_masterVar.tsv --match chromosome:chromosome --overlap begin,end:begin,end --overlap-mode allow-abutting-points --select a.*,b.locus,b.ploidy,b.zygosity,b.varType,b.reference,b.allele1Seq,b.allele2Seq,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality,b.allele1HapLink,b.allele2HapLink,b.allele1XRef,b.allele2XRef,b.evidenceIntervalId,b.allele1ReadCount,b.allele2ReadCount,b.referenceAlleleReadCount,b.totalReadCount,b.allele1Gene,b.allele2Gene,b.pfam,b.miRBaseId,b.repeatMasker,b.segDupOverlap,b.relativeCoverageDiploid,b.calledPloidy,b.relativeCoverageNondiploid,b.calledLevel

cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar.tsv.bak
time awk ' !x[$1]++' testvar_trio_gene_masterVar.tsv > testvar_trio_gene_masterVar_dedup.tsv

wc -l testvar_trio_gene_masterVar_dedup.tsv
cp testvar_trio_gene_masterVar_dedup.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

wc -l testvar_trio_gene_masterVar_exon.tsv
cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

wc -l testvar_trio_gene_masterVar_exon_nonsyn.tsv

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_es6500si.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_esp6500si --overlap-mode allow-abutting-points

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl_cull.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome



/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si_1000g.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_1000g --overlap-mode allow-abutting-points


awk $

26*2 + M/13 + N/14
52+13=65 / 66
gawk ' $65 >= 0.02 ' testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv > testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_esp002.tsv
cgatools evidence2sam --beta --output e2s_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --extract-genomic-region chrX


###2012-12-09
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber

###2012-12-11
python ~/chdm_pipeline/omim/omim_parsers.py -i omim.txt -o omim_tab.txt > out.txt

python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-12
echo
for i in {1..22}; do
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013595-ASM.tsv.bz2 --output GS000013595_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr
done
> ~/chdm_pipeline/cg/evidence2sam.sh

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013595-ASM.tsv.bz2 --output GS000013595_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013595-ASM.tsv.bz2 --output GS000013595_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

echo
for i in {1..22}; do
    samtools view -bS GS000013595_chr${i}.sam > GS000013595_chr${i}.bam
done
> ~/chdm_pipeline/cg/sam2bam.sh
#X&Y
samtools view -bS GS000013595_chr1.sam > GS000013595_chr1.bam &
samtools view -bS GS000013595_chr2.sam > GS000013595_chr2.bam &
samtools view -bS GS000013595_chr3.sam > GS000013595_chr3.bam &
samtools view -bS GS000013595_chr4.sam > GS000013595_chr4.bam &
samtools view -bS GS000013595_chr5.sam > GS000013595_chr5.bam &
samtools view -bS GS000013595_chr6.sam > GS000013595_chr6.bam &
samtools view -bS GS000013595_chr7.sam > GS000013595_chr7.bam &
samtools view -bS GS000013595_chr8.sam > GS000013595_chr8.bam &
samtools view -bS GS000013595_chr9.sam > GS000013595_chr9.bam &
samtools view -bS GS000013595_chr10.sam > GS000013595_chr10.bam &
samtools view -bS GS000013595_chr11.sam > GS000013595_chr11.bam &
samtools view -bS GS000013595_chr12.sam > GS000013595_chr12.bam &
samtools view -bS GS000013595_chr13.sam > GS000013595_chr13.bam &
samtools view -bS GS000013595_chr14.sam > GS000013595_chr14.bam &
samtools view -bS GS000013595_chr15.sam > GS000013595_chr15.bam &
samtools view -bS GS000013595_chr16.sam > GS000013595_chr16.bam &
samtools view -bS GS000013595_chr17.sam > GS000013595_chr17.bam &
samtools view -bS GS000013595_chr18.sam > GS000013595_chr18.bam &
samtools view -bS GS000013595_chr19.sam > GS000013595_chr19.bam &
samtools view -bS GS000013595_chr20.sam > GS000013595_chr20.bam &
samtools view -bS GS000013595_chr21.sam > GS000013595_chr21.bam &
samtools view -bS GS000013595_chr22.sam > GS000013595_chr22.bam &


echo
for i in {1..22}; do
    samtools sort GS000013595_chr${i}.bam GS000013595_chr${i}_sort
done
> ~/chdm_pipeline/cg/sortbam.sh
#X&Y
samtools sort GS000013595_chr1.bam GS000013595_chr1_sort &
samtools sort GS000013595_chr2.bam GS000013595_chr2_sort &
samtools sort GS000013595_chr3.bam GS000013595_chr3_sort &
samtools sort GS000013595_chr4.bam GS000013595_chr4_sort &
samtools sort GS000013595_chr5.bam GS000013595_chr5_sort &
samtools sort GS000013595_chr6.bam GS000013595_chr6_sort &
samtools sort GS000013595_chr7.bam GS000013595_chr7_sort &
samtools sort GS000013595_chr8.bam GS000013595_chr8_sort &
samtools sort GS000013595_chr9.bam GS000013595_chr9_sort &
samtools sort GS000013595_chr10.bam GS000013595_chr10_sort &
samtools sort GS000013595_chr11.bam GS000013595_chr11_sort &
samtools sort GS000013595_chr12.bam GS000013595_chr12_sort &
samtools sort GS000013595_chr13.bam GS000013595_chr13_sort &
samtools sort GS000013595_chr14.bam GS000013595_chr14_sort &
samtools sort GS000013595_chr15.bam GS000013595_chr15_sort &
samtools sort GS000013595_chr16.bam GS000013595_chr16_sort &
samtools sort GS000013595_chr17.bam GS000013595_chr17_sort &
samtools sort GS000013595_chr18.bam GS000013595_chr18_sort &
samtools sort GS000013595_chr19.bam GS000013595_chr19_sort &
samtools sort GS000013595_chr20.bam GS000013595_chr20_sort &
samtools sort GS000013595_chr21.bam GS000013595_chr21_sort &
samtools sort GS000013595_chr22.bam GS000013595_chr22_sort &


echo
for i in {1..22}; do
    samtools index GS000013595_chr${i}_sort.bam
done
> ~/chdm_pipeline/cg/bamindex.sh
#X&Y

load GS000013595_chr3.bam
snapshotDirectory ~/complete_genomics/data/output/sam/green
genome hg19
goto chr3:124998088-124998108
sort
collapse
snapshot




###593
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013593-ASM.tsv.bz2 --output GS000013593_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr1-GS000013593-ASM.tsv.bz2 --output GS000013593_chr1.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr2-GS000013593-ASM.tsv.bz2 --output GS000013593_chr2.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr3-GS000013593-ASM.tsv.bz2 --output GS000013593_chr3.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr4-GS000013593-ASM.tsv.bz2 --output GS000013593_chr4.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr5-GS000013593-ASM.tsv.bz2 --output GS000013593_chr5.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr6-GS000013593-ASM.tsv.bz2 --output GS000013593_chr6.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr7-GS000013593-ASM.tsv.bz2 --output GS000013593_chr7.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr8-GS000013593-ASM.tsv.bz2 --output GS000013593_chr8.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr9-GS000013593-ASM.tsv.bz2 --output GS000013593_chr9.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr10-GS000013593-ASM.tsv.bz2 --output GS000013593_chr10.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr11-GS000013593-ASM.tsv.bz2 --output GS000013593_chr11.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr12-GS000013593-ASM.tsv.bz2 --output GS000013593_chr12.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr13-GS000013593-ASM.tsv.bz2 --output GS000013593_chr13.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr14-GS000013593-ASM.tsv.bz2 --output GS000013593_chr14.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr15-GS000013593-ASM.tsv.bz2 --output GS000013593_chr15.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr16-GS000013593-ASM.tsv.bz2 --output GS000013593_chr16.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr17-GS000013593-ASM.tsv.bz2 --output GS000013593_chr17.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr18-GS000013593-ASM.tsv.bz2 --output GS000013593_chr18.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr19-GS000013593-ASM.tsv.bz2 --output GS000013593_chr19.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr20-GS000013593-ASM.tsv.bz2 --output GS000013593_chr20.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr21-GS000013593-ASM.tsv.bz2 --output GS000013593_chr21.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr22-GS000013593-ASM.tsv.bz2 --output GS000013593_chr22.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013593-ASM.tsv.bz2 --output GS000013593_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013593-ASM.tsv.bz2 --output GS000013593_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &


####REEEDDDDDOOOOOOOOO
mkdir redo
cp testvar_trio_gene_masterVar_dedup.tsv redo/
cd redo
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv

cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"
#1577
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"
#584
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#393
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#322
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"
#300
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#287

python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-13
mkdir /home/jeremiah/data/dm041/redo2
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv
#28438
cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
#5553
perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Input_FileB ~/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02" > out.out
#1389
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02" > out.out
#558
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#366
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#295
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "00"
#273
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#260
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -p GS000013593-ASM -g "11"
#234
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -p GS000013594-ASM -g "11"
#216
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -f GS000013593-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt9.tsv -f GS000013594-ASM -p GS000013595-ASM -g "01"
#204


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber > out.out


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim_take3.tsv -f MimNumber -s "*FIELD* NO" > out.out



##2013-01-03
##RRRRRRRRRRRrrrrrrrrrrrrrrrRRRRRrrrrrrrrrrrrrrrrr
library(ggplot2)
t593 <- read.table("~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/CNV/cnvDetailsDiploidBeta-GS000013593-ASM.tsv.bz2",header=TRUE)
t594 <- read.table("~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/CNV/cnvDetailsDiploidBeta-GS000013594-ASM.tsv.bz2",header=TRUE)
t595 <- read.table("~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/CNV/cnvDetailsDiploidBeta-GS000013595-ASM.tsv.bz2",header=TRUE)

t593_chr1 <- subset(t593, X.chr %in% c("chr1"))
t594_chr1 <- subset(t594, X.chr %in% c("chr1"))
t595_chr1 <- subset(t595, X.chr %in% c("chr1"))


t <- subset(t593_chr1, begin < 1000000)
ggplot(t,aes(x=begin,y=avgNormalizedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=gcCorrectedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=fractionUnique)) + geom_line()
ggplot(t,aes(x=begin,y=relativeCvg)) + geom_line()
t$group <- as.factor(rep("t593",nrow(t)))
u$group <- as.factor(rep("t594",nrow(u)))
v <- rbind(t,u)
ggplot(v,aes(x=begin,y=relativeCvg,group=group,colour=group)) + geom_point() + geom_line()


###2013-05-22
NOTE
593: father
594: mother
595: proband


###2013-06-21


python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table.tsv -o CNV_Diploid_Segments_Table_sub001.tsv --mothergeno 0 --fathergeno 0 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub001.tsv -o CNV_Diploid_Segments_Table_sub110.tsv --mothergeno 1 --fathergeno 1 --probandgeno 0

cp CNV_Diploid_Segments_Table_sub110.tsv CNV_Diploid_Segments_Table_sub110_sub001.tsv

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub110_sub001.tsv -o CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv --mothergeno 0 --fathergeno 1 --probandgeno 0

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i  CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv  -o  CNV_Diploid_Segments_Table_sub110_sub001_sub010_sub100.tsv  --mothergeno 1 --fathergeno 0 --probandgeno 0

###2013-10-12

cd /home/jeremiah/complete_genomics/output/121204/
mkdir 131012
cp testvar_trio_gene_masterVar.tsv 131012/
cd 131012/

cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

time awk ' !x[$1]++' testvar_trio_gene_masterVar_exon_nonsyn.tsv > testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv

cp testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv.bak

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv --match chromosome:chromosome --match end:begin --select 'a.*, b.MAF_esp6500si' --overlap-mode allow-abutting-points


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File estvar_trio_gene_masterVar_esp6500.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

##--2012-12-08
/usr/bin/time -v cgatools join --beta --input testvar_trio.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene.tsv --match chromosome:chromosome --select a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam --overlap begin,end:begin,end --overlap-mode allow-abutting-points

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene_masterVar.tsv --match chromosome:chromosome --overlap begin,end:begin,end --overlap-mode allow-abutting-points --select a.*,b.locus,b.ploidy,b.zygosity,b.varType,b.reference,b.allele1Seq,b.allele2Seq,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality,b.allele1HapLink,b.allele2HapLink,b.allele1XRef,b.allele2XRef,b.evidenceIntervalId,b.allele1ReadCount,b.allele2ReadCount,b.referenceAlleleReadCount,b.totalReadCount,b.allele1Gene,b.allele2Gene,b.pfam,b.miRBaseId,b.repeatMasker,b.segDupOverlap,b.relativeCoverageDiploid,b.calledPloidy,b.relativeCoverageNondiploid,b.calledLevel

cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar.tsv.bak
time awk ' !x[$1]++' testvar_trio_gene_masterVar.tsv > testvar_trio_gene_masterVar_dedup.tsv

wc -l testvar_trio_gene_masterVar_dedup.tsv
cp testvar_trio_gene_masterVar_dedup.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

wc -l testvar_trio_gene_masterVar_exon.tsv
cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

wc -l testvar_trio_gene_masterVar_exon_nonsyn.tsv

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_es6500si.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_esp6500si --overlap-mode allow-abutting-points

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl_cull.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome



/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si_1000g.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_1000g --overlap-mode allow-abutting-points


awk $

26*2 + M/13 + N/14
52+13=65 / 66
gawk ' $65 >= 0.02 ' testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv > testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_esp002.tsv
cgatools evidence2sam --beta --output e2s_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --extract-genomic-region chrX


###2012-12-09
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber

###2012-12-11
python ~/chdm_pipeline/omim/omim_parsers.py -i omim.txt -o omim_tab.txt > out.txt

python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-12
echo
for i in {1..22}; do
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013595-ASM.tsv.bz2 --output GS000013595_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr
done
> ~/chdm_pipeline/cg/evidence2sam.sh

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013595-ASM.tsv.bz2 --output GS000013595_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013595-ASM.tsv.bz2 --output GS000013595_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

echo
for i in {1..22}; do
    samtools view -bS GS000013595_chr${i}.sam > GS000013595_chr${i}.bam
done
> ~/chdm_pipeline/cg/sam2bam.sh
#X&Y
samtools view -bS GS000013595_chr1.sam > GS000013595_chr1.bam &
samtools view -bS GS000013595_chr2.sam > GS000013595_chr2.bam &
samtools view -bS GS000013595_chr3.sam > GS000013595_chr3.bam &
samtools view -bS GS000013595_chr4.sam > GS000013595_chr4.bam &
samtools view -bS GS000013595_chr5.sam > GS000013595_chr5.bam &
samtools view -bS GS000013595_chr6.sam > GS000013595_chr6.bam &
samtools view -bS GS000013595_chr7.sam > GS000013595_chr7.bam &
samtools view -bS GS000013595_chr8.sam > GS000013595_chr8.bam &
samtools view -bS GS000013595_chr9.sam > GS000013595_chr9.bam &
samtools view -bS GS000013595_chr10.sam > GS000013595_chr10.bam &
samtools view -bS GS000013595_chr11.sam > GS000013595_chr11.bam &
samtools view -bS GS000013595_chr12.sam > GS000013595_chr12.bam &
samtools view -bS GS000013595_chr13.sam > GS000013595_chr13.bam &
samtools view -bS GS000013595_chr14.sam > GS000013595_chr14.bam &
samtools view -bS GS000013595_chr15.sam > GS000013595_chr15.bam &
samtools view -bS GS000013595_chr16.sam > GS000013595_chr16.bam &
samtools view -bS GS000013595_chr17.sam > GS000013595_chr17.bam &
samtools view -bS GS000013595_chr18.sam > GS000013595_chr18.bam &
samtools view -bS GS000013595_chr19.sam > GS000013595_chr19.bam &
samtools view -bS GS000013595_chr20.sam > GS000013595_chr20.bam &
samtools view -bS GS000013595_chr21.sam > GS000013595_chr21.bam &
samtools view -bS GS000013595_chr22.sam > GS000013595_chr22.bam &


echo
for i in {1..22}; do
    samtools sort GS000013595_chr${i}.bam GS000013595_chr${i}_sort
done
> ~/chdm_pipeline/cg/sortbam.sh
#X&Y
samtools sort GS000013595_chr1.bam GS000013595_chr1_sort &
samtools sort GS000013595_chr2.bam GS000013595_chr2_sort &
samtools sort GS000013595_chr3.bam GS000013595_chr3_sort &
samtools sort GS000013595_chr4.bam GS000013595_chr4_sort &
samtools sort GS000013595_chr5.bam GS000013595_chr5_sort &
samtools sort GS000013595_chr6.bam GS000013595_chr6_sort &
samtools sort GS000013595_chr7.bam GS000013595_chr7_sort &
samtools sort GS000013595_chr8.bam GS000013595_chr8_sort &
samtools sort GS000013595_chr9.bam GS000013595_chr9_sort &
samtools sort GS000013595_chr10.bam GS000013595_chr10_sort &
samtools sort GS000013595_chr11.bam GS000013595_chr11_sort &
samtools sort GS000013595_chr12.bam GS000013595_chr12_sort &
samtools sort GS000013595_chr13.bam GS000013595_chr13_sort &
samtools sort GS000013595_chr14.bam GS000013595_chr14_sort &
samtools sort GS000013595_chr15.bam GS000013595_chr15_sort &
samtools sort GS000013595_chr16.bam GS000013595_chr16_sort &
samtools sort GS000013595_chr17.bam GS000013595_chr17_sort &
samtools sort GS000013595_chr18.bam GS000013595_chr18_sort &
samtools sort GS000013595_chr19.bam GS000013595_chr19_sort &
samtools sort GS000013595_chr20.bam GS000013595_chr20_sort &
samtools sort GS000013595_chr21.bam GS000013595_chr21_sort &
samtools sort GS000013595_chr22.bam GS000013595_chr22_sort &


echo
for i in {1..22}; do
    samtools index GS000013595_chr${i}_sort.bam
done
> ~/chdm_pipeline/cg/bamindex.sh
#X&Y

load GS000013595_chr3.bam
snapshotDirectory ~/complete_genomics/data/output/sam/green
genome hg19
goto chr3:124998088-124998108
sort
collapse
snapshot




###593
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013593-ASM.tsv.bz2 --output GS000013593_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr1-GS000013593-ASM.tsv.bz2 --output GS000013593_chr1.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr2-GS000013593-ASM.tsv.bz2 --output GS000013593_chr2.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr3-GS000013593-ASM.tsv.bz2 --output GS000013593_chr3.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr4-GS000013593-ASM.tsv.bz2 --output GS000013593_chr4.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr5-GS000013593-ASM.tsv.bz2 --output GS000013593_chr5.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr6-GS000013593-ASM.tsv.bz2 --output GS000013593_chr6.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr7-GS000013593-ASM.tsv.bz2 --output GS000013593_chr7.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr8-GS000013593-ASM.tsv.bz2 --output GS000013593_chr8.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr9-GS000013593-ASM.tsv.bz2 --output GS000013593_chr9.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr10-GS000013593-ASM.tsv.bz2 --output GS000013593_chr10.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr11-GS000013593-ASM.tsv.bz2 --output GS000013593_chr11.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr12-GS000013593-ASM.tsv.bz2 --output GS000013593_chr12.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr13-GS000013593-ASM.tsv.bz2 --output GS000013593_chr13.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr14-GS000013593-ASM.tsv.bz2 --output GS000013593_chr14.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr15-GS000013593-ASM.tsv.bz2 --output GS000013593_chr15.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr16-GS000013593-ASM.tsv.bz2 --output GS000013593_chr16.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr17-GS000013593-ASM.tsv.bz2 --output GS000013593_chr17.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr18-GS000013593-ASM.tsv.bz2 --output GS000013593_chr18.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr19-GS000013593-ASM.tsv.bz2 --output GS000013593_chr19.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr20-GS000013593-ASM.tsv.bz2 --output GS000013593_chr20.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr21-GS000013593-ASM.tsv.bz2 --output GS000013593_chr21.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr22-GS000013593-ASM.tsv.bz2 --output GS000013593_chr22.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013593-ASM.tsv.bz2 --output GS000013593_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013593-ASM.tsv.bz2 --output GS000013593_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &


####REEEDDDDDOOOOOOOOO
mkdir redo
cp testvar_trio_gene_masterVar_dedup.tsv redo/
cd redo
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv

cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"
#1577
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"
#584
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#393
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#322
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"
#300
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#287

python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-13
mkdir /home/jeremiah/data/dm041/redo2
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv
#28438
cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
#5553
perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Input_FileB ~/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02" > out.out
#1389
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02" > out.out
#558
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#366
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#295
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "00"
#273
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#260
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -p GS000013593-ASM -g "11"
#234
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -p GS000013594-ASM -g "11"
#216
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -f GS000013593-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt9.tsv -f GS000013594-ASM -p GS000013595-ASM -g "01"
#204


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber > out.out


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim_take3.tsv -f MimNumber -s "*FIELD* NO" > out.out



##2013-01-03
##RRRRRRRRRRRrrrrrrrrrrrrrrrRRRRRrrrrrrrrrrrrrrrrr
library(ggplot2)
t593 <- read.table("~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/CNV/cnvDetailsDiploidBeta-GS000013593-ASM.tsv.bz2",header=TRUE)
t594 <- read.table("~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/CNV/cnvDetailsDiploidBeta-GS000013594-ASM.tsv.bz2",header=TRUE)
t595 <- read.table("~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/CNV/cnvDetailsDiploidBeta-GS000013595-ASM.tsv.bz2",header=TRUE)

t593_chr1 <- subset(t593, X.chr %in% c("chr1"))
t594_chr1 <- subset(t594, X.chr %in% c("chr1"))
t595_chr1 <- subset(t595, X.chr %in% c("chr1"))


t <- subset(t593_chr1, begin < 1000000)
ggplot(t,aes(x=begin,y=avgNormalizedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=gcCorrectedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=fractionUnique)) + geom_line()
ggplot(t,aes(x=begin,y=relativeCvg)) + geom_line()
t$group <- as.factor(rep("t593",nrow(t)))
u$group <- as.factor(rep("t594",nrow(u)))
v <- rbind(t,u)
ggplot(v,aes(x=begin,y=relativeCvg,group=group,colour=group)) + geom_point() + geom_line()


###2013-05-22
NOTE
593: father
594: mother
595: proband


###2013-06-21


python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table.tsv -o CNV_Diploid_Segments_Table_sub001.tsv --mothergeno 0 --fathergeno 0 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub001.tsv -o CNV_Diploid_Segments_Table_sub110.tsv --mothergeno 1 --fathergeno 1 --probandgeno 0

cp CNV_Diploid_Segments_Table_sub110.tsv CNV_Diploid_Segments_Table_sub110_sub001.tsv

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub110_sub001.tsv -o CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv --mothergeno 0 --fathergeno 1 --probandgeno 0

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i  CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv  -o  CNV_Diploid_Segments_Table_sub110_sub001_sub010_sub100.tsv  --mothergeno 1 --fathergeno 0 --probandgeno 0

###2013-10-12

cd /home/jeremiah/complete_genomics/output/121204/
mkdir 131012
cp testvar_trio_gene_masterVar.tsv 131012/
cd 131012/
cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

time awk ' !x[$1]++' testvar_trio_gene_masterVar_exon_nonsyn.tsv > testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv

cp testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv.bak

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv --match chromosome:chromosome --match end:begin --select 'a.*, b.MAF_esp6500si' --overlap-mode allow-abutting-points


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File estvar_trio_gene_masterVar_esp6500.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

##--2012-12-08
/usr/bin/time -v cgatools join --beta --input testvar_trio.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/gene-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene.tsv --match chromosome:chromosome --select a.*,b.geneId,b.mrnaAcc,b.proteinAcc,b.symbol,b.orientation,b.component,b.componentIndex,b.hasCodingRegion,b.impact,b.nucleotidePos,b.proteinPos,b.annotationRefSequence,b.sampleSequence,b.genomeRefSequence,b.pfam --overlap begin,end:begin,end --overlap-mode allow-abutting-points

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene.tsv ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/masterVarBeta-GS000013595-ASM.tsv.bz2 --output testvar_trio_gene_masterVar.tsv --match chromosome:chromosome --overlap begin,end:begin,end --overlap-mode allow-abutting-points --select a.*,b.locus,b.ploidy,b.zygosity,b.varType,b.reference,b.allele1Seq,b.allele2Seq,b.allele1VarScoreVAF,b.allele2VarScoreVAF,b.allele1VarScoreEAF,b.allele2VarScoreEAF,b.allele1VarQuality,b.allele2VarQuality,b.allele1HapLink,b.allele2HapLink,b.allele1XRef,b.allele2XRef,b.evidenceIntervalId,b.allele1ReadCount,b.allele2ReadCount,b.referenceAlleleReadCount,b.totalReadCount,b.allele1Gene,b.allele2Gene,b.pfam,b.miRBaseId,b.repeatMasker,b.segDupOverlap,b.relativeCoverageDiploid,b.calledPloidy,b.relativeCoverageNondiploid,b.calledLevel

cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar.tsv.bak
time awk ' !x[$1]++' testvar_trio_gene_masterVar.tsv > testvar_trio_gene_masterVar_dedup.tsv

wc -l testvar_trio_gene_masterVar_dedup.tsv
cp testvar_trio_gene_masterVar_dedup.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

wc -l testvar_trio_gene_masterVar_exon.tsv
cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

wc -l testvar_trio_gene_masterVar_exon_nonsyn.tsv

/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn.tsv ~/complete_genomics/data/hg19_esp6500si_all.txt --output testvar_trio_gene_masterVar_exon_nonsyn_es6500si.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_esp6500si --overlap-mode allow-abutting-points

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl_cull.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome



/usr/bin/time -v cgatools join --beta --input testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si.tsv ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --output testvar_trio_gene_masterVar_exon_nonsyn_dedup_es6500si_1000g.tsv --match chromosome:chromosome --match end:begin --select a.*,b.MAF_1000g --overlap-mode allow-abutting-points


awk $

26*2 + M/13 + N/14
52+13=65 / 66
gawk ' $65 >= 0.02 ' testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv > testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_esp002.tsv
cgatools evidence2sam --beta --output e2s_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr --extract-genomic-region chrX


###2012-12-09
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_cull.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"

python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber

###2012-12-11
python ~/chdm_pipeline/omim/omim_parsers.py -i omim.txt -o omim_tab.txt > out.txt

python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-12
echo
for i in {1..22}; do
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013595-ASM.tsv.bz2 --output GS000013595_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr
done
> ~/chdm_pipeline/cg/evidence2sam.sh

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013595-ASM.tsv.bz2 --output GS000013595_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013595-ASM.tsv.bz2 --output GS000013595_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

echo
for i in {1..22}; do
    samtools view -bS GS000013595_chr${i}.sam > GS000013595_chr${i}.bam
done
> ~/chdm_pipeline/cg/sam2bam.sh
#X&Y
samtools view -bS GS000013595_chr1.sam > GS000013595_chr1.bam &
samtools view -bS GS000013595_chr2.sam > GS000013595_chr2.bam &
samtools view -bS GS000013595_chr3.sam > GS000013595_chr3.bam &
samtools view -bS GS000013595_chr4.sam > GS000013595_chr4.bam &
samtools view -bS GS000013595_chr5.sam > GS000013595_chr5.bam &
samtools view -bS GS000013595_chr6.sam > GS000013595_chr6.bam &
samtools view -bS GS000013595_chr7.sam > GS000013595_chr7.bam &
samtools view -bS GS000013595_chr8.sam > GS000013595_chr8.bam &
samtools view -bS GS000013595_chr9.sam > GS000013595_chr9.bam &
samtools view -bS GS000013595_chr10.sam > GS000013595_chr10.bam &
samtools view -bS GS000013595_chr11.sam > GS000013595_chr11.bam &
samtools view -bS GS000013595_chr12.sam > GS000013595_chr12.bam &
samtools view -bS GS000013595_chr13.sam > GS000013595_chr13.bam &
samtools view -bS GS000013595_chr14.sam > GS000013595_chr14.bam &
samtools view -bS GS000013595_chr15.sam > GS000013595_chr15.bam &
samtools view -bS GS000013595_chr16.sam > GS000013595_chr16.bam &
samtools view -bS GS000013595_chr17.sam > GS000013595_chr17.bam &
samtools view -bS GS000013595_chr18.sam > GS000013595_chr18.bam &
samtools view -bS GS000013595_chr19.sam > GS000013595_chr19.bam &
samtools view -bS GS000013595_chr20.sam > GS000013595_chr20.bam &
samtools view -bS GS000013595_chr21.sam > GS000013595_chr21.bam &
samtools view -bS GS000013595_chr22.sam > GS000013595_chr22.bam &


echo
for i in {1..22}; do
    samtools sort GS000013595_chr${i}.bam GS000013595_chr${i}_sort
done
> ~/chdm_pipeline/cg/sortbam.sh
#X&Y
samtools sort GS000013595_chr1.bam GS000013595_chr1_sort &
samtools sort GS000013595_chr2.bam GS000013595_chr2_sort &
samtools sort GS000013595_chr3.bam GS000013595_chr3_sort &
samtools sort GS000013595_chr4.bam GS000013595_chr4_sort &
samtools sort GS000013595_chr5.bam GS000013595_chr5_sort &
samtools sort GS000013595_chr6.bam GS000013595_chr6_sort &
samtools sort GS000013595_chr7.bam GS000013595_chr7_sort &
samtools sort GS000013595_chr8.bam GS000013595_chr8_sort &
samtools sort GS000013595_chr9.bam GS000013595_chr9_sort &
samtools sort GS000013595_chr10.bam GS000013595_chr10_sort &
samtools sort GS000013595_chr11.bam GS000013595_chr11_sort &
samtools sort GS000013595_chr12.bam GS000013595_chr12_sort &
samtools sort GS000013595_chr13.bam GS000013595_chr13_sort &
samtools sort GS000013595_chr14.bam GS000013595_chr14_sort &
samtools sort GS000013595_chr15.bam GS000013595_chr15_sort &
samtools sort GS000013595_chr16.bam GS000013595_chr16_sort &
samtools sort GS000013595_chr17.bam GS000013595_chr17_sort &
samtools sort GS000013595_chr18.bam GS000013595_chr18_sort &
samtools sort GS000013595_chr19.bam GS000013595_chr19_sort &
samtools sort GS000013595_chr20.bam GS000013595_chr20_sort &
samtools sort GS000013595_chr21.bam GS000013595_chr21_sort &
samtools sort GS000013595_chr22.bam GS000013595_chr22_sort &


echo
for i in {1..22}; do
    samtools index GS000013595_chr${i}_sort.bam
done
> ~/chdm_pipeline/cg/bamindex.sh
#X&Y

load GS000013595_chr3.bam
snapshotDirectory ~/complete_genomics/data/output/sam/green
genome hg19
goto chr3:124998088-124998108
sort
collapse
snapshot




###593
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/EVIDENCE/evidenceDnbs-chr${i}-GS000013593-ASM.tsv.bz2 --output GS000013593_chr${i}.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr

cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr1-GS000013593-ASM.tsv.bz2 --output GS000013593_chr1.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr2-GS000013593-ASM.tsv.bz2 --output GS000013593_chr2.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr3-GS000013593-ASM.tsv.bz2 --output GS000013593_chr3.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr4-GS000013593-ASM.tsv.bz2 --output GS000013593_chr4.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr5-GS000013593-ASM.tsv.bz2 --output GS000013593_chr5.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr6-GS000013593-ASM.tsv.bz2 --output GS000013593_chr6.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr7-GS000013593-ASM.tsv.bz2 --output GS000013593_chr7.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr8-GS000013593-ASM.tsv.bz2 --output GS000013593_chr8.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr9-GS000013593-ASM.tsv.bz2 --output GS000013593_chr9.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr10-GS000013593-ASM.tsv.bz2 --output GS000013593_chr10.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr11-GS000013593-ASM.tsv.bz2 --output GS000013593_chr11.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr12-GS000013593-ASM.tsv.bz2 --output GS000013593_chr12.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr13-GS000013593-ASM.tsv.bz2 --output GS000013593_chr13.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr14-GS000013593-ASM.tsv.bz2 --output GS000013593_chr14.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr15-GS000013593-ASM.tsv.bz2 --output GS000013593_chr15.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr16-GS000013593-ASM.tsv.bz2 --output GS000013593_chr16.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr17-GS000013593-ASM.tsv.bz2 --output GS000013593_chr17.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr18-GS000013593-ASM.tsv.bz2 --output GS000013593_chr18.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr19-GS000013593-ASM.tsv.bz2 --output GS000013593_chr19.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr20-GS000013593-ASM.tsv.bz2 --output GS000013593_chr20.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr21-GS000013593-ASM.tsv.bz2 --output GS000013593_chr21.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chr22-GS000013593-ASM.tsv.bz2 --output GS000013593_chr22.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrX-GS000013593-ASM.tsv.bz2 --output GS000013593_chrX.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &
cgatools evidence2sam --beta --evidence-dnbs ~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/EVIDENCE/evidenceDnbs-chrY-GS000013593-ASM.tsv.bz2 --output GS000013593_chrY.sam --reference ~/complete_genomics/data/ReferenceFiles/build37.crr &


####REEEDDDDDOOOOOOOOO
mkdir redo
cp testvar_trio_gene_masterVar_dedup.tsv redo/
cd redo
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv

cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_cull.tsv --Input_FileB ~/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02"
#1577
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02"
#584
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#393
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#322
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "0"
#300
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#287

python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim.tsv -f MimNumber -s "*FIELD* NO"

###2012-12-13
mkdir /home/jeremiah/data/dm041/redo2
python ~/chdm_pipeline/cg/cg_keepers.py -i testvar_trio_gene_masterVar_dedup.tsv -o testvar_trio_gene_masterVar_exon.tsv
#28438
cp testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/\tSYNONYMOUS/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
#5553
perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn.tsv --Input_FileB ~/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


perl ~/tools/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_exon_nonsyn_es6500si_perl.tsv --Input_FileB ~/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome


python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -c MAF_esp6500si -v "0.02" > out.out
#1389
python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -c MAF_1000g -v "0.02" > out.out
#558
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_002b.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "11"
#366
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -p GS000013595-ASM -g "NN"
#295
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt2.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -p GS000013595-ASM -g "00"
#273
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt3.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -p GS000013595-ASM -g "0N"
#260
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt4.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -p GS000013593-ASM -g "11"
#234
python ~/chdm_pipeline/cg/ind_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt5.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -p GS000013594-ASM -g "11"
#216
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt6.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -f GS000013593-ASM -p GS000013595-ASM -g "01"
#204
python ~/chdm_pipeline/cg/duo_filt_remove.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt8.tsv -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt9.tsv -f GS000013594-ASM -p GS000013595-ASM -g "01"
#204


python ~/chdm_pipeline/cg/tsv_merge_mimnum.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt7.tsv -m ~/tools/omim/mim2gene.txt -o testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -t symbol -s ApprovedGeneSymbols -n MimNumber > out.out


python ~/chdm_pipeline/cg/tsv_merge_mimvals.py -i testvar_trio_gene_masterVar_exon_nonsyn_es6500si_1000g_filt_omim.tsv -m ~/tools/omim/omim_tab.txt -o dm041_omim_take3.tsv -f MimNumber -s "*FIELD* NO" > out.out



##2013-01-03
##RRRRRRRRRRRrrrrrrrrrrrrrrrRRRRRrrrrrrrrrrrrrrrrr
library(ggplot2)
t593 <- read.table("~/complete_genomics/GS000018263-DID/GS000013593-ASM/GS01538-DNA_A01/ASM/CNV/cnvDetailsDiploidBeta-GS000013593-ASM.tsv.bz2",header=TRUE)
t594 <- read.table("~/complete_genomics/GS000018264-DID/GS000013594-ASM/GS01538-DNA_B01/ASM/CNV/cnvDetailsDiploidBeta-GS000013594-ASM.tsv.bz2",header=TRUE)
t595 <- read.table("~/complete_genomics/GS000018265-DID/GS000013595-ASM/GS01538-DNA_C01/ASM/CNV/cnvDetailsDiploidBeta-GS000013595-ASM.tsv.bz2",header=TRUE)

t593_chr1 <- subset(t593, X.chr %in% c("chr1"))
t594_chr1 <- subset(t594, X.chr %in% c("chr1"))
t595_chr1 <- subset(t595, X.chr %in% c("chr1"))


t <- subset(t593_chr1, begin < 1000000)
ggplot(t,aes(x=begin,y=avgNormalizedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=gcCorrectedCvg)) + geom_line()
ggplot(t,aes(x=begin,y=fractionUnique)) + geom_line()
ggplot(t,aes(x=begin,y=relativeCvg)) + geom_line()
t$group <- as.factor(rep("t593",nrow(t)))
u$group <- as.factor(rep("t594",nrow(u)))
v <- rbind(t,u)
ggplot(v,aes(x=begin,y=relativeCvg,group=group,colour=group)) + geom_point() + geom_line()


###2013-05-22
NOTE
593: father
594: mother
595: proband


###2013-06-21


python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table.tsv -o CNV_Diploid_Segments_Table_sub001.tsv --mothergeno 0 --fathergeno 0 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub001.tsv -o CNV_Diploid_Segments_Table_sub110.tsv --mothergeno 1 --fathergeno 1 --probandgeno 0

cp CNV_Diploid_Segments_Table_sub110.tsv CNV_Diploid_Segments_Table_sub110_sub001.tsv

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i CNV_Diploid_Segments_Table_sub110_sub001.tsv -o CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv --mothergeno 0 --fathergeno 1 --probandgeno 0

python ~/chdm_pipeline/cg/trio_filt_remove.py -m 13594 -f 13593 -p 13595 -i  CNV_Diploid_Segments_Table_sub110_sub001_sub010.tsv  -o  CNV_Diploid_Segments_Table_sub110_sub001_sub010_sub100.tsv  --mothergeno 1 --fathergeno 0 --probandgeno 0

###2013-10-12

cd /home/jeremiah/complete_genomics/output/121204/
mkdir 131012
cp testvar_trio_gene_masterVar.tsv 131012/
cd 131012/
cp testvar_trio_gene_masterVar.tsv testvar_trio_gene_masterVar_exon.tsv

time sed -i '/INTRON/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/TSS-UPSTREAM/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR3/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR5/d' testvar_trio_gene_masterVar_exon.tsv
time sed -i '/UTR/d' testvar_trio_gene_masterVar_exon.tsv

cp -a testvar_trio_gene_masterVar_exon.tsv testvar_trio_gene_masterVar_exon_nonsyn.tsv

time sed -i '/\tSYN/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
#time sed -i '/NO-CHANGE/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/VQLOW/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv
time sed -i '/no-call/d' testvar_trio_gene_masterVar_exon_nonsyn.tsv


### lost control chr9 KCNT1 control, so
ack SENSE testvar_trio_gene_masterVar.tsv > testvar_trio_gene_masterVar_SENSE.tsv

head -n 20 testvar_trio_gene_masterVar.tsv > testvar_header.tsv

cat testvar_header.tsv testvar_trio_gene_masterVar_SENSE.tsv > temp.tsv
mv temp.tsv testvar_trio_gene_masterVar_SENSE.tsv

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_SENSE.tsv -o testvar_trio_gene_masterVar_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt.tsv -o testvar_trio_gene_masterVar_filt2.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM --fathergeno 11 --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt2.tsv -o testvar_trio_gene_masterVar_filt3.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4.tsv -o testvar_trio_gene_masterVar_filt5.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt5.tsv -o testvar_trio_gene_masterVar_filt6.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt6.tsv -o testvar_trio_gene_masterVar_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt8.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt8.tsv -o testvar_trio_gene_masterVar_filt9.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt9.tsv -o testvar_trio_gene_masterVar_filt10.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt11.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt11.tsv -o testvar_trio_gene_masterVar_filt12.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt12a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt13.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt13.tsv -o testvar_trio_gene_masterVar_filt14.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt15.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt15.tsv -o testvar_trio_gene_masterVar_filt16.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt16.tsv -o testvar_trio_gene_masterVar_filt17.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt17.tsv -o testvar_trio_gene_masterVar_filt18.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt18.tsv -o testvar_trio_gene_masterVar_filt19.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt19.tsv -o testvar_trio_gene_masterVar_filt20.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt20.tsv -o testvar_trio_gene_masterVar_filt21.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt21.tsv -o testvar_trio_gene_masterVar_filt22.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt22.tsv -o testvar_trio_gene_masterVar_filt23.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt23.tsv -o testvar_trio_gene_masterVar_filt24.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt24.tsv -o testvar_trio_gene_masterVar_filt25.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt25.tsv -o testvar_trio_gene_masterVar_filt26.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt26.tsv -o testvar_trio_gene_masterVar_filt27.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt28.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt28.tsv -o testvar_trio_gene_masterVar_filt29.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt29.tsv -o testvar_trio_gene_masterVar_filt30.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt30.tsv -o testvar_trio_gene_masterVar_filt31.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt31.tsv -o testvar_trio_gene_masterVar_filt32.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt32.tsv -o testvar_trio_gene_masterVar_filt33.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt33.tsv -o testvar_trio_gene_masterVar_filt34.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt34.tsv -o testvar_trio_gene_masterVar_filt35.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt35.tsv -o testvar_trio_gene_masterVar_filt36.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt36.tsv -o testvar_trio_gene_masterVar_filt37.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt37.tsv -o testvar_trio_gene_masterVar_filt38.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt38.tsv -o testvar_trio_gene_masterVar_filt39.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 01 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt39.tsv -o testvar_trio_gene_masterVar_filt40.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt40.tsv -o testvar_trio_gene_masterVar_filt41.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt41.tsv -o testvar_trio_gene_masterVar_filt42.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt42.tsv -o testvar_trio_gene_masterVar_filt43.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43.tsv -o testvar_trio_gene_masterVar_filt43a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43a.tsv -o testvar_trio_gene_masterVar_filt43b.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43b.tsv -o testvar_trio_gene_masterVar_filt44.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt44.tsv -o testvar_trio_gene_masterVar_filt45.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt45.tsv -o testvar_trio_gene_masterVar_filt46.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt46.tsv -o testvar_trio_gene_masterVar_filt47.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt47.tsv -o testvar_trio_gene_masterVar_filt48.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt48.tsv -o testvar_trio_gene_masterVar_filt49.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt49.tsv -o testvar_trio_gene_masterVar_filt50.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt50.tsv -o testvar_trio_gene_masterVar_filt51.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt51.tsv -o testvar_trio_gene_masterVar_filt52.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt52.tsv -o testvar_trio_gene_masterVar_filt53.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt53.tsv -o testvar_trio_gene_masterVar_filt54.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt54.tsv -o testvar_trio_gene_masterVar_filt55.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt55.tsv -o testvar_trio_gene_masterVar_filt56.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt56.tsv -o testvar_trio_gene_masterVar_filt57.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt57.tsv -o testvar_trio_gene_masterVar_filt58.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt58.tsv -o testvar_trio_gene_masterVar_filt59.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt59.tsv -o testvar_trio_gene_masterVar_filt60.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt60.tsv -o testvar_trio_gene_masterVar_filt61.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt61.tsv -o testvar_trio_gene_masterVar_filt62.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt62.tsv -o testvar_trio_gene_masterVar_filt63.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt63.tsv -o testvar_trio_gene_masterVar_filt64.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt64.tsv -o testvar_trio_gene_masterVar_filt65.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt65.tsv -o testvar_trio_gene_masterVar_filt66.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt66.tsv -o testvar_trio_gene_masterVar_filt67.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt67.tsv -o testvar_trio_gene_masterVar_filt68.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt68.tsv -o testvar_trio_gene_masterVar_filt69.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt69.tsv -o testvar_trio_gene_masterVar_filt70.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt70.tsv -o testvar_trio_gene_masterVar_filt71.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt71.tsv -o testvar_trio_gene_masterVar_filt72.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt72.tsv -o testvar_trio_gene_masterVar_filt73.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt73.tsv -o testvar_trio_gene_masterVar_filt74.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt74.tsv -o testvar_trio_gene_masterVar_filt75.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt75.tsv -o testvar_trio_gene_masterVar_filt76.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt76.tsv -o testvar_trio_gene_masterVar_filt77.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt77.tsv -o testvar_trio_gene_masterVar_filt78.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt78.tsv -o testvar_trio_gene_masterVar_filt79.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt79.tsv -o testvar_trio_gene_masterVar_filt80.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt80.tsv -o testvar_trio_gene_masterVar_filt81.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt81.tsv -o testvar_trio_gene_masterVar_filt82.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt82.tsv -o testvar_trio_gene_masterVar_filt83.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt83.tsv -o testvar_trio_gene_masterVar_filt84.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt84.tsv -o testvar_trio_gene_masterVar_filt85.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt85.tsv -o testvar_trio_gene_masterVar_filt86.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt86.tsv -o testvar_trio_gene_masterVar_filt87.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt87.tsv -o testvar_trio_gene_masterVar_filt88.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt88.tsv -o testvar_trio_gene_masterVar_filt89.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt89.tsv -o testvar_trio_gene_masterVar_filt90.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt90.tsv -o testvar_trio_gene_masterVar_filt91.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1 --probandgeno 1


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt91.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_filt91_esp6500si.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt91_esp6500si.tsv --Input_FileB /home/jeremiah/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_filt91_esp6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_filt91_esp6500si_1000g.tsv -o testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02a.tsv -c MAF_esp6500si -v "0.02" > out.out

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02a.tsv -o testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b.tsv -c  MAF_1000g -v "0.02" > out.out

time awk ' !x[$1]++' testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b.tsv > testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b_dedup.tsv


####DISRUPT
head -n 20 ../testvar_trio_gene_masterVar.tsv > testvar_header.tsv

cat testvar_header.tsv testvar_trio_gene_masterVar_disrupt.tsv > temp.tsv
mv temp.tsv testvar_trio_gene_masterVar_disrupt.tsv


python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_disrupt.tsv -o testvar_trio_gene_masterVar_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt.tsv -o testvar_trio_gene_masterVar_filt2.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM --fathergeno 11 --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt2.tsv -o testvar_trio_gene_masterVar_filt3.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4.tsv -o testvar_trio_gene_masterVar_filt5.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt5.tsv -o testvar_trio_gene_masterVar_filt6.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt6.tsv -o testvar_trio_gene_masterVar_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt8.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt8.tsv -o testvar_trio_gene_masterVar_filt9.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt9.tsv -o testvar_trio_gene_masterVar_filt10.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt11.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt11.tsv -o testvar_trio_gene_masterVar_filt12.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt12a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt13.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt13.tsv -o testvar_trio_gene_masterVar_filt14.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt15.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt15.tsv -o testvar_trio_gene_masterVar_filt16.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt16.tsv -o testvar_trio_gene_masterVar_filt17.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt17.tsv -o testvar_trio_gene_masterVar_filt18.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt18.tsv -o testvar_trio_gene_masterVar_filt19.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt19.tsv -o testvar_trio_gene_masterVar_filt20.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt20.tsv -o testvar_trio_gene_masterVar_filt21.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt21.tsv -o testvar_trio_gene_masterVar_filt22.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt22.tsv -o testvar_trio_gene_masterVar_filt23.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt23.tsv -o testvar_trio_gene_masterVar_filt24.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt24.tsv -o testvar_trio_gene_masterVar_filt25.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt25.tsv -o testvar_trio_gene_masterVar_filt26.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt26.tsv -o testvar_trio_gene_masterVar_filt27.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt28.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt28.tsv -o testvar_trio_gene_masterVar_filt29.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt29.tsv -o testvar_trio_gene_masterVar_filt30.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt30.tsv -o testvar_trio_gene_masterVar_filt31.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt31.tsv -o testvar_trio_gene_masterVar_filt32.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt32.tsv -o testvar_trio_gene_masterVar_filt33.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt33.tsv -o testvar_trio_gene_masterVar_filt34.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt34.tsv -o testvar_trio_gene_masterVar_filt35.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt35.tsv -o testvar_trio_gene_masterVar_filt36.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt36.tsv -o testvar_trio_gene_masterVar_filt37.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt37.tsv -o testvar_trio_gene_masterVar_filt38.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt38.tsv -o testvar_trio_gene_masterVar_filt39.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 01 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt39.tsv -o testvar_trio_gene_masterVar_filt40.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt40.tsv -o testvar_trio_gene_masterVar_filt41.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt41.tsv -o testvar_trio_gene_masterVar_filt42.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt42.tsv -o testvar_trio_gene_masterVar_filt43.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43.tsv -o testvar_trio_gene_masterVar_filt43a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43a.tsv -o testvar_trio_gene_masterVar_filt44.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt44.tsv -o testvar_trio_gene_masterVar_filt44a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt44a.tsv -o testvar_trio_gene_masterVar_filt45.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt45.tsv -o testvar_trio_gene_masterVar_filt46.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt46.tsv -o testvar_trio_gene_masterVar_filt47.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt47.tsv -o testvar_trio_gene_masterVar_filt48.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt48.tsv -o testvar_trio_gene_masterVar_filt49.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt49.tsv -o testvar_trio_gene_masterVar_filt50.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt50.tsv -o testvar_trio_gene_masterVar_filt51.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt51.tsv -o testvar_trio_gene_masterVar_filt52.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt52.tsv -o testvar_trio_gene_masterVar_filt53.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt53.tsv -o testvar_trio_gene_masterVar_filt54.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt54.tsv -o testvar_trio_gene_masterVar_filt55.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt55.tsv -o testvar_trio_gene_masterVar_filt56.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt56.tsv -o testvar_trio_gene_masterVar_filt57.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt57.tsv -o testvar_trio_gene_masterVar_filt58.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt58.tsv -o testvar_trio_gene_masterVar_filt59.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt59.tsv -o testvar_trio_gene_masterVar_filt60.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt60.tsv -o testvar_trio_gene_masterVar_filt61.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt61.tsv -o testvar_trio_gene_masterVar_filt62.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt62.tsv -o testvar_trio_gene_masterVar_filt63.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt63.tsv -o testvar_trio_gene_masterVar_filt64.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt64.tsv -o testvar_trio_gene_masterVar_filt65.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt65.tsv -o testvar_trio_gene_masterVar_filt66.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt66.tsv -o testvar_trio_gene_masterVar_filt67.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt67.tsv -o testvar_trio_gene_masterVar_filt68.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt68.tsv -o testvar_trio_gene_masterVar_filt69.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt69.tsv -o testvar_trio_gene_masterVar_filt70.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt70.tsv -o testvar_trio_gene_masterVar_filt71.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt71.tsv -o testvar_trio_gene_masterVar_filt72.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt72.tsv -o testvar_trio_gene_masterVar_filt73.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt73.tsv -o testvar_trio_gene_masterVar_filt74.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt74.tsv -o testvar_trio_gene_masterVar_filt75.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt75.tsv -o testvar_trio_gene_masterVar_filt76.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt76.tsv -o testvar_trio_gene_masterVar_filt77.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt77.tsv -o testvar_trio_gene_masterVar_filt78.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt78.tsv -o testvar_trio_gene_masterVar_filt79.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt79.tsv -o testvar_trio_gene_masterVar_filt80.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt80.tsv -o testvar_trio_gene_masterVar_filt81.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt81.tsv -o testvar_trio_gene_masterVar_filt82.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt82.tsv -o testvar_trio_gene_masterVar_filt83.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt83.tsv -o testvar_trio_gene_masterVar_filt84.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt84.tsv -o testvar_trio_gene_masterVar_filt85.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt85.tsv -o testvar_trio_gene_masterVar_filt86.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt86.tsv -o testvar_trio_gene_masterVar_filt87.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt87.tsv -o testvar_trio_gene_masterVar_filt88.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt88.tsv -o testvar_trio_gene_masterVar_filt89.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt89.tsv -o testvar_trio_gene_masterVar_filt90.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt90.tsv -o testvar_trio_gene_masterVar_filt91.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1 --probandgeno 1


perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt91.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_filt91_esp6500si.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt91_esp6500si.tsv --Input_FileB /home/jeremiah/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_filt91_esp6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_filt91_esp6500si_1000g.tsv -o testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02a.tsv -c MAF_esp6500si -v "0.02" > out.out

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02a.tsv -o testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b.tsv -c  MAF_1000g -v "0.02" > out.out

time awk ' !x[$1]++' testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b.tsv > testvar_trio_gene_masterVar_filt91_esp6500si_1000g_02b_dedup.tsv

cp dm41_exon_dedup.tsv dm41_exon_dedup_vqhigh.tsv
sed -i '/VQLOW/d' dm41_exon_dedup_vqhigh.tsv

cp dm41_disrupt_dedup.tsv dm41_disrupt_dedup_vqhigh.tsv
sed -i '/VQLOW/d' dm41_disrupt_dedup_vqhigh.tsv

####################
###13-10-14 redo
####################
mkdir 131014_redo
cp testvar_trio_gene_masterVar.tsv 131014_redo/
cd 131014_redo/
mkdir exon
cd exon

python3 ~/chdm_pipeline/cg/pyawk.py -i ../testvar_trio_gene_masterVar.tsv -o testvar_trio_gene_masterVar_exon.tsv -f component -v "CDS" -m ">"

#head -n 19 ../testvar_trio_gene_masterVar.tsv > testvar_header.tsv

#cat testvar_header.tsv testvar_trio_gene_masterVar_exon.tsv > temp.tsv
#mv temp.tsv testvar_trio_gene_masterVar_exon.tsv



python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_exon.tsv -o testvar_trio_gene_masterVar_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt.tsv -o testvar_trio_gene_masterVar_filt2.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM --fathergeno 11 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt2.tsv -o testvar_trio_gene_masterVar_filt3.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4.tsv -o testvar_trio_gene_masterVar_filt4a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4a.tsv -o testvar_trio_gene_masterVar_filt5.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt5.tsv -o testvar_trio_gene_masterVar_filt6.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt6.tsv -o testvar_trio_gene_masterVar_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 11
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt8.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt9.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt9.tsv -o testvar_trio_gene_masterVar_filt10.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt11.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 01
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt12.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 01
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt13.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt14.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 01
sync
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt15.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt16.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt16.tsv -o testvar_trio_gene_masterVar_filt17.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt17.tsv -o testvar_trio_gene_masterVar_filt18.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 11
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt18.tsv -o testvar_trio_gene_masterVar_filt19.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt19.tsv -o testvar_trio_gene_masterVar_filt20.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 1N
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt20.tsv -o testvar_trio_gene_masterVar_filt21.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt21.tsv -o testvar_trio_gene_masterVar_filt22.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt22.tsv -o testvar_trio_gene_masterVar_filt23.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt23.tsv -o testvar_trio_gene_masterVar_filt24.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 1N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt24.tsv -o testvar_trio_gene_masterVar_filt25.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt25.tsv -o testvar_trio_gene_masterVar_filt26.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt26.tsv -o testvar_trio_gene_masterVar_filt27.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt28.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt29.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt29.tsv -o testvar_trio_gene_masterVar_filt30.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt30.tsv -o testvar_trio_gene_masterVar_filt31.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 1N
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt31.tsv -o testvar_trio_gene_masterVar_filt32.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt32.tsv -o testvar_trio_gene_masterVar_filt33.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt33.tsv -o testvar_trio_gene_masterVar_filt34.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt34.tsv -o testvar_trio_gene_masterVar_filt35.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt35.tsv -o testvar_trio_gene_masterVar_filt36.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1 --probandgeno 1
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt36.tsv -o testvar_trio_gene_masterVar_filt37.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 11 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt37.tsv -o testvar_trio_gene_masterVar_filt38.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt38.tsv -o testvar_trio_gene_masterVar_filt39.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 01 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt39.tsv -o testvar_trio_gene_masterVar_filt40.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 01
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt40.tsv -o testvar_trio_gene_masterVar_filt41.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt41.tsv -o testvar_trio_gene_masterVar_filt42.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt42.tsv -o testvar_trio_gene_masterVar_filt43.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43.tsv -o testvar_trio_gene_masterVar_filt43a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43a.tsv -o testvar_trio_gene_masterVar_filt43b.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43b.tsv -o testvar_trio_gene_masterVar_filt44.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt44.tsv -o testvar_trio_gene_masterVar_filt45.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt45.tsv -o testvar_trio_gene_masterVar_filt46.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt46.tsv -o testvar_trio_gene_masterVar_filt47.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt47.tsv -o testvar_trio_gene_masterVar_filt48.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 0N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt48.tsv -o testvar_trio_gene_masterVar_filt49.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt49.tsv -o testvar_trio_gene_masterVar_filt50.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt50.tsv -o testvar_trio_gene_masterVar_filt51.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt51.tsv -o testvar_trio_gene_masterVar_filt52.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt52.tsv -o testvar_trio_gene_masterVar_filt53.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt53.tsv -o testvar_trio_gene_masterVar_filt54.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt54.tsv -o testvar_trio_gene_masterVar_filt55.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt55.tsv -o testvar_trio_gene_masterVar_filt56.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt56.tsv -o testvar_trio_gene_masterVar_filt57.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt57.tsv -o testvar_trio_gene_masterVar_filt58.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt58.tsv -o testvar_trio_gene_masterVar_filt59.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt59.tsv -o testvar_trio_gene_masterVar_filt60.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt60.tsv -o testvar_trio_gene_masterVar_filt61.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt61.tsv -o testvar_trio_gene_masterVar_filt62.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt62.tsv -o testvar_trio_gene_masterVar_filt63.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt63.tsv -o testvar_trio_gene_masterVar_filt64.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt64.tsv -o testvar_trio_gene_masterVar_filt65.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt65.tsv -o testvar_trio_gene_masterVar_filt66.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt66.tsv -o testvar_trio_gene_masterVar_filt67.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt67.tsv -o testvar_trio_gene_masterVar_filt68.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt68.tsv -o testvar_trio_gene_masterVar_filt69.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt69.tsv -o testvar_trio_gene_masterVar_filt70.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt70.tsv -o testvar_trio_gene_masterVar_filt71.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt71.tsv -o testvar_trio_gene_masterVar_filt72.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt72.tsv -o testvar_trio_gene_masterVar_filt73.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt73.tsv -o testvar_trio_gene_masterVar_filt74.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt74.tsv -o testvar_trio_gene_masterVar_filt75.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt75.tsv -o testvar_trio_gene_masterVar_filt76.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt76.tsv -o testvar_trio_gene_masterVar_filt77.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt77.tsv -o testvar_trio_gene_masterVar_filt78.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt78.tsv -o testvar_trio_gene_masterVar_filt79.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt79.tsv -o testvar_trio_gene_masterVar_filt80.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt80.tsv -o testvar_trio_gene_masterVar_filt81.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt81.tsv -o testvar_trio_gene_masterVar_filt82.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt82.tsv -o testvar_trio_gene_masterVar_filt83.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt83.tsv -o testvar_trio_gene_masterVar_filt84.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt84.tsv -o testvar_trio_gene_masterVar_filt85.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt85.tsv -o testvar_trio_gene_masterVar_filt86.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt86.tsv -o testvar_trio_gene_masterVar_filt87.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt87.tsv -o testvar_trio_gene_masterVar_filt88.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt88.tsv -o testvar_trio_gene_masterVar_filt89.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt89.tsv -o testvar_trio_gene_masterVar_filt90.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 1N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt90.tsv -o testvar_trio_gene_masterVar_filt91.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt91.tsv -o testvar_trio_gene_masterVar_filt92.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt92.tsv -o testvar_trio_gene_masterVar_filt93.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt94.tsv -o testvar_trio_gene_masterVar_filt95.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt95.tsv -o testvar_trio_gene_masterVar_filt96.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt96.tsv -o testvar_trio_gene_masterVar_filt97.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt97.tsv -o testvar_trio_gene_masterVar_filt98.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt98.tsv -o testvar_trio_gene_masterVar_filt99.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt99.tsv -o testvar_trio_gene_masterVar_filt100.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 00
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt100.tsv -o testvar_trio_gene_masterVar_filt101.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt100.tsv -o testvar_trio_gene_masterVar_filt101.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt101.tsv -o testvar_trio_gene_masterVar_filt102.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt102.tsv -o testvar_trio_gene_masterVar_filt103.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt103.tsv -o testvar_trio_gene_masterVar_filt104.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt104.tsv -o testvar_trio_gene_masterVar_filt105.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt105.tsv -o testvar_trio_gene_masterVar_filt106.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt106.tsv -o testvar_trio_gene_masterVar_filt107.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt107.tsv -o testvar_trio_gene_masterVar_filt108.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt108.tsv -o testvar_trio_gene_masterVar_filt109.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno NN
sync
##DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt109.tsv -o testvar_trio_gene_masterVar_filt110.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 00 --probandgeno 01
sync
sleep 1
##RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt110.tsv -o testvar_trio_gene_masterVar_filt111.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt109.tsv -o testvar_trio_gene_masterVar_filt112.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt112.tsv -o testvar_trio_gene_masterVar_filt113.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt113.tsv -o testvar_trio_gene_masterVar_filt114.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt113.tsv -o testvar_trio_gene_masterVar_filt114.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt114.tsv -o testvar_trio_gene_masterVar_filt114a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt114a.tsv -o testvar_trio_gene_masterVar_filt115.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt115.tsv -o testvar_trio_gene_masterVar_filt116.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt116.tsv -o testvar_trio_gene_masterVar_filt117.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt117.tsv -o testvar_trio_gene_masterVar_filt118.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt118.tsv -o testvar_trio_gene_masterVar_filt119.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt119.tsv -o testvar_trio_gene_masterVar_filt120.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt120.tsv -o testvar_trio_gene_masterVar_filt121.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt121.tsv -o testvar_trio_gene_masterVar_filt122.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt122.tsv -o testvar_trio_gene_masterVar_filt123.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt123.tsv -o testvar_trio_gene_masterVar_filt124.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt124.tsv -o testvar_trio_gene_masterVar_filt125.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt125.tsv -o testvar_trio_gene_masterVar_filt126.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt126.tsv -o testvar_trio_gene_masterVar_filt127.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt127.tsv -o testvar_trio_gene_masterVar_filt128.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt128.tsv -o testvar_trio_gene_masterVar_filt129.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt129.tsv -o testvar_trio_gene_masterVar_filt130.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 00
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt130.tsv -o testvar_trio_gene_masterVar_filt131.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt131.tsv -o testvar_trio_gene_masterVar_filt132.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt132.tsv -o testvar_trio_gene_masterVar_filt133.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt133.tsv -o testvar_trio_gene_masterVar_filt134.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt134.tsv -o testvar_trio_gene_masterVar_filt135.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt135.tsv -o testvar_trio_gene_masterVar_filt136.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt136.tsv -o testvar_trio_gene_masterVar_filt137.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt137.tsv -o testvar_trio_gene_masterVar_filt138.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt137.tsv -o testvar_trio_gene_masterVar_filt138.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt138.tsv -o testvar_trio_gene_masterVar_filt139.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt139.tsv -o testvar_trio_gene_masterVar_filt140.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt140.tsv -o testvar_trio_gene_masterVar_filt141.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt141.tsv -o testvar_trio_gene_masterVar_filt142.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt142.tsv -o testvar_trio_gene_masterVar_filt143.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt143.tsv -o testvar_trio_gene_masterVar_filt144.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt144.tsv -o testvar_trio_gene_masterVar_filt145.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 1N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt145.tsv -o testvar_trio_gene_masterVar_filt146.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt146.tsv -o testvar_trio_gene_masterVar_filt147.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno NN --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt147.tsv -o testvar_trio_gene_masterVar_filt148.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt148.tsv -o testvar_trio_gene_masterVar_filt149.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt149.tsv -o testvar_trio_gene_masterVar_filt150.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 0N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt150.tsv -o testvar_trio_gene_masterVar_filt151.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 1N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt151.tsv -o testvar_trio_gene_masterVar_filt152.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 1N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt152.tsv -o testvar_trio_gene_masterVar_filt153.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt150.tsv -o testvar_trio_gene_masterVar_filt154.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt154.tsv -o testvar_trio_gene_masterVar_filt155.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt155.tsv -o testvar_trio_gene_masterVar_filt156.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt156.tsv -o testvar_trio_gene_masterVar_filt157.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno NN
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt157.tsv -o testvar_trio_gene_masterVar_filt158.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt158.tsv -o testvar_trio_gene_masterVar_filt159.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 11
sync
#RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt159.tsv -o testvar_trio_gene_masterVar_filt160.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 11
sync
#DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt160.tsv -o testvar_trio_gene_masterVar_filt161.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt157.tsv -o testvar_trio_gene_masterVar_filt162.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt162.tsv -o testvar_trio_gene_masterVar_filt163.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno NN
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt163.tsv -o testvar_trio_gene_masterVar_filt164.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno NN --probandgeno 1
sync
#maleX
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt164.tsv -o testvar_trio_gene_masterVar_filt165.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 01 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt164.tsv -o testvar_trio_gene_masterVar_filt166.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt166.tsv -o testvar_trio_gene_masterVar_filt167.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt167.tsv -o testvar_trio_gene_masterVar_filt168.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt167.tsv -o testvar_trio_gene_masterVar_filt169.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt169.tsv -o testvar_trio_gene_masterVar_filt170.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno 01
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt170.tsv -o testvar_trio_gene_masterVar_filt171.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt171.tsv -o testvar_trio_gene_masterVar_filt172.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt172.tsv -o testvar_trio_gene_masterVar_filt173.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt173.tsv -o testvar_trio_gene_masterVar_filt174.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt174.tsv -o testvar_trio_gene_masterVar_filt175.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno 01
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt175.tsv -o testvar_trio_gene_masterVar_filt176.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt175.tsv -o testvar_trio_gene_masterVar_filt177.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt177.tsv -o testvar_trio_gene_masterVar_filt178.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt178.tsv -o testvar_trio_gene_masterVar_filt179.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno NN
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt179.tsv -o testvar_trio_gene_masterVar_filt180.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt180.tsv -o testvar_trio_gene_masterVar_filt181.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt181.tsv -o testvar_trio_gene_masterVar_filt182.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt182.tsv -o testvar_trio_gene_masterVar_filt183.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt183.tsv -o testvar_trio_gene_masterVar_filt184.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 0N
sync
#RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt184.tsv -o testvar_trio_gene_masterVar_filt185.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt184.tsv -o testvar_trio_gene_masterVar_filt186.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt186.tsv -o testvar_trio_gene_masterVar_filt187.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt187.tsv -o testvar_trio_gene_masterVar_filt188.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt188.tsv -o testvar_trio_gene_masterVar_filt189.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 01
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt189.tsv -o testvar_trio_gene_masterVar_filt190.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 00
sync
#DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt190.tsv -o testvar_trio_gene_masterVar_filt191.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 00 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt188.tsv -o testvar_trio_gene_masterVar_filt192.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 00 --probandgeno 1
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt192.tsv -o testvar_trio_gene_masterVar_filt193.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt193.tsv -o testvar_trio_gene_masterVar_filt194.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt194.tsv -o testvar_trio_gene_masterVar_filt195.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 0N
sync
#
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt195.tsv -o testvar_trio_gene_masterVar_filt196.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt196.tsv -o testvar_trio_gene_masterVar_filt197.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 11
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt195.tsv -o testvar_trio_gene_masterVar_filt198.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt198.tsv -o testvar_trio_gene_masterVar_filt199.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt199.tsv -o testvar_trio_gene_masterVar_filt200.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 00 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt200.tsv -o testvar_trio_gene_masterVar_filt201.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno NN --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt201.tsv -o testvar_trio_gene_masterVar_filt202.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno NN --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt202.tsv -o testvar_trio_gene_masterVar_filt203.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 11 --probandgeno 1
sync



perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt203.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_filt203_esp6500si.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt203_esp6500si.tsv --Input_FileB /home/jeremiah/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_filt203_esp6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

python ~/pyawk.py -i testvar_trio_gene_masterVar_filt203_esp6500si_1000g.tsv -o testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02a.tsv -c MAF_esp6500si -v "0.02" > out.out


python ~/pyawk.py -i testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02a.tsv -o testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b.tsv -c  MAF_1000g -v "0.02" > out.out

time awk ' !x[$1]++' testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b.tsv > testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b_dedup.tsv

python ~/chdm_pipeline/cg/pyawk.py -i testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b.tsv -o testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b_nochange_syn.tsv -f impact -v "NO-CHANGE SYNONYMOUS" -m ">"


cp testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b_nochange_syn.tsv dm41_exon_dedup.tsv

cp dm41_exon_dedup.tsv dm41_exon_dedup_vqhigh.tsv
sed -i '/VQLOW/d' dm41_exon_dedup_vqhigh.tsv

################
########INTRON##
################
cd /home/jeremiah/complete_genomics/output/121204/131012/131014_redo
mkdir intron
cd intron

python3 ~/chdm_pipeline/cg/pyawk.py -i ../testvar_trio_gene_masterVar.tsv -o testvar_trio_gene_masterVar_intron.tsv -f component -v "ACCEPTOR DONOR" -m ">"

#cp dm41_disrupt_dedup.tsv dm41_disrupt_dedup_vqhigh.tsv
#sed -i '/VQLOW/d' dm41_disrupt_dedup_vqhigh.tsv


python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_intron.tsv -o testvar_trio_gene_masterVar_filt.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt.tsv -o testvar_trio_gene_masterVar_filt2.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM --fathergeno 11 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt2.tsv -o testvar_trio_gene_masterVar_filt3.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt3.tsv -o testvar_trio_gene_masterVar_filt4.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4.tsv -o testvar_trio_gene_masterVar_filt4a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt4a.tsv -o testvar_trio_gene_masterVar_filt5.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt5.tsv -o testvar_trio_gene_masterVar_filt6.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt6.tsv -o testvar_trio_gene_masterVar_filt7.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 11
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt8.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt7.tsv -o testvar_trio_gene_masterVar_filt9.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt9.tsv -o testvar_trio_gene_masterVar_filt10.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 11
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt11.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 01
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt10.tsv -o testvar_trio_gene_masterVar_filt12.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 01
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt13.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt12.tsv -o testvar_trio_gene_masterVar_filt14.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 01
sync
##CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt15.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt14.tsv -o testvar_trio_gene_masterVar_filt16.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt16.tsv -o testvar_trio_gene_masterVar_filt17.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt17.tsv -o testvar_trio_gene_masterVar_filt18.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 11
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt18.tsv -o testvar_trio_gene_masterVar_filt19.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt19.tsv -o testvar_trio_gene_masterVar_filt20.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 1N
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt20.tsv -o testvar_trio_gene_masterVar_filt21.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt21.tsv -o testvar_trio_gene_masterVar_filt22.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt22.tsv -o testvar_trio_gene_masterVar_filt23.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt23.tsv -o testvar_trio_gene_masterVar_filt24.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 1N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt24.tsv -o testvar_trio_gene_masterVar_filt25.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt25.tsv -o testvar_trio_gene_masterVar_filt26.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt26.tsv -o testvar_trio_gene_masterVar_filt27.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt28.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt27.tsv -o testvar_trio_gene_masterVar_filt29.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt29.tsv -o testvar_trio_gene_masterVar_filt30.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt30.tsv -o testvar_trio_gene_masterVar_filt31.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 1N
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt31.tsv -o testvar_trio_gene_masterVar_filt32.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt32.tsv -o testvar_trio_gene_masterVar_filt33.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt33.tsv -o testvar_trio_gene_masterVar_filt34.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt34.tsv -o testvar_trio_gene_masterVar_filt35.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt35.tsv -o testvar_trio_gene_masterVar_filt36.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1 --probandgeno 1
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt36.tsv -o testvar_trio_gene_masterVar_filt37.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 11 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt37.tsv -o testvar_trio_gene_masterVar_filt38.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt38.tsv -o testvar_trio_gene_masterVar_filt39.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 01 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt39.tsv -o testvar_trio_gene_masterVar_filt40.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 01
sync
sleep1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt40.tsv -o testvar_trio_gene_masterVar_filt41.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt41.tsv -o testvar_trio_gene_masterVar_filt42.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt42.tsv -o testvar_trio_gene_masterVar_filt43.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43.tsv -o testvar_trio_gene_masterVar_filt43a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43a.tsv -o testvar_trio_gene_masterVar_filt43b.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt43b.tsv -o testvar_trio_gene_masterVar_filt44.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt44.tsv -o testvar_trio_gene_masterVar_filt45.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt45.tsv -o testvar_trio_gene_masterVar_filt46.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt46.tsv -o testvar_trio_gene_masterVar_filt47.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt47.tsv -o testvar_trio_gene_masterVar_filt48.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 0N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt48.tsv -o testvar_trio_gene_masterVar_filt49.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt49.tsv -o testvar_trio_gene_masterVar_filt50.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt50.tsv -o testvar_trio_gene_masterVar_filt51.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt51.tsv -o testvar_trio_gene_masterVar_filt52.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt52.tsv -o testvar_trio_gene_masterVar_filt53.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt53.tsv -o testvar_trio_gene_masterVar_filt54.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt54.tsv -o testvar_trio_gene_masterVar_filt55.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt55.tsv -o testvar_trio_gene_masterVar_filt56.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt56.tsv -o testvar_trio_gene_masterVar_filt57.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt57.tsv -o testvar_trio_gene_masterVar_filt58.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt58.tsv -o testvar_trio_gene_masterVar_filt59.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt59.tsv -o testvar_trio_gene_masterVar_filt60.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno 0N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt60.tsv -o testvar_trio_gene_masterVar_filt61.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt61.tsv -o testvar_trio_gene_masterVar_filt62.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 11

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt62.tsv -o testvar_trio_gene_masterVar_filt63.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt63.tsv -o testvar_trio_gene_masterVar_filt64.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt64.tsv -o testvar_trio_gene_masterVar_filt65.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt65.tsv -o testvar_trio_gene_masterVar_filt66.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt66.tsv -o testvar_trio_gene_masterVar_filt67.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt67.tsv -o testvar_trio_gene_masterVar_filt68.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno NN

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt68.tsv -o testvar_trio_gene_masterVar_filt69.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt69.tsv -o testvar_trio_gene_masterVar_filt70.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt70.tsv -o testvar_trio_gene_masterVar_filt71.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt71.tsv -o testvar_trio_gene_masterVar_filt72.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt72.tsv -o testvar_trio_gene_masterVar_filt73.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt73.tsv -o testvar_trio_gene_masterVar_filt74.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 00

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt74.tsv -o testvar_trio_gene_masterVar_filt75.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt75.tsv -o testvar_trio_gene_masterVar_filt76.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt76.tsv -o testvar_trio_gene_masterVar_filt77.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt77.tsv -o testvar_trio_gene_masterVar_filt78.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 01

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt78.tsv -o testvar_trio_gene_masterVar_filt79.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt79.tsv -o testvar_trio_gene_masterVar_filt80.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt80.tsv -o testvar_trio_gene_masterVar_filt81.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 1N
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt81.tsv -o testvar_trio_gene_masterVar_filt82.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt82.tsv -o testvar_trio_gene_masterVar_filt83.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 1N

python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt83.tsv -o testvar_trio_gene_masterVar_filt84.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt84.tsv -o testvar_trio_gene_masterVar_filt85.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt85.tsv -o testvar_trio_gene_masterVar_filt86.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt86.tsv -o testvar_trio_gene_masterVar_filt87.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt87.tsv -o testvar_trio_gene_masterVar_filt88.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt88.tsv -o testvar_trio_gene_masterVar_filt89.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt89.tsv -o testvar_trio_gene_masterVar_filt90.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 1N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt90.tsv -o testvar_trio_gene_masterVar_filt91.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt91.tsv -o testvar_trio_gene_masterVar_filt92.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt92.tsv -o testvar_trio_gene_masterVar_filt93.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt93.tsv -o testvar_trio_gene_masterVar_filt94.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt94.tsv -o testvar_trio_gene_masterVar_filt95.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt95.tsv -o testvar_trio_gene_masterVar_filt96.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt96.tsv -o testvar_trio_gene_masterVar_filt97.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt97.tsv -o testvar_trio_gene_masterVar_filt98.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt98.tsv -o testvar_trio_gene_masterVar_filt99.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt99.tsv -o testvar_trio_gene_masterVar_filt100.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 00
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt100.tsv -o testvar_trio_gene_masterVar_filt101.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt100.tsv -o testvar_trio_gene_masterVar_filt101.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt101.tsv -o testvar_trio_gene_masterVar_filt102.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt102.tsv -o testvar_trio_gene_masterVar_filt103.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt103.tsv -o testvar_trio_gene_masterVar_filt104.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt104.tsv -o testvar_trio_gene_masterVar_filt105.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt105.tsv -o testvar_trio_gene_masterVar_filt106.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt106.tsv -o testvar_trio_gene_masterVar_filt107.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt107.tsv -o testvar_trio_gene_masterVar_filt108.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt108.tsv -o testvar_trio_gene_masterVar_filt109.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno NN
sync
##DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt109.tsv -o testvar_trio_gene_masterVar_filt110.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 00 --probandgeno 01
sync
sleep 1
##RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt110.tsv -o testvar_trio_gene_masterVar_filt111.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt109.tsv -o testvar_trio_gene_masterVar_filt112.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt112.tsv -o testvar_trio_gene_masterVar_filt113.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt113.tsv -o testvar_trio_gene_masterVar_filt114.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt113.tsv -o testvar_trio_gene_masterVar_filt114.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt114.tsv -o testvar_trio_gene_masterVar_filt114a.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt114a.tsv -o testvar_trio_gene_masterVar_filt115.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt115.tsv -o testvar_trio_gene_masterVar_filt116.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt116.tsv -o testvar_trio_gene_masterVar_filt117.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt117.tsv -o testvar_trio_gene_masterVar_filt118.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt118.tsv -o testvar_trio_gene_masterVar_filt119.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt119.tsv -o testvar_trio_gene_masterVar_filt120.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 1N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt120.tsv -o testvar_trio_gene_masterVar_filt121.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt121.tsv -o testvar_trio_gene_masterVar_filt122.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt122.tsv -o testvar_trio_gene_masterVar_filt123.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt123.tsv -o testvar_trio_gene_masterVar_filt124.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt124.tsv -o testvar_trio_gene_masterVar_filt125.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt125.tsv -o testvar_trio_gene_masterVar_filt126.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt126.tsv -o testvar_trio_gene_masterVar_filt127.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt127.tsv -o testvar_trio_gene_masterVar_filt128.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt128.tsv -o testvar_trio_gene_masterVar_filt129.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt129.tsv -o testvar_trio_gene_masterVar_filt130.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 00
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt130.tsv -o testvar_trio_gene_masterVar_filt131.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt131.tsv -o testvar_trio_gene_masterVar_filt132.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt132.tsv -o testvar_trio_gene_masterVar_filt133.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt133.tsv -o testvar_trio_gene_masterVar_filt134.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt134.tsv -o testvar_trio_gene_masterVar_filt135.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt135.tsv -o testvar_trio_gene_masterVar_filt136.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt136.tsv -o testvar_trio_gene_masterVar_filt137.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt137.tsv -o testvar_trio_gene_masterVar_filt138.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt137.tsv -o testvar_trio_gene_masterVar_filt138.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt138.tsv -o testvar_trio_gene_masterVar_filt139.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt139.tsv -o testvar_trio_gene_masterVar_filt140.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno 0N
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt140.tsv -o testvar_trio_gene_masterVar_filt141.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt141.tsv -o testvar_trio_gene_masterVar_filt142.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt142.tsv -o testvar_trio_gene_masterVar_filt143.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 01 --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt143.tsv -o testvar_trio_gene_masterVar_filt144.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt144.tsv -o testvar_trio_gene_masterVar_filt145.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 1N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt145.tsv -o testvar_trio_gene_masterVar_filt146.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 11 --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt146.tsv -o testvar_trio_gene_masterVar_filt147.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno NN --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt147.tsv -o testvar_trio_gene_masterVar_filt148.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt148.tsv -o testvar_trio_gene_masterVar_filt149.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt149.tsv -o testvar_trio_gene_masterVar_filt150.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 11 --probandgeno 0N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt150.tsv -o testvar_trio_gene_masterVar_filt151.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 1N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt151.tsv -o testvar_trio_gene_masterVar_filt152.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 00 --probandgeno 1N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt152.tsv -o testvar_trio_gene_masterVar_filt153.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt150.tsv -o testvar_trio_gene_masterVar_filt154.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt154.tsv -o testvar_trio_gene_masterVar_filt155.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt155.tsv -o testvar_trio_gene_masterVar_filt156.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt156.tsv -o testvar_trio_gene_masterVar_filt157.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 01 --probandgeno NN
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt157.tsv -o testvar_trio_gene_masterVar_filt158.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt158.tsv -o testvar_trio_gene_masterVar_filt159.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 11
sync
#RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt159.tsv -o testvar_trio_gene_masterVar_filt160.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 11
sync
#DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt160.tsv -o testvar_trio_gene_masterVar_filt161.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 00 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt157.tsv -o testvar_trio_gene_masterVar_filt162.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt162.tsv -o testvar_trio_gene_masterVar_filt163.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno NN
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt163.tsv -o testvar_trio_gene_masterVar_filt164.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno NN --probandgeno 1
sync
#maleX
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt164.tsv -o testvar_trio_gene_masterVar_filt165.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 01 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt164.tsv -o testvar_trio_gene_masterVar_filt166.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1N --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt166.tsv -o testvar_trio_gene_masterVar_filt167.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 1N --probandgeno N
sync
#CH
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt167.tsv -o testvar_trio_gene_masterVar_filt168.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt167.tsv -o testvar_trio_gene_masterVar_filt169.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 00 --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt169.tsv -o testvar_trio_gene_masterVar_filt170.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno 01
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt170.tsv -o testvar_trio_gene_masterVar_filt171.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 01 --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt171.tsv -o testvar_trio_gene_masterVar_filt172.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 0N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt172.tsv -o testvar_trio_gene_masterVar_filt173.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 0N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt173.tsv -o testvar_trio_gene_masterVar_filt174.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt174.tsv -o testvar_trio_gene_masterVar_filt175.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 0N --probandgeno 01
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt175.tsv -o testvar_trio_gene_masterVar_filt176.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 00 --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt175.tsv -o testvar_trio_gene_masterVar_filt177.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno NN --probandgeno 1N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt177.tsv -o testvar_trio_gene_masterVar_filt178.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt178.tsv -o testvar_trio_gene_masterVar_filt179.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno NN
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt179.tsv -o testvar_trio_gene_masterVar_filt180.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt180.tsv -o testvar_trio_gene_masterVar_filt181.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 1N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt181.tsv -o testvar_trio_gene_masterVar_filt182.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 1N --probandgeno NN
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt182.tsv -o testvar_trio_gene_masterVar_filt183.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 1N --probandgeno 00
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt183.tsv -o testvar_trio_gene_masterVar_filt184.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno NN --probandgeno 0N
sync
#RECESSIVE
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt184.tsv -o testvar_trio_gene_masterVar_filt185.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 01 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt184.tsv -o testvar_trio_gene_masterVar_filt186.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno NN --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt186.tsv -o testvar_trio_gene_masterVar_filt187.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 11 --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt187.tsv -o testvar_trio_gene_masterVar_filt188.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno NN --mothergeno 0N --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt188.tsv -o testvar_trio_gene_masterVar_filt189.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 11 --probandgeno 01
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt189.tsv -o testvar_trio_gene_masterVar_filt190.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 00 --probandgeno 00
sync
#DENOVO
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt190.tsv -o testvar_trio_gene_masterVar_filt191.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 00 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt188.tsv -o testvar_trio_gene_masterVar_filt192.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno 00 --probandgeno 1
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt192.tsv -o testvar_trio_gene_masterVar_filt193.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0 --mothergeno 1N --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt193.tsv -o testvar_trio_gene_masterVar_filt194.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 01
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt194.tsv -o testvar_trio_gene_masterVar_filt195.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 11 --mothergeno 01 --probandgeno 0N
sync
#
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt195.tsv -o testvar_trio_gene_masterVar_filt196.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 01 --mothergeno 00 --probandgeno 11
sync
#SV
#python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt196.tsv -o testvar_trio_gene_masterVar_filt197.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 00 --probandgeno 11
sync
sleep 1
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt195.tsv -o testvar_trio_gene_masterVar_filt198.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 0N --mothergeno 11 --probandgeno 11
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt198.tsv -o testvar_trio_gene_masterVar_filt199.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1N --mothergeno 0N --probandgeno 0N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt199.tsv -o testvar_trio_gene_masterVar_filt200.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 00 --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt200.tsv -o testvar_trio_gene_masterVar_filt201.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno 1 --mothergeno NN --probandgeno N
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt201.tsv -o testvar_trio_gene_masterVar_filt202.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno NN --probandgeno 1
sync
python ~/chdm_pipeline/cg/trio_filt_remove.py -i testvar_trio_gene_masterVar_filt202.tsv -o testvar_trio_gene_masterVar_filt203.tsv -f GS000013593-ASM -m GS000013594-ASM -p GS000013595-ASM  --fathergeno N --mothergeno 11 --probandgeno 1
sync



perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt203.tsv --Input_FileB ~/complete_genomics/data/hg19_esp6500si_all.txt --Output_File testvar_trio_gene_masterVar_filt203_esp6500si.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

perl ~/complete_genomics/data/workflow/Merge_Select_0_0_6.pl --Input_FileA testvar_trio_gene_masterVar_filt203_esp6500si.tsv --Input_FileB /home/jeremiah/complete_genomics/data/hg19_ALL.sites.2012_04.txt --Output_File testvar_trio_gene_masterVar_filt203_esp6500si_1000g.tsv --Select all --Keep_Matching all --Match end:begin --Match chromosome:chromosome

python ~/pyawk.py -i testvar_trio_gene_masterVar_filt203_esp6500si_1000g.tsv -o testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02a.tsv -c MAF_esp6500si -v "0.02" > out.out


python ~/pyawk.py -i testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02a.tsv -o testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b.tsv -c  MAF_1000g -v "0.02" > out.out

time awk ' !x[$1]++' testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b.tsv > testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b_dedup.tsv



cp testvar_trio_gene_masterVar_filt203_esp6500si_1000g_02b_dedup.tsv dm41_intron_dedup.tsv

cp dm41_intron_dedup.tsv dm41_intron_dedup_vqhigh.tsv
sed -i '/VQLOW/d' dm41_intron_dedup_vqhigh.tsv
