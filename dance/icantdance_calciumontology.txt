Process 	GO:0070509 	calcium ion import 	
Process 	GO:1901660 	calcium ion export 	
Function 	GO:0005509 	calcium ion binding 	
Process 	GO:0006816 	calcium ion transport 	
Component 	GO:0034704 	calcium channel complex 	
Function 	GO:0046904 	calcium oxalate binding 	
Process 	GO:0051592 	response to calcium ion 	
Process 	GO:0055074 	calcium ion homeostasis 	
Function 	GO:0005262 	calcium channel activity 	
Process 	GO:0005513 	detection of calcium ion 	
Process 	GO:0061454 	Golgi calcium ion export 	
Process 	GO:0019722 	calcium-mediated signaling 	
Process 	
GO:0006889
	regulation of calcium in ER 	
Process 	GO:0032472 	Golgi calcium ion transport 	
Process 	GO:0051208 	sequestering of calcium ion 	
Process 	GO:0002115 	store-operated calcium entry 	
Function 	
GO:0005514
	calcium ion storage activity 	
Process 	GO:1990034 	calcium ion export from cell 	
Process 	GO:1990035 	calcium ion import into cell 	
Process 	GO:0032468 	Golgi calcium ion homeostasis 	
Process 	GO:0060401 	cytosolic calcium ion transport 	
Process 	GO:0006874 	cellular calcium ion homeostasis 	
Process 	GO:0007036 	vacuolar calcium ion homeostasis 	
Function 	GO:0015278 	calcium-release channel activity 	
Process 	GO:0017156 	calcium ion-dependent exocytosis 	
Process 	GO:0071277 	cellular response to calcium ion 	
Process 	GO:0090279 	regulation of calcium ion import 	
Function 	GO:0030899 	calcium-dependent ATPase activity 	
Function 	GO:0048306 	calcium-dependent protein binding 	
Process 	GO:0051480 	cytosolic calcium ion homeostasis 	
Process 	GO:1901876 	regulation of calcium ion binding 	
Function 	GO:0005246 	calcium channel regulator activity 	
Function 	GO:0005432 	calcium:sodium antiporter activity 	
Process 	
GO:0007230
	calcium-o-sensing receptor pathway 	
Function 	GO:0015368 	calcium:cation antiporter activity 	
Function 	GO:0019855 	calcium channel inhibitor activity 	
Process 	GO:0060402 	calcium ion transport into cytosol 	
Process 	GO:1990092 	calcium-dependent self proteolysis 	
Process 	GO:0006851 	mitochondrial calcium ion transport 	
Process 	GO:0051924 	regulation of calcium ion transport 	
Process 	GO:0070588 	calcium ion transmembrane transport 	
Function 	GO:0005388 	calcium-transporting ATPase activity 	
Function 	GO:0015369 	calcium:hydrogen antiporter activity 	
Process 	GO:0016339 	calcium-dependent cell-cell adhesion 	
Process 	
GO:0045783
	negative regulation of calcium in ER 	
Process 	
GO:0045784
	positive regulation of calcium in ER 	
Component 	GO:0005891 	voltage-gated calcium channel complex 	
Process 	GO:0051560 	mitochondrial calcium ion homeostasis 	
Function 	GO:0005245 	voltage-gated calcium channel activity 	
Function 	GO:0005544 	calcium-dependent phospholipid binding 	
Process 	GO:0016338 	calcium-independent cell-cell adhesion 	
Process 	GO:0016340 	calcium-dependent cell-matrix adhesion 	
Process 	GO:1901472 	regulation of Golgi calcium ion export 	
Function 	GO:0015279 	store-operated calcium channel activity 	
Component 	GO:0090534 	calcium ion-transporting ATPase complex 	
Process 	GO:0007161 	calcium-independent cell-matrix adhesion 	
Function 	GO:0048763 	calcium-induced calcium release activity 	
Process 	GO:0050848 	regulation of calcium-mediated signaling 	
Process 	GO:0097231 	cell motility in response to calcium ion 	
Function 	GO:0005227 	calcium activated cation channel activity 	
Function 	GO:0010857 	calcium-dependent protein kinase activity 	
Process 	GO:0051282 	regulation of sequestering of calcium ion 	
Process 	GO:0090280 	positive regulation of calcium ion import 	
Process 	GO:0090281 	negative regulation of calcium ion import 	
Function 	GO:0008332 	low voltage-gated calcium channel activity 	
Function 	GO:0050429 	calcium-dependent phospholipase C activity 	
Process 	GO:1901877 	negative regulation of calcium ion binding 	
Process 	GO:1901878 	positive regulation of calcium ion binding 	
Process 	GO:2001256 	regulation of store-operated calcium entry 	
Function 	GO:0004698 	calcium-dependent protein kinase C activity 	
Function 	GO:0008331 	high voltage-gated calcium channel activity 	
Function 	GO:0047498 	calcium-dependent phospholipase A2 activity 	
Process 	GO:0072732 	cellular response to calcium ion starvation 	
Function 	GO:0015269 	calcium-activated potassium channel activity 	
Function 	GO:0022849 	glutamate-gated calcium ion channel activity 	
Process 	GO:0051926 	negative regulation of calcium ion transport 	
Process 	GO:0051928 	positive regulation of calcium ion transport 	
Process 	GO:0070073 	clustering of voltage-gated calcium channels 	
Process 	GO:0070296 	sarcoplasmic reticulum calcium ion transport 	
Function 	GO:0004699 	calcium-independent protein kinase C activity 	
Function 	GO:0008273 	calcium , potassium:sodium antiporter activity 	
Process 	GO:0032469 	endoplasmic reticulum calcium ion homeostasis 	
Function 	GO:0047499 	calcium-independent phospholipase A2 activity 	
Function 	GO:0015085 	calcium ion transmembrane transporter activity 	
Process 	GO:0017158 	regulation of calcium ion-dependent exocytosis 	
Process 	GO:0070978 	voltage-gated calcium channel complex assembly 	
Process 	GO:1990036 	calcium ion import into sarcoplasmic reticulum 	
Process 	GO:0010617 	circadian regulation of calcium ion oscillation 	
Process 	GO:0051209 	release of sequestered calcium ion into cytosol 	
Process 	GO:0007204 	elevation of cytosolic calcium ion concentration 	
Process 	GO:0010522 	regulation of calcium ion transport into cytosol 	
Process 	GO:0051481 	reduction of cytosolic calcium ion concentration 	
Function 	GO:0072345 	NAADP-sensitive calcium-release channel activity 	
Function 	GO:0072346 	cADPR-sensitive calcium-release channel activity 	
Function 	
GO:0008014
	calcium-dependent cell adhesion molecule activity 	
Process 	GO:0030925 	calcium incorporation into metallo-oxygen cluster 	
Process 	GO:0050849 	negative regulation of calcium-mediated signaling 	
Process 	GO:0050850 	positive regulation of calcium-mediated signaling 	
Process 	GO:0046586 	regulation of calcium-dependent cell-cell adhesion 	
Process 	GO:0051283 	negative regulation of sequestering of calcium ion 	
Process 	GO:0051284 	positive regulation of sequestering of calcium ion 	
Process 	GO:1901894 	regulation of calcium-transporting ATPase activity 	
Function 	GO:0005218 	intracellular ligand-gated calcium channel activity 	
Function 	GO:0008427 	calcium-dependent protein kinase inhibitor activity 	
Function 	GO:0010858 	calcium-dependent protein kinase regulator activity 	
Function 	
GO:0016347
	calcium-independent cell adhesion molecule activity 	
Function 	GO:1990028 	intermediate voltage-gated calcium channel activity 	
Function 	
GO:0004625
	calcium-dependent secreted phospholipase A2 activity 	
Function 	GO:0005219 	ryanodine-sensitive calcium-release channel activity 	
Process 	GO:0048791 	calcium ion-dependent exocytosis of neurotransmitter 	
Process 	GO:0051040 	regulation of calcium-independent cell-cell adhesion 	
Process 	GO:0051561 	elevation of mitochondrial calcium ion concentration 	
Process 	GO:0051562 	reduction of mitochondrial calcium ion concentration 	
Process 	GO:0051563 	smooth endoplasmic reticulum calcium ion homeostasis 	
Process 	GO:1901385 	regulation of voltage-gated calcium channel activity 	
Function 	
GO:0004627
	calcium-dependent cytosolic phospholipase A2 activity 	
Component 	GO:0008087 	light-activated voltage-gated calcium channel complex 	
Process 	GO:0032237 	activation of store-operated calcium channel activity 	
Process 	GO:1901339 	regulation of store-operated calcium channel activity 	
Function 	GO:0004198 	calcium-dependent cysteine-type endopeptidase activity 	
Function 	GO:0008048 	calcium sensitive guanylate cyclase activator activity 	
Function 	GO:0008086 	light-activated voltage-gated calcium channel activity 	
Process 	GO:0048792 	calcium ion-independent exocytosis of neurotransmitter 	
Function 	
GO:0004628
	calcium-independent cytosolic phospholipase A2 activity 	
Process 	GO:0045955 	negative regulation of calcium ion-dependent exocytosis 	
Process 	GO:0045956 	positive regulation of calcium ion-dependent exocytosis 	
Component 	GO:0005954 	calcium - and calmodulin-dependent protein kinase complex 	
Process 	GO:0035916 	modulation of calcium channel activity in other organism 	
Function 	GO:0005229 	intracellular calcium activated chloride channel activity 	
Process 	GO:0010523 	negative regulation of calcium ion transport into cytosol 	
Process 	GO:0010524 	positive regulation of calcium ion transport into cytosol 	
Process 	GO:1901841 	regulation of high voltage-gated calcium channel activity 	
Process 	GO:0007223 	Wnt receptor signaling pathway, calcium modulating pathway 	
Function 	GO:0009931 	calcium-dependent protein serine/threonine kinase activity 	
Process 	GO:0008377 	light-induced release of internally sequestered calcium ion 	
Process 	GO:0046587 	positive regulation of calcium-dependent cell-cell adhesion 	
Process 	GO:0046588 	negative regulation of calcium-dependent cell-cell adhesion 	
Process 	GO:1901895 	negative regulation of calcium-transporting ATPase activity 	
Process 	GO:1901896 	positive regulation of calcium-transporting ATPase activity 	
Process 	GO:0032470 	elevation of endoplasmic reticulum calcium ion concentration 	
Process 	GO:0032471 	reduction of endoplasmic reticulum calcium ion concentration 	
Process 	GO:1901019 	regulation of calcium ion transmembrane transporter activity 	
Process 	GO:1902080 	regulation of calcium ion import into sarcoplasmic reticulum 	
Function 	GO:0008294 	calcium - and calmodulin-responsive adenylate cyclase activity 	
Function 	GO:0015275 	stretch-activated, cation-selective, calcium channel activity 	
Process 	GO:0035584 	calcium-mediated signaling using intracellular calcium source 	
Process 	GO:0035585 	calcium-mediated signaling using extracellular calcium source 	
Process 	GO:0051041 	positive regulation of calcium-independent cell-cell adhesion 	
Process 	GO:0051042 	negative regulation of calcium-independent cell-cell adhesion 	
Process 	GO:0051279 	regulation of release of sequestered calcium ion into cytosol 	
Process 	GO:1901386 	negative regulation of voltage-gated calcium channel activity 	
Process 	GO:1901387 	positive regulation of voltage-gated calcium channel activity 	
Function 	GO:0016286 	small conductance calcium-activated potassium channel activity 	
Process 	GO:0052391 	induction by symbiont of defense-related host calcium ion flux 	
Function 	GO:0060072 	large conductance calcium-activated potassium channel activity 	
Process 	GO:1901340 	negative regulation of store-operated calcium channel activity 	
Process 	GO:1901341 	positive regulation of store-operated calcium channel activity 	
Function 	GO:0004723 	calcium-dependent protein serine/threonine phosphatase activity 	
Process 	GO:0052162 	modulation by symbiont of defense-related host calcium ion flux 	
Process 	
GO:1900014
	cellular response to calcium ion involved in chemotaxis to cAMP 	
Function 	GO:0010859 	calcium-dependent cysteine-type endopeptidase inhibitor activity 	
Process 	GO:0010882 	regulation of cardiac muscle contraction by calcium ion signaling 	
Process 	GO:0035917 	negative regulation of calcium channel activity in other organism 	
Process 	GO:0014722 	regulation of skeletal muscle contraction by calcium ion signaling 	
Process 	
GO:0052392
	induction by organism of defense-related symbiont calcium ion flux 	
Process 	GO:0060314 	regulation of ryanodine-sensitive calcium-release channel activity 	
Process 	GO:1901842 	negative regulation of high voltage-gated calcium channel activity 	
Process 	GO:1901843 	positive regulation of high voltage-gated calcium channel activity 	
Process 	GO:0021945 	positive regulation of cerebellar granule cell migration by calcium 	
Process 	GO:0051564 	elevation of smooth endoplasmic reticulum calcium ion concentration 	
Process 	GO:0051565 	reduction of smooth endoplasmic reticulum calcium ion concentration 	
Process 	
GO:0052437
	modulation by organism of defense-related symbiont calcium ion flux 	
Process 	GO:0075020 	calcium or calmodulin-mediated activation of appressorium formation 	
Process 	GO:0010462 	regulation of light-activated voltage-gated calcium channel activity 	
Function 	GO:0022894 	Intermediate conductance calcium-activated potassium channel activity 	
Process 	GO:0043006 	activation of phospholipase A2 activity by calcium-mediated signaling 	
Process 	GO:1901020 	negative regulation of calcium ion transmembrane transporter activity 	
Process 	GO:1901021 	positive regulation of calcium ion transmembrane transporter activity 	
Process 	GO:1902081 	negative regulation of calcium ion import into sarcoplasmic reticulum 	
Process 	GO:1902082 	positive regulation of calcium ion import into sarcoplasmic reticulum 	
Process 	GO:0051280 	negative regulation of release of sequestered calcium ion into cytosol 	
Process 	GO:0051281 	positive regulation of release of sequestered calcium ion into cytosol 	
Function 	GO:0005220 	inositol 1,4,5-trisphosphate-sensitive calcium-release channel activity 	
Process 	GO:0008591 	regulation of Wnt receptor signaling pathway, calcium modulating pathway 	
Process 	GO:0052389 	positive regulation by symbiont of defense-related host calcium ion flux 	
Function 	GO:0008597 	calcium-dependent protein serine/threonine phosphatase regulator activity 	
Process 	GO:0014808 	release of sequestered calcium ion into cytosol by sarcoplasmic reticulum 	
Process 	GO:0060315 	negative regulation of ryanodine-sensitive calcium-release channel activity 	
Process 	GO:0060316 	positive regulation of ryanodine-sensitive calcium-release channel activity 	
Process 	GO:0060470 	elevation of cytosolic calcium ion concentration involved in egg activation 	
Process 	
GO:0052531
	positive regulation by organism of defense-related symbiont calcium ion flux 	
Function 	GO:0048101 	calcium - and calmodulin-regulated 3',5'-cyclic-GMP phosphodiesterase activity 	
Process 	
GO:0051925
	regulation of calcium ion transport via voltage-gated calcium channel activity 	
Process 	
GO:0032234
	regulation of calcium ion transport via store-operated calcium channel activity 	
Process 	GO:0035918 	negative regulation of voltage-gated calcium channel activity in other organism 	
Process 	GO:0045812 	negative regulation of Wnt receptor signaling pathway, calcium modulating pathway 	
Process 	GO:0045813 	positive regulation of Wnt receptor signaling pathway, calcium modulating pathway 	
Process 	GO:0075103 	modulation by host of symbiont calcium or calmodulin-mediated signal transduction 	
Process 	GO:0075133 	modulation by symbiont of host calcium or calmodulin-mediated signal transduction 	
Process 	GO:0044472 	envenomation resulting in modulation of calcium channel activity in other organism 	
Process 	GO:0035919 	negative regulation of low voltage-gated calcium channel activity in other organism 	
Process 	GO:0035920 	negative regulation of high voltage-gated calcium channel activity in other organism 	
Process 	GO:0075177 	regulation of calcium or calmodulin-mediated signal transduction in response to host 	
Process 	GO:0031585 	regulation of inositol 1,4,5-trisphosphate-sensitive calcium-release channel activity 	
Process 	GO:0010880 	regulation of release of sequestered calcium ion into cytosol by sarcoplasmic reticulum 	
Process 	
GO:0051927
	negative regulation of calcium ion transport via voltage-gated calcium channel activity 	
Process 	
GO:0051929
	positive regulation of calcium ion transport via voltage-gated calcium channel activity 	
Process 	GO:0052168 	modulation by symbiont of defense-related host calcium-dependent protein kinase pathway 	
Process 	
GO:0032235
	negative regulation of calcium ion transport via store-operated calcium channel activity 	
Process 	
GO:0032236
	positive regulation of calcium ion transport via store-operated calcium channel activity 	
Process 	GO:1900621 	regulation of transcription from RNA polymerase II promoter by calcium-mediated signaling 	
Process 	GO:0075104 	positive regulation by host of symbiont calcium or calmodulin-mediated signal transduction 	
Process 	GO:0075105 	negative regulation by host of symbiont calcium or calmodulin-mediated signal transduction 	
Process 	GO:0075134 	positive regulation by symbiont of host calcium or calmodulin-mediated signal transduction 	
Process 	GO:0075135 	negative regulation by symbiont of host calcium or calmodulin-mediated signal transduction 	
Process 	GO:0044473 	envenomation resulting in negative regulation of calcium channel activity in other organism 	
Process 	
GO:0052436
	modulation by organism of defense-related symbiont calcium-dependent protein kinase pathway 	
Process 	GO:0014809 	regulation of skeletal muscle contraction by regulation of release of sequestered calcium ion 	
Process 	GO:0075178 	positive regulation of calcium or calmodulin-mediated signal transduction in response to host 	
Process 	GO:0075179 	negative regulation of calcium or calmodulin-mediated signal transduction in response to host 	
Process 	GO:0031586 	negative regulation of inositol 1,4,5-trisphosphate-sensitive calcium-release channel activity 	
Process 	GO:0031587 	positive regulation of inositol 1,4,5-trisphosphate-sensitive calcium-release channel activity 	
Function 	GO:0086056 	voltage-gated calcium channel activity involved in regulation of AV node cell action potential 	
Function 	GO:0086059 	voltage-gated calcium channel activity involved in regulation of SA node cell action potential 	
Process 	GO:1901196 	positive regulation of calcium-mediated signaling involved in cellular response to salt stress 	
Process 	GO:1901197 	positive regulation of calcium-mediated signaling involved in cellular response to calcium ion 	
Process 	GO:0014723 	regulation of skeletal muscle contraction by modulation of calcium ion sensitivity of myofibril 	
Process 	GO:0061400 	positive regulation of transcription from RNA polymerase II promoter in response to calcium ion 	
Process 	GO:0010881 	regulation of cardiac muscle contraction by regulation of the release of sequestered calcium ion 	
Process 	GO:0052102 	positive regulation by symbiont of defense-related host calcium-dependent protein kinase pathway 	
Process 	
GO:1900031
	regulation of transcription from RNA polymerase II promoter involved in calcium-mediated signaling 	
Process 	GO:1900622 	positive regulation of transcription from RNA polymerase II promoter by calcium-mediated signaling 	
Function 	GO:0086038 	calcium:sodium antiporter activity involved in regulation of cardiac muscle cell membrane potential 	
Process 	
GO:0052503
	positive regulation by organism of defense-related symbiont calcium-dependent protein kinase pathway 	
Function 	GO:0086057 	voltage-gated calcium channel activity involved in regulation of bundle of His cell action potential 	
Function 	GO:0086007 	voltage-gated calcium channel activity involved in regulation of cardiac muscle cell action potential 	
Function 	GO:0086039 	calcium-transporting ATPase activity involved in regulation of cardiac muscle cell membrane potential 	
Process 	GO:0014810 	positive regulation of skeletal muscle contraction by regulation of release of sequestered calcium ion 	
Process 	GO:0014811 	negative regulation of skeletal muscle contraction by regulation of release of sequestered calcium ion 	
Process 	GO:0060472 	positive regulation of cortical granule exocytosis by elevation of cytosolic calcium ion concentration 	
Process 	GO:1901198 	positive regulation of calcium ion transport into cytosol involved in cellular response to calcium ion 	
Process 	GO:1901199 	positive regulation of calcium ion transport into cytosol involved in cellular response to salt stress 	
Process 	GO:1901200 	negative regulation of calcium ion transport into cytosol involved in cellular response to salt stress 	
Function 	GO:0086058 	voltage-gated calcium channel activity involved in regulation of Purkinje myocyte cell action potential 	
Function 	GO:0097364 	stretch-activated, cation-selective, calcium channel activity involved in regulation of action potential 	
Process 	GO:0044474 	envenomation resulting in negative regulation of voltage-gated calcium channel activity in other organism 	
Process 	GO:0021808 	cytosolic calcium signaling involved in initiation of cell movement in glial-mediated radial cell migration 	
Process 	
GO:1900061
	positive regulation of transcription from RNA polymerase II promoter involved in calcium-mediated signaling 	
Process 	GO:0044476 	envenomation resulting in negative regulation of low voltage-gated calcium channel activity in other organism 	
Process 	GO:0052265 	induction by organism of defense-related calcium ion flux in other organism involved in symbiotic interaction 	
Process 	GO:0044475 	envenomation resulting in negative regulation of high voltage-gated calcium channel activity in other organism 	
Process 	GO:0052301 	modulation by organism of defense-related calcium ion flux in other organism involved in symbiotic interaction 	
Process 	GO:0052065 	positive regulation by organism of defense-related calcium ion flux in other organism involved in symbiotic interaction 	
Process 	GO:0051482 	elevation of cytosolic calcium ion concentration involved in phospholipase C-activating G-protein coupled signaling pathway 	
Function 	GO:0097365 	stretch-activated, cation-selective, calcium channel activity involved in regulation of cardiac muscle cell action potential 	
Process 	GO:0030926 	calcium incorporation into metallo-oxygen cluster via bis-L-aspartato tris-L-glutamato L-histidino calcium tetramanganese tetroxide 	
Process 	GO:0030927 	manganese incorporation into metallo-oxygen cluster via bis-L-aspartato tris-L-glutamato L-histidino calcium tetramanganese tetroxide 	
Process 	GO:0052307 	modulation by organism of defense-related calcium-dependent protein kinase pathway in other organism involved in symbiotic interaction 	
Process 	GO:0052287 	positive regulation by organism of defense-related calcium-dependent protein kinase pathway in other organism involved in symbiotic interaction 	
Process 	GO:0086094 	positive regulation of ryanodine-sensitive calcium-release channel activity by adrenergic receptor signaling pathway involved in positive regulation of cardiac muscle contraction 
