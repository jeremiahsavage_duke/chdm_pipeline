#GTF2BED
fgrep -w transcript_id Danio_rerio.Zv9.71.gtf | sed 's/[";]//g;' | awk '{OFS="\t"; print $1, $4-4,$5,$7,$12}' > Danio_rerio.Zv9.71.bed


#WIR19
/home/jeremiah/Project_Chen/Sample_Wir19_Mut/
mkdir emit_all_sites
cd emit_all_sites

java -d64 -Xmx10G -jar ~/.local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I ../Wir19_Mut_CTTGTA_R1_fixmateinformation.bam -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Wir19_Mut.vcf --output_mode EMIT_ALL_SITES -nt 24 --min_base_quality_score 5 --annotation AlleleBalance --annotation AlleleBalanceBySample --annotation BaseCounts --annotation ChromosomeCounts --annotation Coverage --annotation DepthPerAlleleBySample --annotation FisherStrand --annotation HomopolymerRun --annotation SampleList -stand_call_conf 5 -stand_emit_conf 5 -rf MappingQuality --min_mapping_quality_score 1 -rf ReassignMappingQuality -L 25:31200000-37400000

java -d64 -Xmx10G -jar ~/.local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I ~/Project_Chen/Sample_Wir19_Wt/Wir19_WT_GCCAAT_R1_fixmateinformation.bam -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Wir19_Wt.vcf --output_mode EMIT_ALL_SITES -nt 24 --min_base_quality_score 5 --annotation AlleleBalance --annotation AlleleBalanceBySample --annotation BaseCounts --annotation ChromosomeCounts --annotation Coverage --annotation DepthPerAlleleBySample --annotation FisherStrand --annotation HomopolymerRun --annotation SampleList -stand_call_conf 5 -stand_emit_conf 5 -rf MappingQuality --min_mapping_quality_score 1 -rf ReassignMappingQuality -L 25:31200000-37400000

#java -Xmx4G -d64 -jar ~/.local/bin/GenomeAnalysisTK.jar -T CombineVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa --variant Wir19_Mut.vcf --variant Wir19_Wt.vcf -o combine.vcf --genotypemergeoption UNIQUIFY

#java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar -T SelectVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o region.vcf --variant combine.vcf -L 25:32559436-32681121

java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar -T SelectVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Wir19_region.vcf --variant Wir19_Mut.vcf -L 25:32559436-32681121

python ~/chdm_pipeline/chen/ken_grant_20131030/novel_snps.py -i Wir19_region.vcf -o Wir19_novel.vcf

#java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar -T DepthOfCoverage -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -I ../Wir19_Mut_CTTGTA_R1_fixmateinformation.bam -L 25:32559436-32681121 -o depthofcov


samtools view -H ../Wir19_Mut_CTTGTA_R1_fixmateinformation.bam > header_genome.txt

cat header_genome.txt ~/zv9/Danio_rerio.Zv9.71.bed > Danio_rerio.Zv9.71_genome.bed

java -d64 -Xmx4G -jar /home/jeremiah/.local/bin/picard/CalculateHsMetrics.jar BAIT_INTERVALS=Danio_rerio.Zv9.71_genome.bed TARGET_INTERVALS=Danio_rerio.Zv9.71_genome.bed INPUT=/home/jeremiah/Project_Chen/Sample_Wir19_Mut/Wir19_Mut_CTTGTA_R1_fixmateinformation.bam REFERENCE_SEQUENCE=~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa OUTPUT=Wir19_genome_hybridselection_metrics.tsv



#region
samtools view ../Wir19_Mut_CTTGTA_R1_fixmateinformation.bam 25:32559436-32681121 > Wir19_region.sam

samtools view -bT ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa Wir19_region.sam > Wir19_region.bam

java -Xmx4g -jar ~/.local/bin/picard/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=LENIENT VERBOSITY=ERROR SO=unsorted INPUT=Wir19_region.bam OUTPUT=Wir19_region_rg.bam RGPL=illumina RGLB=temp RGPU=temp RGSM=Wir19_Mut_CTTGTA_R1

samtools view -H Wir19_region.bam > header_region.txt

cat header_region.txt ~/zv9/Danio_rerio.Zv9.71.bed > Danio_rerio.Zv9.71_region.bed



java -d64 -Xmx4G -jar ~/.local/bin/picard/CalculateHsMetrics.jar BAIT_INTERVALS=Danio_rerio.Zv9.71_region.bed TARGET_INTERVALS=Danio_rerio.Zv9.71_region.bed INPUT=Wir19_region_rg.bam REFERENCE_SEQUENCE=~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa OUTPUT=Wir19_region_hybridselection_metrics.tsv



###DIR7
cd /home/jeremiah/Project_Chen/Sample_Dir7_Mut/
mkdir emit_all_sites
cd emit_all_sites

java -d64 -Xmx10G -jar ~/.local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I ../Dir7_Mut_TGACCA_R1_fixmateinformation.bam -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Dir7_Mut.vcf --output_mode EMIT_ALL_SITES -nt 24 --min_base_quality_score 5 --annotation AlleleBalance --annotation AlleleBalanceBySample --annotation BaseCounts --annotation ChromosomeCounts --annotation Coverage --annotation DepthPerAlleleBySample --annotation FisherStrand --annotation HomopolymerRun --annotation SampleList -stand_call_conf 5 -stand_emit_conf 5 -rf MappingQuality --min_mapping_quality_score 1 -rf ReassignMappingQuality -L 1:48000000-51400000

java -d64 -Xmx10G -jar ~/.local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I ~/Project_Chen/Sample_Dir7_Wt/Dir7_WT_CGATGT_R1_fixmateinformation.bam -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Dir7_Wt.vcf --output_mode EMIT_ALL_SITES -nt 24 --min_base_quality_score 5 --annotation AlleleBalance --annotation AlleleBalanceBySample --annotation BaseCounts --annotation ChromosomeCounts --annotation Coverage --annotation DepthPerAlleleBySample --annotation FisherStrand --annotation HomopolymerRun --annotation SampleList -stand_call_conf 5 -stand_emit_conf 5 -rf MappingQuality --min_mapping_quality_score 1 -rf ReassignMappingQuality -L  1:48000000-51400000

#java -Xmx4G -d64 -jar ~/.local/bin/GenomeAnalysisTK.jar -T CombineVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa --variant Dir7_Mut.vcf --variant Dir7_Wt.vcf -o combine.vcf --genotypemergeoption UNIQUIFY

#java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar -T SelectVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o region.vcf --variant combine.vcf -L 1:49839890-50265658

java -d64 -Xmx4G -jar ~/.local/bin/GenomeAnalysisTK.jar -T SelectVariants -R ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa -o Dir7_region.vcf --variant Dir7_Mut.vcf -L 1:49839890-50265658


samtools view -H ../Dir7_Mut_TGACCA_R1_fixmateinformation.bam > header_genome.txt

cat header_genome.txt ~/zv9/Danio_rerio.Zv9.71.bed > Danio_rerio.Zv9.71_genome.bed

java -d64 -Xmx4G -jar /home/jeremiah/.local/bin/picard/CalculateHsMetrics.jar BAIT_INTERVALS=Danio_rerio.Zv9.71_genome.bed TARGET_INTERVALS=Danio_rerio.Zv9.71_genome.bed INPUT=../Dir7_Mut_TGACCA_R1_fixmateinformation.bam REFERENCE_SEQUENCE=~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa OUTPUT=Dir7_genome_hybridselection_metrics.tsv

#region
samtools view ../Dir7_Mut_TGACCA_R1_fixmateinformation.bam 1:49839890-50265658 > Dir7_region.sam

samtools view -bT ~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa Dir7_region.sam > Dir7_region.bam

java -Xmx4g -jar ~/.local/bin/picard/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=LENIENT VERBOSITY=ERROR SO=unsorted INPUT=Dir7_region.bam OUTPUT=Dir7_region_rg.bam RGPL=illumina RGLB=temp RGPU=temp RGSM=Dir7_Mut_TGACCA_R1

samtools view -H Dir7_region_rg.bam > header_region.txt

cat header_region.txt ~/zv9/Danio_rerio.Zv9.71.bed > Danio_rerio.Zv9.71_region.bed

java -d64 -Xmx4G -jar ~/.local/bin/picard/CalculateHsMetrics.jar BAIT_INTERVALS=Danio_rerio.Zv9.71_region.bed TARGET_INTERVALS=Danio_rerio.Zv9.71_region.bed INPUT=Dir7_region_rg.bam REFERENCE_SEQUENCE=~/zv9/Danio_rerio.Zv9.71.dna.toplevel.fa OUTPUT=Wir19_region_hybridselection_metrics.tsv

