ack D500-orig 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D500-nc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D500-nr 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D500-rc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D700-orig 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D700-nc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D700-nr 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l
ack D700-rc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded.fastq | wc -l

ack D500-orig 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D500-nc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D500-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D500-rc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D700-orig 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D700-nc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D700-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
ack D700-rc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l


/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_af_orig.fasta -o 248002_CGAGGCTG-TATCCTCT_R1_scythe_orig.fastq -m 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_orig.fastq 248002_CGAGGCTG-TATCCTCT_R1.fastq -qsanger

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_af_nc.fasta -o 248002_CGAGGCTG-TATCCTCT_R1_scythe_nc.fastq -m 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nc.fastq 248002_CGAGGCTG-TATCCTCT_R1.fastq -qsanger

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_af_nr.fasta -o 248002_CGAGGCTG-TATCCTCT_R1_scythe_nr.fastq -m 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nr.fastq 248002_CGAGGCTG-TATCCTCT_R1.fastq -qsanger

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_af_rc.fasta -o 248002_CGAGGCTG-TATCCTCT_R1_scythe_rc.fastq -m 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_rc.fastq 248002_CGAGGCTG-TATCCTCT_R1.fastq -qsanger

####
[jeremiah@chdm-s001 Sample_248002]$ ack D500-nc-orig 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_orig.fastq | wc -l
186793
[jeremiah@chdm-s001 Sample_248002]$ ack D700-nr-orig 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_orig.fastq | wc -l
45957
####
[jeremiah@chdm-s001 Sample_248002]$ ack D500-nc-nc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nc.fastq | wc -l
186939
[jeremiah@chdm-s001 Sample_248002]$ ack D700-nr-nc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nc.fastq | wc -l
#46169
#####
[jeremiah@chdm-s001 Sample_248002]$ ack D500-nc-nr 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nr.fastq | wc -l
#187748
[jeremiah@chdm-s001 Sample_248002]$ ack D700-nr-nr 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_nr.fastq | wc -l
45781
####
[jeremiah@chdm-s001 Sample_248002]$ ack D500-nc-rc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_rc.fastq | wc -l
184425
[jeremiah@chdm-s001 Sample_248002]$ 
[jeremiah@chdm-s001 Sample_248002]$ ack D700-nr-rc 248002_CGAGGCTG-TATCCTCT_R1_scythediscarded_rc.fastq | wc -l
45890


###R2###
[jeremiah@chdm-s001 Sample_248002]$ ack D500-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
94381
[jeremiah@chdm-s001 Sample_248002]$ ack D700-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded.fastq | wc -l
65511


/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_r2_orig.fasta -o 248002_CGAGGCTG-TATCCTCT_R2_scythe_orig.fastq -m 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_orig.fastq 248002_CGAGGCTG-TATCCTCT_R2.fastq -qsanger &

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_r2_nr.fasta -o 248002_CGAGGCTG-TATCCTCT_R2_scythe_nr.fastq -m 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nr.fastq 248002_CGAGGCTG-TATCCTCT_R2.fastq -qsanger &

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_r2_nc.fasta -o 248002_CGAGGCTG-TATCCTCT_R2_scythe_nc.fastq -m 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nc.fastq 248002_CGAGGCTG-TATCCTCT_R2.fastq -qsanger &

/usr/bin/time scythe -a nextera_adapters_CGAGGCTG_TATCCTCT_r2_rc.fasta -o 248002_CGAGGCTG-TATCCTCT_R2_scythe_rc.fastq -m 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_rc.fastq 248002_CGAGGCTG-TATCCTCT_R2.fastq -qsanger &

####
ack D500-nr-orig 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_orig.fastq | wc -l
125681
ack D700-nr-orig 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_orig.fastq | wc -l
98524
####
ack D500-nr-nc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nc.fastq | wc -l
124971
ack D700-nr-nc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nc.fastq | wc -l
#99253
#####
ack D500-nr-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nr.fastq | wc -l
#127651
ack D700-nr-nr 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_nr.fastq | wc -l
97737
####
ack D500-nr-rc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_rc.fastq | wc -l
126842
ack D700-nr-rc 248002_CGAGGCTG-TATCCTCT_R2_scythediscarded_rc.fastq | wc -l
98892




###cutadapt
Command line parameters: 248002_CGAGGCTG-TATCCTCT_R1.fastq -a CACTGACCTCAAGTCTGCACACGAGAAGGCTAG --info-file=cutadapt_info7b.out -o cutadapt_write7b.out
Maximum error rate: 10.00%
   No. of adapters: 1
   Processed reads:      6155929
   Processed bases:    492474320 bp (492.5 Mbp)
     Trimmed reads:       165870 (2.7%)
     Trimmed bases:       534722 bp (0.5 Mbp) (0.11% of total)
   Too short reads:            0 (0.0% of processed reads)
    Too long reads:            0 (0.0% of processed reads)
        Total time:    158.75 s
     Time per read:      0.03 ms

cutadapt -g AATGATACGGCGACCACCGAGATCTACAC 248002_CGAGGCTG-TATCCTCT_L002_R1_001.fastq --info-file ca_1i.fastq -o ca_1.fastq
cutadapt -g ACACTCTTTCCCTACACGACGCTCTTCCGATCT ca_1.fastq --info-file ca_2i.fastq -o ca_2.fastq
cutadapt -g GATCGGAAGAGCACACGTCTGAACTCCAGTCAC ca_2.fastq --info-file ca_3i.fastq -o ca_3.fastq
cutadapt -g ATCTCGTATGCCGTCTTCTGCTTG ca_3.fastq --info-file ca_4i.fastq -o ca_4.fastq

cutadapt -g TCTAGCCTTCTCGCAGCACATCCCTTTCTCACA ca_4.fastq --info-file ca_5i.fastq -o ca_5.fastq
cutadapt -g CACATCTAGAGCCACCAGCGGCATAGTAA ca_5.fastq --info-file ca_6i.fastq -o ca_6.fastq
cutadapt -g GTTCGTCTTCTGCCGTATGCTCTA ca_6.fastq --info-file ca_7i.fastq -o ca_7.fastq
cutadapt -g CACTGACCTCAAGTCTGCACACGAGAAGGCTAG ca_7.fastq --info-file ca_8i.fastq -o ca_8.fastq

cutadapt -g TTACTATGCCGCTGGTGGCTCTAGATGTG ca_8.fastq --info-file ca_9i.fastq -o ca_9.fastq
cutadapt -g TGTGAGAAAGGGATGTGCTGCGAGAAGGCTAGA ca_9.fastq --info-file ca_10i.fastq -o ca_10.fastq
cutadapt -g CTAGCCTTCTCGTGTGCAGACTTGAGGTCAGTG ca_10.fastq --info-file ca_11i.fastq -o ca_11.fastq
cutadapt -g TAGAGCATACGGCAGAAGACGAAC ca_11.fastq --info-file ca_12i.fastq -o ca_12.fastq

cutadapt -g AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT ca_12.fastq --info-file ca_13i.fastq -o ca_13.fastq
cutadapt -g GTGTAGATCTCGGTGGTCGCCGTATCATT ca_13.fastq --info-file ca_14i.fastq -o ca_14.fastq
cutadapt -g CAAGCAGAAGACGGCATACGAGAT ca_14.fastq --info-file ca_15i.fastq -o ca_15.fastq
cutadapt -g  GTGACTGGAGTTCAGACGTGTGCTCTTCCGATC ca_15.fastq --info-file ca_16i.fastq -o ca_16.fastq


TATAGCCT

nextera_i5=("TAGATCGC" "CTCTCTAT" "TATCCTCT" "AGAGTAGA" "GTAAGGAG" "ACTGCATA" "AAGGAGTA" "CTAAGCCT")
nextera_i7=("TCGCCTTA" "CTAGTACG" "TTCTGCCT" "GCTCAGGA" "AGGAGTCC" "CATGCCTA" "GTAGAGAG" "CCTCTCTG" "AGCGTAGC" "CAGCCTCG" "TGCCTCTT" "TCCTCTAC")

for var in "${nextera_i5[@]}" ; do echo "${var}" ; done

bowtie2-build -f hg19broad.fa hg19broad

"/usr/bin/time bowtie2 -p " + str(numcpus) + " -x " + referencegenomename + " -1 " + read1 + " -2 " + read2 + " -S " + output --phred33 --end-to-end --un " UnpairedFail"
"/usr/bin/time bowtie2 -p " + str(numcpus) + " -x " + referencegenomename + " -1 " + read1 + " -2 " + read2 + " -S " + output --phred33 --end-to-end --very-sensitive
"/usr/bin/time bowtie2 -p " + str(numcpus) + " -x " + referencegenomename + " -1 " + read1 + " -2 " + read2 + " -S " + output --phred33 --local
"/usr/bin/time bowtie2 -p " + str(numcpus) + " -x " + referencegenomename + " -1 " + read1 + " -2 " + read2 + " -S " + output --phred33 --local --very-sensitive-local

/usr/bin/time bowtie2 -p 12 -x /home/jeremiah/hg19broad/hg19broad -1 248002_CGAGGCTG-TATCCTCT_R1_scythe.fastq -2 248002_CGAGGCTG-TATCCTCT_R2_scythe.fastq -S scythe_e2e.sam --phred33 --end-to-end --un scythe_e2e_un

/usr/bin/time bowtie2 -p 12 -x /home/jeremiah/hg19broad/hg19broad -1 248002_CGAGGCTG-TATCCTCT_R1_scythe.fastq -2 248002_CGAGGCTG-TATCCTCT_R2_scythe.fastq -S scythe_local_vs.sam --phred33 --local --very-sensitive-local
