###2013-01-02
python ~/chdm_pipeline/vcf/keep_genes.py -i FINAL.457.CILIARY.SAMPLES.PROJECT.LEVEL.ANNOTATED.UNZIPPED.vcf -f kept_genes.txt -o keptgene.vcf
python ~/chdm_pipeline/vcf/keep_samples.py -i keptgene.vcf -f kept_samples.txt -o keptsamples.vcf


python ~/chdm_pipeline/vcf/collapse_nonref_v2.py -i keptsamples.vcf -o hethom_collapse_lo.vcf
python ~/chdm_pipeline/vcf/ucg_classtypes.py -i hethom_collapse_lo.vcf -o ucg.out
python ~/chdm_pipeline/vcf/sep_ucg.py -i hethom_collapse_lo.vcf -u ucg.out
mkdir loinfo
mv splicing.vcf nonsynonymous_snv_exonic_splicing.vcf utr5.vcf intronic.vcf synonymous_snv.vcf stopgain_snv.vcf stopgain_snv_exonic_splicing.vcf utr3.vcf synonymous_snv_exonic_splicing.vcf ncrna.vcf nonsynonymous_snv.vcf loinfo/
tar -cvvf loinfo.tar loinfo

python ~/chdm_pipeline/vcf/collapse_nonref.py -i keptsamples.vcf -o hethom_collapse_hi.vcf
python ~/chdm_pipeline/vcf/sep_ucg.py -i hethom_collapse_hi.vcf -u ucg.out
mkdir hiinfo
mv splicing.vcf nonsynonymous_snv_exonic_splicing.vcf utr5.vcf intronic.vcf synonymous_snv.vcf stopgain_snv.vcf stopgain_snv_exonic_splicing.vcf utr3.vcf synonymous_snv_exonic_splicing.vcf ncrna.vcf nonsynonymous_snv.vcf hiinfo/
tar -cvvf hiinfo.tar hiinfo

###2013-01-04
cd ~/data/esp500ift/
python ~/chdm_pipeline/vcf/join_vcf_ano6500.py -v thresh18.out.exonic_variant_function -a ~/hg19_esp6500si_all.txt -o thresh18.out.exonic_variant_function.maf

python ~/chdm_pipeline/vcf/join_anno_ano6500.py -v thresh18.out.nonysn -a ~/hg19_esp6500si_all.txt -o thresh18.out.nonysn.maf



###2013-01-07
python ~/chdm_pipeline/vcf/join_splicinganno_ano6500.py -v thresh18.out.splicing -a ~/hg19_esp6500si_all.txt -o thresh18.out.splicing.maf


#splicing
python ~/chdm_pipeline/vcf/join_splicinganno_vcf.py -v ESP500IFTGenesOnly.vcf -a thresh18.out.splicing.maf -o thresh18.splicing.maf.vcf 

python ~/chdm_pipeline/vcf/collapse_ann_vcf_hi.py -i thresh18.splicing.maf.vcf -o esp500.thresh18.splicing.maf.collapse.hi.vcf
python ~/chdm_pipeline/vcf/collapse_ann_vcf_lo.py -i thresh18.splicing.maf.vcf -o esp500.thresh18.splicing.maf.collapse.lo.vcf


#nonsyn
python ~/chdm_pipeline/vcf/join_anno_vcf.py -v ESP500IFTGenesOnly.vcf -a thresh18.out.nonysn.maf -o thresh18.out.nonysn.maf.vcf

python ~/chdm_pipeline/vcf/collapse_ann_vcf_hi.py -i thresh18.out.nonysn.maf.vcf -o esp500.thresh18.nonsyn.maf.collapse.hi.vcf
python ~/chdm_pipeline/vcf/collapse_ann_vcf_lo.py -i thresh18.out.nonysn.maf.vcf -o esp500.thresh18.nonsyn.maf.collapse.lo.vcf

mkdir esp500ift
cp esp500.thresh18.* esp500ift/
tar -cvvf esp500ift.tar esp500ift/


####2013-01-08
python ~/chdm_pipeline/vcf/keep_genes.py -i FINAL.457.CILIARY.SAMPLES.PROJECT.LEVEL.INDELS.ANNOTATED.UNZIPPED.vcf -f kept_genes.txt -o kept_gene_indel.vcf

python ~/chdm_pipeline/vcf/keep_samples.py -i kept_gene_indel.vcf -f kept_samples.txt -o kept_samples_indel.vcf

python ~/chdm_pipeline/vcf/collapse_nonref_v2.py -i kept_samples_indel.vcf -o 457_ciliary_indel_hethom_collapse_lo.vcf
#python ~/chdm_pipeline/vcf/ucg_classtypes.py -i hethom_collapse_lo.vcf -o ucg.out
#python ~/chdm_pipeline/vcf/sep_ucg.py -i hethom_collapse_lo.vcf -u ucg.out
mkdir loinfo
#mv splicing.vcf nonsynonymous_snv_exonic_splicing.vcf utr5.vcf intronic.vcf synonymous_snv.vcf stopgain_snv.vcf stopgain_snv_exonic_splicing.vcf utr3.vcf synonymous_snv_exonic_splicing.vcf ncrna.vcf nonsynonymous_snv.vcf loinfo/
#tar -cvvf loinfo.tar loinfo

python ~/chdm_pipeline/vcf/collapse_nonref.py -i kept_samples_indel.vcf -o 457_ciliary_indel_hethom_collapse_hi.vcf
#python ~/chdm_pipeline/vcf/sep_ucg.py -i hethom_collapse_hi.vcf -u ucg.out
#mkdir hiinfo
#mv splicing.vcf nonsynonymous_snv_exonic_splicing.vcf utr5.vcf intronic.vcf synonymous_snv.vcf stopgain_snv.vcf stopgain_snv_exonic_splicing.vcf utr3.vcf synonymous_snv_exonic_splicing.vcf ncrna.vcf nonsynonymous_snv.vcf hiinfo/
#tar -cvvf hiinfo.tar hiinfo










###2013-01-26
mv MasterVariantList_IFT_20\ genes_201301_update_delgrey.csv MasterVariantList_IFT_20_genes_201301_update_delgrey.tsv

python ~/chdm_pipeline/util/erica_ift_2vcf.py -i MasterVariantList_IFT_20_genes_201301_update_delgrey.tsv

~/tools/annovar/convert2annovar.pl MasterVariantList_IFT_20_genes_201301_update_delgrey.vcf -format vcf4 -includeinfo > MasterVariantList_IFT_20_genes_201301_update_delgrey.annovar

~/tools/annovar/summarize_annovar.pl --buildver hg19 MasterVariantList_IFT_20_genes_201301_update_delgrey.annovar ~/tools/annovar/humandb/ -outfile sum --ver1000g 1000g2012apr --verdbsnp 137 --veresp 6500si

cp MasterVariantList_IFT_20_genes_201301_update_delgrey.annovar refgene.annovar
cp MasterVariantList_IFT_20_genes_201301_update_delgrey.annovar ensgene.annovar
cp MasterVariantList_IFT_20_genes_201301_update_delgrey.annovar knowngene.annovar

~/tools/annovar/annotate_variation.pl --geneanno --buildver hg19 -separate -exonsort -dbtype refgene refgene.annovar ~/tools/annovar/humandb/
~/tools/annovar/annotate_variation.pl --geneanno --buildver hg19 -separate -exonsort -dbtype ensgene ensgene.annovar ~/tools/annovar/humandb/
~/tools/annovar/annotate_variation.pl --geneanno --buildver hg19 -separate -exonsort -dbtype knowngene knowngene.annovar ~/tools/annovar/humandb/
