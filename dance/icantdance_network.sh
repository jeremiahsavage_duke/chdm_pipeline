###rhel 6.3
IFCFG=/etc/sysconfig/network-scripts/ifcfg-eth1

sed -i 's/^BOOTPROTO="dhcp"/BOOTPROTO="static"/g' $IFCFG
sed -i 's/^NM_CONTROLLED="yes"/NM_CONTROLLED="no"/g' $IFCFG
sed -i 's/^ONBOOT="no"/ONBOOT="yes"/g' $IFCFG
echo NETWORK=192.168.0.0 >> $IFCFG
echo NETMASK=255.255.255.0 >> $IFCFG
echo BROADCAST=192.168.0.255 >> $IFCFG
echo IPADDR=192.168.0.100 >> $IFCFG

###rhel 5
IFCFG=/etc/sysconfig/network-scripts/ifcfg-eth1

sed -i 's/^BOOTPROTO=dhcp/BOOTPROTO=none/g' $IFCFG
sed -i 's/^PEERDNS=yes/PEERDNS=no/g' $IFCFG
echo NETWORK=192.168.0.0 >> $IFCFG
echo NETMASK=255.255.255.0 >> $IFCFG
echo BROADCAST=192.168.0.255 >> $IFCFG
echo IPADDR=192.168.0.101 >> $IFCFG
