###2013-07-01
convertxls2csv -x for_xls_DM385-0001.xls -c ~/DM385-0001.csv
python ~/chdm_pipeline/util/csv2tab.py -i DM385-0001.csv -o DM385-0001.tsv

convertxls2csv -x taskforce/Baylor_Exome\ trio\ data/DM385/for_xls_DM385-0102.xls -c ~/DM385-0102.csv
python ~/chdm_pipeline/util/csv2tab.py -i DM385-0102.csv -o DM385-0102.tsv
perl ./parseexcel.pl
   DM385-0102.tsv
   DM385-0102.xls

###
convertxls2csv -x taskforce/Baylor_Exome\ trio\ data/DM268/for_xls_DM268-0001.xls -c DM268-0001.csv
python ~/chdm_pipeline/util/csv2tab.py -i DM268-0001.csv -o DM268-0001.tsv
perl ./parseexcel.pl
   DM268-0001.tsv
   DM268-0001.xls

convertxls2csv -x taskforce/Baylor_Exome\ trio\ data/DM268/for_xls_DM268-0100.xls -c DM268-0100.csv
python ~/chdm_pipeline/util/csv2tab.py -i DM268-0100.csv -o DM268-0100.tsv
perl ./parseexcel.pl
   DM268-0100.tsv
   DM268-0100.xls
