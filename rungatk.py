import inspect
import os
import subprocess
import multiprocessing # .cpu_count()

def realignertargetcreator(bamlist,referencegenomefasta,lastcall):
    numcpus = multiprocessing.cpu_count()
    fname=inspect.stack()[0][3]
    rtclist=[]
    #bamfile=bailist[0].replace(".bai","")
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname+"_"+fname+origextension+".list"
    rtclist.append(outname)
    if os.path.exists(outname):
        print("RealignerTargetCreator already run:",outname)
    else:
        rtccall="time java -d64 -Xmx4g -jar ~/.local/bin/GenomeAnalysisTK.jar -T RealignerTargetCreator -I " + bamfile + " -R " + referencegenomefasta + " -o " + outname + " -nt " + str(numcpus)
        subprocess.call([rtccall],shell=True)
    return rtclist,fname

def indelrealigner(bamlist,referencegenomefasta,rtclist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname.replace(lastcall,fname)+origextension
    outlist.append(outname)
    if os.path.exists(outname):
        print("IndelRealigner already run:",outname)
    else:
        ircall="time java -d64 -Xmx4g -jar ~/.local/bin/GenomeAnalysisTK.jar -T IndelRealigner -I " + bamfile + " -R " + referencegenomefasta + " -targetIntervals " + rtclist[0] + " -o " + outname
        subprocess.call([ircall],shell=True)
    return outlist,fname

def fixmateinformation(bamlist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    outname=bamfile.replace(lastcall,fname)
    outlist.append(outname)
    if os.path.exists(outname):
        print("FixMateInformation already run:",outname)
    else:
        fmicall="time java -d64 -Xmx4g -Djava.io.tmpdir=$HOME/.local/tmp -jar ~/.local/bin/picard/FixMateInformation.jar INPUT=" + bamfile + " OUTPUT=" + outname + " SO=coordinate VALIDATION_STRINGENCY=LENIENT CREATE_INDEX=true TMP_DIR=$HOME/.local/tmp"
        subprocess.call([fmicall],shell=True)
    return outlist,fname

def addreadgroup(bamlist,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    origname,origextension=os.path.splitext(bamlist[0])
    outfile=origname.replace(lastcall,fname)+origextension
    samplename=origname.replace("_"+lastcall,"")
    outlist.append(outfile)
    if os.path.exists(outfile):
        print("readgroups already added:",outfile)
    else:
        argcall="time java -d64 -Xmx4g -jar ~/.local/bin/picard/AddOrReplaceReadGroups.jar SORT_ORDER=coordinate INPUT="+bamlist[0]+" OUTPUT="+outfile+" RGID=temp"+" RGLB=temp"+" RGPL=illumina"+" RGPU=temp"+" RGSM="+samplename
        subprocess.call([argcall],shell=True)
    return outlist,fname

def printreads(bamlist,referencegenomefasta,recalfile,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname.replace(lastcall,fname)+origextension
    outlist.append(outname)
    if os.path.exists(outname):
        print("PrintReads already run:",outname)
    else:
        trcall="time java -d64 -Xmx4g -jar ~/.local/bin/GenomeAnalysisTK.jar -T PrintReads -I " + bamfile + " -R " + referencegenomefasta + " -o " + outname + " -BQSR " + recalfile
        subprocess.call([trcall],shell=True)
    return outlist,fname

def buildbamindex(bamlist):
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outfile=origname+".bai"
    if os.path.exists(outfile):
        print("bamfile already indexed:",outfile)
    else:
        bbicall="time java -d64 -Xmx4g -jar ~/.local/bin/picard/BuildBamIndex.jar INPUT="+bamfile
        subprocess.call([bbicall],shell=True)

def baserecalibrator(bamlist,referencegenomefasta,vcffile):
    numcpus = multiprocessing.cpu_count()
    fname=inspect.stack()[0][3]
    print("Begin baserecalibrator:",fname)
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    #outname=origname+"_"+fname+origextension
    recalfile="input."+origname+".grp"
    outlist.append(recalfile)
    if os.path.exists(recalfile):
        print("BaseRecalibrator already run:",recalfile)
    else:
        cccall="time java -d64 -Xmx16g -jar ~/.local/bin/GenomeAnalysisTK.jar -T BaseRecalibrator -I " + bamfile + " -R " + referencegenomefasta + " -knownSites "+vcffile+" -o " + recalfile + " -nct " + str(numcpus)
        print("cccall:",cccall)
        subprocess.call([cccall],shell=True)
    return outlist,fname

def haplotypecaller(bamlist,referencegenomefasta,targetfile,vcffile,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname+"_"+fname+".vcf"
    outlist.append(outname)
    if os.path.exists(outname):
        print("HaplotypeCaller already run:",outname)
    else:
        subcall="time java -d64 -Xmx4g -jar ~/.local/bin/GenomeAnalysisTK.jar -T HaplotypeCaller -I " + bamfile + " -R " + referencegenomefasta + " -o " + outname + " --dbsnp " + vcffile + " -L " + targetfile
        subprocess.call([subcall],shell=True)
    return outlist,fname

def unifiedgenotyper(bamlist,referencegenomefasta,targetfile,vcffile,lastcall,sampleploidy):
    MINBASEQUALITYSCORE = 5
    numcpus = multiprocessing.cpu_count()
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname+"_"+fname+".vcf"
    outlist.append(outname)
    if os.path.exists(outname):
        print("UnifiedGenotyper already run:",outname)
    else:
        subcall="time java -d64 -Xmx10g -jar ~/.local/bin/GenomeAnalysisTK.jar -T UnifiedGenotyper -glm BOTH -I " + bamfile + " -R " + referencegenomefasta + " -o " + outname + " -metrics snps.metrics --output_mode EMIT_VARIANTS_ONLY " + " -nt " + str(numcpus) + " --min_base_quality_score " +  str(MINBASEQUALITYSCORE) + " --annotation AlleleBalance --annotation AlleleBalanceBySample --annotation BaseCounts --annotation ChromosomeCounts --annotation Coverage --annotation DepthPerAlleleBySample --annotation FisherStrand --annotation HomopolymerRun --annotation SampleList -stand_call_conf 5 -stand_emit_conf 5 -rf MappingQuality --min_mapping_quality_score 1 -rf ReassignMappingQuality" # --sample_ploidy " + sampleploidy
        subprocess.call([subcall],shell=True)
    return outlist,fname

def calculatehsmetrics(bamlist,referencegenomefasta,baitfile,targetfile,lastcall):
    fname=inspect.stack()[0][3]
    outlist=[]
    bamfile=bamlist[0]
    origname,origextension=os.path.splitext(bamfile)
    outname=origname+"_"+fname+".hsmetrics"
    outlist.append(outname)
    if os.path.exists(outname):
        print("CalculateHsMetrics.jar already run:",outname)
    else:
        subcall="time java -d64 -Xmx4g -jar ~/.local/bin/picard/CalculateHsMetrics.jar BAIT_INTERVALS="+baitfile+" TARGET_INTERVALS="+targetfile+" INPUT="+bamfile+" OUTPUT="+outname+" PER_TARGET_COVERAGE=Exon_pertarget.txt VALIDATION_STRINGENCY=LENIENT REFERENCE_SEQUENCE="+referencegenomefasta
        subprocess.call([subcall],shell=True)
    return outlist,fname

def rungatk(bamlist,referencegenomefasta,baitfile,targetfile,vcffile,lastcall,sampleploidy,mutagenized):
    print("rungatk::addreadgroup()")
    bamlist,lastcall=addreadgroup(bamlist,lastcall)
    buildbamindex(bamlist)
    rtclist,lastcallrtc=realignertargetcreator(bamlist,referencegenomefasta,lastcall)
    bamlist,lastcall=indelrealigner(bamlist,referencegenomefasta,rtclist,lastcall)
    #bamlist,lastcall=addreadgroup(bamlist,lastcall)
    bamlist,lastcall=fixmateinformation(bamlist,lastcall)
    if (not mutagenized):
        outlist,lastcallbr=baserecalibrator(bamlist,referencegenomefasta,vcffile)
        bamlist,lastcall=printreads(bamlist,referencegenomefasta,outlist[0],lastcall)
        vcflist,lastcall=unifiedgenotyper(bamlist,referencegenomefasta,targetfile,vcffile,lastcall,sampleploidy)
        return vcflist,lastcall
    else:
        vcflist,lastcall=unifiedgenotyper(bamlist,referencegenomefasta,targetfile,vcffile,lastcall,sampleploidy)
        return vcflist,lastcall
    #hsmlist,lastcall=calculatehsmetrics(bamlist,referencegenomefasta,baitfile,targetfile,lastcall)
    #bailist,lastcall=indexbam(fixmatebamlist,lastcall)
    #vcflist,lastcall=haplotypecaller(bamlist,referencegenomefasta,targetfile,vcffile,lastcall)
