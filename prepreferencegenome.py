import os
import subprocess
import shutil
# local
import pipeutil

def catgenomefiles(chromosomefastalist,genomename,genomesource):
    print("chromosomefastalist=",chromosomefastalist)
    inputfilestring=""
    for c in chromosomefastalist:
        print("c=",c)
        inputfilestring=inputfilestring+c+" "
    outfile=genomename+".fa"
    print("outfile=",outfile)
    if os.path.exists(outfile):
        print("chromosomes already concatenated:",outfile)
    else:
        if ".tar.gz" in genomesource:
            copyextractgenome(genomesource)
            catgenomecall="time cat " + inputfilestring + " > " + outfile
            print(catgenomecall)
            subprocess.call([catgenomecall],shell=True)
            cleanupcall="rm -f chr*.fa " + os.path.basename(genomesource)
            print(cleanupcall)
            subprocess.call([cleanupcall],shell=True)
        elif ".gz" in genomesource:
            copyextractgenome(genomesource)
        elif ".fasta" in genomesource:
            cpgenomecall="time cp " + genomesource + " " + outfile
            print("cpgenomecall:",cpgenomecall)
            subprocess.call([cpgenomecall],shell=True)
    return outfile

def copyextractgenome_old(genomesource):
    shutil.copy(genomesource,os.getcwd())
    if ".tar.gz" in genomesource:
        extractgenomecall="tar -xvf " + os.path.basename(genomesource)
    elif ".gz" in genomesource:
        extractgenomecall="gunzip " + os.path.basename(genomesource)
    print(extractgenomecall)
    subprocess.call([extractgenomecall],shell=True)#should get list of files
                                                   #then rm when done

def copyextractgenome(genomesource,genomedir,genomefile):
    print('genomesource=',genomesource)
    print('genomedir=',genomedir)
    print('genomefile=',genomefile)
    genomepath = os.path.join(genomedir,genomefile)
    if (not os.path.isfile(genomefile)):
        if (genomesource.endswith(".gz")):
            extractgenomecall="gunzip -c " + genomesource + " > " + genomedir + "/" + genomefile
            print(extractgenomecall)
            subprocess.call([extractgenomecall],shell=True)
    else:
        print("genome already exists: ", genomefile)
    genomename,genomeext= os.path.splitext(genomefile)
    return genomename



def samtoolsfaidx(genomefile):
    outfile=genomefile+".fai"
    if os.path.exists(outfile):
        print("fasta file already indexed:",outfile)
    else:
        samtoolsfaidxcall="time samtools faidx " + genomefile
        print(samtoolsfaidxcall)
        subprocess.call([samtoolsfaidxcall],shell=True)
    return outfile

def bowtie2build(genomefile,genomename):
    if os.path.exists(genomename+".1.bt2"):
        print("bowtie2-build indices already created:",genomename)
    else:
        bowtie2buildcall="time bowtie2-build " + genomefile + " " + genomename
        print(bowtie2buildcall)
        subprocess.call([bowtie2buildcall],shell=True)
    return "FILLMEIN"

def picarddict(genomefile):
    origname,origextension=os.path.splitext(genomefile)
    outfile=origname+".dict"
    if os.path.exists(outfile):
        print("picard dictionary already created:",outfile)
    else:
        picarddictcall="java -d64 -Xmx4g -jar ~/.local/bin/picard/CreateSequenceDictionary.jar REFERENCE=" + genomefile + " OUTPUT=" + outfile
        print(picarddictcall)
        subprocess.call([picarddictcall],shell=True)
    return outfile

def copygtffile(genomesource,refgenomedir):
    gtffilesource=genomesource.replace("/fasta/","/gtf/")
    gtffilesource=gtffilesource.replace(".dna.toplevel.fa",".gtf")
    if os.path.exists(os.path.basename(gtffilesource).replace(".gz","")):
        print("gtf file already copied:",os.path.basename(gtffilesource).replace(".gz",""))
    else:
        shutil.copy(gtffilesource,os.getcwd())
        extractgtfcall="gunzip " + os.path.basename(gtffilesource)
        print(extractgtfcall)
        subprocess.call([extractgtfcall],shell=True)#should get list of files
        
def prepreferencegenome(genomedir,genomesource,genomefile):
    #housekeeping
    print('genomedir=',genomedir)
    if not os.path.exists(genomedir):
        os.mkdir(genomedir)
    else:
        if not os.path.isdir(genomedir):
            os.remove(genomedir)
            os.mkdir(genomedir)

    #genomename=pipeutil.pathlastdirname(refgenomedir)
    prevdir=os.getcwd()
    os.chdir(genomedir)
    #work
    #genomefile=catgenomefiles(chromosomefastalist,genomename,genomesource)
    genomename=copyextractgenome(genomesource,genomedir,genomefile)
    print("genomename: ",genomename)
    bowtieindexedlist=bowtie2build(genomefile,genomename)
    samtoolsindexfile=samtoolsfaidx(genomefile)
    picarddictfile=picarddict(genomefile)
    if "ensemble" in genomesource:
        copygtffile(genomesource,genomedir)
    #housekeeping
    os.chdir(prevdir)
    return genomedir+os.sep+genomefile
