rpmdev-setuptree
cd ~/rpmbuild/SOURCES
wget http://www.ebi.ac.uk/goldman-srv/ciftools/download/30032012/ciftools.tgz
mv ciftools.tgz ciftools-20120330.tgz
cd ~/rpmbuild/SPECS
rpmdev-newspec
mv newpackage.spec ciftools.spec

sed -i 's/^Name:\s\+/Name: ciftools/' ciftools.spec
sed -i 's/^Version:\s\+/Version: 20120330/' ciftools.spec
sed -i 's/^Summary:\s\+/Summary: ciftools is a collection of utilities for manipulating CIF format intensity files produced by the Illumina sequencing platform./' ciftools.spec
sed -i 's/^License:\s\+/License: GPLv3/' ciftools.spec
sed -i 's/^URL:\s\+/URL: http://www.ebi.ac.uk/goldman-srv/ciftools//' ciftools.spec
sed -i 's/^Source0:\s\+/Source0: http://www.ebi.ac.uk/goldman-srv/ciftools/download/30032012/ciftools.tgz/' ciftools.spec
sed -i 's/^BuildRequires:\s\+/BuildRequires: glibc-devel/' ciftools.spec
sed -i 's/^BuildRequires:\s\+/BuildRequires: zlib-devel/' ciftools.spec ## which is
sed -i 's/^BuildRequires:\s\+/BuildRequires: libzip-devel/' ciftools.spec ## better?
sed -i 's/^BuildRequires:\s\+/BuildRequires: bzip2-devel/' ciftools.spec
sed -i 's/^Requires:\s\+/Requires: glibc/' ciftools.spec

sed -i 's/^%description\s\+/%description
ciftools is a suite of several utilities for manipulating CIF files.
\tcifboot produces bootstrap replicates of a CIF file.
\tcifcat concatenates CIF format files.
\tcifinfo displays information about CIF file.
\t\cifjoin join several CIF files together into one.
\t\cifsplit splits a CIF into two by cycle.
\tint2cif convert the old-style INTensity format files into CIF files./' ciftools.spec

sed -i 's/^%configure/d' ciftools.spec

sed '^%build/ a\
cd src' ciftools.spec
