import os
import argparse

parser=argparse.ArgumentParser("refseq file parser")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

infile=args['input']
outfile=args['output']


def gbff_parse(infile,outfile):
    with open(infile,'r') as gbfffile:
        gbfflines = gbfffile.readlines()

    gbffoutfile=open(outfile,'w')

    proceednextlocus=False
    insidesource=False
    insidegene=False
    for gbffline in gbfflines:
        if gbffline.startswith("LOCUS"):
            gbffoutfile.write(gbffline)
            proceednextlocus=False
        if gbffline.startswith("     gene ") and (proceednextlocus==False):
            insidegene=True
        if gbffline.startswith("                     /gene=") and (insidegene==True):
            gbffoutfile.write(gbffline)
            insidegene=False
            proceednextlocus=True
        if gbffline.startswith("     source ") and (proceednextlocus==False):
            insidesource=True
        if gbffline.startswith("                     /chromosome=") and (insidesource==True):
            gbffoutfile.write(gbffline)
            insidesource=False
    gbffoutfile.close()
gbff_parse(infile,outfile)
