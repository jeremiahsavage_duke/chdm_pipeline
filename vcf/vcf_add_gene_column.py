import argparse
import os

parser = argparse.ArgumentParser("reads gene from info and creates new column")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=False)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']



def getgeneheader(inline):
    line_as_list=inline.split('\t') # splits line by tab char, and puts in list() data type
    line_as_list.insert(5,'GENE')
    print(line_as_list)
    newline='\t'.join(line_as_list) # turn list back into tab separated line
    return newline


def writeheader(inlines,outfile_open):
    for inline in inlines:
        if inline.startswith('##'):
            outfile_open.write(inline)
        if inline.startswith('#CHROM'):
            newline=getgeneheader(inline)
            outfile_open.write(newline)

def getgenename(infofield):
    infolist=infofield.split(';')
    for infoitem in infolist:
        if infoitem.startswith('IGN='):
            genename=infoitem.split('=')[1]
            return genename
    return 'N/A'


def writedata(inlines,outfile_open):
    for inline in inlines:
        if inline.startswith('#'):
            continue
        else:
            line_as_list=inline.split('\t')
            genename=getgenename(line_as_list[7])
            line_as_list.insert(5,genename)
            newline='\t'.join(line_as_list)
            outfile_open.write(newline)

def addgenecolumn(infile,outfile):
    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()
    print('Done reading ',infile)

    if outfile is None:
        print('here')
        origname,origextension=os.path.splitext(infile)
        outfile=origname+'_gene'+origextension

    outfile_open=open(outfile,'w')
    writeheader(inlines,outfile_open)
    writedata(inlines,outfile_open)

    outfile_open.close()

addgenecolumn(infile,outfile)
