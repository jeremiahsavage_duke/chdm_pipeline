import argparse
import csv
import os
import sys

parser = argparse.ArgumentParser("cull vcf file on ratios")
parser.add_argument('--combined',required=True)
parser.add_argument('--control',required=True)
parser.add_argument('--mutant',required=True)
parser.add_argument('--output',required=True)
args=vars(parser.parse_args())

combined=args['combined']
control=args['control']
mutant=args['mutant']
output=args['output']

def mutantphase(mutantratiolist):
    mutantref = mutantratiolist[0]
    mutantalt = mutantratiolist[1]
    keep = False
    if (mutantalt > 50):
        if ( (float(mutantref)/float(mutantalt)) <= 0.04):
            keep = True
        else:
            keep = False
    elif (mutantalt == 0):
        keep = False
    else:
        if ( (float(mutantref)/float(mutantalt)) <= 0.1):
            keep = True
        else:
            keep = False
    return keep

def controlphase(controlratiolist):
    controlref = controlratiolist[0]
    controlalt = controlratiolist[1]
    keep = False
    if ( abs(controlref - controlalt) <= 5 ):
        keep = True
    if (controlalt == 0):
        keep = True
    elif (float(controlref) / float(controlalt) >= 0.7):
        keep = True
    return keep

def isintersection(combinedinfo):
    return 'set=Intersection' in combinedinfo

def getbasecounts(inputinfo,inputrefallele,inputaltallele):
    if (inputrefallele.lower().startswith('a')):
        refidx=0
    elif (inputrefallele.lower().startswith('c')):
        refidx=1
    elif (inputrefallele.lower().startswith('g')):
        refidx=2
    elif (inputrefallele.lower().startswith('t')):
        refidx=3
    else:
        print('no ref allele!')
        sys.exit()
    if (inputaltallele.lower().startswith('a')):
        altidx=0
    elif (inputaltallele.lower().startswith('c')):
        altidx=1
    elif (inputaltallele.lower().startswith('g')):
        altidx=2
    elif (inputaltallele.lower().startswith('t')):
        altidx=3
    else:
        print('no alt allele')
        sys.exit()
    infosplit=inputinfo.split(';')
    for infofield in infosplit:
        if infofield.startswith('BaseCounts='):
            countssplit=infofield.split('=')[1]
            refcount=int(countssplit.split(',')[refidx])
            altcount=int(countssplit.split(',')[altidx])
            basecountlist=[refcount,altcount]
            return basecountlist


def getratio(inputlines,combchrom,combbp,typeof):
    for inputline in inputlines:
        if inputline.startswith('#'):
            continue
        else:
            inputsplit=inputline.split('\t')
            inputchrom=int(inputsplit[0])
            inputbp=int(inputsplit[1])
            inputrefallele=inputsplit[3]
            inputaltallele=inputsplit[4]
            inputinfo=inputsplit[7]
            if (combchrom == inputchrom):
                if (combbp == inputbp):
                    basecountlist=getbasecounts(inputinfo,inputrefallele,inputaltallele)
                    return basecountlist

def addratio(combinedsplit,controlratiolist,mutantratiolist):
    combinedinfo=combinedsplit[7]
    newinfo=combinedinfo+';CRN='+str(controlratiolist[0])+','+str(controlratiolist[1])+';MRN='+str(mutantratiolist[0])+','+str(mutantratiolist[1])
    combinedsplit[7]=newinfo
    newline='\t'.join(combinedsplit)
    return newline

def sufficientsum(controlratiolist,mutantratiolist):
    sumcontrol=controlratiolist[0]+controlratiolist[1]
    summutant=mutantratiolist[0]+mutantratiolist[1]
    if sumcontrol <= 0 and summutant <= 0:
        return False
    else:
        return True


def doit(combined,control,mutant,output):
    with open(combined,'r') as combinedfile:
        combinedlines = combinedfile.readlines()

    with open(control,'r') as controlfile:
        controllines = controlfile.readlines()

    with open(mutant,'r') as mutantfile:
        mutantlines = mutantfile.readlines()

    outputfile = open(output,'w')

    lineno=0
    for combinedline in combinedlines:
        lineno+=1
        if combinedline.startswith('##INFO=<ID=BaseQRankSum'):
            outputfile.write(combinedline)
            outputfile.write('##INFO=<ID=CRN,Number=4,Type=Integer,Description=\"Control fish ref:mut alleles\">\n')
        elif combinedline.startswith('##INFO=<ID=MQRankSum'):
            outputfile.write(combinedline)
            outputfile.write('##INFO=<ID=MRN,Number=4,Type=Integer,Description=\"Mutan tfish ref:mut alleles\">\n')
        elif combinedline.startswith('#'):
            outputfile.write(combinedline)
        else:
            combinedsplit=combinedline.split('\t')
            combinedinfo=combinedsplit[7]
            if isintersection(combinedinfo):
                combchrom=int(combinedsplit[0])
                combbp=int(combinedsplit[1])
                controlratiolist=getratio(controllines,combchrom,combbp,'control')
                mutantratiolist=getratio(mutantlines,combchrom,combbp,'mutant')
                keepcon = controlphase(controlratiolist)
                keepmut = mutantphase(mutantratiolist)
                if not sufficientsum(controlratiolist,mutantratiolist):
                    keepcon=False
                    keepmut=False
                if (keepmut and keepcon):
                    newline=addratio(combinedsplit,controlratiolist,mutantratiolist)
                    outputfile.write(newline)

    outputfile.close()

doit(combined,control,mutant,output)
