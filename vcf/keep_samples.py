import os
import argparse

parser=argparse.ArgumentParser("collapse vcf to nonref")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-f','--posfilter',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

input=args['input']
output=args['output']
posfilter=args['posfilter']

def keptsamples(lines,keptsamplelines):
    samplesnumlist=list()
    for line in lines:
        if line.startswith("#CHROM"):
            records=line.split('\t')
            samplenum=0
            numinforecords=0
            begin=False
            for record in records:
                if (begin == True):
                    for keptsampleline in keptsamplelines:
                        keptsampleline=keptsampleline.rstrip('\n')
                        if (record.lower() == keptsampleline.lower()):
                            samplesnumlist.append(numinforecords+samplenum)
                    samplenum += 1
                    continue
                elif (record == "FORMAT"):
                        numinforecords += 1
                        begin = True
                else:
                    numinforecords += 1
    return samplesnumlist,numinforecords

def dowork(lines,samplesnumlist,numinforecords,outfile):
    for line in lines:
        if line.startswith("##"):
            outfile.write(line)
        else:
            outputline = ""
            records = line.split('\t')
            currentrecordnum=0
            for record in records:
                if (currentrecordnum < numinforecords):
                    outputline += record + '\t'
                    currentrecordnum += 1
                else:
                    for samplenum in samplesnumlist:
                        if (currentrecordnum == samplenum):
                            outputline += record + '\t'
                            break
                    currentrecordnum += 1
            outfile.write(outputline.rstrip('\t')+'\n')

with open(input,"r") as infile:
    lines=infile.readlines()

with open(posfilter,"r") as posfile:
    keptsampleslines=posfile.readlines()

outfile = open(output,"w")
samplesnumlist,numinforecords = keptsamples(lines,keptsampleslines)
#outfile.write(str(samplesnumlist)+'\n')
dowork(lines,samplesnumlist,numinforecords,outfile)
outfile.close()
