import argparse
import os

#parser = argparse.ArgumentParser("add maf to annovar file")
#parser.add_argument('-i','--infile',required=False)
#args=vars(parser.parse_args())

#infile=args['infile']

def getfilteridlist(inlines):
    filteridlist=list()
    for inline in inlines:
        if inline.startswith('##FILTER=<ID='):
            filtername=inline.split('=')[2].split(',')[0]
            if (filtername != 'LowQual'):
                filteridlist.append(filtername)
    return filteridlist

def getoutfilelist(infile,numfilt):
    outfilelist=list()
    origname,origext=os.path.splitext(infile)
    for anum in range(numfilt):
        outfilelist.append(origname+'_'+str(anum)+origext)
    outfilelist.append(origname+'_'+str(numfilt)+origext)
    return outfilelist

def getheader(inlines):
    headerlines=str()
    for inline in inlines:
        if inline.startswith('#'):
            headerlines+=inline
    return headerlines

def writeheaders(outfileopenlist,inlines):
    headerlines=getheader(inlines)
    for outptr in outfileopenlist:
        outptr.write(headerlines)

def writevariants(outfileopenlist,inlines):
    histogramcountlist=[0] * len(outfileopenlist)
    for inline in inlines:
        if inline.startswith('#'):
            continue
        else:
            filterfield=inline.split('\t')[6]
            filterlist=list(filterfield.split(';'))
            filternum=len(filterlist)
            if ( filternum == 1):
                if filterlist[0] == 'PASS':
                    histogramcountlist[0]+=1
                    outfileopenlist[0].write(inline)
                else:
                    histogramcountlist[1]+=1
                    outfileopenlist[1].write(inline)
            else:
                histogramcountlist[filternum]+=1
                outfileopenlist[filternum].write(inline)
    return histogramcountlist

def splitnumfilt(infile):
    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()
    print('Done reading '+infile)

    filteridlist=getfilteridlist(inlines)
    print('filteridlist=',filteridlist)
    numfilt=len(filteridlist)
    print('numfilt=',numfilt)

    outfilelist=getoutfilelist(infile,numfilt)
    print('outfilelist=',outfilelist)

    outfileopenlist=list()
    i = 0
    for outfile in outfilelist:
        outptr='outfile_'+str(i)
        outptr=open(outfile,'w')
        outfileopenlist.append(outptr)
        i+=1

    writeheaders(outfileopenlist,inlines)
    histogramcountlist=writevariants(outfileopenlist,inlines)
    print('histogramcountlist=',histogramcountlist)

    for outptr in outfileopenlist:
        outptr.close()

    return histogramcountlist,outfilelist
