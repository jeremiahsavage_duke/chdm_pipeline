import argparse
import os
import re

parser = argparse.ArgumentParser("add maf to annovar file")
parser.add_argument('-i','--infile',required=True)
args=vars(parser.parse_args())

infile=args['infile']

def zv2danr(infile):

    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()
    print('Done reading '+infile)

    origfile,origext=os.path.splitext(infile)
    outfile=origfile+'_ucsc'+origext

    outfile_open=open(outfile,'w')

    for inline in inlines:
        if re.match('##contig=<ID=(\d+)', inline):
            ils=inline.split('=')
            newchr='chr'+ils[2]
            ils[2]=newchr
            newline='='.join(ils)
            outfile_open.write(newline)
        elif inline.startswith('##contig=<ID=MT'):
            ils=inline.split('=')
            newchr='chr'+ils[2].replace('MT','M')
            ils[2]=newchr
            newline='='.join(ils)
            outfile_open.write(newline)
        elif inline.startswith('#'):
            outfile_open.write(inline)
        else:
            ils=inline.split('\t')
            newchr='chr'+ils[0]
            ils[0]=newchr
            newline='\t'.join(ils)
            outfile_open.write(newline)

    outfile_open.close()

zv2danr(infile)
