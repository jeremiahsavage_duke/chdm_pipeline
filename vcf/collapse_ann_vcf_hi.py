import os
import argparse

parser=argparse.ArgumentParser("collapse vcf to nonref")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

input=args['input']
output=args['output']

def process_header(lines,outfile):
    for line in lines:
        outfile.write(line)
        if line.startswith("ANV_"):
            samples,numinforecords = get_samples(line)
            break
    return samples,numinforecords

def get_samples(line):
    samples = list()
    numinforecords = 0
    begin = False
    records = line.split('\t')
    for record in records:
        if (begin == True):
            samples.append(record.rstrip('\n'))
            continue
        if (record == "FORMAT"):
            numinforecords += 1
            begin = True
        else:
            numinforecords += 1
    return samples,numinforecords

def process_records(lines,samples,numinforecords,outfile):
    testout = open("testout.out","w")
    for line in lines:
        if not (line.startswith('#') or line.startswith("ANV_")):
            outline = ""
            testline = ""
            values = line.split('\t')
            for value in values:
                testline += value.rstrip('\n')+'\t'
            testout.write(testline+'\n')
            numrecord = 0
            for value in values:
                if (numrecord >= numinforecords):
                    genotypescore = value.split(':')[0]
                    if ( (genotypescore == "0/0") or (genotypescore == ".") ):
                        aint=0
                        #delete_field(line,)
                    if ( (genotypescore == "1/0") or (genotypescore == "0/1") ):
                        outline += value.rstrip('\n') + ":" + str(samples[numrecord-numinforecords]) + ":het" + '\t'
                        #heter_nonref()
                    if (genotypescore == "1/1"):
                        outline += value.rstrip('\n') + ":" + str(samples[numrecord-numinforecords]) + ":hom" + '\t'
                        #homo_nonref()
                    numrecord += 1
                else:
                    outline += value + '\t'
                    numrecord += 1
            outfile.write(outline.rstrip('\t')+'\n')
    testout.close()

with open(input,"r") as infile:
    lines=infile.readlines()

outfile = open(output,"w")
samples,numinforecords = process_header(lines,outfile)
process_records(lines,samples,numinforecords,outfile)
outfile.close()
