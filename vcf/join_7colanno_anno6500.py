import argparse
import os

parser = argparse.ArgumentParser("add maf to annovar file")
parser.add_argument('-v','--vcffile',required=True)
parser.add_argument('-a','--annovar6500',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']
annfile=args['annovar6500']
outfile=args['outfile']

def joinvcfmaf(vcfile,annfile,outfile):

    with open(vcffile,'r') as vcf_file:
        vcflines = vcf_file.readlines()

    with open(annfile,'r') as ann_file:
        annlines = ann_file.readlines()

    with open(outfile,'w') as out_file:
        headerline="Gene	strand	chrom	hg19pos	SNP ID	Ref gen	Alt gen	ESP6500SI MAF" + '\n'
        out_file.write(headerline)
        for vcfline in vcflines:
            if (vcfline.split('\t')[0] == "Gene"):
                continue
            vcfchrom = vcfline.split('\t')[2]
            vcfchromnum = vcfchrom[3:5]
            if (vcfchromnum[0] == '0'):
                vcfchromnumtrue = vcfchromnum[1]
            else:
                vcfchromnumtrue = vcfchromnum
            print("vcfchromnum=",vcfchromnum)
            print("vcfchromnumtrue=",vcfchromnumtrue)
            vcfbp = vcfline.split('\t')[3]
            foundmaf = False
            vcfref = vcfline.split('\t')[5]
            vcfalt = vcfline.split('\t')[6].rstrip('\n')
            print("vcfchrom=",vcfchrom)
            print("vcfbp=",vcfbp)
            print("vcfref=",vcfref)
            print("vcfalt=",vcfalt)
            for annline in annlines:
                out_file.flush()
                annchrom = annline.split('\t')[0]
                annbp = annline.split('\t')[1]
                annmaf = annline.split('\t')[5].rstrip('\n')
                annref = annline.split('\t')[3]
                annalt = annline.split('\t')[4]
                
                if ((vcfchromnumtrue == annchrom) and (vcfbp == annbp) and (vcfref == annref) and (vcfalt == annalt)):
                    out_file.write(vcfline.rstrip('\n') +'\t'+annmaf + '\n')
                    foundmaf = True
                    break;
            if (foundmaf == False):
                out_file.write(vcfline.rstrip('\n')+'\t'+"NOESPMAF"+'\n')

joinvcfmaf(vcffile,annfile,outfile)
