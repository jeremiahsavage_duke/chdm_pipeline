import argparse
import os
import re

parser = argparse.ArgumentParser("dedot")
parser.add_argument('-i','--infile',required=True)
args=vars(parser.parse_args())

infile=args['infile']

def vcfdeldot(infile):

    with open(infile,'r') as infile_open:
        inlines = infile_open.readlines()
    print('Done reading '+infile)

    origfile,origext=os.path.splitext(infile)
    outfile=origfile+'_dedot'+origext

    outfile_open=open(outfile,'w')

    for inline in inlines:
        if not inline.startswith('#'):
            ils=inline.split('\t')
            ref=ils[3]
            newref=ref.strip('.~')
            ils[3]=newref
            newline='\t'.join(ils)
            outfile_open.write(newline)
        else:
            outfile_open.write(inline)

    outfile_open.close()

vcfdeldot(infile)
