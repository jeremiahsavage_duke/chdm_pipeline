import argparse
import os

parser = argparse.ArgumentParser("add maf to annovar file")
parser.add_argument('-v','--vcffile',required=True)
parser.add_argument('-a','--annfile',required=True)
parser.add_argument('-m','--anntype',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']
annfile=args['annfile']
anntype=args['anntype']
outfile=args['outfile']
 #if ((vcfchrom == annchrom) and ((int(vcfbp) == int(annbp)) or (int(vcfbp) == (int(annbp)-1))) and (vcfref == annref) and (vcfalt == annalt)):
def joinvcfmaf(vcfile,annfile,anntype,outfile):

    with open(vcffile,'r') as vcf_file:
        vcflines = vcf_file.readlines()
    print('Done reading '+vcffile)

    with open(annfile,'r') as ann_file:
        annlines = ann_file.readlines()
    print('Done reading '+annfile)

    with open(outfile,'w') as out_file:
        for vcfline in vcflines:
            if (vcfline.startswith("#")):
                out_file.write(vcfline)
            else:
                vcfchrom = vcfline.split('\t')[0]
                vcfbp = int(vcfline.split('\t')[1])
                vcfref = vcfline.split('\t')[3]
                vcfalt = vcfline.split('\t')[4]
                foundmaf = False
                counter = 0
                for annline in annlines:
                    if (counter%1000000==0):
                        print('counter:',counter)
                    counter += 1
                    annchrom = annline.split('\t')[0]
                    annbp = int(annline.split('\t')[1])
                    annref = annline.split('\t')[2]
                    annalt = annline.split('\t')[3]
                    if ((vcfchrom == annchrom) and ((vcfbp == annbp) or (vcfbp == annbp-1))):
                        print("Found!")
                        annmaf = annline.split('\t')[4]
                        foundmaf = True
                        break;
                if (foundmaf == False):
                    print("NotFound!")
                    annmaf = 'NA'                
                infofield = vcfline.split('\t')[7]
                print('infofield:',infofield)
                if (infofield == '.'):
                    outinfofield = infofield
                else:
                    outinfofield += ';'+anntype+'='+annmaf
                vcfsplit = vcfline.split('\t')
                vcfsplit[7] = outinfofield
                newvcfline = '\t'.join(vcfsplit)
                out_file.write(newvcfline)
            print('read line')

joinvcfmaf(vcffile,annfile,anntype,outfile)
