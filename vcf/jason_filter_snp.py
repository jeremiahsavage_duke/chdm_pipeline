import glob
import os
import shutil
import subprocess

def get_vcf_list():
    vcflist = list()
    for vcffile in glob.glob("*.vcf"):
        vcflist.append(vcffile)
    return vcflist

def get_vcf_mut_list():
    vcflist = list()
    for vcffile in glob.glob("*Mut*.vcf"):
        vcflist.append(vcffile)
    return vcflist

def get_wt_pair(vcfmut):
    vcfwt = vcfmut.replace('Mut','WT')
    return vcfwt

def get_mask_list(vcflist,vcfmut):
    masklist = vcflist
    masklist.remove(vcfmut)
    vcfwt = get_wt_pair(vcfmut)
    masklist.remove(vcfwt)
    return masklist

def get_filter_dir(vcfmut):
    filterdir = vcfmut.split('_')[0]
    return filterdir

def variantfiltration(filtervcf,masklist):
    for maskfile in masklist:
        maskfilesplit = maskfile.split('_')
        maskname = maskfilesplit[0]+'_'+maskfilesplit[1]
        reffasta = '/home/jeremiah/zv9/Danio_rerio.Zv9.70.dna.toplevel.fa'
        subcall='/usr/bin/time java -d64 -Xmx4g -jar ~/.local/bin/GenomeAnalysisTK.jar -R '+reffasta+' -T VariantFiltration -o output.vcf --variant '+filtervcf+' --mask '+maskfile+' --maskName '+maskname
        subprocess.call([subcall],shell=True)
        shutil.move('output.vcf',filtervcf)

def filtervcfs():
    vcflist = get_vcf_list()
    vcfmutlist = get_vcf_mut_list()

    for vcfmut in vcfmutlist:
        masklist = get_mask_list(vcflist,vcfmut)
        filterdir = get_filter_dir(vcfmut)
        if not os.path.exists(filterdir):
            os.makedirs(filterdir)
        filtervcf = os.path.join(filterdir,vcfmut)
        if not os.path.exists(filtervcf):
            shutil.copyfile(vcfmut,filtervcf)
        variantfiltration(filtervcf,masklist)
            
        
filtervcfs()
