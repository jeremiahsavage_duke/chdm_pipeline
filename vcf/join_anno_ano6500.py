import argparse
import os

parser = argparse.ArgumentParser("add maf to annovar file")
parser.add_argument('-v','--vcffile',required=True)
parser.add_argument('-a','--annovar6500',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']
annfile=args['annovar6500']
outfile=args['outfile']

def joinvcfmaf(vcfile,annfile,outfile):

    with open(vcffile,'r') as vcf_file:
        vcflines = vcf_file.readlines()

    with open(annfile,'r') as ann_file:
        annlines = ann_file.readlines()

    with open(outfile,'w') as out_file:
        headerline="ANV_vcf_line	ANV_snp_type	ANV_genotype	ANV_chromosome	ANV_start	ANV_end	ANV_ref_allele	ANV_alt_allele	ANV_zygosity	ANV_QUAL	ANV_RMS_mapping_quality	ANV_Variant_Confidence/Quality_by_Depth	ANV_esp6500si_MAF" + '\n'
        out_file.write(headerline)
        for vcfline in vcflines:
            vcfchrom = vcfline.split('\t')[3]
            vcfbp = vcfline.split('\t')[4]
            foundmaf = False
            vcfref = vcfline.split('\t')[6]
            vcfalt = vcfline.split('\t')[7]
            print("vcfchrom=",vcfchrom)
            print("vcfbp=",vcfbp)
            for annline in annlines:
                annchrom = annline.split('\t')[0]
                annchrom = "chr"+annchrom
                annbp = annline.split('\t')[1]
                annmaf = annline.split('\t')[5].rstrip('\n')
                annref = annline.split('\t')[3]
                annalt = annline.split('\t')[4]
                if ((vcfchrom == annchrom) and (vcfbp == annbp) and (vcfref == annref) and (vcfalt == annalt)):
                    out_file.write(vcfline.rstrip('\n') +'\t'+annmaf + '\n')
                    foundmaf = True
                    break;
            if (foundmaf == False):
                out_file.write(vcfline.rstrip('\n')+'\t'+"NOESPMAF"+'\n')

joinvcfmaf(vcffile,annfile,outfile)
