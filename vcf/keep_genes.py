import os
import argparse

parser=argparse.ArgumentParser("collapse vcf to nonref")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-f','--posfilter',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

input=args['input']
output=args['output']
posfilter=args['posfilter']

def process_header(lines,outfile):
    for line in lines:
        outfile.write(line)
        if line.startswith("#CHROM"):
            break

def dowork(lines,keptgeneslines,outfile):
    for line in lines:
        if line.startswith("#"):
            outfile.write(line)
        else:
            for keptgene in keptgeneslines:
                records = line.split('\t')
                inforecord=records[7]
                infofields=inforecord.split(';')
                for infofield in infofields:
                    if ((infofield.startswith("GN=")) or (infofield.startswith("IGN="))):
                        genename=infofield.split('=')[1].lower()
                        #outfile.write(genename+'\n')
                        #outfile.write(keptgene.lower())
                        if (genename in keptgene.lower()):
                            outfile.write(line)
                            break
 

with open(input,"r") as infile:
    lines=infile.readlines()

with open(posfilter,"r") as posfile:
    keptgeneslines=posfile.readlines()

outfile = open(output,"w")
dowork(lines,keptgeneslines,outfile)
outfile.close()
