for vcffile in *.vcf
do
    ~/tools/vcfsorter.pl ~/zv9/Danio_rerio.Zv9.70.dna.toplevel.dict ${vcffile} > new.vcf
    mv new.vcf ${vcffile}
done
