import os
import argparse

parser=argparse.ArgumentParser("collapse vcf to nonref")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-o','--output',required=True)
args=vars(parser.parse_args())

input=args['input']
output=args['output']


def dowork(lines,outfile):
    classtypes=set()
    for line in lines:
        if line.startswith("#"):
            aint=0
        else:
            records = line.split('\t')
            inforecord=records[7]
            infofields=inforecord.split(';')
            for infofield in infofields:
                if infofield.startswith("UCG="):
                    classname=infofield.split('=')[1].lower()
                    classtypes.add(classname)
    return classtypes
 

with open(input,"r") as infile:
    lines=infile.readlines()


outfile = open(output,"w")
classtypes=dowork(lines,outfile)
for classtype in classtypes:
    outfile.write(classtype+'\n')
#outfile.write(str(classtypes))
outfile.close()
