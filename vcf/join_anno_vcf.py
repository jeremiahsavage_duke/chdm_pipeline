import argparse
import os

parser = argparse.ArgumentParser("add maf to annovar file")
parser.add_argument('-v','--vcffile',required=True)
parser.add_argument('-a','--annovar6500',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

vcffile=args['vcffile']
annfile=args['annovar6500']
outfile=args['outfile']

def joinvcfmaf(vcfile,annfile,outfile):

    with open(vcffile,'r') as vcf_file:
        vcflines = vcf_file.readlines()

    with open(annfile,'r') as ann_file:
        annlines = ann_file.readlines()

    with open(outfile,'w') as out_file:
        for vcfline in vcflines:
            if (vcfline.startswith("##")):
                out_file.write(vcfline)
        for vcfline in vcflines:
            if (vcfline.startswith("#CHROM")):
                print("#CHROM")
                vcfheader = vcfline
                annheader = annlines[0]
                out_file.write(annheader.rstrip('\n') + '\t' + vcfheader)
        for annline in annlines:
            if (annline.startswith("ANV_")):
                annheader = annline
            else:
                annchrom = annline.split('\t')[3]
                annbp = annline.split('\t')[4]
                for vcfline in vcflines:
                    if (vcfline.startswith("#")):
                        aint = 0
                    else:
                        foundmaf = False
                        vcfchrom = vcfline.split('\t')[0]
                        vcfbp = vcfline.split('\t')[1]
                        if ((vcfchrom == annchrom) and ((int(vcfbp) == int(annbp)) or (int(vcfbp) == (int(annbp)-1)))):
                            out_file.write(annline.rstrip('\n') + '\t' + vcfline)
                            foundmaf = True
                            break;
                if (foundmaf == False):
                    print(annline.rstrip('\n'))
                #out_file.write(vcfline.rstrip('\n')+'\t'+"BYEBYEBLACKBIRD"+'\n')

joinvcfmaf(vcffile,annfile,outfile)
