import os
import argparse

parser=argparse.ArgumentParser("collapse vcf to nonref")
parser.add_argument('-i','--input',required=True)
parser.add_argument('-u','--ucgtypes',required=True)
args=vars(parser.parse_args())

input=args['input']
ucgtypes=args['ucgtypes']

def dowork(lines,ucgvalue,outfile):
    classtypes=set()
    for line in lines:
        if line.startswith("#"):
            outfile.write(line)
        else:
            records = line.split('\t')
            inforecord=records[7]
            infofields=inforecord.split(';')
            for infofield in infofields:
                if infofield.startswith("UCG="):
                    classname=infofield.split('=')[1].lower()
                    if(ucgvalue == classname):
                        outfile.write(line)
 

with open(input,"r") as infile:
    lines=infile.readlines()

with open(ucgtypes,"r") as ucgfile:
    ucglines=ucgfile.readlines()

for ucgvalue in ucglines:
    ucgvalue = ucgvalue.rstrip('\n')
    output = ucgvalue.replace('/','_')
    output += ".vcf"
    outfile = open(output,"w")
    dowork(lines,ucgvalue,outfile)
    outfile.close()

outfile.close()
