import os
import argparse

parser=argparse.ArgumentParser("refseq reduced file parser")
parser.add_argument('-r','--redref',required=True)
parser.add_argument('-o','--ontology',required=True)
parser.add_argument('-j','--joinedoutput',required=True)
args=vars(parser.parse_args())

reffile=args['redref']
ontfile=args['ontology']
outfile=args['joinedoutput']

def gbff_parse(reffile,ontfile,outfile):
    with open(reffile,'r') as redreffile:
        redreflines = redreffile.readlines()
    with open(ontfile,'r') as ontfile:
        ontlines = ontfile.readlines()

    joinfile=open(outfile,'w')

    for ontline in ontlines:
        ontname=ontline.split()[2]
        for redrefline in redreflines:
            redrefname=redrefline.split()[2]
            if (redrefname==ontname):
                joinfile.write(redrefline[:-1]+'\t'+ontline)
                break
            if (redrefline==redreflines[len(redreflines)-1]):
                joinfile.write("?"+'\t'+"?"+'\t'+"?"+'\t'+ontline)

    joinfile.close()

gbff_parse(reffile,ontfile,outfile)
