import os
import shutil
import subprocess
import pickle
import re
import inspect
##
from multiprocessing import Pool

def isfastqgz(s):
    return s.endswith(".fastq.gz")

def isfastq(s):
    return s.endswith(".fastq")

def whichtag(fqfile,taglist):
    for t in taglist:
        if t in fqfile:
            return t
    return None


def buildfastqgzlist(currdir):
    filelist=os.listdir(currdir)
    return sorted(list(filter(isfastqgz,filelist)))

def buildfastqlist(currdir):
    filelist=os.listdir(currdir)
    return sorted(list(filter(isfastq,filelist)))

def parsubproc(subcall):
    print('parallel')
    print('\tsubcall=',subcall)
    subprocess.call([subcall],shell=True)

def uncompressfastqgzfiles(fastqgzlist):
    subproclist=list()
    fastqlist=list()
    for fastqgz in fastqgzlist:
        fbase,fext = os.path.splitext(fastqgz)
        fastqlist.append(fbase)
        if not os.path.exists(fbase):
            subcall="gunzip -c " + fastqgz + " > " + fbase#should parse gz
            print("gunzipcall: ", subcall)
            subproclist.append(subcall)
            p=Pool(len(subproclist))
            p.map(parsubproc,subproclist)
    return fastqlist

def buildpresenttagslist(fastqlist,tagindexlist):
    taglist=list()
    print('prepinput.py::buildpresenttagslist::fastqlist=',fastqlist)
    print('prepinput.py::buildpresenttagslist::tagindexlist=',tagindexlist)
    for i in tagindexlist:
        for f in fastqlist:
            if i in f:
                taglist.append(i)
                break
    print('taglist=',taglist)
    return taglist #perhaps no break, and use unique()?

def createadapterremovalfiles(truseqtemplatefile,taglist,replacestring):
    for i in taglist:
        #make files
        print("taglist[i]=",i)
        origname,origextension=os.path.splitext(truseqtemplatefile)
        origname=os.path.basename(origname)
        tagfile=origname+"_"+i+origextension
        print("create tagfile:",tagfile)
        shutil.copyfile(truseqtemplatefile,tagfile)
        #edit files
        with open(tagfile,"r") as seqfile:
            lines = seqfile.readlines()
        with open(tagfile,"w") as seqfile:
            for line in lines:
                seqfile.write(line.replace(replacestring,i))

def buildfastqtagdict(fastqlist,taglist):
    fastqtagdict=dict()
    j=0
    for fqfile in fastqlist:
        tagstring=whichtag(fqfile,taglist)
        fastqtagdict[fqfile]=tagstring
        j+=1
    del j
    return fastqtagdict

def writefastqtagdict(fastqtagdict,dictfile):
    output=open(dictfile,'wb')
    pickle.dump(fastqtagdict,output)
    output.close()

def readfastqtagdict(dictfile):
    input=open(dictfile,'rb')
    fastqtagdict=pickle.load(input)
    input.close()
    return fastqtagdict

def concatreadn(fastqlist):
    r1list=list()
    r2list=list()
    for fqfile in fastqlist:
        if "_R1_" in fqfile:
            r1list.append(fqfile)
        if "_R2_" in fqfile:
            r2list.append(fqfile)
    print('r1list=',r1list)
    print('r2list=',r2list)
    if r1list and r2list:
        outname1,origextension=os.path.splitext(r1list[0])
        outname2,origextension=os.path.splitext(r2list[0])
        shortname1=re.sub("_L.*_R1_...","",outname1)+"_R1"+origextension
        shortname2=re.sub("_L.*_R2_...","",outname2)+"_R2"+origextension
        if os.path.exists(shortname1):
            print("fastq files already concat:",shortname1)
        else:
            cat1string=" ".join(r1list)
            cat1call="time cat " + cat1string + " > " + shortname1
            print(cat1call)
            subprocess.call([cat1call],shell=True)
        if os.path.exists(shortname2):
            print("fastq files already concat:",shortname2)
        else:
            cat2string=" ".join(r2list)
            cat2call="time cat " + cat2string + " > " + shortname2
            print(cat2call)
            subprocess.call([cat2call],shell=True)
    if not r1list and not r2list and fastqlist:
        fqlist = fastqlist
    elif r1list and r2list:
        fqlist=[shortname1,shortname2]
    elif r1list and not r2list:
        outname1,origextension=os.path.splitext(r1list[0])
        shortname1=re.sub("_L.*_R1_...","",outname1)+"_R1"+origextension
        if os.path.exists(shortname1):
            print("fastq files already concat:",shortname1)
        else:
            cat1string=" ".join(r1list)
            cat1call="time cat " + cat1string + " > " + shortname1
            print(cat1call)
            subprocess.call([cat1call],shell=True)
        fqlist=[shortname1]
    return fqlist

def prepinput(dictfile,currdir,tagindexlist,truseqtemplatefile,replacestring):
    callerframerecord = inspect.stack()[1]
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    if not os.path.exists(dictfile):
        fastqgzlist=buildfastqgzlist(currdir)
        print('fastqgzlist=%s' % fastqgzlist)
        fastqlist=uncompressfastqgzfiles(fastqgzlist)
        print('fastqlist=%s' % fastqlist)
        if len(fastqlist) is 0:
            fastqlist=buildfastqlist(currdir)
        print('bye none')
#        fastqlist=buildfastqlist(currdir)
        fastqlist=concatreadn(fastqlist)
        print('fastqlist=%s' % fastqlist)
        taglist=buildpresenttagslist(fastqlist,tagindexlist)
        #need an if not exist adapterfiles
        createadapterremovalfiles(truseqtemplatefile,taglist,replacestring)
        fastqtagdict=buildfastqtagdict(fastqlist,taglist)
        writefastqtagdict(fastqtagdict,dictfile)
        createadapterremovalfiles(truseqtemplatefile,taglist,replacestring)
    else:
        fastqtagdict=readfastqtagdict(dictfile)
        print(info.filename+'::'+info.function+'::'+str(info.lineno)+'::preppedfastqdict=',fastqtagdict)
    return fastqtagdict
