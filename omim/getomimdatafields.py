import os
import argparse
import string

parser=argparse.ArgumentParser("get omim records")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']

def isuppercase(astring):
    if(astring == string.upper(astring)):
        return True
    else:
        return False

num_lines= sum(1 for line in open(infile))

with open(infile,"r") as miminput:
    mimlines=miminput.readlines()

records=set()
cur_line=0
while (cur_line < num_lines-2):
    if(len(mimlines[cur_line]) == 1):
        if(isuppercase(mimlines[cur_line+1])):
            if(len(mimlines[cur_line+2]) == 1):
                records.add(mimlines[cur_line+1])
    cur_line+=1

with open(outfile,"w") as mimoutput:
    for record in records:
        mimoutput.write(record)
