import os
import argparse
from collections import OrderedDict

parser=argparse.ArgumentParser("remove genotypes")
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=True)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']


def getnextrecordline(mimlines,cur_line,omim_num_lines):
    next_record_line=cur_line+1
    while(not mimlines[next_record_line].startswith(RECORD)):
          next_record_line+=1
          if (next_record_line == omim_num_lines):
              return -1
    return next_record_line+1


def getnextfieldline(mimlines,field_line,next_record_line,omim_num_lines,omimheaderdict,currentheaderkey):
    next_field_line=field_line+1
    while((next_field_line != next_record_line) and (next_field_line != omim_num_lines)):
        for tryheaderkey in omimheaderdict:
            if(mimlines[next_field_line].startswith(omimheaderdict[tryheaderkey])):
                return next_field_line
        next_field_line+=1
    return next_record_line-1


def getfieldtext(mimlines,start,end):
    index=start
    fieldtext=""
    while (index <= end):
        fieldtext+=mimlines[index]
        index+=1
    fieldtext=fieldtext.rstrip('\n').rstrip('\n')
    fieldtext=fieldtext.lstrip('\n')
    fieldtext=fieldtext.replace('\n'," NN ")
    return fieldtext


def getfield(mimlines,start_line,next_record_line,omim_num_lines,omimheaderdict,headerkey):
    field_line=start_line
    while(field_line != next_record_line):
        if( (mimlines[field_line]).startswith(omimheaderdict[headerkey]) ):
            next_field_line=getnextfieldline(mimlines,field_line,next_record_line,omim_num_lines,omimheaderdict,headerkey)
            fieldtext=getfieldtext(mimlines,field_line+1,next_field_line-1)
            return fieldtext
        field_line+=1
    emptytext=""
    return emptytext


def dict2row(adict):
    arow=""
    for akey in adict:
        arow+=adict[akey]+'\t'
    arow.rstrip('\t')
    arow+='\n'
    return arow


def writerecord(mimlines,cur_line,next_record_line,omim_num_lines,omimvaluedict,omimheaderdict):
    for headerkey in omimheaderdict:
        omimvaluedict[headerkey]=getfield(mimlines,cur_line,next_record_line,omim_num_lines,omimheaderdict,headerkey)
    fieldrow=dict2row(omimvaluedict)
    mimoutput.write(fieldrow)


##main##
RECORD="*RECORD*"
omimheaderdict=OrderedDict(
    FIELDNO="*FIELD* NO",
    FIELDTI="*FIELD* TI",
    FIELDAV="*FIELD* AV",
    FIELDTX="*FIELD* TX",
    FIELDCS="*FIELD* CS",
    FIELDCN="*FIELD* CN",
    FIELDSA="*FIELD* SA",
    FIELDRF="*FIELD* RF",
    FIELDED="*FIELD* ED",
    FIELDCD="*FIELD* CD",
    MOLECULARGENETICS="MOLECULAR GENETICS",
    CLINICALFEATURES="CLINICAL FEATURES",
    GENEFUNCTION="GENE FUNCTION",
    DESCRIPTION="DESCRIPTION",
    ANIMALMODEL="ANIMAL MODEL",
    BIOCHEMICALFEATURES="BIOCHEMICAL FEATURES",
    MAPPING="MAPPING",
    CLONING="CLONING",
    GENOTYPEPHENOTYPECORRELATIONS="GENOTYPE/PHENOTYPE CORRELATIONS",
    GENOTYPE="GENOTYPE",
    HISTORY="HISTORY",
    DIAGNOSIS="DIAGNOSIS",
    INHERITANCE="INHERITANCE",
    PATHOGENESIS="PATHOGENESIS",
    STUDIESOFPHENOTYPICASSOCIATIONS="STUDIES OF PHENOTYPIC ASSOCIATIONS",
    POPULATIONGENETICS="POPULATION GENETICS",
    MODIFICATIONOFEFFECT="MODIFICATION OF EFFECT",
    CLINICALMANIFESTATIONS="CLINICAL MANIFESTATIONS",
    CLINICALMANAGEMENT="CLINICAL MANAGEMENT",
    NOMENCLATURE="NOMENCLATURE",
    GENETHERAPY="GENE THERAPY",
    PHENOTYPES="PHENOTYPES",
    GENESTRUCTURE="GENE STRUCTURE",
    HETEROGENEITY="HETEROGENEITY",
    HOMOZYGOSITY="HOMOZYGOSITY",
    EVOLUTION="EVOLUTION",
    GENEFAMILY="GENE FAMILY"
)
omimheaderdict=OrderedDict(sorted(omimheaderdict.items()))


##read omim file
with open(infile,"r") as miminput:
    mimlines=miminput.readlines()
omim_num_lines= sum(1 for line in open(infile))

##data storage
omimvaluedict=OrderedDict()
for headerkey in omimheaderdict:
    omimvaluedict[headerkey]=""

##prep output
mimoutput=open(outfile,"w")
headerrow=dict2row(omimheaderdict)
mimoutput.write(headerrow)

##start parsing
cur_line=0
while ((cur_line < omim_num_lines) and (cur_line != -1)):
    next_record_line=getnextrecordline(mimlines,cur_line,omim_num_lines)
    if (next_record_line != -1):
        writerecord(mimlines,cur_line,next_record_line,omim_num_lines,omimvaluedict,omimheaderdict)
    cur_line=next_record_line

##done
mimoutput.close()
