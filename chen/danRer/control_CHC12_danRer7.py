import os
import sys
import subprocess
import inspect
# local
import pipeutil

homedir=os.path.expanduser("~/")
pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)
#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"truseq_adapters.fasta"))
TAGINDEXLIST=pipeutil.addquote("ATCACG, CGATGT, TTAGGC, TGACCA, ACAGTG, GCCAAT, CAGATC, ACTTGA, GATCAG, TAGCTT, GGCTAC, CTTGTA, AGTCAA, AGTTCC, ATGTCA, CCGTCC, GTCCGC, GTGAAA, GTGGCC, GTTTCG, CGTACG, GAGTGG, ACTGAT, ATTCCT")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNN]")

#variable
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/home2/danRer7/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/hgdownload.cse.ucsc.edu/goldenPath/danRer7/bigZips/danRer7.fa.gz"))
VCFFILE=pipeutil.addquote(os.path.normpath(homedir+"/home2/danRer7/Danio_rerio.vcf"))
BAITFILE="None"
TARGETFILE="None"

subprocesscall="python "+pipelinedir+"pipeline.py " +" -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR+ " -c " + CHROMOSOMEFASTALIST + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE

subprocess.call([subprocesscall],shell=True)

### cleanup
#rm *.sam *.bam *scythe* *paired* *.stats *.dict *.out nohup* *.fasta
