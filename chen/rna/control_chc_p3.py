import sys
sys.path.append('/home/jeremiah/chdm_pipeline/chen/')
from control_base import *
import pipeutil

POOLSIZE=pipeutil.addquote("17")
ISRNA=str(True)
MUTAGENIZED=str(True)
SNPTRACK_CHROMOSOME=1
SNPTRACK_STARTPEAK=43000000
SNPTRACK_ENDPEAK=52000000

subprocesscall="python3 "+ pipelinedir + "../pipeline.py" + " -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + " -p " + POOLSIZE + ' -z ' + MUTAGENIZED + " --snptrack_chromosome " + str(SNPTRACK_CHROMOSOME) + " --snptrack_startpeak " + str(SNPTRACK_STARTPEAK) + " --snptrack_endpeak " + str(SNPTRACK_ENDPEAK) + ' --isrna ' + ISRNA + ' --gtffile ' + GTFFILE

subprocess.call([subprocesscall],shell=True)
