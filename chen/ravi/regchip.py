import sys
sys.path.append('/home/jeremiah/chdm_pipeline/chen/')
from control_chipseq_base import *
import pipeutil

POOLSIZE=pipeutil.addquote("999")
ISRNA=str(False)
print('regchip.py::ISRNA=',ISRNA)
MUTAGENIZED=str(False)
SNPTRACK_CHROMOSOME='999'
SNPTRACK_STARTPEAK='999'
SNPTRACK_ENDPEAK='999'

subprocesscall="python3 "+ pipelinedir + "../pipeline.py" + " -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR + " -g " + GENOMESOURCE + " -v " + VCFFILE + " -b " + BAITFILE + " -t " + TARGETFILE + " -m " + GENOMENAME + " -p " + POOLSIZE + ' -z ' + MUTAGENIZED + " --snptrack_chromosome " + str(SNPTRACK_CHROMOSOME) + " --snptrack_startpeak " + str(SNPTRACK_STARTPEAK) + " --snptrack_endpeak " + str(SNPTRACK_ENDPEAK) + ' --isrna ' + ISRNA + ' --gtffile ' + GTFFILE

subprocess.call([subprocesscall],shell=True)
