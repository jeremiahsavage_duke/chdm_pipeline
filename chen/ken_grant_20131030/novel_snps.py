import argparse
import vcf

parser=argparse.ArgumentParser('filter vcf alleles')
parser.add_argument('-i','--infile',required=True)
parser.add_argument('-o','--outfile',required=False)
args=vars(parser.parse_args())

infile=args['infile']
outfile=args['outfile']

def filter(infile,outfile):
    vcf_reader=vcf.Reader(open(infile,'r'))
    vcf_writer=vcf.Writer(open(outfile,'w'),vcf_reader)
    i=0
    for record in vcf_reader:
        if record.is_snp:
            if record.samples[0]['GT']=='0/1' or record.samples[0]['GT']=='1/1':
                i+=1
                vcf_writer.write_record(record)
    print('i=%s' % i)

def doit(infile,outfile):
    filter(infile,outfile)

doit(infile,outfile)
