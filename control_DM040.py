import os
import sys
import subprocess
import inspect
# local
import pipeutil

homedir=os.path.expanduser("~/")
pipelinedir=(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + os.sep)
#static
TRUSEQTEMPLATE=pipeutil.addquote(os.path.normpath(homedir+"truseq_adapters.fasta"))
TAGINDEXLIST=pipeutil.addquote("ATCACG, CGATGT, TTAGGC, TGACCA, ACAGTG, GCCAAT, CAGATC, ACTTGA, GATCAG, TAGCTT, GGCTAC, CTTGTA, AGTCAA, AGTTCC, ATGTCA, CCGTCC, GTCCGC, GTGAAA, GTGGCC, GTTTCG, CGTACG, GAGTGG, ACTGAT, ATTCCT")
FASTQTAGDICTFILE=pipeutil.addquote("fastqfiletag.dict")
REPLACESTRING=pipeutil.addquote("[NNNNNN]")

#variable
REFERENCEGENOMEDIR=pipeutil.addquote(os.path.normpath(homedir+"/home2/hg19broad/"))
CHROMOSOMEFASTALIST=pipeutil.addquote("Homo_sapiens_assembly19.fasta")
GENOMESOURCE=pipeutil.addquote(os.path.normpath(homedir+"/Homo_sapiens_assembly19.fasta"))
BAITFILE=pipeutil.addquote(os.path.normpath(homedir+"/home2/hg19broad/037233_D_BED_20111101.bed"))
TARGETFILE=pipeutil.addquote(os.path.normpath(homedir+"/home2/hg19broad/hg19_refSeq_refGene.bed"))
VCFFILE=pipeutil.addquote(os.path.normpath(homedir+"/home2/hg19broad/00-All.vcf"))

subprocesscall="python "+pipelinedir+"pipeline.py " +" -s "+ TRUSEQTEMPLATE + " -i " + TAGINDEXLIST + " -d " + FASTQTAGDICTFILE + " -n " + REPLACESTRING + " -r " + REFERENCEGENOMEDIR+ " -c " + CHROMOSOMEFASTALIST + " -g " + GENOMESOURCE + " -b " + BAITFILE + " -t " + TARGETFILE + " -v " + VCFFILE

subprocess.call([subprocesscall],shell=True)

### cleanup
#rm *.sam *.bam *scythe* *paired* *.stats *.dict *.out nohup* *.fasta
